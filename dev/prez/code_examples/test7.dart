class User
{
  String firstname = "";
  String lastname = "";

  User (this.firstname, this.lastname);

  get fullname => "$firstname $lastname";
}

var users = {
  "jan": new User("Jan", "Kowalski"),
  "karol": new User("Karol", "Kowalski"),
  "edek": new User("Edek", "Kowalski")
};

void main(args) {
  users.forEach((key, user){
    var f = user.fullname;
    print(f);
  });
}
