// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'server.dart';

// **************************************************************************
// SwiftGenerator
// **************************************************************************

//no interceptor for [DeferredLibrary]
//no interceptor for [DeferredLoadException]
//no interceptor for [FutureOr]
//no interceptor for [Future]
//no interceptor for [TimeoutException]
//no interceptor for [Completer]
//no interceptor for [Stream]
//no interceptor for [StreamSubscription]
//no interceptor for [EventSink]
//no interceptor for [StreamView]
//no interceptor for [StreamConsumer]
//no interceptor for [StreamSink]
//no interceptor for [StreamTransformer]
//no interceptor for [StreamTransformerBase]
//no interceptor for [StreamIterator]
//no interceptor for [MultiStreamController]
//no interceptor for [StreamController]
//no interceptor for [SynchronousStreamController]
//no interceptor for [Timer]
//no interceptor for [AsyncError]
//no interceptor for [ZoneSpecification]
//no interceptor for [ZoneDelegate]
//no interceptor for [Zone]
//no interceptor for [HttpAction]
//no interceptor for [UrlPattern]
//no interceptor for [Redirect]
//no interceptor for [Error404]
//no interceptor for [JsonAction]
//interceptor for [Routing]
//interceptor for Routing
//can be singleton: TRUE
//parent: Routing [@bool get Compose]
class $Routing extends Routing implements Pluggable {
  $Routing() {}
  T plugin<T>() {
    return null;
  }

  HttpAction createAction(String className) {
    switch (className) {
      case 'module_core.AppController':
        return new $module_core_AppController();
      case 'module_contact.ContactController':
        return new $module_contact_ContactController();
      case 'module_om.PlaceOrderController':
        return new $module_om_PlaceOrderController();
      case 'module_om.CheckoutController':
        return new $module_om_CheckoutController();
      case 'module_customdata.CustomDataController':
        return new $module_customdata_CustomDataController();
    }
  }
}

//interceptor for [ServerConfig]
//interceptor for ServerConfig
//can be singleton: TRUE
//parent: ServerConfig [@bool get Compose]
class $ServerConfig extends ServerConfig implements Pluggable {
  $ServerConfig() {
//Map<dynamic, dynamic>
//create
  }
  T plugin<T>() {
    return null;
  }
}

//interceptor for [Server]
//interceptor for Server
//can be singleton: TRUE
//parent: Server [@bool get Compose]
class $Server extends Server implements Pluggable {
  $Server() {
//String
//int
  }
  T plugin<T>() {
    return null;
  }

  Routing get routing => $om.routing;
  ServerConfig get config => $om.serverConfig;
}

//no interceptor for [AnnotatedWith]
//no interceptor for [Pluggable]
//no interceptor for [TypePlugin]
//no interceptor for [module_core.SaveStatus]
//no interceptor for [module_core.Repository]
//interceptor for [module_core.AppRepository]
//interceptor for module_core.AppRepository
//T[212882645] => App[275838045]
//can be singleton: TRUE
//parent: AppRepository []
//parent: Repository [@bool get ComposeSubtypes]
class $module_core_AppRepository extends module_core.AppRepository
    implements Pluggable {
  $module_core_AppRepository() {}
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  Map<String, module_core.App> get emptyInstances =>
      $om.instancesOfmodule_core_App;
  Map<String, module_core.Repository<module_core.Entity>>
      get relatedRepositories =>
          $om.instancesOfmodule_core_Repository_module_core_Entity_;
  module_core.App instanceCreator(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
    }
  }
}

//interceptor for [module_core.UserRepository]
//interceptor for module_core.UserRepository
//T[212882645] => User[533911946]
//can be singleton: TRUE
//parent: UserRepository []
//parent: Repository [@bool get ComposeSubtypes]
class $module_core_UserRepository extends module_core.UserRepository
    implements Pluggable {
  $module_core_UserRepository() {}
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  Map<String, module_core.User> get emptyInstances =>
      $om.instancesOfmodule_core_User;
  Map<String, module_core.Repository<module_core.Entity>>
      get relatedRepositories =>
          $om.instancesOfmodule_core_Repository_module_core_Entity_;
  module_core.User instanceCreator(String className) {
    switch (className) {
      case 'module_core.User':
        return new $module_core_User();
    }
  }
}

//interceptor for [module_core.AppController]
//interceptor for module_core.AppController
//can be singleton: TRUE
//parent: AppController []
//parent: JsonAction [@bool get ComposeSubtypes]
//parent: HttpAction [@bool get ComposeSubtypes]
class $module_core_AppController extends module_core.AppController
    implements Pluggable {
  $module_core_AppController() {
//HttpRequest
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  module_core.Repository<module_core.App> get appRepository =>
      $om.module_core_AppRepository;
  void setPostArgs(Map json) {
//compiled method
//@PostArg
//@PostArg
//@PostArg
  }
}

//no interceptor for [module_core.Entity]
//interceptor for [module_core.App]
//interceptor for module_core.App
//can be singleton: TRUE
//parent: App []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_App extends module_core.App implements Pluggable {
  $module_core_App() {
//Id
//String
//App
//User
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_core.App'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['parent'] = "module_core.App";
    }
    {
      target['owner'] = "module_core.User";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.parent = relations['parent'];
    }
    {
      this.owner = relations['owner'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['url'] = this.url;
    }
    {
      target['name'] = this.name;
    }
//@Field
//@Field
    {
      target['parent'] = this.parent != null ? this.parent.toJson() : null;
    }
    {
      target['owner'] = this.owner != null ? this.owner.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.url = target.containsKey('url') ? target['url'] : this.url;
    }
    {
      this.name = target.containsKey('name') ? target['name'] : this.name;
    }
//@Field
//@Field
//@Field
    {
      if (target['parent'] != null) {
        if (target['parent'] is String) {
          this.parent = relatedFactory("module_core.App");
          this.parent.id = idFromString(target['parent']);
        } else {
          this.parent = relatedFactory(target['parent']['classes'].last);
          this.parent.fromJson(target['parent']);
        }
      } else {
        this.parent = null;
      }
    }
    {
      if (target['owner'] != null) {
        if (target['owner'] is String) {
          this.owner = relatedFactory("module_core.User");
          this.owner.id = idFromString(target['owner']);
        } else {
          this.owner = relatedFactory(target['owner']['classes'].last);
          this.owner.fromJson(target['owner']);
        }
      } else {
        this.owner = null;
      }
    }
  }
}

//interceptor for [module_core.User]
//interceptor for module_core.User
//can be singleton: TRUE
//parent: User []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_User extends module_core.User implements Pluggable {
  $module_core_User() {
//Id
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_core.User'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['email'] = this.email;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.email = target.containsKey('email') ? target['email'] : this.email;
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_core.Image]
//interceptor for module_core.Image
//can be singleton: TRUE
//parent: Image []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_Image extends module_core.Image implements Pluggable {
  $module_core_Image() {
//Id
//String
//App
//String
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_core.Image'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['app'] = "module_core.App";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.app = relations['app'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['mimetype'] = this.mimetype;
    }
    {
      target['filename'] = this.filename;
    }
    {
      target['thumb'] = this.thumb;
    }
//@Field
//@Field
    {
      target['app'] = this.app != null ? this.app.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.mimetype =
          target.containsKey('mimetype') ? target['mimetype'] : this.mimetype;
    }
    {
      this.filename =
          target.containsKey('filename') ? target['filename'] : this.filename;
    }
    {
      this.thumb = target.containsKey('thumb') ? target['thumb'] : this.thumb;
    }
//@Field
//@Field
//@Field
    {
      if (target['app'] != null) {
        if (target['app'] is String) {
          this.app = relatedFactory("module_core.App");
          this.app.id = idFromString(target['app']);
        } else {
          this.app = relatedFactory(target['app']['classes'].last);
          this.app.fromJson(target['app']);
        }
      } else {
        this.app = null;
      }
    }
  }
}

//no interceptor for [module_contact.ContactMessage]
//interceptor for [module_contact.ContactController]
//interceptor for module_contact.ContactController
//can be singleton: TRUE
//parent: ContactController []
//parent: JsonAction [@bool get ComposeSubtypes]
//parent: HttpAction [@bool get ComposeSubtypes]
class $module_contact_ContactController extends module_contact.ContactController
    implements Pluggable {
  $module_contact_ContactController() {
//HttpRequest
//String
//String
//Map<dynamic, dynamic>
  }
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  module_core.Repository<module_core.App> get appRepository =>
      $om.module_core_AppRepository;
  void setPostArgs(Map json) {
//compiled method
//@PostArg
//@PostArg
    {
      this.message = (json.containsKey('message') ? json['message'] : null);
    }
//@PostArg
  }
}

//interceptor for [module_om.OrdersRepository]
//interceptor for module_om.OrdersRepository
//T[212882645] => Order[105961759]
//can be singleton: TRUE
//parent: OrdersRepository []
//parent: Repository [@bool get ComposeSubtypes]
class $module_om_OrdersRepository extends module_om.OrdersRepository
    implements Pluggable {
  $module_om_OrdersRepository() {}
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  Map<String, module_om.Order> get emptyInstances =>
      $om.instancesOfmodule_om_Order;
  Map<String, module_core.Repository<module_core.Entity>>
      get relatedRepositories =>
          $om.instancesOfmodule_core_Repository_module_core_Entity_;
  module_om.Order instanceCreator(String className) {
    switch (className) {
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }
}

//interceptor for [module_om.PlaceOrderController]
//interceptor for module_om.PlaceOrderController
//can be singleton: TRUE
//parent: PlaceOrderController []
//parent: JsonAction [@bool get ComposeSubtypes]
//parent: HttpAction [@bool get ComposeSubtypes]
class $module_om_PlaceOrderController extends module_om.PlaceOrderController
    implements Pluggable {
  $module_om_PlaceOrderController() {
//HttpRequest
//String
//String
//Map<dynamic, dynamic>
  }
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  module_core.Repository<module_core.App> get appRepository =>
      $om.module_core_AppRepository;
  module_om.OrdersRepository get repository => $om.module_om_OrdersRepository;
  void setPostArgs(Map json) {
//compiled method
//@PostArg
//@PostArg
    {
      this.order = (json.containsKey('order') ? json['order'] : null);
    }
//@PostArg
  }
}

//interceptor for [module_om.CheckoutController]
//interceptor for module_om.CheckoutController
//can be singleton: TRUE
//parent: CheckoutController [@Deprecated get deprecated]
//parent: JsonAction [@bool get ComposeSubtypes]
//parent: HttpAction [@bool get ComposeSubtypes]
class $module_om_CheckoutController extends module_om.CheckoutController
    implements Pluggable {
  $module_om_CheckoutController() {
//HttpRequest
//String
//String
//Map<dynamic, dynamic>
//int
  }
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  module_core.Repository<module_core.App> get appRepository =>
      $om.module_core_AppRepository;
  void setPostArgs(Map json) {
//compiled method
//@PostArg
//@PostArg
    {
      this.order = (json.containsKey('order') ? json['order'] : null);
    }
//@PostArg
  }
}

//no interceptor for [module_om.DeliveryMethod]
//no interceptor for [module_om.PaymentMethod]
//interceptor for [module_om.OrderItem]
//interceptor for module_om.OrderItem
//can be singleton: TRUE
//parent: OrderItem [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_om_OrderItem extends module_om.OrderItem implements Pluggable {
  $module_om_OrderItem() {
//Id
//String
//ProductBase
//int
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_om.OrderItem'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['product'] = "ProductBase";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.product = relations['product'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
    {
      target['quantity'] = this.quantity;
    }
//@Field
    {
      target['slug'] = this.slug;
    }
//@Field
//@Field
    {
      target['product'] = this.product != null ? this.product.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
//@Field
    {
      this.quantity =
          target.containsKey('quantity') ? target['quantity'] : this.quantity;
    }
//@Field
//@Field
    {
      if (target['product'] != null) {
        if (target['product'] is String) {
          this.product = relatedFactory("ProductBase");
          this.product.id = idFromString(target['product']);
        } else {
          this.product = relatedFactory(target['product']['classes'].last);
          this.product.fromJson(target['product']);
        }
      } else {
        this.product = null;
      }
    }
  }
}

//interceptor for [module_om.Order]
//interceptor for module_om.Order
//can be singleton: TRUE
//parent: Order []
//parent: Entity [@bool get ComposeSubtypes]
class $module_om_Order extends module_om.Order implements Pluggable {
  $module_om_Order() {
//Id
//String
//String
//List<OrderItem>
//DeliveryMethod
//PaymentMethod
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_om.Order'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['deliveryMethod'] = "module_om.DeliveryMethod";
    }
    {
      target['paymentMethod'] = "module_om.PaymentMethod";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.deliveryMethod = relations['deliveryMethod'];
    }
    {
      this.paymentMethod = relations['paymentMethod'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['name'] = this.name;
    }
//@Field
    {
      target['items'] = this.items.map((e) => e.toJson()).toList();
    }
//@Field
    {
      target['deliveryMethod'] =
          this.deliveryMethod != null ? this.deliveryMethod.toJson() : null;
    }
    {
      target['paymentMethod'] =
          this.paymentMethod != null ? this.paymentMethod.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.name = target.containsKey('name') ? target['name'] : this.name;
    }
//@Field
//@Field
    {
      this.items = [];
      if (target['items'] != null) {
        target['items'].forEach((data) {
          var e = relatedFactory(data['classes'].last);
          e.fromJson(data);
          this.items.add(e);
        });
      }
    }
//@Field
    {
      if (target['deliveryMethod'] != null) {
        if (target['deliveryMethod'] is String) {
          this.deliveryMethod = relatedFactory("module_om.DeliveryMethod");
          this.deliveryMethod.id = idFromString(target['deliveryMethod']);
        } else {
          this.deliveryMethod =
              relatedFactory(target['deliveryMethod']['classes'].last);
          this.deliveryMethod.fromJson(target['deliveryMethod']);
        }
      } else {
        this.deliveryMethod = null;
      }
    }
    {
      if (target['paymentMethod'] != null) {
        if (target['paymentMethod'] is String) {
          this.paymentMethod = relatedFactory("module_om.PaymentMethod");
          this.paymentMethod.id = idFromString(target['paymentMethod']);
        } else {
          this.paymentMethod =
              relatedFactory(target['paymentMethod']['classes'].last);
          this.paymentMethod.fromJson(target['paymentMethod']);
        }
      } else {
        this.paymentMethod = null;
      }
    }
  }

  module_om.OrderItem createOrderItem() {
//module_om.OrderItem
    return new $module_om_OrderItem();
  }
}

//interceptor for [module_customdata.CustomDataController]
//interceptor for module_customdata.CustomDataController
//can be singleton: TRUE
//parent: CustomDataController []
//parent: HttpAction [@bool get ComposeSubtypes]
class $module_customdata_CustomDataController
    extends module_customdata.CustomDataController implements Pluggable {
  $module_customdata_CustomDataController() {
//HttpRequest
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  module_core.Repository<module_core.App> get appRepository =>
      $om.module_core_AppRepository;
  void setPostArgs(Map json) {
//compiled method
//@PostArg
//@PostArg
//@PostArg
  }
}

class $ObjectManager {
  $Routing _routing;
  $Routing get routing {
    if (_routing == null) {
      _routing = new $Routing();
    }
    return _routing;
  }

  $ServerConfig _serverConfig;
  $ServerConfig get serverConfig {
    if (_serverConfig == null) {
      _serverConfig = new $ServerConfig();
    }
    return _serverConfig;
  }

  $Server _server;
  $Server get server {
    if (_server == null) {
      _server = new $Server();
    }
    return _server;
  }

  $module_core_AppRepository _module_core_AppRepository;
  $module_core_AppRepository get module_core_AppRepository {
    if (_module_core_AppRepository == null) {
      _module_core_AppRepository = new $module_core_AppRepository();
    }
    return _module_core_AppRepository;
  }

  $module_core_UserRepository _module_core_UserRepository;
  $module_core_UserRepository get module_core_UserRepository {
    if (_module_core_UserRepository == null) {
      _module_core_UserRepository = new $module_core_UserRepository();
    }
    return _module_core_UserRepository;
  }

  $module_core_AppController _module_core_AppController;
  $module_core_AppController get module_core_AppController {
    if (_module_core_AppController == null) {
      _module_core_AppController = new $module_core_AppController();
    }
    return _module_core_AppController;
  }

  $module_core_App _module_core_App;
  $module_core_App get module_core_App {
    if (_module_core_App == null) {
      _module_core_App = new $module_core_App();
    }
    return _module_core_App;
  }

  $module_core_User _module_core_User;
  $module_core_User get module_core_User {
    if (_module_core_User == null) {
      _module_core_User = new $module_core_User();
    }
    return _module_core_User;
  }

  $module_core_Image _module_core_Image;
  $module_core_Image get module_core_Image {
    if (_module_core_Image == null) {
      _module_core_Image = new $module_core_Image();
    }
    return _module_core_Image;
  }

  $module_contact_ContactController _module_contact_ContactController;
  $module_contact_ContactController get module_contact_ContactController {
    if (_module_contact_ContactController == null) {
      _module_contact_ContactController =
          new $module_contact_ContactController();
    }
    return _module_contact_ContactController;
  }

  $module_om_OrdersRepository _module_om_OrdersRepository;
  $module_om_OrdersRepository get module_om_OrdersRepository {
    if (_module_om_OrdersRepository == null) {
      _module_om_OrdersRepository = new $module_om_OrdersRepository();
    }
    return _module_om_OrdersRepository;
  }

  $module_om_PlaceOrderController _module_om_PlaceOrderController;
  $module_om_PlaceOrderController get module_om_PlaceOrderController {
    if (_module_om_PlaceOrderController == null) {
      _module_om_PlaceOrderController = new $module_om_PlaceOrderController();
    }
    return _module_om_PlaceOrderController;
  }

  $module_om_CheckoutController _module_om_CheckoutController;
  $module_om_CheckoutController get module_om_CheckoutController {
    if (_module_om_CheckoutController == null) {
      _module_om_CheckoutController = new $module_om_CheckoutController();
    }
    return _module_om_CheckoutController;
  }

  $module_om_OrderItem _module_om_OrderItem;
  $module_om_OrderItem get module_om_OrderItem {
    if (_module_om_OrderItem == null) {
      _module_om_OrderItem = new $module_om_OrderItem();
    }
    return _module_om_OrderItem;
  }

  $module_om_Order _module_om_Order;
  $module_om_Order get module_om_Order {
    if (_module_om_Order == null) {
      _module_om_Order = new $module_om_Order();
    }
    return _module_om_Order;
  }

  $module_customdata_CustomDataController
      _module_customdata_CustomDataController;
  $module_customdata_CustomDataController
      get module_customdata_CustomDataController {
    if (_module_customdata_CustomDataController == null) {
      _module_customdata_CustomDataController =
          new $module_customdata_CustomDataController();
    }
    return _module_customdata_CustomDataController;
  }

  Map<String, module_core.App> get instancesOfmodule_core_App {
    return {
      "module_core.App": new $module_core_App(),
    };
  }

  Map<String, module_core.Repository>
      get instancesOfmodule_core_Repository_module_core_Entity_ {
    return {
      "module_core.AppRepository": new $module_core_AppRepository(),
      "module_core.UserRepository": new $module_core_UserRepository(),
      "module_om.OrdersRepository": new $module_om_OrdersRepository(),
    };
  }

  Map<String, module_core.User> get instancesOfmodule_core_User {
    return {
      "module_core.User": new $module_core_User(),
    };
  }

  Map<String, module_om.Order> get instancesOfmodule_om_Order {
    return {
      "module_om.Order": new $module_om_Order(),
    };
  }
}

$ObjectManager $om = new $ObjectManager();
//generated in 124ms
