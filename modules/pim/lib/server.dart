import 'model.dart';
export 'model.dart';

import 'package:c7server/c7server.dart';
import 'package:mongo_dart/mongo_dart.dart';

import 'package:module_pim/model.dart';
import 'package:swift_ids/swift_ids.dart';

import "package:module_core/server.dart";

abstract class ProductsRepository extends Repository<ProductBase> {
  String get collectionName => 'products';
}

abstract class GetBySlugsController extends JsonAction {
  @PostArg
  List<String> slugs;

  @Inject
  ProductsRepository get repository;

  Future<List> run () async {
    //TODO
    //return (await repository.rawQuery({'slug' : { '\$in': slugs }})).map(repository.toJson).toList();
    var products = [];
    var query = await (await server.db).collection('products')
      .aggregate([{
        '\$lookup':
          {
            'from': 'images',
            'localField': '_image',
            'foreignField': '_id',
            'as': 'image'
          }
      },{
        '\$match' : {
          '\$and': [
            {
              '_site' : new Id.fromString(appId).value
            },{
              'slug' : { '\$in': slugs }
            }
          ]
        }
      }], cursor: {'batchSize': 12});

    query['cursor']['firstBatch'].forEach((p){
      p['id'] = new Id(p['_id'].toInt()).toString();
      p.remove('_id');
      p.remove('_site');
      p.remove('_image');
      var image = p['image'].length > 0 ? p['image'][0] : null;
      if (image != null) {
        image['id'] = new Id(image['_id'].toInt()).toString();
        image.remove('_id');
      }
      p['image'] = image;

      products.add(p);
    });

    return products;

  }
}

abstract class SearchController extends JsonAction {

    @PostArg
    String query;

    Future<List> run () async {
        var products = [];
        var query = await (await server.db).collection('products')
          .aggregate([{
            '\$lookup':
              {
                'from': 'images',
                'localField': '_image',
                'foreignField': '_id',
                'as': 'image'
              }
          },{
            '\$match' :
              {
                '_site' : new Id.fromString(appId).value
              }
          }], cursor: {'batchSize': 12});

        query['cursor']['firstBatch'].forEach((p){
          p['id'] = new Id(p['_id'].toInt()).toString();
          p.remove('_id');
          p.remove('_site');
          p.remove('_image');
          var image = p['image'].length > 0 ? p['image'][0] : null;
          if (image != null) {
            image['id'] = new Id(image['_id'].toInt()).toString();
            image.remove('_id');
          }
          p['image'] = image;

          products.add(p);
        });

          /*.find(
            where.eq('_site', new Id.fromString(appId).value)
          )*/
          /*
        products.forEach((p){
          p.remove('_user');
        });*/
        return products;
    }
}

class SaveStatus {
  bool success;
  List<String> messages;
}

abstract class SaveController extends JsonAction {

    @PostArg
    List<dynamic> items;

    Future run() async {

        Db db = await server.db;
        await db.open();
        List<Map<String, dynamic>> newDocuments = [];
        List<dynamic> status = [];

        for (int i=0; i< items.length; i++){
          Map map = items[i] as Map;
          map['_site'] = new Id.fromString(appId).value;
          if (map['id'] == null) {
            var ret = await db.collection('counters').findAndModify(
              query: where.eq('_id', 'products'),
              update: modify.inc('seq', 1),
              returnNew: true
            );
            int id = ret['seq'].round();;
            map['_id'] = id;
            map.remove('id');
            newDocuments.add(map);
            status.add(new Id(id).toString());
          } else {
            map['_id'] = new Id.fromString(map['id']).value;
            status.add(map['id']);
            map.remove('id');
            print('UPDATING');
            await db.collection('products').save(map);
          }
        }
        if (newDocuments.length > 0) {
          //print('NEW');
          //print(newDocuments);
          //newDocuments[0].remove('classes');
          var ret = await db.collection('products').insertAll(newDocuments);
          print(ret);
        }

        /*items.value.forEach((p){
          p['id'] = new Id(p['_id'].toInt()).toString();
          p.remove('_id');
        });*/

        print(items);
        print('XXXXXX');
        //products = await db.collection('products').save();
        return status;
    }
}
