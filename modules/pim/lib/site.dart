library mocule_pim;

import 'client.dart';
export 'client.dart';
import 'package:module_core/site.dart';

abstract class ProductAction extends SiteShowAction<ProductBase> {

  Future<String> get title => item.then((e) => e.name);

  @Factory
  ProductsAction listFactory();

  ProductsAction get parent => listFactory();

}

abstract class ShowProductWidget extends ShowItemWidget<ProductBase> {

  @Create
  Slot buttons;

  String get html => '''
  <div>
    <div class="card mt-4">
      <img class="card-img-top img-fluid" src="http://placehold.it/900x400" alt="">
      <div class="card-body">
        <h3 class="card-title">${item.name}</h3>
        <h4>\$24.99</h4>
        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente dicta fugit fugiat hic aliquam itaque facere, soluta. Totam id dolores, sint aperiam sequi pariatur praesentium animi perspiciatis molestias iure, ducimus!</p>
        <span class="text-warning">★ ★ ★ ★ ☆</span>
        4.0 stars
        <br/>
        <a href="#" class="btn btn-success">Add to Cart</a>
      </div>
    </div>
    <!-- /.card -->

    <div class="card card-outline-secondary my-4">
      <div class="card-header">
        Product Reviews
      </div>
      <div class="card-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis et enim aperiam inventore, similique necessitatibus neque non! Doloribus, modi sapiente laboriosam aperiam fugiat laborum. Sequi mollitia, necessitatibus quae sint natus.</p>
        <small class="text-muted">Posted by Anonymous on 3/1/17</small>
        <hr>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis et enim aperiam inventore, similique necessitatibus neque non! Doloribus, modi sapiente laboriosam aperiam fugiat laborum. Sequi mollitia, necessitatibus quae sint natus.</p>
        <small class="text-muted">Posted by Anonymous on 3/1/17</small>
        <hr>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis et enim aperiam inventore, similique necessitatibus neque non! Doloribus, modi sapiente laboriosam aperiam fugiat laborum. Sequi mollitia, necessitatibus quae sint natus.</p>
        <small class="text-muted">Posted by Anonymous on 3/1/17</small>
        <hr>
        <a href="#" class="btn btn-success">Leave a Review</a>
      </div>
    </div>
    <!-- /.card -->
  </div>
  ''';
}

@SiteMenuItem('Shop', 'Shop')
abstract class ProductsAction extends SiteListAction<ProductBase> {
    String get title => "Products";
}


abstract class ProductWidget extends ListItemWidget<ProductBase> {

  @Create
  Slot buttons;

  String get html => '''
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="card h-100">
          <a href="/product/${item.slug}"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
          <div class="card-body">
            <h4 class="card-title">
              <a data-type="local" href="/product/${item.slug}">${item.name}</a>
            </h4>
            <h5>\$${item.price}</h5>
            <p class="card-text">${item.shortDescription}</p>
          </div>
          <div class="card-footer">
            <div class="float-left">
              <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
            </div>
            <div class="float-right">
              <a data-type="add-to-cart" href="/cart/add/${item.id}" class="btn">
                Add to Cart
              </a>
            </div>
          </div>
        </div>
      </div>
    ''';

}
