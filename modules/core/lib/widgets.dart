import 'dart:html';
import 'dart:async';

import 'package:swift_composer/swift_composer.dart';
import 'package:swift_ids/swift_ids.dart';

import 'actions.dart';

class Slot {
  Widget container;
  Element element;

  Set<Widget> widgetsToRemove = new Set<Widget>();
  Set<Widget> widgets = new Set<Widget>();

  Element fadingOuts;

  void clear() {
    if (fadingOuts == null) {
      fadingOuts = new DivElement()..className = 'fo';
      element.append(fadingOuts);
    }
    widgets.forEach((w){
      fadingOuts.append(w.element);
    });
    widgetsToRemove.addAll(widgets);
    widgets.clear();
    new Timer(const Duration(milliseconds: 200), (){
      widgetsToRemove.forEach((w){
        w.element.remove();
      });
    });
  }

  void setChild(Widget child) {
    clear();
    append(child);
  }

  void append(Widget child) {
    child.container = this;
    widgets.add(child);
    widgetsToRemove.remove(child);
    element.append(child.element);
  }

}

abstract class NavbarLink extends Widget {

  String href;
  String title;

  String get html => '''
    <a href="$href" data-type="local" class="dropdown-item">$title</a>
  ''';
}

abstract class NavbarDropdown extends Widget {

  @Create
  Slot items;

  String name;

  String get html => '''
  <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button">
      $name
    </a>
    <div id="items" class="dropdown-menu">
    </div>
  </li>
  ''';
}

/* class RedBox extends HtmlElement {
  RedBox.created() : super.created() {
    style.background = "red";
  }
  factory RedBox(text) => new Element.tag('my-redbox')..text = text;
} */

class SwiftUriPolicy implements UriPolicy {
  bool allowsUri(String uri) {
    return true;
  }
}

final NodeValidatorBuilder _htmlValidator=new NodeValidatorBuilder()
  ..allowHtml5(uriPolicy:new SwiftUriPolicy())
  ..allowElement('img', attributes: ['loading'])
  ..allowElement('div', attributes: ['data-slot'])
  ..allowElement('a', attributes: ['data-target', 'data-type', 'rel', 'aria-label'])
  ..allowElement('button', attributes: ['data-target', 'data-type']);

class ExpandedElement {
  Element target;
  Element activator;
  ExpandedElement(this.target, this.activator);
}

@ComposeSubtypes
abstract class Widget {
    @Inject
    Router get router;

    @InjectClassName
    String get className;

    @Compile
    void initializeSlots();

    @CompileFieldsOfType
    void _initializeSlotsSlot(String name, Slot field) {
      field.container = this;
      field.element = element.querySelector('[data-slot="' + name + '"]');
      //field.element.removeAttribute('id');
      if (field.element != null) {
        field.element.classes.add('slot');
      } else {
        print('slot ' + name + ' is missing');
      }
    }


    dynamic container;

    static List<ExpandedElement> _currentlyExpanded = [];

    void init() {
    }

    void _expand(Element activator, Element target) {
      //TODODODODOD
      print(target.contains(target));
      Element removedElement = null;
      while (
        _currentlyExpanded.isNotEmpty &&
          !(_currentlyExpanded.last.target.contains(target) &&
              _currentlyExpanded.last.target != target)
      ) {
        removedElement = _currentlyExpanded.last.target;
        _currentlyExpanded.last.target.classes.remove('show');
        _currentlyExpanded.last.activator.classes.remove('active');
        print('HIDE');
        print(_currentlyExpanded.last.target);
        _currentlyExpanded.removeLast();
      }
      if (target != removedElement) {
        print('SHOW');
        print(target);

        target.classes.add('show');
        activator.classes.add('active');
        _currentlyExpanded.add(new ExpandedElement(target, activator));
      }
    }

    //TODO???
    void repaint() {
      var _tmp = _element;
      _element = null;
      element;
      /*
      var parent = _element.parent;
      element.remove();
      _element = null;
      element;
      if (parent != null) {
        parent.append(element);
        element.classes.add('show');
      }
      repaintSlots();*/
    }

    @Compile
    void repaintSlots();

    @CompileFieldsOfType
    void _repaintSlotsSlot(String name, Slot field) {
      field.widgets.forEach((widget){
        field.element.append(widget.element);
      });
    }


    Element _element;
    Element get element {
      if (_element == null) {
        //document.registerElement('a-local', RedBox);
        //var validator = new NodeValidatorBuilder.common();
        //validator.allowElement('a-local', attributes: ['key']);
        //validator.allowsElement(WidgetElement);
        //var element = new Element.html(html, validator:validator);

        _element = new Element.html(html, validator:_htmlValidator);
        _element.classes.add('widget');
        _element.classes.add(className.replaceAll('.', '_'));

        initializeSlots();
        /*element.querySelector('.dropdown-toggle')*/
        init();

        _element.querySelectorAll('a, area, button').forEach((a){

          if (a.dataset.containsKey('type')) {
            switch (a.dataset['type']) {
              case 'local':
                router.localiseAnchor(a);
                break;
              case 'toggle':
                a.onClick.listen((e){
                  e.preventDefault();
                  _expand(a, a.dataset.containsKey('target') ? document.querySelector(a.dataset['target']) : a.nextElementSibling);
                });
                break;
              case 'back':
                a.onClick.listen((e) {
                  window.history.back();
                });
                break;
            }
          }
        });
      }
      return _element;
    }

    @Template
    String get html;
}


class AsyncWidget {




}

@Compose
abstract class TextWidget extends Widget {
  @Require
  String text;

  String get html => '<span>$text</slot>';
}
