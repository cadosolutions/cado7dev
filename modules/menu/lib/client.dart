import 'package:module_core/site.dart';
import 'model.dart';
export 'model.dart';

class MenuApi extends CRUDApi<MenuItem> {
    String get serviceCode => 'api';
    String get moduleCode => 'menu';
}
