import 'package:swift_composer/swift_composer.dart';
export 'package:swift_composer/swift_composer.dart';
import 'package:swift_ids/swift_ids.dart';
export 'package:swift_ids/swift_ids.dart';

import 'package:service_worker/window.dart' as sw;

import 'actions.dart';
export 'actions.dart';
import 'widgets.dart';
export 'widgets.dart';
import 'model.dart';
export 'model.dart';

import 'dart:html';
//export 'dart:html';
import 'dart:convert';
import 'dart:async';

@Compose
abstract class Site {

  @InjectConfig
  String get name;
  @InjectConfig
  String get url;

  String get domain => url.split('/')[0];

  @InjectConfig
  Id get id;

  String getEnvUrl() {
    return Uri.base.origin;
  }

  String getApiUrl({String service = 'api'}) {
    return Uri.base.origin.replaceFirst(domain, service + '.cado7.com') + '/' + id.toString() + '/';
  }
}

@Compose
abstract class Application {

  @Inject
  Router get router;

  @Inject
  Site get site;

  @Inject
  Layout get layout;

  void run() {
    document.body.innerHtml = '';
    document.body.append(layout.element);
    document.body.classes.remove('loading');

    if (sw.isSupported) {
      print('registering ServiceWorker.');
      sw.register('/pwa.dart.js');
    } else {
      print('ServiceWorkers are not supported.');
    }

    router.init();
  }

  @deprecated
  Map<String, Future<String>> customDataGets = new Map<String, Future<String>>();
  void loadCustomData(name, callback){
    if (!customDataGets.containsKey(name)) {
      customDataGets[name] = HttpRequest.getString(site.getApiUrl() + "customdata.CustomData/" + name + ".json");
    }
    customDataGets[name].then((String responseText) {
      callback(json.decode(responseText));
    });
  }
}

@Compose
abstract class Layout extends Widget {

    @Create
    Slot navbar;

    @Create
    Slot body;

    @Create
    Slot modal;

    @Inject
    Site get site;

    T plugin<T>();

    String get html => '''
    <div>
      <nav id="navbar">${site.name}</nav>
      <div class="container">
        <div id="body" class="row">
        </div>
      </div>
      <footer>
        &copy; ${site.name} 2019;
        powered by <a href="http://swift.shop/" target="_blank" rel="noopener">swift.shop</a></p>
      </footer>
    </div>
    ''';
}

@ComposeSubtypes
abstract class ApiModule {
    String get serviceCode;
    String get moduleCode;

    @InjectClassName
    String get className;

    @Inject
    Site get site;

    dynamic call(String name, {Map data = null}) async {
        //print('CLASS: $className');
        String url = site.getApiUrl(service:serviceCode) + moduleCode + '.' + name;

        HttpRequest xhr = await HttpRequest.request(
          url,
          method:'POST',
          sendData: data != null ? json.encode(data) : null,
          requestHeaders: {
          //  'Content-Type': 'application/json; charset=UTF-8'
          }
        );
        return json.decode(xhr.responseText);
    }
}

@ComposeSubtypes
abstract class CRUDApi<T extends Entity> extends ApiModule {

  @SubtypeFactory
  T newEntityByType(String className);

  @InjectInstances
  Map<String, CRUDApi> get relatedApis;

  T _entityFromJson(Map json) {
    String className = json['classes'] is List ? (json['classes'] as List).last : '';
    var ret = newEntityByType(className);
    ret.fromJson(json);
    return ret;
  }

  Future<List<T>> search(String q) async {
      List list = await call('Search', data:{'query': q});
      List<T> ret = [];
      for (var e in list) {
        ret.add(_entityFromJson(e));
      }
      return ret;
  }

  Future<List<T>> getByIds(List<String> ids) async {
      List list = await call('GetByIds', data:{'ids': ids});
      List<T> ret = [];
      for (var e in list) {
        ret.add(_entityFromJson(e));
      }
      return ret;
  }

  Future<List<T>> getBySlugs(List<String> slugs) async {
      List list = await call('GetBySlugs', data:{'slugs': slugs});
      List<T> ret = [];
      for (var e in list) {
        ret.add(_entityFromJson(e));
      }
      return ret;
  }

  Future<T> getBySlug(String slug) async {
    return (await getBySlugs([slug])).first;
  }

  Future<T> getById(Id id) async {
    return (await getByIds([id.toString()])).first;
  }

  Future<bool> save(List<T> products) async {
      return await call('Save', data: {'items':products.map((p) => p.toJson()).toList()});
  }
}

/*
@Compose
abstract class User {

  String email;

  bool isAdmin() {
    return false;
  }

  bool isLoggedIn() {
    return window.sessionStorage.containsKey('user');
  }

  Future<bool> logIn(Map data) async {
    window.sessionStorage['user'] = '1';
    return true;
  }

  void logOut() {
    window.sessionStorage.remove('user');
  }
}*/
