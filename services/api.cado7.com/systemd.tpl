[Unit]
Description=$DOMAIN service
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=1
User=fsw
WorkingDirectory=/home/capps/$DOMAIN
ExecStart=/home/capps/$DOMAIN/server.bin

[Install]
WantedBy=multi-user.target
