import 'dart:html';
import 'dart:convert';
import 'dart:async';
import 'dart:js';

class UnitType {

  String category;
  String name;
  String examples;
  int paintBasePrice;
  int assemblyPrice;
  int baseBasePrice;
  UnitType (this.category, this.name, this.paintBasePrice, this.assemblyPrice, this.baseBasePrice, this.examples);

  int count1 = 0;
  int count2 = 0;
  NumberInputElement countInput1;
  NumberInputElement countInput2;
}


var priceBases = [
  new UnitType('Infantry', 'Mini', 400, 300, 100, 'Smaller than human. ex: Goblin, Tau Drone, Nurgling, Brimstone Horror'),
  new UnitType('Infantry', 'Regular', 500, 300, 100, 'Human sized infantry. ex: Space Marine, Zombie, Tau Fire Warriors, Tzaangors.'),
  new UnitType('Infantry', 'Large', 800, 300, 150, 'Shield-Captain, Plague Marine, Rubric Marine, Lychguard'),
  new UnitType('Infantry', 'XLarge', 1000, 450, 150, 'Roboute Guilliman, Saint Celestine'),

  new UnitType('Cavalry', 'Regular', 1400, 450, 150, 'Space Marines Bike, Black Knights, Freeguild Outriders'),
  new UnitType('Cavalry', 'Large', 3000, 600, 200,'Beastclaw Raiders, Mighty Skullcrushers, Stormcast Eternals Evocators on Celestial Dracolines'),
  new UnitType('Cavalry', 'XLarge', 4000, 600, 300, 'Kairos Fateweaver, Ravager, Stonehorn, Carnosaur'),

  new UnitType('Heroes', 'Regular', 2000, 1000, 300, 'Saurus Oldblood, Librarian, Imperial Fists Captain Lysander'),
  new UnitType('Heroes', 'Large', 4000, 1500, 400, 'Gaunt Summoner on Disc of Tzeentch, Lord-Aquilor'),
  new UnitType('Heroes', 'XLarge', 8000, 2500, 600, 'Herald of Tzeentch on Burning Chariot, Morathi'),
  new UnitType('Heroes', 'XXLarge', 10000, 3500, 800, 'Thanquol and Boneripper, The Glottkin, Lord of Change'),

  new UnitType('Monsters', 'Regular', 4000, 750, 300, 'Carnifex, Demon Prince, Foetid'),
  new UnitType('Monsters', 'Large', 6000, 2250, 400, 'Riptide, Hive Tyrant, Great Unclean One, Crisis Battlesuit'),
  new UnitType('Monsters', 'XLarge', 8000, 3750, 0, 'Knight Castellan, Imperial Knight, Kairos Fateweaver, Stormsurge'),

  new UnitType('Vehicles', 'Regular', 4000, 750, 0, 'Dreadnought, Rhino, Razorback, Night Scythe, Fire Prism'),
  new UnitType('Vehicles', 'Large', 7000, 1500, 0, 'Leman Russ Tank, Repulsor, Landraider, Stormraven'),
  new UnitType('Vehicles', 'XLarge', 11000, 3750, 0, 'Hammerhead, Tesseract Vault, Baneblade, Monolith, Thunderhawk')
];


var currencyMap = {
  'USD': 1.0,
  'EUR': 0.88,
  'GBP': 0.77
};
var currency = 'USD';

var currencySymbol = {
  'USD': '\$',
  'EUR': '\€',
  'GBP': '\£'
};

String formatPrice(int price) {

  return currencySymbol[currency] + ((currencyMap[currency] * price).round()/100).toString();

}


void setCurrency(String code) {

  querySelector('#unitEUR').classes.remove('btn-warning');
  querySelector('#unitEUR').classes.add('btn-secondary');
  querySelector('#unitUSD').classes.remove('btn-warning');
  querySelector('#unitUSD').classes.add('btn-secondary');
  querySelector('#unitGBP').classes.remove('btn-warning');
  querySelector('#unitGBP').classes.add('btn-secondary');

  querySelector('#unit$code').classes.remove('btn-secondary');
  querySelector('#unit$code').classes.add('btn-warning');

  currency = code;
  updateOrderPrices();
}

var typeCode = 'typeAssemble';

void setType(String code) {
  querySelector('#typePaint').classes.remove('btn-warning');
  querySelector('#typePaint').classes.add('btn-secondary');
  querySelector('#typeAssemble').classes.remove('btn-warning');
  querySelector('#typeAssemble').classes.add('btn-secondary');
  querySelector('#typeBuy').classes.remove('btn-warning');
  querySelector('#typeBuy').classes.add('btn-secondary');

  querySelector('#$code').classes.remove('btn-secondary');
  querySelector('#$code').classes.add('btn-warning');
  typeCode = code;
}

void updateOrderPrices() {

  var levelFactors = [1.0, 1.8, 2.9, 4.6, 10];

  int selectedPaintTotal1 = 0;
  int selectedPaintTotal2 = 0;
  int selectedBasesTotal = 0;

  for (int i=1; i<=5; i++) {
    int paintTotal1 = 0;
    int paintTotal2 = 0;
    int basesTotal = 0;
    priceBases.forEach((p){
      basesTotal += (p.baseBasePrice > 0 ? p.baseBasePrice + (i*100) - 1 : 0) * (p.count1 + p.count2);
      paintTotal1 += (p.paintBasePrice * p.count1 * levelFactors[i-1]).round();
      paintTotal2 += (p.paintBasePrice * p.count2 * levelFactors[i-1]).round();
    });
    querySelector('#quoteBase$i .subtotal').text = formatPrice(basesTotal);
    querySelector('#quotePrice1$i .subtotal').text = formatPrice(paintTotal1);
    querySelector('#quotePrice2$i .subtotal').text = formatPrice(paintTotal2);

    if (selectedLevels['quoteBase'] == i) selectedBasesTotal = basesTotal;
    if (selectedLevels['quotePrice1'] == i) selectedPaintTotal1 = paintTotal1;
    if (selectedLevels['quotePrice2'] == i) selectedPaintTotal2 = paintTotal2;
  };

  int assemblyTotal = 0;
  priceBases.forEach((p){
    assemblyTotal += p.assemblyPrice * (p.count1 + p.count2);
  });
  querySelector('#assemblyPrice1').text = formatPrice(assemblyTotal);
  querySelector('#assemblyPrice2').text = formatPrice(assemblyTotal);


  querySelector('#totalPrice').text = formatPrice(selectedPaintTotal1 + selectedPaintTotal2 + selectedBasesTotal + (typeCode!='typePaint' ? assemblyTotal : 0));

}

var selectedLevels = {
  'quoteBase' : 1,
  'quotePrice1' : 1,
  'quotePrice2' : 1
};


void highlightButton(String name, int id) {

  selectedLevels[name] = id;
  for (int i=1; i<=5; i++) {
    querySelector('#$name$i').classes.remove('btn-warning');
    querySelector('#$name$i').classes.add('btn-secondary');
  };
  querySelector('#$name$id').classes.remove('btn-secondary');
  querySelector('#$name$id').classes.add('btn-warning');

  updateOrderPrices();
}

typedef QuoteCallback = void Function(String);

void initQuoteForm(QuoteCallback quoteCallback) {

  var lastCategory = '';

  for (int i=1; i<=5; i++) {
    querySelector('#quoteBase$i').onClick.listen((e){
      highlightButton('quoteBase', i);
      e.preventDefault();
    });
    querySelector('#quotePrice1$i').onClick.listen((e){
      highlightButton('quotePrice1', i);
      e.preventDefault();
    });
    querySelector('#quotePrice2$i').onClick.listen((e){
      highlightButton('quotePrice2', i);
      e.preventDefault();
    });
  };

  highlightButton('quoteBase', 2);
  highlightButton('quotePrice1', 2);
  highlightButton('quotePrice2', 2);

  querySelector('#unitUSD').onClick.listen((e){
    setCurrency('USD');
    e.preventDefault();
  });
  querySelector('#unitEUR').onClick.listen((e){
    setCurrency('EUR');
    e.preventDefault();
  });
  querySelector('#unitGBP').onClick.listen((e){
    setCurrency('GBP');
    e.preventDefault();
  });

  querySelector('#typePaint').onClick.listen((e){
    setType('typePaint');
    e.preventDefault();
  });
  querySelector('#typeAssemble').onClick.listen((e){
    setType('typeAssemble');
    e.preventDefault();
  });
  querySelector('#typeBuy').onClick.listen((e){
    setType('typeBuy');
    e.preventDefault();
  });


  priceBases.forEach((p){

    if(lastCategory != p.category) {
      lastCategory = p.category;
      querySelector( "#quoteUnits" ).appendHtml('<h5 class="text-center text-uppercase divider"><span>${p.category}</span></h5>');
      querySelector( "#quoteUnits" ).appendHtml('<div class="row"><div class="col-2 col-sm-4"></div><div class="col-5 col-sm-4 text-center">regular <a class="js-scroll-trigger align-top legend" href="#quoteLegend">*</a></div><div class="col-5 col-sm-4 text-center">premium <a class="js-scroll-trigger align-top legend" href="#quoteLegend">*</a></div></div>');
    }

    var div = new DivElement();
    div.className = 'row';

    div.appendHtml("<div class='col-2 col-sm-4'><strong class='quoteType'>${p.name}</strong></div>");

    var count1 = NumberInputElement()
      ..className='form-control';

    var count2 = NumberInputElement()
      ..className='form-control';

    p.countInput1 = count1;
    p.countInput2 = count2;

    div.append(new DivElement()
      ..className = 'col-5 col-sm-4 controls'
      ..append(count1)
    );
    div.append(new DivElement()
      ..className = 'col-5 col-sm-4 controls'
      ..append(count2)
    );

    count1.onChange.listen((e){
      p.count1 = count1.valueAsNumber;
      updateOrderPrices();
    });

    count2.onChange.listen((e){
      p.count2 = count2.valueAsNumber;
      updateOrderPrices();
    });

    querySelector( "#quoteUnits" ).append(div);
    querySelector( "#quoteUnits" ).appendHtml('<div class="mb-3">${p.examples}</div>');

  });

  updateOrderPrices();
  //print(getApiUrl());

  querySelector( "#quoteForm" ).onSubmit.listen((e){
      e.preventDefault();

      String msg = "";
      msg += "Please replace (...) with model names below:\n";
      msg += "I would like a quote on a project:\n";

      priceBases.forEach((p){
        if (p.countInput1.valueAsNumber > 0) {
          msg += "${p.name} ${p.category} - ${p.countInput1.valueAsNumber} units on level ${selectedLevels['quotePrice1']}:\n";
          msg += "...\n";
        }
        if (p.countInput2.valueAsNumber > 0) {
          msg += "${p.name} ${p.category} - ${p.countInput2.valueAsNumber} units on level ${selectedLevels['quotePrice2']}:\n";
          msg += "...\n";
        }
      });

      msg += "I would like bases to be painted on level ${selectedLevels['quoteBase']}.\n";

      switch (typeCode) {
        case 'typePaint':
          msg += "I will ship assemblied models to you.\n";
          break;
        case 'typeAssemble':
          msg += "I will ship unassemblied models to you and want you to assemble them.\n";
          break;
        case 'typeBuy':
          msg += "I want you to buy models for me.\n";
          break;
      }
      msg += "Estimated cost from the online calculator was: " + querySelector('#totalPrice').text + "\n";
      msg += "Please provide final price";
      quoteCallback(msg);
  });
}
