// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'main.dart';

// **************************************************************************
// SwiftGenerator
// **************************************************************************

//no interceptor for [AnnotatedWith]
//no interceptor for [Pluggable]
//no interceptor for [TypePlugin]
//no interceptor for [module_core.SiteAction]
//interceptor for [module_core.Error404Action]
//interceptor for module_core.Error404Action
//can be singleton: FALSE
//parent: Error404Action [@bool get Compose]
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $module_core_Error404Action extends module_core.Error404Action
    implements Pluggable {
  $module_core_Error404Action(args) {
//List<String>
    this.args = args;
//Error404Widget
//create
    this.index = new $module_core_Error404Widget();
  }
  T plugin<T>() {
    return null;
  }

  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.Error404Action";
}

//interceptor for [module_core.Error404Widget]
//interceptor for module_core.Error404Widget
//can be singleton: TRUE
//parent: Error404Widget []
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_Error404Widget extends module_core.Error404Widget
    implements Pluggable {
  $module_core_Error404Widget() {
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.Error404Widget";
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_core_Error404Widget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_core_Error404Widget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_core_Error404Widget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_core_Error404Widget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_core_Error404Widget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_core_Error404Widget.html
}

//no interceptor for [module_core.SiteMenuItem]
//no interceptor for [module_core.SiteShowAction]
//no interceptor for [module_core.SiteListAction]
//interceptor for [module_core.ListWidget]
//interceptor for module_core.ListWidget
//can be singleton: TRUE
//parent: ListWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_ListWidget extends module_core.ListWidget
    implements Pluggable {
  $module_core_ListWidget() {
//dynamic
//Slot
//create
    this.items = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.ListWidget";
  void initializeSlots() {
//compiled method
    {
      this.items.container = this;
      this.items.element =
          element.querySelector('[data-slot="' + 'items' + '"]');
      if (this.items.element != null) {
        this.items.element.classes.add('slot');
      } else {
        print('slot ' + 'items' + ' is missing');
      }
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.items.widgets.forEach((widget) {
        this.items.element.append(widget.element);
      });
    }
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_core_ListWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_core_ListWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_core_ListWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_core_ListWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_core_ListWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_core_ListWidget.html
}

//no interceptor for [module_core.ListItemWidget]
//no interceptor for [module_core.ShowItemWidget]
//interceptor for [module_core.Site]
//interceptor for module_core.Site
//can be singleton: TRUE
//parent: Site [@bool get Compose]
class $module_core_Site extends module_core.Site implements Pluggable {
  $module_core_Site() {}
  T plugin<T>() {
    return null;
  }

  String get name => "Swift Shop Demo PL";
  String get url => "demo.swift.shop/pl";
  module_core.Id get id => new module_core.Id.fromString("JA2Q-KBVU");
}

//interceptor for [module_core.Application]
//interceptor for module_core.Application
//can be singleton: TRUE
//parent: Application [@bool get Compose]
//plugin: module_om.InitCartButton
class $module_core_Application extends module_core.Application
    implements Pluggable {
  module_om.InitCartButton module_om_InitCartButton;
  $module_core_Application() {
//Map<String, Future<String>>
    module_om_InitCartButton = new $module_om_InitCartButton(this);
  }
  T plugin<T>() {
    if (T == module_om.InitCartButton) {
      return module_om_InitCartButton as T;
    }
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  module_core.Site get site => $om.module_core_Site;
  module_core.Layout get layout => $om.module_core_Layout;
  void run() {
    super.run();
    module_om_InitCartButton.afterRun();
  }
}

//interceptor for [module_core.Layout]
//interceptor for module_core.Layout
//can be singleton: TRUE
//parent: Layout [@bool get Compose]
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_Layout extends module_core.Layout implements Pluggable {
  $module_core_Layout() {
//dynamic
//Slot
//create
    this.navbar = new module_core.Slot();
//Slot
//create
    this.body = new module_core.Slot();
//Slot
//create
    this.modal = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.Layout";
  module_core.Site get site => $om.module_core_Site;
  void initializeSlots() {
//compiled method
    {
      this.navbar.container = this;
      this.navbar.element =
          element.querySelector('[data-slot="' + 'navbar' + '"]');
      if (this.navbar.element != null) {
        this.navbar.element.classes.add('slot');
      } else {
        print('slot ' + 'navbar' + ' is missing');
      }
    }
    {
      this.body.container = this;
      this.body.element = element.querySelector('[data-slot="' + 'body' + '"]');
      if (this.body.element != null) {
        this.body.element.classes.add('slot');
      } else {
        print('slot ' + 'body' + ' is missing');
      }
    }
    {
      this.modal.container = this;
      this.modal.element =
          element.querySelector('[data-slot="' + 'modal' + '"]');
      if (this.modal.element != null) {
        this.modal.element.classes.add('slot');
      } else {
        print('slot ' + 'modal' + ' is missing');
      }
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.navbar.widgets.forEach((widget) {
        this.navbar.element.append(widget.element);
      });
    }
    {
      this.body.widgets.forEach((widget) {
        this.body.element.append(widget.element);
      });
    }
    {
      this.modal.widgets.forEach((widget) {
        this.modal.element.append(widget.element);
      });
    }
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_core_Layout.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_core_Layout.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_core_Layout.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_core_Layout.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_core_Layout.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_core_Layout.html
}

//no interceptor for [module_core.ApiModule]
//no interceptor for [module_core.CRUDApi]
//no interceptor for [module_core.AnnotatedWith]
//no interceptor for [module_core.Pluggable]
//no interceptor for [module_core.TypePlugin]
//no interceptor for [module_core.PossibleTypoException]
//no interceptor for [module_core.Id]
//no interceptor for [module_core.Action]
//interceptor for [module_core.Router]
//interceptor for module_core.Router
//can be singleton: TRUE
//parent: Router [@bool get Compose]
class $module_core_Router extends module_core.Router implements Pluggable {
  $module_core_Router() {
//Action
//Action
//Element
  }
  T plugin<T>() {
    return null;
  }

  String get indexUrl => "products";
  module_core.Action actionFactory(String className, List<String> args) {
    switch (className) {
      case 'module_core.Error404Action':
        return new $module_core_Error404Action(args);
      case 'module_pim.ProductAction':
        return new $module_pim_ProductAction(args);
      case 'module_pim.ProductsAction':
        return new $module_pim_ProductsAction(args);
      case 'module_snippets.PageAction':
        return new $module_snippets_PageAction(args);
      case 'module_om.CartAction':
        return new $module_om_CartAction(args);
      case 'module_om.PaymentSuccessAction':
        return new $module_om_PaymentSuccessAction(args);
      case 'module_om.PlaceOrderAction':
        return new $module_om_PlaceOrderAction(args);
      case 'module_om.OrderStatusAction':
        return new $module_om_OrderStatusAction(args);
      case 'module_om.CheckoutAction':
        return new $module_om_CheckoutAction(args);
    }
  }

  String getCode<T extends module_core.Action>() {
    if (T == module_core.Error404Action) return 'module_core.Error404Action';
    if (T == module_pim.ProductAction) return 'module_pim.ProductAction';
    if (T == module_pim.ProductsAction) return 'module_pim.ProductsAction';
    if (T == module_snippets.PageAction) return 'module_snippets.PageAction';
    if (T == module_om.CartAction) return 'module_om.CartAction';
    if (T == module_om.PaymentSuccessAction)
      return 'module_om.PaymentSuccessAction';
    if (T == module_om.PlaceOrderAction) return 'module_om.PlaceOrderAction';
    if (T == module_om.OrderStatusAction) return 'module_om.OrderStatusAction';
    if (T == module_om.CheckoutAction) return 'module_om.CheckoutAction';
  }
}

//no interceptor for [module_core.Slot]
//interceptor for [module_core.NavbarLink]
//interceptor for module_core.NavbarLink
//can be singleton: TRUE
//parent: NavbarLink []
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_NavbarLink extends module_core.NavbarLink
    implements Pluggable {
  $module_core_NavbarLink() {
//dynamic
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.NavbarLink";
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_core_NavbarLink.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_core_NavbarLink.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_core_NavbarLink.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_core_NavbarLink.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_core_NavbarLink.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_core_NavbarLink.html
}

//interceptor for [module_core.NavbarDropdown]
//interceptor for module_core.NavbarDropdown
//can be singleton: TRUE
//parent: NavbarDropdown []
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_NavbarDropdown extends module_core.NavbarDropdown
    implements Pluggable {
  $module_core_NavbarDropdown() {
//dynamic
//Slot
//create
    this.items = new module_core.Slot();
//String
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.NavbarDropdown";
  void initializeSlots() {
//compiled method
    {
      this.items.container = this;
      this.items.element =
          element.querySelector('[data-slot="' + 'items' + '"]');
      if (this.items.element != null) {
        this.items.element.classes.add('slot');
      } else {
        print('slot ' + 'items' + ' is missing');
      }
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.items.widgets.forEach((widget) {
        this.items.element.append(widget.element);
      });
    }
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_core_NavbarDropdown.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_core_NavbarDropdown.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_core_NavbarDropdown.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_core_NavbarDropdown.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_core_NavbarDropdown.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_core_NavbarDropdown.html
}

//no interceptor for [module_core.SwiftUriPolicy]
//no interceptor for [module_core.ExpandedElement]
//no interceptor for [module_core.Widget]
//no interceptor for [module_core.AsyncWidget]
//interceptor for [module_core.TextWidget]
//interceptor for module_core.TextWidget
//can be singleton: FALSE
//parent: TextWidget [@bool get Compose]
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_TextWidget extends module_core.TextWidget
    implements Pluggable {
  $module_core_TextWidget(text) {
//dynamic
//String
    this.text = text;
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.TextWidget";
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_core_TextWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_core_TextWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_core_TextWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_core_TextWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_core_TextWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_core_TextWidget.html
}

//no interceptor for [module_core.Entity]
//interceptor for [module_core.App]
//interceptor for module_core.App
//can be singleton: TRUE
//parent: App []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_App extends module_core.App implements Pluggable {
  $module_core_App() {
//Id
//String
//App
//User
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_core.App'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['parent'] = "module_core.App";
    }
    {
      target['owner'] = "module_core.User";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.parent = relations['parent'];
    }
    {
      this.owner = relations['owner'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['url'] = this.url;
    }
    {
      target['name'] = this.name;
    }
//@Field
//@Field
    {
      target['parent'] = this.parent != null ? this.parent.toJson() : null;
    }
    {
      target['owner'] = this.owner != null ? this.owner.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.url = target.containsKey('url') ? target['url'] : this.url;
    }
    {
      this.name = target.containsKey('name') ? target['name'] : this.name;
    }
//@Field
//@Field
//@Field
    {
      if (target['parent'] != null) {
        if (target['parent'] is String) {
          this.parent = relatedFactory("module_core.App");
          this.parent.id = idFromString(target['parent']);
        } else {
          this.parent = relatedFactory(target['parent']['classes'].last);
          this.parent.fromJson(target['parent']);
        }
      } else {
        this.parent = null;
      }
    }
    {
      if (target['owner'] != null) {
        if (target['owner'] is String) {
          this.owner = relatedFactory("module_core.User");
          this.owner.id = idFromString(target['owner']);
        } else {
          this.owner = relatedFactory(target['owner']['classes'].last);
          this.owner.fromJson(target['owner']);
        }
      } else {
        this.owner = null;
      }
    }
  }
}

//interceptor for [module_core.User]
//interceptor for module_core.User
//can be singleton: TRUE
//parent: User []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_User extends module_core.User implements Pluggable {
  $module_core_User() {
//Id
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_core.User'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['email'] = this.email;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.email = target.containsKey('email') ? target['email'] : this.email;
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_core.Image]
//interceptor for module_core.Image
//can be singleton: TRUE
//parent: Image []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_Image extends module_core.Image implements Pluggable {
  $module_core_Image() {
//Id
//String
//App
//String
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_core.Image'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['app'] = "module_core.App";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.app = relations['app'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['mimetype'] = this.mimetype;
    }
    {
      target['filename'] = this.filename;
    }
    {
      target['thumb'] = this.thumb;
    }
//@Field
//@Field
    {
      target['app'] = this.app != null ? this.app.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.mimetype =
          target.containsKey('mimetype') ? target['mimetype'] : this.mimetype;
    }
    {
      this.filename =
          target.containsKey('filename') ? target['filename'] : this.filename;
    }
    {
      this.thumb = target.containsKey('thumb') ? target['thumb'] : this.thumb;
    }
//@Field
//@Field
//@Field
    {
      if (target['app'] != null) {
        if (target['app'] is String) {
          this.app = relatedFactory("module_core.App");
          this.app.id = idFromString(target['app']);
        } else {
          this.app = relatedFactory(target['app']['classes'].last);
          this.app.fromJson(target['app']);
        }
      } else {
        this.app = null;
      }
    }
  }
}

//interceptor for [module_pim.ProductAction]
//interceptor for module_pim.ProductAction
//T[269908264] => ProductBase[447620454]
//can be singleton: FALSE
//parent: ProductAction []
//parent: SiteShowAction [@bool get ComposeSubtypes]
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $module_pim_ProductAction extends module_pim.ProductAction
    implements Pluggable {
  $module_pim_ProductAction(args) {
//Future<T>
//T
//ShowItemWidget<T>
//List<String>
    this.args = args;
  }
  T plugin<T>() {
    return null;
  }

  module_core.CRUDApi<module_pim.ProductBase> get api => $om.module_pim_PimApi;
  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "module_pim.ProductAction";
  module_core.ShowItemWidget<module_pim.ProductBase> createWidget(
      module_pim.ProductBase item) {
//module_core.ShowItemWidget
    return new $module_pim_ShowProductWidget(item);
  }

  module_pim.ProductsAction listFactory() {
//module_pim.ProductsAction
    return new $module_pim_ProductsAction(args);
  }
}

//interceptor for [module_pim.ShowProductWidget]
//interceptor for module_pim.ShowProductWidget
//T[391897947] => ProductBase[447620454]
//can be singleton: FALSE
//parent: ShowProductWidget []
//parent: ShowItemWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
//plugin: module_om.AddToShowProductWidget
class $module_pim_ShowProductWidget extends module_pim.ShowProductWidget
    implements Pluggable {
  module_om.AddToShowProductWidget module_om_AddToShowProductWidget;
  $module_pim_ShowProductWidget(item) {
//T
    this.item = item;
//dynamic
//Slot
//create
    this.buttons = new module_core.Slot();
    module_om_AddToShowProductWidget =
        new $module_om_AddToShowProductWidget(this);
  }
  T plugin<T>() {
    if (T == module_om.AddToShowProductWidget) {
      return module_om_AddToShowProductWidget as T;
    }
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_pim.ShowProductWidget";
  void initializeSlots() {
//compiled method
    {
      this.buttons.container = this;
      this.buttons.element =
          element.querySelector('[data-slot="' + 'buttons' + '"]');
      if (this.buttons.element != null) {
        this.buttons.element.classes.add('slot');
      } else {
        print('slot ' + 'buttons' + ' is missing');
      }
    }
  }

  void init() {
    super.init();
    module_om_AddToShowProductWidget.afterInit();
  }

  void repaintSlots() {
//compiled method
    {
      this.buttons.widgets.forEach((widget) {
        this.buttons.element.append(widget.element);
      });
    }
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_pim_ShowProductWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_pim_ShowProductWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_pim_ShowProductWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_pim_ShowProductWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_pim_ShowProductWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_pim_ShowProductWidget.html
}

//interceptor for [module_pim.ProductsAction]
//interceptor for module_pim.ProductsAction
//T[182183646] => ProductBase[447620454]
//can be singleton: FALSE
//parent: ProductsAction [@SiteMenuItem SiteMenuItem(String section, String title)]
//parent: SiteListAction [@bool get ComposeSubtypes]
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $module_pim_ProductsAction extends module_pim.ProductsAction
    implements Pluggable {
  $module_pim_ProductsAction(args) {
//dynamic
//List<T>
//ListWidget
//create
    this.listWidget = new $module_core_ListWidget();
//List<String>
    this.args = args;
  }
  T plugin<T>() {
    return null;
  }

  module_core.CRUDApi<module_pim.ProductBase> get api => $om.module_pim_PimApi;
  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "module_pim.ProductsAction";
  module_core.ListItemWidget<module_pim.ProductBase> createListWidget(
      module_pim.ProductBase item) {
//module_core.ListItemWidget
    return new $module_pim_ProductWidget(item);
  }
}

//interceptor for [module_pim.ProductWidget]
//interceptor for module_pim.ProductWidget
//T[370206959] => ProductBase[447620454]
//can be singleton: FALSE
//parent: ProductWidget []
//parent: ListItemWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
//plugin: module_om.AddToProductWidget
class $module_pim_ProductWidget extends module_pim.ProductWidget
    implements Pluggable {
  module_om.AddToProductWidget module_om_AddToProductWidget;
  $module_pim_ProductWidget(item) {
//T
    this.item = item;
//dynamic
//Slot
//create
    this.buttons = new module_core.Slot();
    module_om_AddToProductWidget = new $module_om_AddToProductWidget(this);
  }
  T plugin<T>() {
    if (T == module_om.AddToProductWidget) {
      return module_om_AddToProductWidget as T;
    }
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_pim.ProductWidget";
  void initializeSlots() {
//compiled method
    {
      this.buttons.container = this;
      this.buttons.element =
          element.querySelector('[data-slot="' + 'buttons' + '"]');
      if (this.buttons.element != null) {
        this.buttons.element.classes.add('slot');
      } else {
        print('slot ' + 'buttons' + ' is missing');
      }
    }
  }

  void init() {
    super.init();
    module_om_AddToProductWidget.afterInit();
  }

  void repaintSlots() {
//compiled method
    {
      this.buttons.widgets.forEach((widget) {
        this.buttons.element.append(widget.element);
      });
    }
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_pim_ProductWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_pim_ProductWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_pim_ProductWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_pim_ProductWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_pim_ProductWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_pim_ProductWidget.html
}

//interceptor for [module_pim.PimApi]
//interceptor for module_pim.PimApi
//T[534851839] => ProductBase[447620454]
//can be singleton: TRUE
//parent: PimApi [@bool get Compose]
//parent: CRUDApi [@bool get ComposeSubtypes]
//parent: ApiModule [@bool get ComposeSubtypes]
class $module_pim_PimApi extends module_pim.PimApi implements Pluggable {
  $module_pim_PimApi() {}
  T plugin<T>() {
    return null;
  }

  Map<String, module_core.CRUDApi<module_core.Entity>> get relatedApis =>
      $om.instancesOfmodule_core_CRUDApi_module_core_Entity_;
  String get className => "module_pim.PimApi";
  module_core.Site get site => $om.module_core_Site;
  module_pim.ProductBase newEntityByType(String className) {
    switch (className) {
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
    }
  }
}

//no interceptor for [module_pim.UnitType]
//no interceptor for [module_pim.ConfigurableField]
//no interceptor for [module_pim.ProductBase]
//interceptor for [module_pim.SimpleProduct]
//interceptor for module_pim.SimpleProduct
//can be singleton: TRUE
//parent: SimpleProduct []
//parent: ProductBase [@bool get ComposeSubtypes]
//parent: Entity [@bool get ComposeSubtypes]
class $module_pim_SimpleProduct extends module_pim.SimpleProduct
    implements Pluggable {
  $module_pim_SimpleProduct() {
//String
//int
//String
//ConfigurableField<UnitType>
//Image
//Id
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_pim.ProductBase', 'module_pim.SimpleProduct'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['image'] = "module_core.Image";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.image = relations['image'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
    {
      target['price'] = this.price;
    }
//@Field
    {
      target['name'] = this.name;
    }
    {
      target['shortDescription'] = this.shortDescription;
    }
    {
      target['slug'] = this.slug;
    }
//@Field
//@Field
    {
      target['image'] = this.image != null ? this.image.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.name = target.containsKey('name') ? target['name'] : this.name;
    }
    {
      this.shortDescription = target.containsKey('shortDescription')
          ? target['shortDescription']
          : this.shortDescription;
    }
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
//@Field
    {
      this.price = target.containsKey('price') ? target['price'] : this.price;
    }
//@Field
//@Field
    {
      if (target['image'] != null) {
        if (target['image'] is String) {
          this.image = relatedFactory("module_core.Image");
          this.image.id = idFromString(target['image']);
        } else {
          this.image = relatedFactory(target['image']['classes'].last);
          this.image.fromJson(target['image']);
        }
      } else {
        this.image = null;
      }
    }
  }
}

//interceptor for [module_snippets.PageAction]
//interceptor for module_snippets.PageAction
//T[269908264] => Page[414991068]
//can be singleton: FALSE
//parent: PageAction []
//parent: SiteShowAction [@bool get ComposeSubtypes]
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $module_snippets_PageAction extends module_snippets.PageAction
    implements Pluggable {
  $module_snippets_PageAction(args) {
//Future<T>
//T
//ShowItemWidget<T>
//List<String>
    this.args = args;
  }
  T plugin<T>() {
    return null;
  }

  module_core.CRUDApi<module_snippets.Page> get api =>
      $om.module_snippets_CmsPagesApi;
  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "module_snippets.PageAction";
  module_core.ShowItemWidget<module_snippets.Page> createWidget(
      module_snippets.Page item) {
//module_core.ShowItemWidget
    return new $module_snippets_ShowPageWidget(item);
  }

  module_snippets.CmsWidget<module_snippets.CmsWidgetConfig> createItemWidget(
      String className, module_snippets.CmsWidgetConfig config) {
    switch (className) {
      case 'module_snippets.HtmlWidget':
        return new $module_snippets_HtmlWidget(config);
      case 'module_snippets.ImageWidget':
        return new $module_snippets_ImageWidget(config);
      case 'module_snippets.ParagraphWidget':
        return new $module_snippets_ParagraphWidget(config);
      case 'module_snippets.HeadingWidget':
        return new $module_snippets_HeadingWidget(config);
    }
  }
}

//interceptor for [module_snippets.ShowPageWidget]
//interceptor for module_snippets.ShowPageWidget
//T[391897947] => Page[414991068]
//can be singleton: FALSE
//parent: ShowPageWidget []
//parent: ShowItemWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $module_snippets_ShowPageWidget extends module_snippets.ShowPageWidget
    implements Pluggable {
  $module_snippets_ShowPageWidget(item) {
//T
    this.item = item;
//dynamic
//Slot
//create
    this.pageContent = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_snippets.ShowPageWidget";
  void initializeSlots() {
//compiled method
    {
      this.pageContent.container = this;
      this.pageContent.element =
          element.querySelector('[data-slot="' + 'pageContent' + '"]');
      if (this.pageContent.element != null) {
        this.pageContent.element.classes.add('slot');
      } else {
        print('slot ' + 'pageContent' + ' is missing');
      }
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.pageContent.widgets.forEach((widget) {
        this.pageContent.element.append(widget.element);
      });
    }
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_snippets_ShowPageWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_snippets_ShowPageWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_snippets_ShowPageWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_snippets_ShowPageWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_snippets_ShowPageWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_snippets_ShowPageWidget.html
}

//no interceptor for [module_snippets.CmsWidget]
//interceptor for [module_snippets.HtmlWidget]
//interceptor for module_snippets.HtmlWidget
//CT[171676771] => HtmlConfig[389581089]
//can be singleton: FALSE
//parent: HtmlWidget []
//parent: CmsWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $module_snippets_HtmlWidget extends module_snippets.HtmlWidget
    implements Pluggable {
  $module_snippets_HtmlWidget(config) {
//CT
    this.config = config;
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_snippets.HtmlWidget";
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_snippets_HtmlWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_snippets_HtmlWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_snippets_HtmlWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_snippets_HtmlWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_snippets_HtmlWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_snippets_HtmlWidget.html
}

//interceptor for [module_snippets.ImageWidget]
//interceptor for module_snippets.ImageWidget
//CT[171676771] => ImageConfig[445488254]
//can be singleton: FALSE
//parent: ImageWidget []
//parent: CmsWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $module_snippets_ImageWidget extends module_snippets.ImageWidget
    implements Pluggable {
  $module_snippets_ImageWidget(config) {
//CT
    this.config = config;
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_snippets.ImageWidget";
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_snippets_ImageWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_snippets_ImageWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_snippets_ImageWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_snippets_ImageWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_snippets_ImageWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_snippets_ImageWidget.html
}

//interceptor for [module_snippets.ParagraphWidget]
//interceptor for module_snippets.ParagraphWidget
//CT[171676771] => ParagraphConfig[153581726]
//can be singleton: FALSE
//parent: ParagraphWidget []
//parent: CmsWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $module_snippets_ParagraphWidget extends module_snippets.ParagraphWidget
    implements Pluggable {
  $module_snippets_ParagraphWidget(config) {
//CT
    this.config = config;
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_snippets.ParagraphWidget";
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_snippets_ParagraphWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_snippets_ParagraphWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_snippets_ParagraphWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_snippets_ParagraphWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_snippets_ParagraphWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_snippets_ParagraphWidget.html
}

//interceptor for [module_snippets.HeadingWidget]
//interceptor for module_snippets.HeadingWidget
//CT[171676771] => HeadingConfig[26884972]
//can be singleton: FALSE
//parent: HeadingWidget []
//parent: CmsWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $module_snippets_HeadingWidget extends module_snippets.HeadingWidget
    implements Pluggable {
  $module_snippets_HeadingWidget(config) {
//CT
    this.config = config;
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_snippets.HeadingWidget";
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_snippets_HeadingWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_snippets_HeadingWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_snippets_HeadingWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_snippets_HeadingWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_snippets_HeadingWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_snippets_HeadingWidget.html
}

//interceptor for [module_snippets.CmsPagesApi]
//interceptor for module_snippets.CmsPagesApi
//T[534851839] => Page[414991068]
//can be singleton: TRUE
//parent: CmsPagesApi []
//parent: CRUDApi [@bool get ComposeSubtypes]
//parent: ApiModule [@bool get ComposeSubtypes]
class $module_snippets_CmsPagesApi extends module_snippets.CmsPagesApi
    implements Pluggable {
  $module_snippets_CmsPagesApi() {}
  T plugin<T>() {
    return null;
  }

  Map<String, module_core.CRUDApi<module_core.Entity>> get relatedApis =>
      $om.instancesOfmodule_core_CRUDApi_module_core_Entity_;
  String get className => "module_snippets.CmsPagesApi";
  module_core.Site get site => $om.module_core_Site;
  module_snippets.Page newEntityByType(String className) {
    switch (className) {
      case 'module_snippets.Page':
        return new $module_snippets_Page();
    }
  }
}

//interceptor for [module_snippets.CmsWidgetConfig]
//interceptor for module_snippets.CmsWidgetConfig
//can be singleton: TRUE
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_snippets_CmsWidgetConfig extends module_snippets.CmsWidgetConfig
    implements Pluggable {
  $module_snippets_CmsWidgetConfig() {
//Id
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_snippets.CmsWidgetConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_snippets.HeadingConfig]
//interceptor for module_snippets.HeadingConfig
//can be singleton: TRUE
//parent: HeadingConfig []
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_snippets_HeadingConfig extends module_snippets.HeadingConfig
    implements Pluggable {
  $module_snippets_HeadingConfig() {
//Id
//String
//String
//String
//int
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_snippets.CmsWidgetConfig', 'module_snippets.HeadingConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
    {
      target['level'] = this.level;
    }
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['title'] = this.title;
    }
    {
      target['subtitle'] = this.subtitle;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.title = target.containsKey('title') ? target['title'] : this.title;
    }
    {
      this.subtitle =
          target.containsKey('subtitle') ? target['subtitle'] : this.subtitle;
    }
//@Field
    {
      this.level = target.containsKey('level') ? target['level'] : this.level;
    }
//@Field
//@Field
  }
}

//interceptor for [module_snippets.ImageConfig]
//interceptor for module_snippets.ImageConfig
//can be singleton: TRUE
//parent: ImageConfig []
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_snippets_ImageConfig extends module_snippets.ImageConfig
    implements Pluggable {
  $module_snippets_ImageConfig() {
//Id
//String
//Image
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_snippets.CmsWidgetConfig', 'module_snippets.ImageConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['image'] = "module_core.Image";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.image = relations['image'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['caption'] = this.caption;
    }
//@Field
//@Field
    {
      target['image'] = this.image != null ? this.image.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.caption =
          target.containsKey('caption') ? target['caption'] : this.caption;
    }
//@Field
//@Field
//@Field
    {
      if (target['image'] != null) {
        if (target['image'] is String) {
          this.image = relatedFactory("module_core.Image");
          this.image.id = idFromString(target['image']);
        } else {
          this.image = relatedFactory(target['image']['classes'].last);
          this.image.fromJson(target['image']);
        }
      } else {
        this.image = null;
      }
    }
  }
}

//interceptor for [module_snippets.HtmlConfig]
//interceptor for module_snippets.HtmlConfig
//can be singleton: TRUE
//parent: HtmlConfig []
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_snippets_HtmlConfig extends module_snippets.HtmlConfig
    implements Pluggable {
  $module_snippets_HtmlConfig() {
//Id
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_snippets.CmsWidgetConfig', 'module_snippets.HtmlConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['html'] = this.html;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.html = target.containsKey('html') ? target['html'] : this.html;
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_snippets.ParagraphConfig]
//interceptor for module_snippets.ParagraphConfig
//can be singleton: TRUE
//parent: ParagraphConfig []
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_snippets_ParagraphConfig extends module_snippets.ParagraphConfig
    implements Pluggable {
  $module_snippets_ParagraphConfig() {
//Id
//String
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_snippets.CmsWidgetConfig', 'module_snippets.ParagraphConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['title'] = this.title;
    }
    {
      target['body'] = this.body;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.title = target.containsKey('title') ? target['title'] : this.title;
    }
    {
      this.body = target.containsKey('body') ? target['body'] : this.body;
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_snippets.Page]
//interceptor for module_snippets.Page
//can be singleton: TRUE
//parent: Page []
//parent: Entity [@bool get ComposeSubtypes]
class $module_snippets_Page extends module_snippets.Page implements Pluggable {
  $module_snippets_Page() {
//Id
//String
//App
//String
//List<CmsWidgetConfig>
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_snippets.Page'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['app'] = "module_core.App";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.app = relations['app'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['title'] = this.title;
    }
//@Field
    {
      target['widgets'] = this.widgets.map((e) => e.toJson()).toList();
    }
//@Field
    {
      target['app'] = this.app != null ? this.app.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.title = target.containsKey('title') ? target['title'] : this.title;
    }
//@Field
//@Field
    {
      this.widgets = [];
      if (target['widgets'] != null) {
        target['widgets'].forEach((data) {
          var e = relatedFactory(data['classes'].last);
          e.fromJson(data);
          this.widgets.add(e);
        });
      }
    }
//@Field
    {
      if (target['app'] != null) {
        if (target['app'] is String) {
          this.app = relatedFactory("module_core.App");
          this.app.id = idFromString(target['app']);
        } else {
          this.app = relatedFactory(target['app']['classes'].last);
          this.app.fromJson(target['app']);
        }
      } else {
        this.app = null;
      }
    }
  }
}

//interceptor for [module_om.ShoppingCart]
//interceptor for module_om.ShoppingCart
//can be singleton: TRUE
//parent: ShoppingCart [@bool get Compose]
class $module_om_ShoppingCart extends module_om.ShoppingCart
    implements Pluggable {
  $module_om_ShoppingCart() {}
  T plugin<T>() {
    return null;
  }

  module_om.Order orderFactory() {
//module_om.Order
    return new $module_om_Order();
  }
}

//interceptor for [module_om.CartAction]
//interceptor for module_om.CartAction
//can be singleton: FALSE
//parent: CartAction [@SiteMenuItem SiteMenuItem(String section, String title)]
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $module_om_CartAction extends module_om.CartAction implements Pluggable {
  $module_om_CartAction(args) {
//List<String>
    this.args = args;
  }
  T plugin<T>() {
    return null;
  }

  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "module_om.CartAction";
  module_om.CartWidget get cart => $om.module_om_CartWidget;
}

//interceptor for [module_om.PaymentSuccessAction]
//interceptor for module_om.PaymentSuccessAction
//can be singleton: FALSE
//parent: PaymentSuccessAction []
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $module_om_PaymentSuccessAction extends module_om.PaymentSuccessAction
    implements Pluggable {
  $module_om_PaymentSuccessAction(args) {
//List<String>
    this.args = args;
  }
  T plugin<T>() {
    return null;
  }

  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "module_om.PaymentSuccessAction";
  module_om.PaymentSuccessWidget get paymentSuccessWidget =>
      $om.module_om_PaymentSuccessWidget;
}

//interceptor for [module_om.PaymentSuccessWidget]
//interceptor for module_om.PaymentSuccessWidget
//can be singleton: TRUE
//parent: PaymentSuccessWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $module_om_PaymentSuccessWidget extends module_om.PaymentSuccessWidget
    implements Pluggable {
  $module_om_PaymentSuccessWidget() {
//dynamic
//String
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_om.PaymentSuccessWidget";
  module_om.ShoppingCart get shoppingCart => $om.module_om_ShoppingCart;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_om_PaymentSuccessWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_om_PaymentSuccessWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_om_PaymentSuccessWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_om_PaymentSuccessWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_om_PaymentSuccessWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_om_PaymentSuccessWidget.html
}

//interceptor for [module_om.PlaceOrderAction]
//interceptor for module_om.PlaceOrderAction
//can be singleton: FALSE
//parent: PlaceOrderAction []
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $module_om_PlaceOrderAction extends module_om.PlaceOrderAction
    implements Pluggable {
  $module_om_PlaceOrderAction(args) {
//List<String>
    this.args = args;
  }
  T plugin<T>() {
    return null;
  }

  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "module_om.PlaceOrderAction";
  module_om.OmApi get omApi => $om.module_om_OmApi;
  module_om.ShoppingCart get shoppingCart => $om.module_om_ShoppingCart;
}

//interceptor for [module_om.PaymentMethodWidget]
//interceptor for module_om.PaymentMethodWidget
//can be singleton: FALSE
//parent: PaymentMethodWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $module_om_PaymentMethodWidget extends module_om.PaymentMethodWidget
    implements Pluggable {
  $module_om_PaymentMethodWidget(paymentMethod) {
//dynamic
//PaymentMethod
    this.paymentMethod = paymentMethod;
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_om.PaymentMethodWidget";
  module_om.ShoppingCart get shoppingCart => $om.module_om_ShoppingCart;
  module_core.Application get app => $om.module_core_Application;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_om_PaymentMethodWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_om_PaymentMethodWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_om_PaymentMethodWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_om_PaymentMethodWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_om_PaymentMethodWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_om_PaymentMethodWidget.html
}

//interceptor for [module_om.OrderStatusAction]
//interceptor for module_om.OrderStatusAction
//can be singleton: FALSE
//parent: OrderStatusAction []
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $module_om_OrderStatusAction extends module_om.OrderStatusAction
    implements Pluggable {
  $module_om_OrderStatusAction(args) {
//List<String>
    this.args = args;
//Order
  }
  T plugin<T>() {
    return null;
  }

  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "module_om.OrderStatusAction";
  module_om.OmApi get omApi => $om.module_om_OmApi;
  module_om.ShoppingCart get shoppingCart => $om.module_om_ShoppingCart;
  Map<String, module_om.PaymentMethod> get paymentMethods =>
      $om.instancesOfmodule_om_PaymentMethod;
  module_om.OrderStatusWidget orderStatusWidget(module_om.Order order) {
//module_om.OrderStatusWidget
    return new $module_om_OrderStatusWidget(order);
  }

  module_om.PaymentMethodWidget createPaymentMethodWidget(
      module_om.PaymentMethod paymentMethod) {
//module_om.PaymentMethodWidget
    return new $module_om_PaymentMethodWidget(paymentMethod);
  }
}

//interceptor for [module_om.OrderStatusWidget]
//interceptor for module_om.OrderStatusWidget
//can be singleton: FALSE
//parent: OrderStatusWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $module_om_OrderStatusWidget extends module_om.OrderStatusWidget
    implements Pluggable {
  $module_om_OrderStatusWidget(order) {
//dynamic
//Order
    this.order = order;
//Slot
//create
    this.paymentInput = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_om.OrderStatusWidget";
  void initializeSlots() {
//compiled method
    {
      this.paymentInput.container = this;
      this.paymentInput.element =
          element.querySelector('[data-slot="' + 'paymentInput' + '"]');
      if (this.paymentInput.element != null) {
        this.paymentInput.element.classes.add('slot');
      } else {
        print('slot ' + 'paymentInput' + ' is missing');
      }
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.paymentInput.widgets.forEach((widget) {
        this.paymentInput.element.append(widget.element);
      });
    }
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_om_OrderStatusWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_om_OrderStatusWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_om_OrderStatusWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_om_OrderStatusWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_om_OrderStatusWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_om_OrderStatusWidget.html
}

//interceptor for [module_om.CheckoutAction]
//interceptor for module_om.CheckoutAction
//can be singleton: FALSE
//parent: CheckoutAction []
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $module_om_CheckoutAction extends module_om.CheckoutAction
    implements Pluggable {
  $module_om_CheckoutAction(args) {
//List<String>
    this.args = args;
  }
  T plugin<T>() {
    return null;
  }

  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "module_om.CheckoutAction";
  module_om.ShoppingCart get shoppingCart => $om.module_om_ShoppingCart;
  module_om.CheckoutWidget get checkoutWidget => $om.module_om_CheckoutWidget;
  Map<String, module_om.DeliveryMethod> get deliveryMethods =>
      $om.instancesOfmodule_om_DeliveryMethod;
  module_om.DeliveryMethodWidget createDeliveryMethodWidget(
      module_om.DeliveryMethod deliveryMethod) {
//module_om.DeliveryMethodWidget
    return new $module_om_DeliveryMethodWidget(deliveryMethod);
  }
}

//interceptor for [module_om.DeliveryMethodWidget]
//interceptor for module_om.DeliveryMethodWidget
//can be singleton: FALSE
//parent: DeliveryMethodWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $module_om_DeliveryMethodWidget extends module_om.DeliveryMethodWidget
    implements Pluggable {
  $module_om_DeliveryMethodWidget(deliveryMethod) {
//dynamic
//DeliveryMethod
    this.deliveryMethod = deliveryMethod;
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_om.DeliveryMethodWidget";
  module_om.ShoppingCart get shoppingCart => $om.module_om_ShoppingCart;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_om_DeliveryMethodWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_om_DeliveryMethodWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_om_DeliveryMethodWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_om_DeliveryMethodWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_om_DeliveryMethodWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_om_DeliveryMethodWidget.html
}

//interceptor for [module_om.CheckoutWidget]
//interceptor for module_om.CheckoutWidget
//can be singleton: TRUE
//parent: CheckoutWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $module_om_CheckoutWidget extends module_om.CheckoutWidget
    implements Pluggable {
  $module_om_CheckoutWidget() {
//dynamic
//Slot
//create
    this.deliveryInput = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_om.CheckoutWidget";
  module_om.ShoppingCart get shoppingCart => $om.module_om_ShoppingCart;
  void initializeSlots() {
//compiled method
    {
      this.deliveryInput.container = this;
      this.deliveryInput.element =
          element.querySelector('[data-slot="' + 'deliveryInput' + '"]');
      if (this.deliveryInput.element != null) {
        this.deliveryInput.element.classes.add('slot');
      } else {
        print('slot ' + 'deliveryInput' + ' is missing');
      }
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.deliveryInput.widgets.forEach((widget) {
        this.deliveryInput.element.append(widget.element);
      });
    }
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_om_CheckoutWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_om_CheckoutWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_om_CheckoutWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_om_CheckoutWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_om_CheckoutWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_om_CheckoutWidget.html
}

//interceptor for [module_om.CartWidget]
//interceptor for module_om.CartWidget
//can be singleton: TRUE
//parent: CartWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $module_om_CartWidget extends module_om.CartWidget implements Pluggable {
  $module_om_CartWidget() {
//dynamic
//Slot
//create
    this.rows = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_om.CartWidget";
  module_om.ShoppingCart get shoppingCart => $om.module_om_ShoppingCart;
  void initializeSlots() {
//compiled method
    {
      this.rows.container = this;
      this.rows.element = element.querySelector('[data-slot="' + 'rows' + '"]');
      if (this.rows.element != null) {
        this.rows.element.classes.add('slot');
      } else {
        print('slot ' + 'rows' + ' is missing');
      }
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.rows.widgets.forEach((widget) {
        this.rows.element.append(widget.element);
      });
    }
  }

  module_om.CartItemWidget createCartItemWidget(module_om.OrderItem item) {
//module_om.CartItemWidget
    return new $module_om_CartItemWidget(item);
  }

  module_om.EmptyCartWidget createEmptyCartWidget() {
//module_om.EmptyCartWidget
    return new $module_om_EmptyCartWidget();
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_om_CartWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_om_CartWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_om_CartWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_om_CartWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_om_CartWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_om_CartWidget.html
}

//interceptor for [module_om.EmptyCartWidget]
//interceptor for module_om.EmptyCartWidget
//can be singleton: TRUE
//parent: EmptyCartWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $module_om_EmptyCartWidget extends module_om.EmptyCartWidget
    implements Pluggable {
  $module_om_EmptyCartWidget() {
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_om.EmptyCartWidget";
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_om_EmptyCartWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_om_EmptyCartWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_om_EmptyCartWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_om_EmptyCartWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_om_EmptyCartWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_om_EmptyCartWidget.html
}

//interceptor for [module_om.CartItemWidget]
//interceptor for module_om.CartItemWidget
//can be singleton: FALSE
//parent: CartItemWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $module_om_CartItemWidget extends module_om.CartItemWidget
    implements Pluggable {
  $module_om_CartItemWidget(item) {
//dynamic
//OrderItem
    this.item = item;
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_om.CartItemWidget";
  module_om.CartWidget get cart => $om.module_om_CartWidget;
  module_om.ShoppingCart get shoppingCart => $om.module_om_ShoppingCart;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_om_CartItemWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_om_CartItemWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_om_CartItemWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_om_CartItemWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_om_CartItemWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_om_CartItemWidget.html
}

//interceptor for [module_om.InitCartButton]
//interceptor for module_om.InitCartButton
//T[362979300] => Application[123648522]
//can be singleton: FALSE
//parent: InitCartButton []
//parent: TypePlugin [@bool get ComposeSubtypes]
class $module_om_InitCartButton extends module_om.InitCartButton
    implements Pluggable {
  $module_om_InitCartButton(parent) {
//T
    this.parent = parent;
  }
  T plugin<T>() {
    return null;
  }

  module_om.ShoppingCart get shoppingCart => $om.module_om_ShoppingCart;
}

//interceptor for [module_om.AddToCartButton]
//interceptor for module_om.AddToCartButton
//can be singleton: FALSE
//parent: AddToCartButton []
//parent: Widget [@bool get ComposeSubtypes]
class $module_om_AddToCartButton extends module_om.AddToCartButton
    implements Pluggable {
  $module_om_AddToCartButton(product) {
//dynamic
//ProductBase
    this.product = product;
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_om.AddToCartButton";
  module_core.Layout get layout => $om.module_core_Layout;
  module_om.ShoppingCart get shoppingCart => $om.module_om_ShoppingCart;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_om_AddToCartButton.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_om_AddToCartButton.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_om_AddToCartButton.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_om_AddToCartButton.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_om_AddToCartButton.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_om_AddToCartButton.html
}

//interceptor for [module_om.AddToProductWidget]
//interceptor for module_om.AddToProductWidget
//T[362979300] => ProductWidget[190132292]
//can be singleton: FALSE
//parent: AddToProductWidget []
//parent: TypePlugin [@bool get ComposeSubtypes]
class $module_om_AddToProductWidget extends module_om.AddToProductWidget
    implements Pluggable {
  $module_om_AddToProductWidget(parent) {
//T
    this.parent = parent;
  }
  T plugin<T>() {
    return null;
  }

  module_om.AddToCartButton newButton(module_pim.ProductBase product) {
//module_om.AddToCartButton
    return new $module_om_AddToCartButton(product);
  }
}

//interceptor for [module_om.AddToShowProductWidget]
//interceptor for module_om.AddToShowProductWidget
//T[362979300] => ShowProductWidget[326672256]
//can be singleton: FALSE
//parent: AddToShowProductWidget []
//parent: TypePlugin [@bool get ComposeSubtypes]
class $module_om_AddToShowProductWidget extends module_om.AddToShowProductWidget
    implements Pluggable {
  $module_om_AddToShowProductWidget(parent) {
//T
    this.parent = parent;
  }
  T plugin<T>() {
    return null;
  }

  module_om.AddToCartButton newButton(module_pim.ProductBase product) {
//module_om.AddToCartButton
    return new $module_om_AddToCartButton(product);
  }
}

//interceptor for [module_om.OmApi]
//interceptor for module_om.OmApi
//T[534851839] => Order[105961759]
//can be singleton: TRUE
//parent: OmApi []
//parent: CRUDApi [@bool get ComposeSubtypes]
//parent: ApiModule [@bool get ComposeSubtypes]
class $module_om_OmApi extends module_om.OmApi implements Pluggable {
  $module_om_OmApi() {}
  T plugin<T>() {
    return null;
  }

  Map<String, module_core.CRUDApi<module_core.Entity>> get relatedApis =>
      $om.instancesOfmodule_core_CRUDApi_module_core_Entity_;
  String get className => "module_om.OmApi";
  module_core.Site get site => $om.module_core_Site;
  module_om.Order newEntityByType(String className) {
    switch (className) {
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }
}

//no interceptor for [module_om.DeliveryMethod]
//no interceptor for [module_om.PaymentMethod]
//interceptor for [module_om.OrderItem]
//interceptor for module_om.OrderItem
//can be singleton: TRUE
//parent: OrderItem [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_om_OrderItem extends module_om.OrderItem implements Pluggable {
  $module_om_OrderItem() {
//Id
//String
//ProductBase
//int
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_om.OrderItem'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['product'] = "module_pim.ProductBase";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.product = relations['product'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
    {
      target['quantity'] = this.quantity;
    }
//@Field
    {
      target['slug'] = this.slug;
    }
//@Field
//@Field
    {
      target['product'] = this.product != null ? this.product.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
//@Field
    {
      this.quantity =
          target.containsKey('quantity') ? target['quantity'] : this.quantity;
    }
//@Field
//@Field
    {
      if (target['product'] != null) {
        if (target['product'] is String) {
          this.product = relatedFactory("module_pim.ProductBase");
          this.product.id = idFromString(target['product']);
        } else {
          this.product = relatedFactory(target['product']['classes'].last);
          this.product.fromJson(target['product']);
        }
      } else {
        this.product = null;
      }
    }
  }
}

//interceptor for [module_om.Order]
//interceptor for module_om.Order
//can be singleton: TRUE
//parent: Order []
//parent: Entity [@bool get ComposeSubtypes]
class $module_om_Order extends module_om.Order implements Pluggable {
  $module_om_Order() {
//Id
//String
//String
//List<OrderItem>
//DeliveryMethod
//PaymentMethod
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_om.Order'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['deliveryMethod'] = "module_om.DeliveryMethod";
    }
    {
      target['paymentMethod'] = "module_om.PaymentMethod";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.deliveryMethod = relations['deliveryMethod'];
    }
    {
      this.paymentMethod = relations['paymentMethod'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['name'] = this.name;
    }
//@Field
    {
      target['items'] = this.items.map((e) => e.toJson()).toList();
    }
//@Field
    {
      target['deliveryMethod'] =
          this.deliveryMethod != null ? this.deliveryMethod.toJson() : null;
    }
    {
      target['paymentMethod'] =
          this.paymentMethod != null ? this.paymentMethod.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.name = target.containsKey('name') ? target['name'] : this.name;
    }
//@Field
//@Field
    {
      this.items = [];
      if (target['items'] != null) {
        target['items'].forEach((data) {
          var e = relatedFactory(data['classes'].last);
          e.fromJson(data);
          this.items.add(e);
        });
      }
    }
//@Field
    {
      if (target['deliveryMethod'] != null) {
        if (target['deliveryMethod'] is String) {
          this.deliveryMethod = relatedFactory("module_om.DeliveryMethod");
          this.deliveryMethod.id = idFromString(target['deliveryMethod']);
        } else {
          this.deliveryMethod =
              relatedFactory(target['deliveryMethod']['classes'].last);
          this.deliveryMethod.fromJson(target['deliveryMethod']);
        }
      } else {
        this.deliveryMethod = null;
      }
    }
    {
      if (target['paymentMethod'] != null) {
        if (target['paymentMethod'] is String) {
          this.paymentMethod = relatedFactory("module_om.PaymentMethod");
          this.paymentMethod.id = idFromString(target['paymentMethod']);
        } else {
          this.paymentMethod =
              relatedFactory(target['paymentMethod']['classes'].last);
          this.paymentMethod.fromJson(target['paymentMethod']);
        }
      } else {
        this.paymentMethod = null;
      }
    }
  }

  module_om.OrderItem createOrderItem() {
//module_om.OrderItem
    return new $module_om_OrderItem();
  }
}

class $ObjectManager {
  $module_core_Error404Widget _module_core_Error404Widget;
  $module_core_Error404Widget get module_core_Error404Widget {
    if (_module_core_Error404Widget == null) {
      _module_core_Error404Widget = new $module_core_Error404Widget();
    }
    return _module_core_Error404Widget;
  }

  $module_core_ListWidget _module_core_ListWidget;
  $module_core_ListWidget get module_core_ListWidget {
    if (_module_core_ListWidget == null) {
      _module_core_ListWidget = new $module_core_ListWidget();
    }
    return _module_core_ListWidget;
  }

  $module_core_Site _module_core_Site;
  $module_core_Site get module_core_Site {
    if (_module_core_Site == null) {
      _module_core_Site = new $module_core_Site();
    }
    return _module_core_Site;
  }

  $module_core_Application _module_core_Application;
  $module_core_Application get module_core_Application {
    if (_module_core_Application == null) {
      _module_core_Application = new $module_core_Application();
    }
    return _module_core_Application;
  }

  $module_core_Layout _module_core_Layout;
  $module_core_Layout get module_core_Layout {
    if (_module_core_Layout == null) {
      _module_core_Layout = new $module_core_Layout();
    }
    return _module_core_Layout;
  }

  $module_core_Router _module_core_Router;
  $module_core_Router get module_core_Router {
    if (_module_core_Router == null) {
      _module_core_Router = new $module_core_Router();
    }
    return _module_core_Router;
  }

  $module_core_NavbarLink _module_core_NavbarLink;
  $module_core_NavbarLink get module_core_NavbarLink {
    if (_module_core_NavbarLink == null) {
      _module_core_NavbarLink = new $module_core_NavbarLink();
    }
    return _module_core_NavbarLink;
  }

  $module_core_NavbarDropdown _module_core_NavbarDropdown;
  $module_core_NavbarDropdown get module_core_NavbarDropdown {
    if (_module_core_NavbarDropdown == null) {
      _module_core_NavbarDropdown = new $module_core_NavbarDropdown();
    }
    return _module_core_NavbarDropdown;
  }

  $module_core_App _module_core_App;
  $module_core_App get module_core_App {
    if (_module_core_App == null) {
      _module_core_App = new $module_core_App();
    }
    return _module_core_App;
  }

  $module_core_User _module_core_User;
  $module_core_User get module_core_User {
    if (_module_core_User == null) {
      _module_core_User = new $module_core_User();
    }
    return _module_core_User;
  }

  $module_core_Image _module_core_Image;
  $module_core_Image get module_core_Image {
    if (_module_core_Image == null) {
      _module_core_Image = new $module_core_Image();
    }
    return _module_core_Image;
  }

  $module_pim_PimApi _module_pim_PimApi;
  $module_pim_PimApi get module_pim_PimApi {
    if (_module_pim_PimApi == null) {
      _module_pim_PimApi = new $module_pim_PimApi();
    }
    return _module_pim_PimApi;
  }

  $module_pim_SimpleProduct _module_pim_SimpleProduct;
  $module_pim_SimpleProduct get module_pim_SimpleProduct {
    if (_module_pim_SimpleProduct == null) {
      _module_pim_SimpleProduct = new $module_pim_SimpleProduct();
    }
    return _module_pim_SimpleProduct;
  }

  $module_snippets_CmsPagesApi _module_snippets_CmsPagesApi;
  $module_snippets_CmsPagesApi get module_snippets_CmsPagesApi {
    if (_module_snippets_CmsPagesApi == null) {
      _module_snippets_CmsPagesApi = new $module_snippets_CmsPagesApi();
    }
    return _module_snippets_CmsPagesApi;
  }

  $module_snippets_CmsWidgetConfig _module_snippets_CmsWidgetConfig;
  $module_snippets_CmsWidgetConfig get module_snippets_CmsWidgetConfig {
    if (_module_snippets_CmsWidgetConfig == null) {
      _module_snippets_CmsWidgetConfig = new $module_snippets_CmsWidgetConfig();
    }
    return _module_snippets_CmsWidgetConfig;
  }

  $module_snippets_HeadingConfig _module_snippets_HeadingConfig;
  $module_snippets_HeadingConfig get module_snippets_HeadingConfig {
    if (_module_snippets_HeadingConfig == null) {
      _module_snippets_HeadingConfig = new $module_snippets_HeadingConfig();
    }
    return _module_snippets_HeadingConfig;
  }

  $module_snippets_ImageConfig _module_snippets_ImageConfig;
  $module_snippets_ImageConfig get module_snippets_ImageConfig {
    if (_module_snippets_ImageConfig == null) {
      _module_snippets_ImageConfig = new $module_snippets_ImageConfig();
    }
    return _module_snippets_ImageConfig;
  }

  $module_snippets_HtmlConfig _module_snippets_HtmlConfig;
  $module_snippets_HtmlConfig get module_snippets_HtmlConfig {
    if (_module_snippets_HtmlConfig == null) {
      _module_snippets_HtmlConfig = new $module_snippets_HtmlConfig();
    }
    return _module_snippets_HtmlConfig;
  }

  $module_snippets_ParagraphConfig _module_snippets_ParagraphConfig;
  $module_snippets_ParagraphConfig get module_snippets_ParagraphConfig {
    if (_module_snippets_ParagraphConfig == null) {
      _module_snippets_ParagraphConfig = new $module_snippets_ParagraphConfig();
    }
    return _module_snippets_ParagraphConfig;
  }

  $module_snippets_Page _module_snippets_Page;
  $module_snippets_Page get module_snippets_Page {
    if (_module_snippets_Page == null) {
      _module_snippets_Page = new $module_snippets_Page();
    }
    return _module_snippets_Page;
  }

  $module_om_ShoppingCart _module_om_ShoppingCart;
  $module_om_ShoppingCart get module_om_ShoppingCart {
    if (_module_om_ShoppingCart == null) {
      _module_om_ShoppingCart = new $module_om_ShoppingCart();
    }
    return _module_om_ShoppingCart;
  }

  $module_om_PaymentSuccessWidget _module_om_PaymentSuccessWidget;
  $module_om_PaymentSuccessWidget get module_om_PaymentSuccessWidget {
    if (_module_om_PaymentSuccessWidget == null) {
      _module_om_PaymentSuccessWidget = new $module_om_PaymentSuccessWidget();
    }
    return _module_om_PaymentSuccessWidget;
  }

  $module_om_CheckoutWidget _module_om_CheckoutWidget;
  $module_om_CheckoutWidget get module_om_CheckoutWidget {
    if (_module_om_CheckoutWidget == null) {
      _module_om_CheckoutWidget = new $module_om_CheckoutWidget();
    }
    return _module_om_CheckoutWidget;
  }

  $module_om_CartWidget _module_om_CartWidget;
  $module_om_CartWidget get module_om_CartWidget {
    if (_module_om_CartWidget == null) {
      _module_om_CartWidget = new $module_om_CartWidget();
    }
    return _module_om_CartWidget;
  }

  $module_om_EmptyCartWidget _module_om_EmptyCartWidget;
  $module_om_EmptyCartWidget get module_om_EmptyCartWidget {
    if (_module_om_EmptyCartWidget == null) {
      _module_om_EmptyCartWidget = new $module_om_EmptyCartWidget();
    }
    return _module_om_EmptyCartWidget;
  }

  $module_om_OmApi _module_om_OmApi;
  $module_om_OmApi get module_om_OmApi {
    if (_module_om_OmApi == null) {
      _module_om_OmApi = new $module_om_OmApi();
    }
    return _module_om_OmApi;
  }

  $module_om_OrderItem _module_om_OrderItem;
  $module_om_OrderItem get module_om_OrderItem {
    if (_module_om_OrderItem == null) {
      _module_om_OrderItem = new $module_om_OrderItem();
    }
    return _module_om_OrderItem;
  }

  $module_om_Order _module_om_Order;
  $module_om_Order get module_om_Order {
    if (_module_om_Order == null) {
      _module_om_Order = new $module_om_Order();
    }
    return _module_om_Order;
  }

  Map<String, module_core.CRUDApi>
      get instancesOfmodule_core_CRUDApi_module_core_Entity_ {
    return {
      "module_pim.PimApi": new $module_pim_PimApi(),
      "module_snippets.CmsPagesApi": new $module_snippets_CmsPagesApi(),
      "module_om.OmApi": new $module_om_OmApi(),
    };
  }

  Map<String, module_om.PaymentMethod> get instancesOfmodule_om_PaymentMethod {
    return {};
  }

  Map<String, module_om.DeliveryMethod>
      get instancesOfmodule_om_DeliveryMethod {
    return {};
  }
}

$ObjectManager $om = new $ObjectManager();
//generated in 168ms
