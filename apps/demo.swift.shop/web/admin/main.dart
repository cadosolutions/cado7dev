import 'package:swift_composer/swift_composer.dart';

import 'package:module_core/admin.dart' as module_core;
import 'package:module_pim/admin.dart' as module_pim;
import 'package:module_cms/admin.dart' as module_snippets;
import 'package:module_om/admin.dart' as module_om;

part 'main.c.dart';

void main() {
  $om.module_core_Application.run();
}
