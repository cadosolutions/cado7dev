// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'main.dart';

// **************************************************************************
// SwiftGenerator
// **************************************************************************

//no interceptor for [module_core.SiteAction]
//interceptor for [module_core.Error404Action]
//interceptor for module_core.Error404Action
//can be singleton: TRUE
//parent: Error404Action [@bool get Compose]
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $module_core_Error404Action extends module_core.Error404Action {
  $module_core_Error404Action() {
//List
//create
//Error404Widget
//create
    this.index = new $module_core_Error404Widget();
  }
  module_core.User get user => $om.module_core_User;
  module_core.Router get router => $om.module_core_Router;
  module_core.Application get app => $om.module_core_Application;
  String get className => "Error404Action";
}

//interceptor for [module_core.Error404Widget]
//interceptor for module_core.Error404Widget
//can be singleton: TRUE
//parent: Error404Widget []
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_Error404Widget extends module_core.Error404Widget {
  $module_core_Error404Widget() {
//Router
//dynamic
  }
  void initializeSlots() {}
}

//no interceptor for [module_core.SiteMenuItem]
//no interceptor for [module_core.SiteShowAction]
//no interceptor for [module_core.SiteListAction]
//interceptor for [module_core.ListWidget]
//interceptor for module_core.ListWidget
//can be singleton: TRUE
//parent: ListWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_ListWidget extends module_core.ListWidget {
  $module_core_ListWidget() {
//Router
//dynamic
//Slot
//create
    this.items = new module_core.Slot();
  }
  void initializeSlots() {
    {
      print('items');
      this.items.container = this;
      this.items.element = element.querySelector('#' + 'items');
      this.items.element.classes.add('slot');
    }
  }
}

//no interceptor for [module_core.ListItemWidget]
//no interceptor for [module_core.ShowItemWidget]
//no interceptor for [module_core.CompileFieldsAnnotatedWith]
//no interceptor for [module_core.TypePlugin]
//no interceptor for [module_core.PossibleTypoException]
//no interceptor for [module_core.Id]
//interceptor for [module_core.Site]
//interceptor for module_core.Site
//can be singleton: TRUE
//parent: Site [@bool get Compose]
class $module_core_Site extends module_core.Site {
  $module_core_Site() {}
  String get name => "Blank App";
  String get domain => null;
  module_core.Id get id => new module_core.Id.fromString("HY9U");
}

//interceptor for [module_core.Application]
//interceptor for module_core.Application
//can be singleton: TRUE
//parent: Application [@bool get Compose]
class $module_core_Application extends module_core.Application {
  $module_core_Application() {}
  module_core.Router get router => $om.module_core_Router;
  module_core.Site get site => $om.module_core_Site;
  module_core.Layout get layout => $om.module_core_Layout;
}

//interceptor for [module_core.Layout]
//interceptor for module_core.Layout
//can be singleton: TRUE
//parent: Layout [@bool get Compose]
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_Layout extends module_core.Layout {
  $module_core_Layout() {
//Router
//dynamic
//Slot
//create
    this.navbar = new module_core.Slot();
//Slot
//create
    this.body = new module_core.Slot();
  }
  module_core.Site get site => $om.module_core_Site;
  void initializeSlots() {
    {
      print('navbar');
      this.navbar.container = this;
      this.navbar.element = element.querySelector('#' + 'navbar');
      this.navbar.element.classes.add('slot');
    }
    {
      print('body');
      this.body.container = this;
      this.body.element = element.querySelector('#' + 'body');
      this.body.element.classes.add('slot');
    }
  }
}

//no interceptor for [module_core.ApiModule]
//no interceptor for [module_core.CRUDApi]
//interceptor for [module_core.User]
//interceptor for module_core.User
//can be singleton: TRUE
//parent: User [@bool get Compose]
class $module_core_User extends module_core.User {
  $module_core_User() {
//String
  }
}

//no interceptor for [module_core.Action]
//interceptor for [module_core.Router]
//interceptor for module_core.Router
//can be singleton: TRUE
//parent: Router [@bool get Compose]
class $module_core_Router extends module_core.Router {
  $module_core_Router() {
//Map
//Action
  }
  Map<String, module_core.Action> get actions =>
      $om.instancesOfmodule_core_Action;
  String get indexUrl => "";
}

//no interceptor for [module_core.Slot]
//interceptor for [module_core.NavbarLink]
//interceptor for module_core.NavbarLink
//can be singleton: TRUE
//parent: NavbarLink []
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_NavbarLink extends module_core.NavbarLink {
  $module_core_NavbarLink() {
//Router
//dynamic
//String
//String
  }
  void initializeSlots() {}
}

//interceptor for [module_core.NavbarDropdown]
//interceptor for module_core.NavbarDropdown
//can be singleton: TRUE
//parent: NavbarDropdown []
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_NavbarDropdown extends module_core.NavbarDropdown {
  $module_core_NavbarDropdown() {
//Router
//dynamic
//Slot
//create
    this.items = new module_core.Slot();
//String
  }
  void initializeSlots() {
    {
      print('items');
      this.items.container = this;
      this.items.element = element.querySelector('#' + 'items');
      this.items.element.classes.add('slot');
    }
  }
}

//no interceptor for [module_core.SwiftUriPolicy]
//no interceptor for [module_core.Widget]
//no interceptor for [module_core.AsyncWidget]
//interceptor for [module_core.TextWidget]
//interceptor for module_core.TextWidget
//can be singleton: FALSE
//parent: TextWidget [@bool get Compose]
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_TextWidget extends module_core.TextWidget {
  $module_core_TextWidget(text) {
//Router
//dynamic
//String
    this.text = text;
  }
  void initializeSlots() {}
}

//no interceptor for [module_core.Entity]
//interceptor for [module_core.AppEntity]
//interceptor for module_core.AppEntity
//can be singleton: TRUE
//parent: AppEntity []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_AppEntity extends module_core.AppEntity {
  $module_core_AppEntity() {
//Id
//List
//AppEntity
//UserEntity
//String
//String
  }
  void fieldsToJson(Map target) {}
  void fieldsFromJson(Map target) {}
}

//interceptor for [module_core.UserEntity]
//interceptor for module_core.UserEntity
//can be singleton: TRUE
//parent: UserEntity []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_UserEntity extends module_core.UserEntity {
  $module_core_UserEntity() {
//Id
//List
//String
  }
  void fieldsToJson(Map target) {}
  void fieldsFromJson(Map target) {}
}

class $ObjectManager {
  $module_core_Error404Action _module_core_Error404Action;
  $module_core_Error404Action get module_core_Error404Action {
    if (_module_core_Error404Action == null) {
      _module_core_Error404Action = new $module_core_Error404Action();
    }
    return _module_core_Error404Action;
  }

  $module_core_Error404Widget _module_core_Error404Widget;
  $module_core_Error404Widget get module_core_Error404Widget {
    if (_module_core_Error404Widget == null) {
      _module_core_Error404Widget = new $module_core_Error404Widget();
    }
    return _module_core_Error404Widget;
  }

  $module_core_ListWidget _module_core_ListWidget;
  $module_core_ListWidget get module_core_ListWidget {
    if (_module_core_ListWidget == null) {
      _module_core_ListWidget = new $module_core_ListWidget();
    }
    return _module_core_ListWidget;
  }

  $module_core_Site _module_core_Site;
  $module_core_Site get module_core_Site {
    if (_module_core_Site == null) {
      _module_core_Site = new $module_core_Site();
    }
    return _module_core_Site;
  }

  $module_core_Application _module_core_Application;
  $module_core_Application get module_core_Application {
    if (_module_core_Application == null) {
      _module_core_Application = new $module_core_Application();
    }
    return _module_core_Application;
  }

  $module_core_Layout _module_core_Layout;
  $module_core_Layout get module_core_Layout {
    if (_module_core_Layout == null) {
      _module_core_Layout = new $module_core_Layout();
    }
    return _module_core_Layout;
  }

  $module_core_User _module_core_User;
  $module_core_User get module_core_User {
    if (_module_core_User == null) {
      _module_core_User = new $module_core_User();
    }
    return _module_core_User;
  }

  $module_core_Router _module_core_Router;
  $module_core_Router get module_core_Router {
    if (_module_core_Router == null) {
      _module_core_Router = new $module_core_Router();
    }
    return _module_core_Router;
  }

  $module_core_NavbarLink _module_core_NavbarLink;
  $module_core_NavbarLink get module_core_NavbarLink {
    if (_module_core_NavbarLink == null) {
      _module_core_NavbarLink = new $module_core_NavbarLink();
    }
    return _module_core_NavbarLink;
  }

  $module_core_NavbarDropdown _module_core_NavbarDropdown;
  $module_core_NavbarDropdown get module_core_NavbarDropdown {
    if (_module_core_NavbarDropdown == null) {
      _module_core_NavbarDropdown = new $module_core_NavbarDropdown();
    }
    return _module_core_NavbarDropdown;
  }

  $module_core_AppEntity _module_core_AppEntity;
  $module_core_AppEntity get module_core_AppEntity {
    if (_module_core_AppEntity == null) {
      _module_core_AppEntity = new $module_core_AppEntity();
    }
    return _module_core_AppEntity;
  }

  $module_core_UserEntity _module_core_UserEntity;
  $module_core_UserEntity get module_core_UserEntity {
    if (_module_core_UserEntity == null) {
      _module_core_UserEntity = new $module_core_UserEntity();
    }
    return _module_core_UserEntity;
  }

  Map<String, module_core.Action> get instancesOfmodule_core_Action {
    return {
      "module_core.Error404Action": new $module_core_Error404Action(),
    };
  }
}

$ObjectManager $om = new $ObjectManager();
//generated in 51ms
