#!/bin/bash

#convert kuba/torus_view01_blue.png -crop 600x850+85+600 p01.png
convert kuba/torus_knot_blue_01_low.png -crop 600x850+85+600 p01.png


#convert p01.png -alpha extract -brightness-contrast -10,10 alpha.png
#convert p01.png alpha.png -alpha Off -compose CopyOpacity -composite p01.png

convert kuba/torus_view01_green.png -crop 600x850+85+600 p03.png
convert kuba/torus_view01_pink.png -crop 600x850+85+600 p05.png
convert kuba/torus_view01_red.png -crop 600x850+85+600 p07.png
convert kuba/torus_view01_sand.png -crop 600x850+85+600 p09.png
convert kuba/torus_view01_turquoise.png -crop 600x850+85+600 p11.png
convert kuba/torus_view01_yellow.png -crop 600x850+85+600 p13.png

convert p01.png -flop p08.png
convert p02.png -flop p06.png
convert p03.png -flop p15.png
convert p04.png -flop p17.png
convert p05.png -flop p19.png
convert p06.png -flop p21.png
convert p07.png -flop p22.png



convert kuba/hedra_view_01_blue.png -crop 750x1062+25+475 p02.png
convert kuba/hedra_view_01_green.png -crop 750x1062+25+475 p04.png
convert kuba/hedra_view_01_purple.png -crop 750x1062+25+475 p10.png
convert kuba/hedra_view_01_red.png -crop 750x1062+25+475 p12.png
convert kuba/hedra_view_01_sand.png -crop 750x1062+25+475 p14.png
convert kuba/hedra_view_01_yellow.png -crop 750x1062+25+475 p16.png
convert kuba/hedra_view_turquoise.png -crop 750x1062+25+475 p18.png
convert kuba/hedra_view_01_pink.png -crop 750x1062+25+475 p20.png
