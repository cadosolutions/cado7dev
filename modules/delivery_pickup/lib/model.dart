library module_delivery_pickup;

import 'package:module_core/model.dart';
import 'package:module_om/model.dart';
import 'package:swift_composer/swift_composer.dart';

import 'dart:html';

abstract class PickupDeliveryMethod extends DeliveryMethod {
    String get fullName => "Self Pickup";
    int getDeliveryCost(Order order) {
        return 0;
    }

}
