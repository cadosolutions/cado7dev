#!/bin/bash

source "$(dirname "$0")/lib.sh"

function checkRepo() {
	local path="${1}"
	local remote="${2}"
	echo "$path $remote"
	#[ -d $repo ] || git clone $remote $repo
}

for x in $(libs); do
	checkRepo "libs/$x" "git@bitbucket.org:cadosolutions/$x.git"
done;

for x in $(modules); do
	checkRepo "modules/$x" "git@bitbucket.org:cadosolutions/$x.git"
done;


