// Copyright (c) CadoSolutions. Please see LICENSE file for details.

import "dart:io";
import "dart:convert";
import "dart:async";
import 'package:args/args.dart';

Future main(List<String> args) async {

  var parser = new ArgParser();
  parser.addOption('port', abbr:'p', help: 'Port to listen on', defaultsTo: '7201');
  parser.addFlag('help', abbr:'h', help: 'Prints help');

  var arguments = parser.parse(args);
  if (arguments['help']) {
    print(parser.usage);
    return;
  }

  var uploader = new Uploader(int.parse(arguments['port']));
  await uploader.serve();
}

class Uploader {

  int port;

  Uploader(this.port);

  Future serve() async {
    print("starting server...");
    HttpServer server = await HttpServer.bind(InternetAddress.ANY_IP_V4, port);


    server.listen((HttpRequest request) async {
      int start = new DateTime.now().millisecondsSinceEpoch;
      print("${request.method} ${request.uri} ${request.response.statusCode} [${new DateTime.now().millisecondsSinceEpoch - start}ms]");
      /*try {
        //await dataRead
        //await action.data();
        //request.response.headers.set('', action.contentType);
        //String body = action.render();
        //request.response.write(body);
      } catch (error, stackTrace) {
          request.response.statusCode = 500;
          request.response.headers.contentType = ContentType.HTML;
          request.response.write("<h1>500 ERROR</h1>");
          request.response.write("<pre>${new HtmlEscape().convert(error.toString())}</pre>");
          request.response.write("<pre>${new HtmlEscape().convert(stackTrace.toString())}</pre>");
      }*/
      //print(await request.toList());
      /* = () {
        var data = request.inputStream.read();
        var content = new String.fromCharCodes(data);
        print(content); // print the file content.
      };*/

      final file = new File('request.png');
      await request.listen((list)=>file.writeAsBytes(list));

      request.response.headers.set('Access-Control-Allow-Origin', '*');
      request.response.headers.set('Access-Control-Allow-Headers', 'X-Progress-Handle, Content-Type');
      request.response.headers.contentType = ContentType.JSON;
      request.response.write('ddd');
      request.response.close();
      print("${request.method} ${request.uri} ${request.response.statusCode} [${new DateTime.now().millisecondsSinceEpoch - start}ms]");
    });

    print("listening on port $port" );
  }
}
