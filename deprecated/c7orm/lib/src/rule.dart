// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.

part of c7orm;

abstract class Rule<T> {

    const Rule();
    void validate(Validation validator, T value);

}
