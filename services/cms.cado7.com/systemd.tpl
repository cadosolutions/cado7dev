[Unit]
Description=$DOMAIN service
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=1
User=fsw
WorkingDirectory=/home/capps/$DOMAIN
ExecStart=/usr/lib/dart/bin/dartaotruntime /home/capps/$DOMAIN/server.aot

[Install]
WantedBy=multi-user.target
