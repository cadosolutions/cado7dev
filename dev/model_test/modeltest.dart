class Dupa {
}

class PocoModel {

	String _firstname = '';
	String _lastname = '';
	String get fullname => _firstname + ' ' + _lastname;

	@Slot
	String testFunction(String arg, int x) {
		return _firstname + (arg * x) + _lastname;
	}
}

@Plugin
class PocoPlugin {

	PocoModel model;
	PocoPlugin(this.model);

	@Around(PocoModel.testFunction)
	String pluginTestFunction(String arg, int x, String next(String, int)) {
		arg = "__";
		return next(arg, x + 1);
	}

}


class $PocoModel extends PocoModel {

	PocoPlugin pocoPlugin;

	$PocoModel() {
		pocoPlugin = new PocoPlugin(this);
	}

	String _firstname;
	int counter = 0;
	String get firstname => _firstname;
	void set firstname(String value) {
		_firstname = value;
		counter ++;
	}

	String get fullname => 'TEST';

	String testFunction(String arg, int x) {
		return pocoPlugin.pluginTestFunction(arg, x, super.testFunction);
	}
}

void main() {
	print('asd');
	var test = new $PocoModel();
	test._lastname="YYYY";
	test.firstname="XXXX";
	print(test.firstname);
	print(test.counter);
	print(test.fullname);
	print(test.testFunction('arg1', 5));


}
