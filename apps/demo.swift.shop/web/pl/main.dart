import 'package:swift_composer/swift_composer.dart';

import 'package:module_core/site.dart' as module_core;
import 'package:module_pim/site.dart' as module_pim;
import 'package:module_cms/site.dart' as module_snippets;
import 'package:module_om/site.dart' as module_om;

part 'main.c.dart';

void main() {
  $om.module_core_Application.run();
}
