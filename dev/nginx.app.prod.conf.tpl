server {
	listen 80;
	server_name $DOMAIN www.$DOMAIN;
	return 301 https://$DOMAIN$request_uri;
}

server {
	server_name $DOMAIN;

	listen 443 ssl http2;
  ssl on;
  ssl_certificate /etc/letsencrypt/live/$DOMAIN/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/$DOMAIN/privkey.pem;
	ssl_ciphers EECDH+CHACHA20:EECDH+AES128:RSA+AES128:EECDH+AES256:RSA+AES256:EECDH+3DES:RSA+3DES:!MD5;

	root /home/capps/$DOMAIN;
	index index.html;

  error_page 500 /500.html;

	location ~* \.(?:ico|css|js|gif|jpe?g|png)$ {
	    expires max;
	    add_header Vary Accept-Encoding;
	}

	location / {
		http2_push /style.css;
		http2_push /main.dart.js;
		try_files $uri /index.html;
	}


}
