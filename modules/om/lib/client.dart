library mocule_om;
import 'package:module_core/client.dart';

import 'model.dart';
export 'model.dart';

abstract class OmApi extends CRUDApi<Order> {
    String get serviceCode => 'api';
    String get moduleCode => 'om';

    Future<bool> placeOrder(Order order) async {
      var ret = await call('PlaceOrder', data: {'order':order.toJson()});
      order.id = new Id.fromString(ret['id']);
      return true;
    }
}
