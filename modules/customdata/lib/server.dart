
import 'package:c7server/c7server.dart';
import 'dart:convert';
import 'dart:io';

abstract class CustomDataController extends HttpAction {

  dynamic handleRequest() async
  {
      List<String> path = request.uri.pathSegments.sublist(2);
      print((await app).id.toString());
      var file = new File(server.workdir + '/' + (await app).id.toString() + '/' + path.join('/'));
      if (file.existsSync()) {
        request.response.statusCode = 200;
        //cast by ext
        request.response.headers.contentType = ContentType.JSON;
        await file.openRead().pipe(request.response);

      } else {
        print(file.path);
        request.response.statusCode = 404;
        request.response.headers.contentType = ContentType.TEXT;
        request.response.write("custom data not found");
      }
  }
}
