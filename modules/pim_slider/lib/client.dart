import 'package:swift_composer/swift_composer.dart';

import 'package:module_core/client.dart';
import 'package:module_core/site.dart';
import 'package:module_cms/client.dart';
import 'package:module_pim/client.dart';

import 'dart:html';
import 'dart:async';

import 'model.dart';
export 'model.dart';

abstract class ProductsSliderWidget extends CmsWidget<ProductsSliderConfig> {

  @Create
  Slot slider;

  @Inject
  CRUDApi<ProductBase> get api;

  @Factory
  ListItemWidget<ProductBase> createListWidget(ProductBase item);

  String get html => """
  <div class="container">
      <div class="slider">
        <h2>${config.title}</h2>
        <div class="s-wrap">
          <div data-slot="slider" class="s-items s-fixed"></div>
        </div>
        <div class="s-move s-left"><span class="icon-arrow-left"></span></div>
        <div class="s-move s-right"><span class="icon-arrow-right"></span></div>
      </div>
  </div>
  """;

  void move(bool reversed) {

    slider.element.classes.toggle('s-reversing', reversed);
    ref = reversed ? prevRef : nextRef;


    ref.parent.children.forEach((c){ c.classes.remove('s-wrapped');});
    var r = ref;
    while (r != null){
      r.classes.add('s-wrapped');
      r = r.nextElementSibling;
    }

    //new_seat = next(new_seat).css('order', i) for i in [2..seats.length]
    slider.element.classes.remove('s-fixed');

    new Timer(const Duration(milliseconds: 30), (){
      slider.element.classes.add('s-fixed');
    });
  }

  void left() {
    move(true);
  }

  void right() {
    move(false);
  }

  Element ref;

  Element get nextRef => ref.nextElementSibling != null ? ref.nextElementSibling : ref.parent.children.first;
  Element get prevRef => ref.previousElementSibling != null ? ref.previousElementSibling : ref.parent.children.last;

  void init() {
    element.querySelector('.s-left').onClick.listen((e){left();});
    element.querySelector('.s-right').onClick.listen((e){right();});

    api.search('test').then((ps){
      ps.sublist(0,6).forEach((p){
        var w = createListWidget(p);
        slider.append(w);
      });
      ref = slider.widgets.last.element;
      ref.classes.add('s-wrapped');
    });
  }
}
