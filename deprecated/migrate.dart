#!/usr/bin/env dart
// Copyright (c) CadoSolutions. Please see LICENSE file for details.

import "package:api/api.dart";
import "dart:async";

part 'migrate.c.dart';

main () async {

      //TODO
      print("DEV MODE DB CHECK");
        var queries = await Migrator.quessMigrationQueries();
        for (var sql in queries){
          print("running query:\n$sql\n");
          await sites.db.query(sql);
          print("DONE");
      }
}
