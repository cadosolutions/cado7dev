import 'package:swift_ids/swift_ids.dart';
import 'package:swift_composer/swift_composer.dart';
import 'package:module_core/model.dart';

@Compose
abstract class CmsWidgetConfig extends Entity {


}
abstract class HeadingConfig extends CmsWidgetConfig {
  @Field
  String title;
  @Field
  String subtitle;
  @Field
  int level = 1;
}

abstract class ImageConfig extends CmsWidgetConfig {
  @Field
  Image image;
  @Field
  String caption;
}

abstract class HtmlConfig extends CmsWidgetConfig {
  @Field
  String html;

}

abstract class ParagraphConfig extends CmsWidgetConfig {
  @Field
  String title;
  @Field
  String body;
}


abstract class Page extends Entity {

  @Field
  App app;

  @Field
  String title;

  /* @Field
  CmsWidgetConfig headerWidget; */

  @Field
  List<CmsWidgetConfig> widgets;

}
