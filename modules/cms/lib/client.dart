import 'package:module_core/site.dart';
import 'model.dart';
export 'model.dart';

@ComposeSubtypes
abstract class CmsWidget<CT extends CmsWidgetConfig> extends Widget {
  @Require
  CT config;

}

abstract class HtmlWidget extends CmsWidget<HtmlConfig> {

  String get html => """
  <div>
    ${config.html}
  </div>
  """;

}

abstract class ImageWidget extends CmsWidget<ImageConfig> {

  String get html => """
  <div>
    <img class="image" loading="lazy" src="${config.image?.url}" alt="test" />
  </div>
  """;

}

abstract class ParagraphWidget extends CmsWidget<ParagraphConfig> {

  String get html => """
  <div>
      <div class="container">
          <h2>${config.title}</h2>
          <p>
            ${config.body}
          </p>
      </div>
  </div>
  """;
}

abstract class HeadingWidget extends CmsWidget<HeadingConfig> {

  String get html => """
  <div>
    <div class='container'>
      <h${config.level}>
        ${config.title}
      </h${config.level}>
      <h${config.level + 1}>
        ${config.subtitle}
      </h${config.level + 1}>
    </div>
  </div>
  """;
}

abstract class CmsPagesApi extends CRUDApi<Page> {
    String get serviceCode => 'cms';
    String get moduleCode => 'cms';
}
