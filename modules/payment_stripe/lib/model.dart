library module_payment_stripe;

import 'package:module_core/model.dart';
import 'package:module_om/model.dart';
import 'package:swift_composer/swift_composer.dart';

import 'dart:html';

abstract class StripePaymentMethod extends PaymentMethod {

    String get fullName => "Stripe";

    String get paymentActionUrl => 'https://sandbox.przelewy24.pl/trnDirect';

    Map<String, String> getPaymentActionFields(Order order, String returnUrl) {
      return {
        'null': 'null'
      };
    }
}
