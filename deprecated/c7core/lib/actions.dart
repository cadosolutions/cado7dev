// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.

part of c7core;

abstract class Action {
  User user;
  Site site;
}

abstract class HttpAction {
  HttpRequest request;
  @Liquid
  User user;
  @Liquid
  Site site;
  ContentType get contentType;

  Map<String, String> post = {};

  Future data() {
    return new Future(() => "");
  }

  String render() {
    return "PLEASE IMPLEMENT THIS";
  }
}

abstract class ContentAction {}

String staticUrl = "http://localhost:8080/";

class HtmlStyleSheet {
  String path;
  List<String> media;
  String type = "text/x-scss";
  String charset = "utf-8";
  HtmlStyleSheet(this.path, [this.media = const ['projection', 'screen']]);
  String toString() {
    return '<link rel="stylesheet" href="${staticUrl}${this.path}" type="${this.type}" charset="${this.charset}" media="${this.media.join(', ')}"/>';
  }
}

class HtmlScript {
  String path;
  HtmlScript(this.path);
  String toString() {
    if (this.path.endsWith('.dart'))
      return '<script type="application/dart" src="${staticUrl}${this.path}"></script>';
    else
      return '<script src="${staticUrl}${this.path}"></script>';
  }
}

abstract class Html5Action extends HttpAction {
  ContentType get contentType => ContentType.HTML;
  String lang;
  String seoDescription;
  String seoTitle;
  String seoKeywords;

  Message message;

  String get sectionBody => "";
  String get sectionMessages => (message != null)
      ? '<div class="alert alert-${message.type} fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>${message.body}</div>'
      : '';

  List<HtmlStyleSheet> styleSheets = [];
  List<HtmlScript> scripts = [];

  ITemplate get template {
      //site config logic ???
      var template = new Template();
      template.body = "CHUJ";
      return template;
  }

  String render() {
    var renderer = new Renderer(this.template, Html5Action);
    return renderer.render(this);

    return """
      <!DOCTYPE html>
      <html class="no-js" lang="${lang}">
          <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width">
            <meta name="description" content="${seoDescription}">
            <meta name="keywords" content="${seoKeywords}">
            <title>${seoTitle}</title>

            <meta name="robots" content="index,follow">
            <meta name="GOOGLEBOT" content="index,follow">
            <meta name="revisit-after" content="14 days">

            ${styleSheets.join()}

            <link rel="shortcut icon" href="${staticUrl}favicon.png" />
          </head>
          <body>
            <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or
            <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a>to improve your experience.</p>
            <![endif]-->
            ${sectionBody}

            ${scripts.join()}
          </body>
        </html>


      </html>
      """;
  }
}

class DevIndexAction extends Html5Action {
  ContentType get contentType => ContentType.HTML;
  Collection<Site> sites;
  Future data() {
    sites.all();

    return await;
  }

  ITemplate get template => new FileTemplate('devIndex');

  String get sectionBody {
    return """
    CADO7 development server.

    <button>ADD NEW SITE</button>
    """;
  }
}

abstract class BackendAction extends Html5Action {
  Admin admin;

  //admin panel action
}

abstract class FrontendAction extends Html5Action {
  String pageTitle;

  FrontendAction() {
    styleSheets
        .add(new HtmlStyleSheet('packages/bootjack/css/bootstrap.min.css'));
    styleSheets.add(new HtmlStyleSheet('assets/c7_core/cado7.css'));
    scripts.add(new HtmlScript('c7_net.dart'));
    scripts.add(new HtmlScript('packages/browser/dart.js'));
  }

  String get sectionHeader => "";
  String get sectionContent => "";
  String get sectionAbove => "";
  String get sectionBelow => "";
  String get sectionSidebar => "";

  String get sectionBody {
    return """
    <div class="container">
      <div class="row clearfix">
          $sectionHeader
      </div>
      <div class="row clearfix">
        <div class="col-md-9 column">
          $sectionAbove
          $sectionContent
          $sectionBelow
        </div>
        <div class="col-md-3 column">
          $sectionSidebar
        </div>
      </div>
    </div>
    """;
  }
}

class ActionFoo extends FrontendAction {
  @UrlParam
  int a;
  @UrlParam
  String b;
  @UrlParam
  User c;

  String get sectionContent => "";
}

class ActionIndex extends FrontendAction {
  String get sectionContent {
    return "";
  }
}

class ActionBar extends FrontendAction {
  @UrlParam
  int a;
  @UrlParam
  String b;

  String seoTitle = 'TEST';

  String get sectionContent {
    return "FOO ${a.toString()} ${b}";
  }
}
