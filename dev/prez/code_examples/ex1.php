<?php

/**
 * Return attribute instance by code or false
 *
 * @param string $attributeCode
 * @return \Magento\Eav\Model\Entity\Attribute|bool
 */
public function getAttribute($attributeCode)
{
    $attributes = $this->getAttributes();
    if (isset($attributes[$attributeCode])) {
        return $attributes[$attributeCode];
    }
    return false;
}

$reasonOtherAttribute = $itemForm->setFormCode('default')->getAttribute('reason_other');
$label = $reasonOtherAttribute->getStoreLabel();

