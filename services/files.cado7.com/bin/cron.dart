// Copyright (c) CadoSolutions. Please see LICENSE file for details.

import "dart:async";
import "dart:io";
import 'package:args/args.dart';
import "package:dart_amqp/dart_amqp.dart";
import 'dart:convert';

Future main(List<String> args) async {

  var parser = new ArgParser();
  parser.addFlag('help', abbr:'h', help: 'Prints help');
  parser.addOption('dir', abbr:'d', help: 'storage directory', defaultsTo: 'files');

  var arguments = parser.parse(args);
  if (arguments['help']) {
    print(parser.usage);
    return;
  }
  var queue = new QueueProcessor(new Directory(arguments['dir']));
  await queue.listen();
}


class QueueProcessor {
  Directory dir;

  QueueProcessor(this.dir);

  String getExt(String fileName) {
    int lastDot = fileName.lastIndexOf('.', fileName.length - 1);
    return (lastDot != -1) ? fileName.substring(lastDot + 1) : null;
  }

  Future process(String path) async {
    print("processing $path");
    String fullPath = dir.path + path;
    String ext = getExt(path);
    if (ext == null) {
      return;
    }
    switch(ext) {
      case 'jpg':
        print("optimising JPG");
        Process.runSync('mogrify', [
          '-strip',
          '-interlace', 'Plane',
          '-gaussian-blur', '0.05',
          '-quality', '85%',
          fullPath
        ]);
        break;
      case 'png':
        print("optimising PNG");
        Process.runSync('mogrify', [
          '-strip',
          fullPath
        ]);
        break;
      default:
        break;
    }

    String width = Process.runSync('convert',
      [fullPath, '-print', '%w', '/dev/null']
    ).stdout;

    String height = Process.runSync('convert',
      [fullPath, '-print', '%h', '/dev/null']
    ).stdout;

    //generate webm thumbnail
    String webpPath = fullPath.replaceRange(fullPath.length-ext.length, fullPath.length, 'webp');
    print("generating $webpPath");
    Process.runSync('convert',
      [fullPath, '-resize', '24x24', webpPath]
    );
    String base64 = Process.runSync('base64', ['-w', '0', webpPath]).stdout;
    new File(webpPath).writeAsStringSync("<img width=\"$width\" height=\"$height\" src=\"data:image/webp;base64,$base64\"/>");

    String svgPath = fullPath.replaceRange(fullPath.length-ext.length, fullPath.length, 'svg');
    print("generating $svgPath");
    Process.runSync(
      '/home/fsw/go/bin/primitive',
      [
        '-i', fullPath,
        '-n', '10',
        '-m', '8',
        '-o', svgPath
      ]
    );
    String svg = (new File(svgPath)).readAsStringSync();
    svg = svg.replaceFirst('>',' style="filter:url(#a)"><defs><filter id="a"><feGaussianBlur stdDeviation="15"/></filter></defs>');
    (new File(svgPath)).writeAsStringSync(svg);
    Process.runSync('svgo', [svgPath]);

    Map info = {
      'width' : int.parse(width),
      'height' : int.parse(height),
      'url' : 'http://files.swift.localhost/' + path,
      'thumb.webp' : new File(webpPath).readAsStringSync(),
      'thumb.svg' : new File(svgPath).readAsStringSync()
    };
    String infoPath = fullPath.replaceRange(fullPath.length-ext.length, fullPath.length, 'json');
    new File(infoPath).writeAsStringSync(JSON.encode(info));
  }

  Future listen() async {
    print("starting queue...");
    Client client = new Client();

    Channel channel = await client.channel(); // auto-connect to localhost:5672 using guest credentials
    Queue queue = await channel.queue('uploaded');

    Consumer consumer = await queue.consume();

    consumer.listen((AmqpMessage message) {
      // Get the payload as a string
      process(message.payloadAsString);

    });
  }

}
