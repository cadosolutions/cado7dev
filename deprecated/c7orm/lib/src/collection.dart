// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.

part of c7orm;


class One<E> {
  //Collection collection;
  int uid;
  //E e;
  //E get entity => e;
  /*set entity(E value) {
    this.e = value;
  }*/

  /*
  E operator = (E x){
    return x;
  }*/

  E get fetch => null;
}

class Many<E> implements Iterable<E> {
  //i extenduje QuerySet !!!
  Many(){

  }

  E operator [](int index){
    return null;
  }

  void operator []=(int index, E value){
    //dodaj!
  }

  int get length => 5;
}

//AdultsOnlyFilter extends Filter<User>

//TODO:
//build

class Collection<T> { //T is a POCO class

  String tableName;
  ClassMirror _mirror;
  FieldController controller;
  ConnectionPool db;
  static Map<Symbol, Collection>  _allCollections;
  static get getAllCollections{
    return _allCollections;
  }

  static Collection getCollection(Symbol symbol) {
    return _allCollections[symbol];
  }

  Map<Symbol, List<FieldController>> keys;

  /*
  static get byType(Type x){
    return _allCollections;
  }*/

  Collection(this.db) {

    _mirror = reflectClass(T);
    if (_allCollections == null)
      _allCollections = new Map<Symbol, Collection>();
    _allCollections[_mirror.qualifiedName] = this;
    tableName = MirrorSystem.getName(_mirror.qualifiedName).replaceAll('.', '_');
    controller = new FieldController.factory(_mirror);
    keys = new Map<Symbol, List<FieldController>>();
    for (var field in controller.allFields) {
      if (field.hasDbColumn) {
        for (var key in field.getKeys){
          if (!keys.containsKey(key.name)) {
            keys[key.name] = new List<FieldController>();
          }
          keys[key.name].add(field);
        }
      }
    }

  }

  Future<T> getByUid(Uid uid) async {
    return getByKey(#PRIMARY_KEY, [uid]);
  }

  Future<T> getByKey(Symbol key, List<dynamic> args) async {
    var q = [];
    if (!keys.containsKey(key)) {
      throw new Exception('Unidentifed key $key');
    }
    for (var field in keys[key]) {
      q.add("`${field.getColumnName}` = ?");
    }
    String query = "select * from ${tableName} WHERE " + q.join(" AND ");
    print(query);
    Results results = await db.prepareExecute(query, args);
    print(results.elementAt(0));

    Row row = await results.single;

    print(row);
    List<dynamic> writeableRow = new List<dynamic>.from(row);
    print(writeableRow);
    return controller.buildFromDb(writeableRow);
  }


  Future<T> getOne(String query, List params) async {
    return new Query<FlatPage>();
  }

  Future<T> getByUidasdasd(Uid uid) async {
    String q = 'select * from ${tableName} WHERE id=?';
    return db.prepareExecute(q, [uid]).then((Results results) {
      //print('EEEEE');
      return results.single.then((Row row){
        //print('RRRRRRRR');
        Map<String,String> zip = {};
        for(int i=0;i<results.fields.length;i++){
          zip[results.fields[i].name] = row[i];
        }
        //print(zip);
        var x = _mirror.newInstance(new Symbol(''), [], {});
        //print('FROM');
        for (Symbol s in _fields.keys) {
          print(s);
          print(_fields[s].fromDbVelues(MirrorSystem.getName(s),zip));
          print(_fields[s].fromDbVelues(MirrorSystem.getName(s),zip).runtimeType);
          x.setField(s, _fields[s].fromDbVelues(MirrorSystem.getName(s),zip));
        }
        //print('TO');
        //Object fromFieldsVelues(String name, zip){return row[name];}
        //x.reflectee.title = 'ASDADS';
        //x.reflectee.body = 'aASDASd';
        //print(x.reflectee.body);
        return x.reflectee;
      });
    }).catchError((error){
      throw new Error404();
    });
  }

  Query<T> all(){
      return new Query<T>(this);

        /* String q = 'select id from ${tableName}';
        return db.prepareExecute(q, []).then((Results results) {
          return results.toList().then((list){
            List<Future<T>> resp = [];
            list.forEach((elem){
              resp.add(getById(elem[0]));
            });
            return Future.wait(resp);
          });
        }); */
  }

  Query<T> filter(Filter filter){
      return all()->filter(filter);
  }

  Future add(T object) async {
    if (true || object.id != Null) { //TODO use PrimaryKey annotation
        String q = 'INSERT INTO ${tableName} SET ';
        List<String> l = new List<String>();
        List<Object> params = new List<Object>();
        controller.toDbRow(object).forEach((name, value){
            l.add(name + ' = ?');
            params.add(value);
        });
        q += l.join(', ');

        print(q);

        var query = await db.prepare(q);

        var result = await query.execute(params);
        print(result);
        return;
        //pool.query(q).then((r)=>print('lol'));
    }
  }

  void update(T object) {

  }

}




class ManyToMany<T1, T2> {

}

/*
class SluggableCollection {

  Future<T> fromUrlParam(String param){

  }

}
class Version<T> {
  T entity;
  DateTime created;

}

class VersionedCollection<T> extends Collection<T> {

  VersionedCollection(db) : super(db);

}*/
