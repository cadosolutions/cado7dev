import 'package:swift_composer/swift_composer.dart';
import 'package:module_core/model.dart';
import 'package:module_cms/model.dart';


abstract class ProductsSliderConfig extends CmsWidgetConfig {
  @Field
  String title;
}
