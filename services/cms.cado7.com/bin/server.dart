#!/usr/bin/env dart
// Copyright (c) CadoSolutions. Please see LICENSE file for details.

import "dart:async";
import "package:c7server/c7server.dart";

import 'package:module_core/server.dart' as module_core;
import 'package:module_cms/server.dart' as module_cms;

part 'server.c.dart';

Future main (List<String> arguments) async {
  await $om.server.serve();
}
