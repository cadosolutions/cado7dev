library module_breadcrumbs;

import 'client.dart';
export 'client.dart';
import 'package:module_core/site.dart';
import 'dart:html';

abstract class ActionPlugin extends TypePlugin<SiteAction> {

    @MethodPlugin
    void afterReady() {
      List<Action> path = [];
      Action a = parent.router.current;
      while (a != null) {
        path.insert(0, a);
        a = a.parent;
      }
      if (path[0].url != '/') {
        path.insert(0, parent.router.actionFromPath('/'));
      }
      parent.layout.plugin<LayoutPlugin>().breadCrumbsWidget.setPath(path);
    }
}

abstract class LayoutPlugin extends TypePlugin<Layout> {
    @Inject
    BreadCrumbs get breadCrumbsWidget;

    @Create
    Slot breadcrumbs;

    @MethodPlugin
    void afterInit() {
      //breadcrumbs.append(breadCrumbsWidget);
    }

}

abstract class BreadCrumbs extends Widget {

  void setPath(List<Action> path) {
    element.children.clear();
    path.forEach((action){
      Element inner;
      if (path.last == action) {
        inner = new SpanElement();
      } else {
        inner = new AnchorElement(href:action.url);
        router.localiseAnchor(inner);
      }
      if (action.url == '/') {
          inner.innerHtml = '<span class="icon-home"></span>';
      } else if (action.title is Future) {
          inner.innerText = '...';
          (action.title as Future).then((s) {inner.innerText = s;});
      } else {
          inner.innerText = action.title;
      }
      element.append(new LIElement()..append(inner));
    });
    if (path.length < 2) {
      element.style.display = 'none';
    } else {
      element.style.display = '';
    }
  }

  String get html => "<ol id='breadcrumbs'></ol>";
}
