#!/bin/bash
set -e
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
cd "${DIR}"
echo "dev env: ${DIR}"

project="${1}"
host="${2}"
mode="${3}"

if [ "${mode}" == "prod" ]; then
  domain="${project}"
  warmup="https://${domain}/"
elif [ "${mode}" == "test" ]; then
  domain="${project}.test.cado7.com"
  warmup="https://swift:shop@${domain}/"
else
  echo "usage: deploy project host mode"
  exit
fi

echo "deploying ${project} to ${domain}"

if test -d "$DIR/apps/${project}"; then
  echo "BUILDING ${project}"
  cd "$DIR/apps/${project}"
  if test -f pubspec.yaml; then
    pub run build_runner build --delete-conflicting-outputs
    webdev build
    rsync -ah --info=progress2 "$DIR/apps/${project}/build/" "${host}:/home/capps/${domain}/" --delete
  else
    rsync -ah --info=progress2 "$DIR/apps/${project}/" "${host}:/home/capps/${domain}/" --delete
  fi
  cat "${DIR}/dev/nginx.app.${mode}.conf.tpl" | DOMAIN="${domain}" envsubst '$DOMAIN' > .tmp.tpl
  if ! diff <(ssh "${host}" cat "/etc/nginx/sites-enabled/$domain") .tmp.tpl; then
    echo "WARNING: nginx config files differ. updating"
    ssh "${host}" "test -f /etc/letsencrypt/renewal/$domain.conf || sudo certbot certonly --nginx -d ${domain}"
    scp .tmp.tpl "${host}:/etc/nginx/sites-enabled/$domain"
    ssh "${host}" "sudo systemctl restart nginx"
  fi
  rm .tmp.tpl
elif test -d "$DIR/services/${project}"; then
  echo "BUILDING ${project}"
  cd "$DIR/services/${project}"
  pub run build_runner build --delete-conflicting-outputs
  dart2native bin/server.dart -o server.bin
  ssh "${host}" "mkdir -p /home/capps/${domain}"

  rsync -ah --info=progress2 server.bin "${host}:/home/capps/${domain}/server.bin" --delete

  port=$(ssh "${host}" cat /home/capps/${domain}/config.json | jq -r '.port')

  echo "REBOOTING"
  cat "systemd.tpl" | DOMAIN="${domain}" PORT="${port}" envsubst '$DOMAIN $PORT' > .tmp.tpl
  if ! diff <(ssh "${host}" cat "/etc/systemd/system/$domain.service") .tmp.tpl; then
    echo "ERROR: systemd config files differ. update, and run"
    ssh "${host}" "sudo systemctl daemon-reload"
  fi
  ssh "${host}" "sudo systemctl restart ${domain}"

  cat "nginx.conf.tpl" | DOMAIN="${domain}" PORT="${port}" envsubst '$DOMAIN $PORT' > .tmp.tpl
  if ! diff <(ssh "${host}" cat "/etc/nginx/sites-enabled/$domain") .tmp.tpl; then
    echo "WARNING: nginx config files differ. updating"
    ssh "${host}" "test -f /etc/letsencrypt/renewal/$domain.conf || sudo certbot certonly --nginx -d ${domain}"
    scp .tmp.tpl "${host}:/etc/nginx/sites-enabled/$domain"
    ssh "${host}" "sudo systemctl restart nginx"
  fi
  rm .tmp.tpl
else
  echo "unknown app/service ${project}"
  exit
fi

wget --server-response --spider "${warmup}"
echo "deployed. go to: ${warmup}"
