// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'main.dart';

// **************************************************************************
// SwiftGenerator
// **************************************************************************

//no interceptor for [AnnotatedWith]
//no interceptor for [Pluggable]
//no interceptor for [TypePlugin]
//interceptor for [module_core.TODODODOD]
//interceptor for module_core.TODODODOD
//can be singleton: TRUE
//parent: TODODODOD [@bool get Compose]
class $module_core_TODODODOD extends module_core.TODODODOD
    implements Pluggable {
  $module_core_TODODODOD() {
//Site
//AdminLayout
//UserForm
//AdminNavbar
  }
  T plugin<T>() {
    return null;
  }
}

//interceptor for [module_core.AdminLayout]
//interceptor for module_core.AdminLayout
//can be singleton: TRUE
//parent: AdminLayout [@bool get Compose]
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_AdminLayout extends module_core.AdminLayout
    implements Pluggable {
  $module_core_AdminLayout() {
//dynamic
//Slot
//create
    this.menu = new module_core.Slot();
//Slot
//create
    this.body = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.AdminLayout";
  void initializeSlots() {
//compiled method
    {
      this.menu.container = this;
      this.menu.element = element.querySelector('[data-slot="' + 'menu' + '"]');
      if (this.menu.element != null) {
        this.menu.element.classes.add('slot');
      } else {
        print('slot ' + 'menu' + ' is missing');
      }
    }
    {
      this.body.container = this;
      this.body.element = element.querySelector('[data-slot="' + 'body' + '"]');
      if (this.body.element != null) {
        this.body.element.classes.add('slot');
      } else {
        print('slot ' + 'body' + ' is missing');
      }
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.menu.widgets.forEach((widget) {
        this.menu.element.append(widget.element);
      });
    }
    {
      this.body.widgets.forEach((widget) {
        this.body.element.append(widget.element);
      });
    }
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_core_AdminLayout.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_core_AdminLayout.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_core_AdminLayout.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_core_AdminLayout.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_core_AdminLayout.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_core_AdminLayout.html
}

//no interceptor for [module_core.AdminMenuItem]
//no interceptor for [module_core.AdminGridColumn]
//no interceptor for [module_core.IdColumn]
//no interceptor for [module_core.ClassColumn]
//no interceptor for [module_core.AdminGridFieldColumn]
//no interceptor for [module_core.InputFor]
//no interceptor for [module_core.StringInput]
//no interceptor for [module_core.PriceInput]
//interceptor for [module_core.AdminAction]
//interceptor for module_core.AdminAction
//can be singleton: FALSE
//parent: AdminAction []
//parent: Action [@bool get ComposeSubtypes]
class $module_core_AdminAction extends module_core.AdminAction
    implements Pluggable {
  $module_core_AdminAction(args) {
//List<String>
    this.args = args;
//User
//Router
//Application
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.AdminAction";
}

//interceptor for [module_core.IndexAction]
//interceptor for module_core.IndexAction
//can be singleton: FALSE
//parent: IndexAction []
//parent: AdminAction []
//parent: Action [@bool get ComposeSubtypes]
class $module_core_IndexAction extends module_core.IndexAction
    implements Pluggable {
  $module_core_IndexAction(args) {
//User
//Router
//Application
//List<String>
    this.args = args;
//Dashboard
//create
    this.dashboard = new $module_core_Dashboard();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.IndexAction";
}

//interceptor for [module_core.Dashboard]
//interceptor for module_core.Dashboard
//can be singleton: TRUE
//parent: Dashboard [@bool get Compose]
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_Dashboard extends module_core.Dashboard
    implements Pluggable {
  $module_core_Dashboard() {
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.Dashboard";
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_core_Dashboard.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_core_Dashboard.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_core_Dashboard.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_core_Dashboard.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_core_Dashboard.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_core_Dashboard.html
}

//interceptor for [module_core.LoginAction]
//interceptor for module_core.LoginAction
//can be singleton: FALSE
//parent: LoginAction []
//parent: Action [@bool get ComposeSubtypes]
class $module_core_LoginAction extends module_core.LoginAction
    implements Pluggable {
  $module_core_LoginAction(args) {
//List<String>
    this.args = args;
//Application
//User
//UserLoginForm
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.LoginAction";
}

//interceptor for [module_core.LogoutAction]
//interceptor for module_core.LogoutAction
//can be singleton: FALSE
//parent: LogoutAction []
//parent: Action [@bool get ComposeSubtypes]
class $module_core_LogoutAction extends module_core.LogoutAction
    implements Pluggable {
  $module_core_LogoutAction(args) {
//List<String>
    this.args = args;
//Router
//User
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.LogoutAction";
}

//interceptor for [module_core.ModulesAction]
//interceptor for module_core.ModulesAction
//can be singleton: FALSE
//parent: ModulesAction [@AdminMenuItem AdminMenuItem(String section, String subsection, String title)]
//parent: AdminAction []
//parent: Action [@bool get ComposeSubtypes]
class $module_core_ModulesAction extends module_core.ModulesAction
    implements Pluggable {
  $module_core_ModulesAction(args) {
//User
//Router
//Application
//List<String>
    this.args = args;
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.ModulesAction";
}

//interceptor for [module_core.SettingsAction]
//interceptor for module_core.SettingsAction
//can be singleton: FALSE
//parent: SettingsAction [@AdminMenuItem AdminMenuItem(String section, String subsection, String title)]
//parent: AdminAction []
//parent: Action [@bool get ComposeSubtypes]
class $module_core_SettingsAction extends module_core.SettingsAction
    implements Pluggable {
  $module_core_SettingsAction(args) {
//User
//Router
//Application
//List<String>
    this.args = args;
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.SettingsAction";
}

//no interceptor for [module_core.AdminGridAction]
//interceptor for [module_core.AdminGridView]
//interceptor for module_core.AdminGridView
//can be singleton: TRUE
//parent: AdminGridView [@bool get Compose]
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_AdminGridView extends module_core.AdminGridView
    implements Pluggable {
  $module_core_AdminGridView() {
//dynamic
//Slot
//create
    this.header = new module_core.Slot();
//Slot
//create
    this.body = new module_core.Slot();
//Slot
//create
    this.saveBtn = new module_core.Slot();
//Slot
//create
    this.searchBtn = new module_core.Slot();
//Slot
//create
    this.creatorButtons = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.AdminGridView";
  void initializeSlots() {
//compiled method
    {
      this.header.container = this;
      this.header.element =
          element.querySelector('[data-slot="' + 'header' + '"]');
      if (this.header.element != null) {
        this.header.element.classes.add('slot');
      } else {
        print('slot ' + 'header' + ' is missing');
      }
    }
    {
      this.body.container = this;
      this.body.element = element.querySelector('[data-slot="' + 'body' + '"]');
      if (this.body.element != null) {
        this.body.element.classes.add('slot');
      } else {
        print('slot ' + 'body' + ' is missing');
      }
    }
    {
      this.saveBtn.container = this;
      this.saveBtn.element =
          element.querySelector('[data-slot="' + 'saveBtn' + '"]');
      if (this.saveBtn.element != null) {
        this.saveBtn.element.classes.add('slot');
      } else {
        print('slot ' + 'saveBtn' + ' is missing');
      }
    }
    {
      this.searchBtn.container = this;
      this.searchBtn.element =
          element.querySelector('[data-slot="' + 'searchBtn' + '"]');
      if (this.searchBtn.element != null) {
        this.searchBtn.element.classes.add('slot');
      } else {
        print('slot ' + 'searchBtn' + ' is missing');
      }
    }
    {
      this.creatorButtons.container = this;
      this.creatorButtons.element =
          element.querySelector('[data-slot="' + 'creatorButtons' + '"]');
      if (this.creatorButtons.element != null) {
        this.creatorButtons.element.classes.add('slot');
      } else {
        print('slot ' + 'creatorButtons' + ' is missing');
      }
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.header.widgets.forEach((widget) {
        this.header.element.append(widget.element);
      });
    }
    {
      this.body.widgets.forEach((widget) {
        this.body.element.append(widget.element);
      });
    }
    {
      this.saveBtn.widgets.forEach((widget) {
        this.saveBtn.element.append(widget.element);
      });
    }
    {
      this.searchBtn.widgets.forEach((widget) {
        this.searchBtn.element.append(widget.element);
      });
    }
    {
      this.creatorButtons.widgets.forEach((widget) {
        this.creatorButtons.element.append(widget.element);
      });
    }
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_core_AdminGridView.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_core_AdminGridView.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_core_AdminGridView.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_core_AdminGridView.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_core_AdminGridView.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_core_AdminGridView.html
}

//interceptor for [module_core.UserLoginForm]
//interceptor for module_core.UserLoginForm
//can be singleton: TRUE
//parent: UserLoginForm [@bool get Compose]
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_UserLoginForm extends module_core.UserLoginForm
    implements Pluggable {
  $module_core_UserLoginForm() {
//dynamic
//User
//Router
//Site
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.UserLoginForm";
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_core_UserLoginForm.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_core_UserLoginForm.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_core_UserLoginForm.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_core_UserLoginForm.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_core_UserLoginForm.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_core_UserLoginForm.html
}

//interceptor for [module_core.AdminMenuLink]
//interceptor for module_core.AdminMenuLink
//can be singleton: TRUE
//parent: AdminMenuLink [@bool get Compose]
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_AdminMenuLink extends module_core.AdminMenuLink
    implements Pluggable {
  $module_core_AdminMenuLink() {
//dynamic
//bool
//String
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.AdminMenuLink";
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_core_AdminMenuLink.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_core_AdminMenuLink.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_core_AdminMenuLink.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_core_AdminMenuLink.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_core_AdminMenuLink.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_core_AdminMenuLink.html
}

//interceptor for [module_core.AdminNavbar]
//interceptor for module_core.AdminNavbar
//can be singleton: TRUE
//parent: AdminNavbar [@bool get Compose]
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_AdminNavbar extends module_core.AdminNavbar
    implements Pluggable {
  $module_core_AdminNavbar() {
//dynamic
//Slot
//create
    this.menu = new module_core.Slot();
//Slot
//create
    this.right = new module_core.Slot();
//Router
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.AdminNavbar";
  void initializeSlots() {
//compiled method
    {
      this.menu.container = this;
      this.menu.element = element.querySelector('[data-slot="' + 'menu' + '"]');
      if (this.menu.element != null) {
        this.menu.element.classes.add('slot');
      } else {
        print('slot ' + 'menu' + ' is missing');
      }
    }
    {
      this.right.container = this;
      this.right.element =
          element.querySelector('[data-slot="' + 'right' + '"]');
      if (this.right.element != null) {
        this.right.element.classes.add('slot');
      } else {
        print('slot ' + 'right' + ' is missing');
      }
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.menu.widgets.forEach((widget) {
        this.menu.element.append(widget.element);
      });
    }
    {
      this.right.widgets.forEach((widget) {
        this.right.element.append(widget.element);
      });
    }
  }

  module_core.NavbarDropdown navbarDropdownFactory() {
//module_core.NavbarDropdown
    return new $module_core_NavbarDropdown();
  }

  module_core.NavbarLink navbarLinkFactory() {
//module_core.NavbarLink
    return new $module_core_NavbarLink();
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_core_AdminNavbar.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_core_AdminNavbar.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_core_AdminNavbar.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_core_AdminNavbar.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_core_AdminNavbar.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_core_AdminNavbar.html
}

//interceptor for [module_core.UserForm]
//interceptor for module_core.UserForm
//can be singleton: TRUE
//parent: UserForm [@bool get Compose]
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_UserForm extends module_core.UserForm implements Pluggable {
  $module_core_UserForm() {
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.UserForm";
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_core_UserForm.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_core_UserForm.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_core_UserForm.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_core_UserForm.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_core_UserForm.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_core_UserForm.html
}

//no interceptor for [module_core.AnnotatedWith]
//no interceptor for [module_core.Pluggable]
//no interceptor for [module_core.TypePlugin]
//no interceptor for [module_core.PossibleTypoException]
//no interceptor for [module_core.Id]
//interceptor for [module_core.Site]
//interceptor for module_core.Site
//can be singleton: TRUE
//parent: Site [@bool get Compose]
class $module_core_Site extends module_core.Site implements Pluggable {
  $module_core_Site() {}
  T plugin<T>() {
    return null;
  }

  String get name => "Swift Shop Admin";
  String get url => "demo.swift.shop/admin";
  module_core.Id get id => new module_core.Id.fromString("JA2Q-C84X");
}

//interceptor for [module_core.Application]
//interceptor for module_core.Application
//can be singleton: TRUE
//parent: Application [@bool get Compose]
class $module_core_Application extends module_core.Application
    implements Pluggable {
  $module_core_Application() {
//Map<String, Future<String>>
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  module_core.Site get site => $om.module_core_Site;
  module_core.Layout get layout => $om.module_core_Layout;
}

//interceptor for [module_core.Layout]
//interceptor for module_core.Layout
//can be singleton: TRUE
//parent: Layout [@bool get Compose]
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_Layout extends module_core.Layout implements Pluggable {
  $module_core_Layout() {
//dynamic
//Slot
//create
    this.navbar = new module_core.Slot();
//Slot
//create
    this.body = new module_core.Slot();
//Slot
//create
    this.modal = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.Layout";
  module_core.Site get site => $om.module_core_Site;
  void initializeSlots() {
//compiled method
    {
      this.navbar.container = this;
      this.navbar.element =
          element.querySelector('[data-slot="' + 'navbar' + '"]');
      if (this.navbar.element != null) {
        this.navbar.element.classes.add('slot');
      } else {
        print('slot ' + 'navbar' + ' is missing');
      }
    }
    {
      this.body.container = this;
      this.body.element = element.querySelector('[data-slot="' + 'body' + '"]');
      if (this.body.element != null) {
        this.body.element.classes.add('slot');
      } else {
        print('slot ' + 'body' + ' is missing');
      }
    }
    {
      this.modal.container = this;
      this.modal.element =
          element.querySelector('[data-slot="' + 'modal' + '"]');
      if (this.modal.element != null) {
        this.modal.element.classes.add('slot');
      } else {
        print('slot ' + 'modal' + ' is missing');
      }
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.navbar.widgets.forEach((widget) {
        this.navbar.element.append(widget.element);
      });
    }
    {
      this.body.widgets.forEach((widget) {
        this.body.element.append(widget.element);
      });
    }
    {
      this.modal.widgets.forEach((widget) {
        this.modal.element.append(widget.element);
      });
    }
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_core_Layout.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_core_Layout.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_core_Layout.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_core_Layout.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_core_Layout.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_core_Layout.html
}

//no interceptor for [module_core.ApiModule]
//no interceptor for [module_core.CRUDApi]
//no interceptor for [module_core.Action]
//interceptor for [module_core.Router]
//interceptor for module_core.Router
//can be singleton: TRUE
//parent: Router [@bool get Compose]
class $module_core_Router extends module_core.Router implements Pluggable {
  $module_core_Router() {
//Action
//Action
//Element
  }
  T plugin<T>() {
    return null;
  }

  String get indexUrl => "products";
  module_core.Action actionFactory(String className, List<String> args) {
    switch (className) {
      case 'module_core.AdminAction':
        return new $module_core_AdminAction(args);
      case 'module_core.IndexAction':
        return new $module_core_IndexAction(args);
      case 'module_core.LoginAction':
        return new $module_core_LoginAction(args);
      case 'module_core.LogoutAction':
        return new $module_core_LogoutAction(args);
      case 'module_core.ModulesAction':
        return new $module_core_ModulesAction(args);
      case 'module_core.SettingsAction':
        return new $module_core_SettingsAction(args);
      case 'module_pim.ProductsAction':
        return new $module_pim_ProductsAction(args);
      case 'module_om.OrdersAction':
        return new $module_om_OrdersAction(args);
    }
  }

  String getCode<T extends module_core.Action>() {
    if (T == module_core.AdminAction) return 'module_core.AdminAction';
    if (T == module_core.IndexAction) return 'module_core.IndexAction';
    if (T == module_core.LoginAction) return 'module_core.LoginAction';
    if (T == module_core.LogoutAction) return 'module_core.LogoutAction';
    if (T == module_core.ModulesAction) return 'module_core.ModulesAction';
    if (T == module_core.SettingsAction) return 'module_core.SettingsAction';
    if (T == module_pim.ProductsAction) return 'module_pim.ProductsAction';
    if (T == module_om.OrdersAction) return 'module_om.OrdersAction';
  }
}

//no interceptor for [module_core.Slot]
//interceptor for [module_core.NavbarLink]
//interceptor for module_core.NavbarLink
//can be singleton: TRUE
//parent: NavbarLink []
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_NavbarLink extends module_core.NavbarLink
    implements Pluggable {
  $module_core_NavbarLink() {
//dynamic
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.NavbarLink";
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_core_NavbarLink.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_core_NavbarLink.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_core_NavbarLink.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_core_NavbarLink.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_core_NavbarLink.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_core_NavbarLink.html
}

//interceptor for [module_core.NavbarDropdown]
//interceptor for module_core.NavbarDropdown
//can be singleton: TRUE
//parent: NavbarDropdown []
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_NavbarDropdown extends module_core.NavbarDropdown
    implements Pluggable {
  $module_core_NavbarDropdown() {
//dynamic
//Slot
//create
    this.items = new module_core.Slot();
//String
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.NavbarDropdown";
  void initializeSlots() {
//compiled method
    {
      this.items.container = this;
      this.items.element =
          element.querySelector('[data-slot="' + 'items' + '"]');
      if (this.items.element != null) {
        this.items.element.classes.add('slot');
      } else {
        print('slot ' + 'items' + ' is missing');
      }
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.items.widgets.forEach((widget) {
        this.items.element.append(widget.element);
      });
    }
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_core_NavbarDropdown.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_core_NavbarDropdown.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_core_NavbarDropdown.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_core_NavbarDropdown.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_core_NavbarDropdown.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_core_NavbarDropdown.html
}

//no interceptor for [module_core.SwiftUriPolicy]
//no interceptor for [module_core.ExpandedElement]
//no interceptor for [module_core.Widget]
//no interceptor for [module_core.AsyncWidget]
//interceptor for [module_core.TextWidget]
//interceptor for module_core.TextWidget
//can be singleton: FALSE
//parent: TextWidget [@bool get Compose]
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_TextWidget extends module_core.TextWidget
    implements Pluggable {
  $module_core_TextWidget(text) {
//dynamic
//String
    this.text = text;
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.TextWidget";
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_core_TextWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_core_TextWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_core_TextWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_core_TextWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_core_TextWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_core_TextWidget.html
}

//no interceptor for [module_core.Entity]
//interceptor for [module_core.App]
//interceptor for module_core.App
//can be singleton: TRUE
//parent: App []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_App extends module_core.App implements Pluggable {
  $module_core_App() {
//Id
//String
//App
//User
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_core.App'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['parent'] = "module_core.App";
    }
    {
      target['owner'] = "module_core.User";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.parent = relations['parent'];
    }
    {
      this.owner = relations['owner'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['url'] = this.url;
    }
    {
      target['name'] = this.name;
    }
//@Field
//@Field
    {
      target['parent'] = this.parent != null ? this.parent.toJson() : null;
    }
    {
      target['owner'] = this.owner != null ? this.owner.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.url = target.containsKey('url') ? target['url'] : this.url;
    }
    {
      this.name = target.containsKey('name') ? target['name'] : this.name;
    }
//@Field
//@Field
//@Field
    {
      if (target['parent'] != null) {
        if (target['parent'] is String) {
          this.parent = relatedFactory("module_core.App");
          this.parent.id = idFromString(target['parent']);
        } else {
          this.parent = relatedFactory(target['parent']['classes'].last);
          this.parent.fromJson(target['parent']);
        }
      } else {
        this.parent = null;
      }
    }
    {
      if (target['owner'] != null) {
        if (target['owner'] is String) {
          this.owner = relatedFactory("module_core.User");
          this.owner.id = idFromString(target['owner']);
        } else {
          this.owner = relatedFactory(target['owner']['classes'].last);
          this.owner.fromJson(target['owner']);
        }
      } else {
        this.owner = null;
      }
    }
  }
}

//interceptor for [module_core.User]
//interceptor for module_core.User
//can be singleton: TRUE
//parent: User []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_User extends module_core.User implements Pluggable {
  $module_core_User() {
//Id
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_core.User'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['email'] = this.email;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.email = target.containsKey('email') ? target['email'] : this.email;
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_core.Image]
//interceptor for module_core.Image
//can be singleton: TRUE
//parent: Image []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_Image extends module_core.Image implements Pluggable {
  $module_core_Image() {
//Id
//String
//App
//String
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_core.Image'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['app'] = "module_core.App";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.app = relations['app'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['mimetype'] = this.mimetype;
    }
    {
      target['filename'] = this.filename;
    }
    {
      target['thumb'] = this.thumb;
    }
//@Field
//@Field
    {
      target['app'] = this.app != null ? this.app.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.mimetype =
          target.containsKey('mimetype') ? target['mimetype'] : this.mimetype;
    }
    {
      this.filename =
          target.containsKey('filename') ? target['filename'] : this.filename;
    }
    {
      this.thumb = target.containsKey('thumb') ? target['thumb'] : this.thumb;
    }
//@Field
//@Field
//@Field
    {
      if (target['app'] != null) {
        if (target['app'] is String) {
          this.app = relatedFactory("module_core.App");
          this.app.id = idFromString(target['app']);
        } else {
          this.app = relatedFactory(target['app']['classes'].last);
          this.app.fromJson(target['app']);
        }
      } else {
        this.app = null;
      }
    }
  }
}

//interceptor for [module_pim.ProductsAction]
//interceptor for module_pim.ProductsAction
//T[501968309] => ProductBase[447620454]
//can be singleton: FALSE
//parent: ProductsAction [@AdminMenuItem AdminMenuItem(String section, String subsection, String title)]
//parent: AdminGridAction [@bool get ComposeSubtypes]
//parent: AdminAction []
//parent: Action [@bool get ComposeSubtypes]
class $module_pim_ProductsAction extends module_pim.ProductsAction
    implements Pluggable {
  $module_pim_ProductsAction(args) {
//Application
//CRUDApi<T>
//Map<String, AdminGridColumn<T>>
//AdminGridView
//create
    this.gridView = new $module_core_AdminGridView();
//dynamic
//List<T>
//User
//Router
//Application
//List<String>
    this.args = args;
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_pim.ProductsAction";
  module_pim.ProductBase create(String className) {
    switch (className) {
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
    }
  }

  module_core.NavbarLink navbarLinkFactory() {
//module_core.NavbarLink
    return new $module_core_NavbarLink();
  }
}

//interceptor for [module_pim.NameColumn]
//interceptor for module_pim.NameColumn
//E[272729536] => ProductBase[447620454]
//T[122938487] => String[301336291]
//E[126265621] => ProductBase[447620454]
//can be singleton: TRUE
//parent: NameColumn []
//parent: AdminGridFieldColumn [@bool get ComposeSubtypes]
//parent: AdminGridColumn []
class $module_pim_NameColumn extends module_pim.NameColumn
    implements Pluggable {
  $module_pim_NameColumn() {
//InputFor<T>
  }
  T plugin<T>() {
    return null;
  }
}

//interceptor for [module_pim.PriceColumn]
//interceptor for module_pim.PriceColumn
//E[272729536] => ProductBase[447620454]
//T[122938487] => int[199984284]
//E[126265621] => ProductBase[447620454]
//can be singleton: TRUE
//parent: PriceColumn []
//parent: AdminGridFieldColumn [@bool get ComposeSubtypes]
//parent: AdminGridColumn []
class $module_pim_PriceColumn extends module_pim.PriceColumn
    implements Pluggable {
  $module_pim_PriceColumn() {
//InputFor<T>
  }
  T plugin<T>() {
    return null;
  }
}

//interceptor for [module_pim.ShortDescriptionColumn]
//interceptor for module_pim.ShortDescriptionColumn
//E[272729536] => ProductBase[447620454]
//T[122938487] => String[301336291]
//E[126265621] => ProductBase[447620454]
//can be singleton: TRUE
//parent: ShortDescriptionColumn []
//parent: AdminGridFieldColumn [@bool get ComposeSubtypes]
//parent: AdminGridColumn []
class $module_pim_ShortDescriptionColumn
    extends module_pim.ShortDescriptionColumn implements Pluggable {
  $module_pim_ShortDescriptionColumn() {
//InputFor<T>
  }
  T plugin<T>() {
    return null;
  }
}

//interceptor for [module_pim.PimApi]
//interceptor for module_pim.PimApi
//T[534851839] => ProductBase[447620454]
//can be singleton: TRUE
//parent: PimApi [@bool get Compose]
//parent: CRUDApi [@bool get ComposeSubtypes]
//parent: ApiModule [@bool get ComposeSubtypes]
class $module_pim_PimApi extends module_pim.PimApi implements Pluggable {
  $module_pim_PimApi() {}
  T plugin<T>() {
    return null;
  }

  Map<String, module_core.CRUDApi<module_core.Entity>> get relatedApis =>
      $om.instancesOfmodule_core_CRUDApi_module_core_Entity_;
  String get className => "module_pim.PimApi";
  module_core.Site get site => $om.module_core_Site;
  module_pim.ProductBase newEntityByType(String className) {
    switch (className) {
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
    }
  }
}

//no interceptor for [module_pim.UnitType]
//no interceptor for [module_pim.ConfigurableField]
//no interceptor for [module_pim.ProductBase]
//interceptor for [module_pim.SimpleProduct]
//interceptor for module_pim.SimpleProduct
//can be singleton: TRUE
//parent: SimpleProduct []
//parent: ProductBase [@bool get ComposeSubtypes]
//parent: Entity [@bool get ComposeSubtypes]
class $module_pim_SimpleProduct extends module_pim.SimpleProduct
    implements Pluggable {
  $module_pim_SimpleProduct() {
//String
//int
//String
//ConfigurableField<UnitType>
//Image
//Id
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_pim.ProductBase', 'module_pim.SimpleProduct'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['image'] = "module_core.Image";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.image = relations['image'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
    {
      target['price'] = this.price;
    }
//@Field
    {
      target['name'] = this.name;
    }
    {
      target['shortDescription'] = this.shortDescription;
    }
    {
      target['slug'] = this.slug;
    }
//@Field
//@Field
    {
      target['image'] = this.image != null ? this.image.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.name = target.containsKey('name') ? target['name'] : this.name;
    }
    {
      this.shortDescription = target.containsKey('shortDescription')
          ? target['shortDescription']
          : this.shortDescription;
    }
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
//@Field
    {
      this.price = target.containsKey('price') ? target['price'] : this.price;
    }
//@Field
//@Field
    {
      if (target['image'] != null) {
        if (target['image'] is String) {
          this.image = relatedFactory("module_core.Image");
          this.image.id = idFromString(target['image']);
        } else {
          this.image = relatedFactory(target['image']['classes'].last);
          this.image.fromJson(target['image']);
        }
      } else {
        this.image = null;
      }
    }
  }
}

//no interceptor for [module_snippets.CmsWidget]
//interceptor for [module_snippets.HtmlWidget]
//interceptor for module_snippets.HtmlWidget
//CT[171676771] => HtmlConfig[389581089]
//can be singleton: FALSE
//parent: HtmlWidget []
//parent: CmsWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $module_snippets_HtmlWidget extends module_snippets.HtmlWidget
    implements Pluggable {
  $module_snippets_HtmlWidget(config) {
//CT
    this.config = config;
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_snippets.HtmlWidget";
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_snippets_HtmlWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_snippets_HtmlWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_snippets_HtmlWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_snippets_HtmlWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_snippets_HtmlWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_snippets_HtmlWidget.html
}

//interceptor for [module_snippets.ImageWidget]
//interceptor for module_snippets.ImageWidget
//CT[171676771] => ImageConfig[445488254]
//can be singleton: FALSE
//parent: ImageWidget []
//parent: CmsWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $module_snippets_ImageWidget extends module_snippets.ImageWidget
    implements Pluggable {
  $module_snippets_ImageWidget(config) {
//CT
    this.config = config;
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_snippets.ImageWidget";
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_snippets_ImageWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_snippets_ImageWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_snippets_ImageWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_snippets_ImageWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_snippets_ImageWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_snippets_ImageWidget.html
}

//interceptor for [module_snippets.ParagraphWidget]
//interceptor for module_snippets.ParagraphWidget
//CT[171676771] => ParagraphConfig[153581726]
//can be singleton: FALSE
//parent: ParagraphWidget []
//parent: CmsWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $module_snippets_ParagraphWidget extends module_snippets.ParagraphWidget
    implements Pluggable {
  $module_snippets_ParagraphWidget(config) {
//CT
    this.config = config;
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_snippets.ParagraphWidget";
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_snippets_ParagraphWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_snippets_ParagraphWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_snippets_ParagraphWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_snippets_ParagraphWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_snippets_ParagraphWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_snippets_ParagraphWidget.html
}

//interceptor for [module_snippets.HeadingWidget]
//interceptor for module_snippets.HeadingWidget
//CT[171676771] => HeadingConfig[26884972]
//can be singleton: FALSE
//parent: HeadingWidget []
//parent: CmsWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $module_snippets_HeadingWidget extends module_snippets.HeadingWidget
    implements Pluggable {
  $module_snippets_HeadingWidget(config) {
//CT
    this.config = config;
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_snippets.HeadingWidget";
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/cado7dev/apps/demo.swift.shop/web/templates/module_snippets_HeadingWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/om/templates/module_snippets_HeadingWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/cms/templates/module_snippets_HeadingWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/pim/templates/module_snippets_HeadingWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../modules/core/templates/module_snippets_HeadingWidget.html
  ///home/fsw/cado7dev/apps/demo.swift.shop/../../libs/swift_composer/templates/module_snippets_HeadingWidget.html
}

//interceptor for [module_snippets.CmsPagesApi]
//interceptor for module_snippets.CmsPagesApi
//T[534851839] => Page[414991068]
//can be singleton: TRUE
//parent: CmsPagesApi []
//parent: CRUDApi [@bool get ComposeSubtypes]
//parent: ApiModule [@bool get ComposeSubtypes]
class $module_snippets_CmsPagesApi extends module_snippets.CmsPagesApi
    implements Pluggable {
  $module_snippets_CmsPagesApi() {}
  T plugin<T>() {
    return null;
  }

  Map<String, module_core.CRUDApi<module_core.Entity>> get relatedApis =>
      $om.instancesOfmodule_core_CRUDApi_module_core_Entity_;
  String get className => "module_snippets.CmsPagesApi";
  module_core.Site get site => $om.module_core_Site;
  module_snippets.Page newEntityByType(String className) {
    switch (className) {
      case 'module_snippets.Page':
        return new $module_snippets_Page();
    }
  }
}

//interceptor for [module_snippets.CmsWidgetConfig]
//interceptor for module_snippets.CmsWidgetConfig
//can be singleton: TRUE
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_snippets_CmsWidgetConfig extends module_snippets.CmsWidgetConfig
    implements Pluggable {
  $module_snippets_CmsWidgetConfig() {
//Id
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_snippets.CmsWidgetConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_snippets.HeadingConfig]
//interceptor for module_snippets.HeadingConfig
//can be singleton: TRUE
//parent: HeadingConfig []
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_snippets_HeadingConfig extends module_snippets.HeadingConfig
    implements Pluggable {
  $module_snippets_HeadingConfig() {
//Id
//String
//String
//String
//int
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_snippets.CmsWidgetConfig', 'module_snippets.HeadingConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
    {
      target['level'] = this.level;
    }
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['title'] = this.title;
    }
    {
      target['subtitle'] = this.subtitle;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.title = target.containsKey('title') ? target['title'] : this.title;
    }
    {
      this.subtitle =
          target.containsKey('subtitle') ? target['subtitle'] : this.subtitle;
    }
//@Field
    {
      this.level = target.containsKey('level') ? target['level'] : this.level;
    }
//@Field
//@Field
  }
}

//interceptor for [module_snippets.ImageConfig]
//interceptor for module_snippets.ImageConfig
//can be singleton: TRUE
//parent: ImageConfig []
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_snippets_ImageConfig extends module_snippets.ImageConfig
    implements Pluggable {
  $module_snippets_ImageConfig() {
//Id
//String
//Image
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_snippets.CmsWidgetConfig', 'module_snippets.ImageConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['image'] = "module_core.Image";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.image = relations['image'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['caption'] = this.caption;
    }
//@Field
//@Field
    {
      target['image'] = this.image != null ? this.image.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.caption =
          target.containsKey('caption') ? target['caption'] : this.caption;
    }
//@Field
//@Field
//@Field
    {
      if (target['image'] != null) {
        if (target['image'] is String) {
          this.image = relatedFactory("module_core.Image");
          this.image.id = idFromString(target['image']);
        } else {
          this.image = relatedFactory(target['image']['classes'].last);
          this.image.fromJson(target['image']);
        }
      } else {
        this.image = null;
      }
    }
  }
}

//interceptor for [module_snippets.HtmlConfig]
//interceptor for module_snippets.HtmlConfig
//can be singleton: TRUE
//parent: HtmlConfig []
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_snippets_HtmlConfig extends module_snippets.HtmlConfig
    implements Pluggable {
  $module_snippets_HtmlConfig() {
//Id
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_snippets.CmsWidgetConfig', 'module_snippets.HtmlConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['html'] = this.html;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.html = target.containsKey('html') ? target['html'] : this.html;
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_snippets.ParagraphConfig]
//interceptor for module_snippets.ParagraphConfig
//can be singleton: TRUE
//parent: ParagraphConfig []
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_snippets_ParagraphConfig extends module_snippets.ParagraphConfig
    implements Pluggable {
  $module_snippets_ParagraphConfig() {
//Id
//String
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_snippets.CmsWidgetConfig', 'module_snippets.ParagraphConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['title'] = this.title;
    }
    {
      target['body'] = this.body;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.title = target.containsKey('title') ? target['title'] : this.title;
    }
    {
      this.body = target.containsKey('body') ? target['body'] : this.body;
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_snippets.Page]
//interceptor for module_snippets.Page
//can be singleton: TRUE
//parent: Page []
//parent: Entity [@bool get ComposeSubtypes]
class $module_snippets_Page extends module_snippets.Page implements Pluggable {
  $module_snippets_Page() {
//Id
//String
//App
//String
//List<CmsWidgetConfig>
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_snippets.Page'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['app'] = "module_core.App";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.app = relations['app'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['title'] = this.title;
    }
//@Field
    {
      target['widgets'] = this.widgets.map((e) => e.toJson()).toList();
    }
//@Field
    {
      target['app'] = this.app != null ? this.app.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.title = target.containsKey('title') ? target['title'] : this.title;
    }
//@Field
//@Field
    {
      this.widgets = [];
      if (target['widgets'] != null) {
        target['widgets'].forEach((data) {
          var e = relatedFactory(data['classes'].last);
          e.fromJson(data);
          this.widgets.add(e);
        });
      }
    }
//@Field
    {
      if (target['app'] != null) {
        if (target['app'] is String) {
          this.app = relatedFactory("module_core.App");
          this.app.id = idFromString(target['app']);
        } else {
          this.app = relatedFactory(target['app']['classes'].last);
          this.app.fromJson(target['app']);
        }
      } else {
        this.app = null;
      }
    }
  }
}

//interceptor for [module_om.OrdersAction]
//interceptor for module_om.OrdersAction
//T[501968309] => Order[105961759]
//can be singleton: FALSE
//parent: OrdersAction [@AdminMenuItem AdminMenuItem(String section, String subsection, String title)]
//parent: AdminGridAction [@bool get ComposeSubtypes]
//parent: AdminAction []
//parent: Action [@bool get ComposeSubtypes]
class $module_om_OrdersAction extends module_om.OrdersAction
    implements Pluggable {
  $module_om_OrdersAction(args) {
//Application
//CRUDApi<T>
//Map<String, AdminGridColumn<T>>
//AdminGridView
//create
    this.gridView = new $module_core_AdminGridView();
//dynamic
//List<T>
//User
//Router
//Application
//List<String>
    this.args = args;
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  String get className => "module_om.OrdersAction";
  module_om.Order create(String className) {
    switch (className) {
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  module_core.NavbarLink navbarLinkFactory() {
//module_core.NavbarLink
    return new $module_core_NavbarLink();
  }
}

//interceptor for [module_om.OmApi]
//interceptor for module_om.OmApi
//T[534851839] => Order[105961759]
//can be singleton: TRUE
//parent: OmApi []
//parent: CRUDApi [@bool get ComposeSubtypes]
//parent: ApiModule [@bool get ComposeSubtypes]
class $module_om_OmApi extends module_om.OmApi implements Pluggable {
  $module_om_OmApi() {}
  T plugin<T>() {
    return null;
  }

  Map<String, module_core.CRUDApi<module_core.Entity>> get relatedApis =>
      $om.instancesOfmodule_core_CRUDApi_module_core_Entity_;
  String get className => "module_om.OmApi";
  module_core.Site get site => $om.module_core_Site;
  module_om.Order newEntityByType(String className) {
    switch (className) {
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }
}

//no interceptor for [module_om.DeliveryMethod]
//no interceptor for [module_om.PaymentMethod]
//interceptor for [module_om.OrderItem]
//interceptor for module_om.OrderItem
//can be singleton: TRUE
//parent: OrderItem [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_om_OrderItem extends module_om.OrderItem implements Pluggable {
  $module_om_OrderItem() {
//Id
//String
//ProductBase
//int
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_om.OrderItem'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['product'] = "module_pim.ProductBase";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.product = relations['product'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
    {
      target['quantity'] = this.quantity;
    }
//@Field
    {
      target['slug'] = this.slug;
    }
//@Field
//@Field
    {
      target['product'] = this.product != null ? this.product.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
//@Field
    {
      this.quantity =
          target.containsKey('quantity') ? target['quantity'] : this.quantity;
    }
//@Field
//@Field
    {
      if (target['product'] != null) {
        if (target['product'] is String) {
          this.product = relatedFactory("module_pim.ProductBase");
          this.product.id = idFromString(target['product']);
        } else {
          this.product = relatedFactory(target['product']['classes'].last);
          this.product.fromJson(target['product']);
        }
      } else {
        this.product = null;
      }
    }
  }
}

//interceptor for [module_om.Order]
//interceptor for module_om.Order
//can be singleton: TRUE
//parent: Order []
//parent: Entity [@bool get ComposeSubtypes]
class $module_om_Order extends module_om.Order implements Pluggable {
  $module_om_Order() {
//Id
//String
//String
//List<OrderItem>
//DeliveryMethod
//PaymentMethod
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_om.Order'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_snippets.CmsWidgetConfig':
        return new $module_snippets_CmsWidgetConfig();
      case 'module_snippets.HeadingConfig':
        return new $module_snippets_HeadingConfig();
      case 'module_snippets.ImageConfig':
        return new $module_snippets_ImageConfig();
      case 'module_snippets.HtmlConfig':
        return new $module_snippets_HtmlConfig();
      case 'module_snippets.ParagraphConfig':
        return new $module_snippets_ParagraphConfig();
      case 'module_snippets.Page':
        return new $module_snippets_Page();
      case 'module_om.OrderItem':
        return new $module_om_OrderItem();
      case 'module_om.Order':
        return new $module_om_Order();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['deliveryMethod'] = "module_om.DeliveryMethod";
    }
    {
      target['paymentMethod'] = "module_om.PaymentMethod";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.deliveryMethod = relations['deliveryMethod'];
    }
    {
      this.paymentMethod = relations['paymentMethod'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['name'] = this.name;
    }
//@Field
    {
      target['items'] = this.items.map((e) => e.toJson()).toList();
    }
//@Field
    {
      target['deliveryMethod'] =
          this.deliveryMethod != null ? this.deliveryMethod.toJson() : null;
    }
    {
      target['paymentMethod'] =
          this.paymentMethod != null ? this.paymentMethod.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.name = target.containsKey('name') ? target['name'] : this.name;
    }
//@Field
//@Field
    {
      this.items = [];
      if (target['items'] != null) {
        target['items'].forEach((data) {
          var e = relatedFactory(data['classes'].last);
          e.fromJson(data);
          this.items.add(e);
        });
      }
    }
//@Field
    {
      if (target['deliveryMethod'] != null) {
        if (target['deliveryMethod'] is String) {
          this.deliveryMethod = relatedFactory("module_om.DeliveryMethod");
          this.deliveryMethod.id = idFromString(target['deliveryMethod']);
        } else {
          this.deliveryMethod =
              relatedFactory(target['deliveryMethod']['classes'].last);
          this.deliveryMethod.fromJson(target['deliveryMethod']);
        }
      } else {
        this.deliveryMethod = null;
      }
    }
    {
      if (target['paymentMethod'] != null) {
        if (target['paymentMethod'] is String) {
          this.paymentMethod = relatedFactory("module_om.PaymentMethod");
          this.paymentMethod.id = idFromString(target['paymentMethod']);
        } else {
          this.paymentMethod =
              relatedFactory(target['paymentMethod']['classes'].last);
          this.paymentMethod.fromJson(target['paymentMethod']);
        }
      } else {
        this.paymentMethod = null;
      }
    }
  }

  module_om.OrderItem createOrderItem() {
//module_om.OrderItem
    return new $module_om_OrderItem();
  }
}

class $ObjectManager {
  $module_core_TODODODOD _module_core_TODODODOD;
  $module_core_TODODODOD get module_core_TODODODOD {
    if (_module_core_TODODODOD == null) {
      _module_core_TODODODOD = new $module_core_TODODODOD();
    }
    return _module_core_TODODODOD;
  }

  $module_core_AdminLayout _module_core_AdminLayout;
  $module_core_AdminLayout get module_core_AdminLayout {
    if (_module_core_AdminLayout == null) {
      _module_core_AdminLayout = new $module_core_AdminLayout();
    }
    return _module_core_AdminLayout;
  }

  $module_core_Dashboard _module_core_Dashboard;
  $module_core_Dashboard get module_core_Dashboard {
    if (_module_core_Dashboard == null) {
      _module_core_Dashboard = new $module_core_Dashboard();
    }
    return _module_core_Dashboard;
  }

  $module_core_AdminGridView _module_core_AdminGridView;
  $module_core_AdminGridView get module_core_AdminGridView {
    if (_module_core_AdminGridView == null) {
      _module_core_AdminGridView = new $module_core_AdminGridView();
    }
    return _module_core_AdminGridView;
  }

  $module_core_UserLoginForm _module_core_UserLoginForm;
  $module_core_UserLoginForm get module_core_UserLoginForm {
    if (_module_core_UserLoginForm == null) {
      _module_core_UserLoginForm = new $module_core_UserLoginForm();
    }
    return _module_core_UserLoginForm;
  }

  $module_core_AdminMenuLink _module_core_AdminMenuLink;
  $module_core_AdminMenuLink get module_core_AdminMenuLink {
    if (_module_core_AdminMenuLink == null) {
      _module_core_AdminMenuLink = new $module_core_AdminMenuLink();
    }
    return _module_core_AdminMenuLink;
  }

  $module_core_AdminNavbar _module_core_AdminNavbar;
  $module_core_AdminNavbar get module_core_AdminNavbar {
    if (_module_core_AdminNavbar == null) {
      _module_core_AdminNavbar = new $module_core_AdminNavbar();
    }
    return _module_core_AdminNavbar;
  }

  $module_core_UserForm _module_core_UserForm;
  $module_core_UserForm get module_core_UserForm {
    if (_module_core_UserForm == null) {
      _module_core_UserForm = new $module_core_UserForm();
    }
    return _module_core_UserForm;
  }

  $module_core_Site _module_core_Site;
  $module_core_Site get module_core_Site {
    if (_module_core_Site == null) {
      _module_core_Site = new $module_core_Site();
    }
    return _module_core_Site;
  }

  $module_core_Application _module_core_Application;
  $module_core_Application get module_core_Application {
    if (_module_core_Application == null) {
      _module_core_Application = new $module_core_Application();
    }
    return _module_core_Application;
  }

  $module_core_Layout _module_core_Layout;
  $module_core_Layout get module_core_Layout {
    if (_module_core_Layout == null) {
      _module_core_Layout = new $module_core_Layout();
    }
    return _module_core_Layout;
  }

  $module_core_Router _module_core_Router;
  $module_core_Router get module_core_Router {
    if (_module_core_Router == null) {
      _module_core_Router = new $module_core_Router();
    }
    return _module_core_Router;
  }

  $module_core_NavbarLink _module_core_NavbarLink;
  $module_core_NavbarLink get module_core_NavbarLink {
    if (_module_core_NavbarLink == null) {
      _module_core_NavbarLink = new $module_core_NavbarLink();
    }
    return _module_core_NavbarLink;
  }

  $module_core_NavbarDropdown _module_core_NavbarDropdown;
  $module_core_NavbarDropdown get module_core_NavbarDropdown {
    if (_module_core_NavbarDropdown == null) {
      _module_core_NavbarDropdown = new $module_core_NavbarDropdown();
    }
    return _module_core_NavbarDropdown;
  }

  $module_core_App _module_core_App;
  $module_core_App get module_core_App {
    if (_module_core_App == null) {
      _module_core_App = new $module_core_App();
    }
    return _module_core_App;
  }

  $module_core_User _module_core_User;
  $module_core_User get module_core_User {
    if (_module_core_User == null) {
      _module_core_User = new $module_core_User();
    }
    return _module_core_User;
  }

  $module_core_Image _module_core_Image;
  $module_core_Image get module_core_Image {
    if (_module_core_Image == null) {
      _module_core_Image = new $module_core_Image();
    }
    return _module_core_Image;
  }

  $module_pim_NameColumn _module_pim_NameColumn;
  $module_pim_NameColumn get module_pim_NameColumn {
    if (_module_pim_NameColumn == null) {
      _module_pim_NameColumn = new $module_pim_NameColumn();
    }
    return _module_pim_NameColumn;
  }

  $module_pim_PriceColumn _module_pim_PriceColumn;
  $module_pim_PriceColumn get module_pim_PriceColumn {
    if (_module_pim_PriceColumn == null) {
      _module_pim_PriceColumn = new $module_pim_PriceColumn();
    }
    return _module_pim_PriceColumn;
  }

  $module_pim_ShortDescriptionColumn _module_pim_ShortDescriptionColumn;
  $module_pim_ShortDescriptionColumn get module_pim_ShortDescriptionColumn {
    if (_module_pim_ShortDescriptionColumn == null) {
      _module_pim_ShortDescriptionColumn =
          new $module_pim_ShortDescriptionColumn();
    }
    return _module_pim_ShortDescriptionColumn;
  }

  $module_pim_PimApi _module_pim_PimApi;
  $module_pim_PimApi get module_pim_PimApi {
    if (_module_pim_PimApi == null) {
      _module_pim_PimApi = new $module_pim_PimApi();
    }
    return _module_pim_PimApi;
  }

  $module_pim_SimpleProduct _module_pim_SimpleProduct;
  $module_pim_SimpleProduct get module_pim_SimpleProduct {
    if (_module_pim_SimpleProduct == null) {
      _module_pim_SimpleProduct = new $module_pim_SimpleProduct();
    }
    return _module_pim_SimpleProduct;
  }

  $module_snippets_CmsPagesApi _module_snippets_CmsPagesApi;
  $module_snippets_CmsPagesApi get module_snippets_CmsPagesApi {
    if (_module_snippets_CmsPagesApi == null) {
      _module_snippets_CmsPagesApi = new $module_snippets_CmsPagesApi();
    }
    return _module_snippets_CmsPagesApi;
  }

  $module_snippets_CmsWidgetConfig _module_snippets_CmsWidgetConfig;
  $module_snippets_CmsWidgetConfig get module_snippets_CmsWidgetConfig {
    if (_module_snippets_CmsWidgetConfig == null) {
      _module_snippets_CmsWidgetConfig = new $module_snippets_CmsWidgetConfig();
    }
    return _module_snippets_CmsWidgetConfig;
  }

  $module_snippets_HeadingConfig _module_snippets_HeadingConfig;
  $module_snippets_HeadingConfig get module_snippets_HeadingConfig {
    if (_module_snippets_HeadingConfig == null) {
      _module_snippets_HeadingConfig = new $module_snippets_HeadingConfig();
    }
    return _module_snippets_HeadingConfig;
  }

  $module_snippets_ImageConfig _module_snippets_ImageConfig;
  $module_snippets_ImageConfig get module_snippets_ImageConfig {
    if (_module_snippets_ImageConfig == null) {
      _module_snippets_ImageConfig = new $module_snippets_ImageConfig();
    }
    return _module_snippets_ImageConfig;
  }

  $module_snippets_HtmlConfig _module_snippets_HtmlConfig;
  $module_snippets_HtmlConfig get module_snippets_HtmlConfig {
    if (_module_snippets_HtmlConfig == null) {
      _module_snippets_HtmlConfig = new $module_snippets_HtmlConfig();
    }
    return _module_snippets_HtmlConfig;
  }

  $module_snippets_ParagraphConfig _module_snippets_ParagraphConfig;
  $module_snippets_ParagraphConfig get module_snippets_ParagraphConfig {
    if (_module_snippets_ParagraphConfig == null) {
      _module_snippets_ParagraphConfig = new $module_snippets_ParagraphConfig();
    }
    return _module_snippets_ParagraphConfig;
  }

  $module_snippets_Page _module_snippets_Page;
  $module_snippets_Page get module_snippets_Page {
    if (_module_snippets_Page == null) {
      _module_snippets_Page = new $module_snippets_Page();
    }
    return _module_snippets_Page;
  }

  $module_om_OmApi _module_om_OmApi;
  $module_om_OmApi get module_om_OmApi {
    if (_module_om_OmApi == null) {
      _module_om_OmApi = new $module_om_OmApi();
    }
    return _module_om_OmApi;
  }

  $module_om_OrderItem _module_om_OrderItem;
  $module_om_OrderItem get module_om_OrderItem {
    if (_module_om_OrderItem == null) {
      _module_om_OrderItem = new $module_om_OrderItem();
    }
    return _module_om_OrderItem;
  }

  $module_om_Order _module_om_Order;
  $module_om_Order get module_om_Order {
    if (_module_om_Order == null) {
      _module_om_Order = new $module_om_Order();
    }
    return _module_om_Order;
  }

  Map<String, module_core.CRUDApi>
      get instancesOfmodule_core_CRUDApi_module_core_Entity_ {
    return {
      "module_pim.PimApi": new $module_pim_PimApi(),
      "module_snippets.CmsPagesApi": new $module_snippets_CmsPagesApi(),
      "module_om.OmApi": new $module_om_OmApi(),
    };
  }
}

$ObjectManager $om = new $ObjectManager();
//generated in 200ms
