library mocule_om;

import 'package:module_core/model.dart';
import 'package:module_pim/model.dart';
import 'package:swift_composer/swift_composer.dart';

@ComposeSubtypes
abstract class DeliveryMethod extends Entity {
  String get fullName;
  int getDeliveryCost(Order order);
}

@ComposeSubtypes
abstract class PaymentMethod extends Entity {
  String get fullName;

  //TODO http method etc.
  String get paymentActionUrl;
  Map<String, String> getPaymentActionFields(Order order, String returnUrl);
}

@Compose
abstract class OrderItem extends Entity {

  @Field
  ProductBase product;

  @Field
  int quantity;

  String get itemPriceFormatted {
    return '\$' + (quantity * product.price / 100).toStringAsFixed(2);
  }

  String get productPriceFormatted {
    return '\$' + (product.price / 100).toStringAsFixed(2);
  }

  //Map<String, ConfigurableField> fields;
  /*int price => {
    //set fields
    //product->getPrice()
    //quantity
  }*/
}


abstract class Order extends Entity {

  @Field
  String name;

  @Field
  List<OrderItem> items = [];

  @Field
  DeliveryMethod deliveryMethod;

  @Field
  PaymentMethod paymentMethod;

  @Factory
  OrderItem createOrderItem();

  void addItem (ProductBase p, int quantity) {
    var oi = createOrderItem();
    oi.product = p;
    oi.quantity = quantity;
    items.add(oi);
    print(items);
  }

  int get totalQuantity {
    int ret = 0;
    items.forEach((e) {
      ret += e.quantity;
    });
    return ret;
  }

  int get totalPrice {
    int ret = 0;
    items.forEach((e) {
      ret += e.quantity * e.product.price;
    });
    return ret;
  }

  String get totalPriceFormatted {
    return '\$' + (totalPrice / 100).toStringAsFixed(2);
  }

}
