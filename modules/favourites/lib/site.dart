library mocule_favourites;

import 'package:module_core/site.dart';
import 'package:module_pim/site.dart';

import 'dart:async';
import 'dart:html';
import 'dart:convert';

import 'model.dart';
export 'model.dart';

@Compose
abstract class FavouritesList {

  @Factory
  Favourites favouritesFactory();

  Favourites _favouritesCache;
  Favourites get favourites {
    if (_favouritesCache == null) {
      _favouritesCache = favouritesFactory();
      if (window.localStorage.containsKey('favourites')) {
        _favouritesCache.fromJson(json.decode(window.localStorage['favourites']));
      }
    }
    return _favouritesCache;
  }

  void save() {
    window.localStorage['favourites'] = json.encode(favourites.toJson());
  }
}


abstract class AddToFavouritesButton extends Widget {

  @Require
  ProductBase product;

  @Inject
  Layout get layout;

  @Inject
  FavouritesList get favouritesList;

  void addToFavourites() {
    favouritesList.favourites.addProduct(product);
    favouritesList.save();

    var itemElement = element;
    while (itemElement != null && !itemElement.classes.contains('item')) {
      itemElement = itemElement.parent;
    }
    if (itemElement != null) {
      var badgeRect = layout.element.querySelector('#favourites').getBoundingClientRect();
      var itemRect = itemElement.getBoundingClientRect();

      Element clone = itemElement.clone(true);// as Element;
      clone.style
        ..transition = 'top 1s ease, left 1s ease, width 1s ease, height 1s ease'
        ..position = 'fixed'
        ..overflow = 'hidden'
        ..left = (itemRect.left).round().toString() + 'px'
        ..top = (itemRect.top).round().toString() + 'px'
        ..width = (itemRect.width).round().toString() + 'px'
        ..height = (itemRect.height).round().toString() + 'px';

      document.body.append(clone);

      new Timer(const Duration(milliseconds: 10), (){
        clone.style
          ..left = (badgeRect.left).round().toString() + 'px'
          ..top = (badgeRect.top).round().toString() + 'px'
          ..width = (badgeRect.width).round().toString() + 'px'
          ..height = (badgeRect.height).round().toString() + 'px';
      });

      new Timer(const Duration(milliseconds: 1000), (){
        clone.remove();
      });
    }
  }

  void removeFromFavourites() {
    favouritesList.favourites.removeProduct(product);
    favouritesList.save();
  }

  void updateState() {
    print('UPDATING STATE');
    element.classes.toggle('remove', favouritesList.favourites.containsProduct(product));
    element.title =
      favouritesList.favourites.containsProduct(product) ? 'Remove from Favourites' : 'Add to Favourites';
    element.querySelector('.title').text =
      favouritesList.favourites.containsProduct(product) ? 'Remove from Favourites' : 'Add to Favourites';

  }

  void init() {
    super.init();
    element.onClick.listen((e){
      e.preventDefault();
      if (favouritesList.favourites.containsProduct(product)) {
        removeFromFavourites();
      } else {
        addToFavourites();
      }
      updateState();
    });
    updateState();
  }

  String get html => """
    <a href="#" class="btn" title="">
      <span class="add-icon icon-heart"></span>
      <span class="remove-icon icon-heart-full"></span>
      <span class="title"></span>
    </a>
  """;

}


abstract class AddToProductWidget extends TypePlugin<ProductWidget> {

  @Factory
  AddToFavouritesButton newButton(ProductBase product);

  @MethodPlugin
  void afterInit(){
    parent.buttons.append(newButton(parent.item));
  }
}

abstract class AddToShowProductWidget extends TypePlugin<ShowProductWidget> {

  @Factory
  AddToFavouritesButton newButton(ProductBase product);

  @MethodPlugin
  void afterInit(){
    parent.buttons.append(newButton(parent.item));
  }
}

abstract class FavouritesAction extends SiteAction {

  String get title => "Favourites";

  @Inject
  FavouritesWidget get favourites;

  void push() {
    app.layout.modal.setChild(favourites);
    document.body.classes.add('modal');
    app.layout.modal.element.parent.parent.classes.add('side');
    app.layout.modal.element.parent.parent.classes.add('show');
    favourites.regenerateItems();
  }

  void pop() {
    app.layout.modal.element.parent.parent.classes.remove('show');
    app.layout.modal.element.parent.parent.classes.remove('side');
    document.body.classes.remove('modal');
  }
}

abstract class FavouritesWidget extends Widget {

  @Inject
  FavouritesList get favouritesList;

  @Create
  Slot rows;

  @Factory
  FavouriteItemWidget createFavouriteItemWidget(ProductBase product);

  @Factory
  EmptyFavouritesWidget createEmptyFavouritesWidget();

  void regenerateItems() {
    rows.clear();
    if (favouritesList.favourites.list.isEmpty) {
      rows.append(createEmptyFavouritesWidget());
    } else {
      favouritesList.favourites.list.forEach((i) {
        rows.append(createFavouriteItemWidget(i));
      });
    }
    element.querySelector('#addFavouritesToCart').classes.toggle(
        'show',
        favouritesList.favourites.list.isNotEmpty
    );
  }

  void init() {
    print('CART INIT');
    regenerateItems();
  }


  String get html => """
  <div>
    <h1 class="modal-title">Your Favourites</h1>
    <div class="modal-body cart-items">
      <div class="cart-header">
        Product
        <div class="subtotal">Subtotal</div>
        <div class="qty">Qty</div>
      </div>
      <div data-slot="rows">
      </div>
    </div>
    <div class="modal-actions">
      <a id="addFavouritesToCart" data-type="local" href="#" class="btn primary">
        Add all to Cart
      </a>
    </div>
  </div>
  """;
}

abstract class EmptyFavouritesWidget extends Widget {
  String get html => """
  <div>
    Your favourites list is empty.
  </div>
  """;
}

abstract class FavouriteItemWidget extends Widget {

  @Require
  ProductBase product;

  @Inject
  FavouritesList get favouritesList;

  @Inject
  FavouritesWidget get favouritesWidget;

  void remove() {
    favouritesList.favourites.removeProduct(product);
    favouritesWidget.regenerateItems();
    favouritesList.save();
  }

  void init(){
    super.init();
    element.querySelector('.remove').onClick.listen((e){
      remove();
    });
  }

  String get html => """
  <div class="cart-item"><div class="subtotal"></div>
  
    <img width="64" height="64" src="${product.image?.thumbUrl}" alt="${product.name}"/>
    <div class="details">
      <h3>${product.name}</h3>
    </div>
    <div class="qty">
      <button class="remove"><span class="icon-minus"></span></button>
    </div>
  </div>
  """;
}