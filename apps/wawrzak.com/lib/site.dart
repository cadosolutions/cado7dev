import 'dart:html';
import 'dart:convert';
import 'dart:math';
import 'dart:async';
import 'dart:js' as js;

import 'package:module_core/site.dart';

abstract class HomeAction extends SiteAction {
  @Create
  HomeWidget widget;

  String get title => 'Jakub Wawrzak';

  void push() {
    layout.body.setChild(widget);
  }
}

abstract class HomeWidget extends Widget {}

@ComposeSubtypes
abstract class GalleryAction extends SiteAction {

    void putIntoCarousel(String code, Slot slot) {

      String path = code[0].toUpperCase() + code.substring(1);

      app.loadCustomData(code, (Map works){
        themes[code] = new Map<int, String>();
        subs[code] = new Map<int, String>();
        int i = 0;
        int total = 0;
        works.forEach((var key, var work){
          total = total + work['slides'].length;
        });
        slot.element.innerHtml = '';
        works.forEach((var key, var work){
          work['slides'].asMap().forEach((int id, var slide){

            var indicator = new LIElement();
            indicator.dataset['target'] = '#' + code;
            indicator.dataset['slideTo'] = i.toString();
            //querySelector("#" + code + " .carousel-indicators").append(indicator);

            var item = new DivElement();
            item.className = 'carousel-item';

            if (slide.containsKey('image')) {
              //image slide
              item.append(new ImageElement(src:slide['image']));
              item.append(
                new DivElement()
                  ..className = 'carousel-caption d-none d-md-block'
                  ..text = slide['caption']
              );
              item.classes.add('fit-' + slide['fit']);
              subs[code][i] = work['name'] + ' - ' + id.toString() + ' of ' +  (work['slides'].asMap().length - 1).toString();
            } else {
              //text slide

              var playA = new AnchorElement(href:'/' + path + '/' + (i + 1).toString())
                ..text = 'play »';
              router.localiseAnchor(playA);

              item.append(new DivElement()..className='title-item container'..appendHtml(
                '<h1>' + slide['title'] + '</h1>' + slide['body']
              )..append(
                new DivElement()
                ..className = 'start-link'
                ..append(playA)
              ));


              indicator.className = 'title';
              subs[code][i] = '';
            }

            slot.element.append(item);

            themes[code][i] = slide['theme'];
            if (i == int.parse(args[0])) {
              indicator.classes.add('active');
              item.classes.add('active');
              querySelector('body').className = themes[code][i];
              //print(subs[code][i]);
              slot.element.parent.querySelector(".carousel-indicators").innerHtml = subs[code][i];
              int prev = i > 0 ? i - 1 : (total - 1);
              int next = i < (total - 1) ? i + 1 : 0;
              (slot.element.parent.querySelector(".carousel-control-prev") as AnchorElement).href = '/' + path + '/' + prev.toString();
              (slot.element.parent.querySelector(".carousel-control-next") as AnchorElement).href = '/' + path + '/' + next.toString();
              router.localiseAnchor(slot.element.parent.querySelector(".carousel-control-prev"));
              router.localiseAnchor(slot.element.parent.querySelector(".carousel-control-next"));
            }
            i++;

          });
        });
      });
    }
}

abstract class WorksAction extends GalleryAction {
  @Create
  WorksWidget widget;

  String get title => 'Works | Jakub Wawrzak';


  void push() {
    putIntoCarousel('works', widget.worksCarousel);
    layout.body.setChild(widget);
  }
}

abstract class WorksWidget extends Widget {
  @Create
  Slot worksCarousel;
}

abstract class CommisionedAction extends GalleryAction {

  @Create
  CommisionedWidget widget;

  String get title => 'Commisioned | Jakub Wawrzak';

  void push() {
    putIntoCarousel('commisioned', widget.commisionedCarousel);
    layout.body.setChild(widget);
  }
}

abstract class CommisionedWidget extends Widget {
    @Create
    Slot commisionedCarousel;
}

abstract class AwardsAction extends SiteAction {
  @Create
  AwardsWidget widget;

  String get title => 'Jakub Wawrzak';

  void push() {
    layout.body.setChild(widget);

    querySelector('body').className = 'light';
    querySelector('nav .navbar-toggler').className = 'navbar-toggler collapsed';
    querySelector('nav .navbar-collapse').className = 'navbar-collapse collapse';

    app.loadCustomData('settings', (Map settings){
     settings['awards'].forEach((var t){
       widget.awardsContent.element.append(new LIElement()..innerHtml=t);
     });
   });
  }
}

abstract class AwardsWidget extends Widget {
  @Create
  Slot awardsContent;
}

abstract class ExhibitionsAction extends SiteAction {
  @Create
  ExhibitionsWidget widget;

  String get title => 'Jakub Wawrzak';

  void push() {
    layout.body.setChild(widget);
    querySelector('body').className = 'light';
    querySelector('nav .navbar-toggler').className = 'navbar-toggler collapsed';
    querySelector('nav .navbar-collapse').className = 'navbar-collapse collapse';

    app.loadCustomData('settings', (Map settings){
      widget.exhibitionsContent.element.appendHtml('<h1>Solo Exhibitions</h1>');
      settings['exhibitions-solo'].forEach((var t){
        widget.exhibitionsContent.element.append(new LIElement()..text=t);
      });
      widget.exhibitionsContent.element.appendHtml('<h1>Group Exhibitions</h1>');
      settings['exhibitions-group'].forEach((var t){
        widget.exhibitionsContent.element.append(new LIElement()..text=t);
      });
    });
  }
}

abstract class ExhibitionsWidget extends Widget {
  @Create
  Slot exhibitionsContent;
}

abstract class AboutAction extends SiteAction {
  @Create
  AboutWidget widget;

  String get title => 'Jakub Wawrzak';

  void push() {
    layout.body.setChild(widget);
    querySelector('body').className = 'light';
    querySelector('nav .navbar-toggler').className = 'navbar-toggler collapsed';
    querySelector('nav .navbar-collapse').className = 'navbar-collapse collapse';
    app.loadCustomData('settings', (Map settings){
      settings['about'].forEach((var t){
        widget.aboutContent.element.append(new ParagraphElement()..text=t);
      });
    });
  }
}

abstract class AboutWidget extends Widget {
  @Create
  Slot aboutContent;
}

var themes = new Map<String, Map<int,String>>();
var subs = new Map<String, Map<int,String>>();

abstract class PrepareData extends TypePlugin<Application> {

  @Inject
  Router get router;

  @MethodPlugin
  void afterRun()
  {
    for (var code in ['works', 'commisioned']) {
      parent.loadCustomData(code, (Map works){
        themes[code] = new Map<int, String>();
        subs[code] = new Map<int, String>();
        int i = 0;
        works.forEach((var key, var work){
          String path = code[0].toUpperCase() + code.substring(1);
          querySelector('#' + code + 'Dropdown').append(
            new AnchorElement(href: '/' + path + '/' + i.toString())
            ..className = 'dropdown-item'
            ..text = work['name']
            ..onClick.listen((e){
              e.preventDefault();
              querySelector('nav .navbar-toggler').className = 'navbar-toggler collapsed';
              querySelector('nav .navbar-collapse').className = 'navbar-collapse collapse';
              querySelectorAll('.dropdown-menu').forEach((e){e.classes.remove('show');});
              querySelector('nav .navbar-collapse').className = 'navbar-collapse collapse';
              router.goToUrl(Uri.parse((e.target as AnchorElement).href).path);
            })
          );

          i = i + work['slides'].length;
        });
      });
    }
  }
}
