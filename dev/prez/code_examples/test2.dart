class User
{
  String firstname = "";
  String lastname = "";

  User (this.firstname, this.lastname);

  String get fullname {
    return "$firstname $lastname";
  }
}

Map<String, User> users = {
  "jan": new User("Jan", "Kowalski"),
  "karol": new User("Karol", "Kowalski"),
  "edek": new User("Edek", "Kowalski")
};

void main(List<String> args) {
  users.forEach((String key, User user){
    String f = user.fullname;
    print(f);
  });
}
