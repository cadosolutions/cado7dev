#!/usr/bin/env dart
// Copyright (c) CadoSolutions. Please see LICENSE file for details.

import "dart:async";
import 'dart:io';
import 'dart:convert';
import 'dart:math';
import 'package:mime_type/mime_type.dart';

class Service {
  int localPort;
  String root;
  String command;
  List<String> args;
  Process process;
  String get name => '[{$root} {$command}[${process != null ? process.pid : 'null'}]]';

  bool running = false;
  Service (this.localPort, this.root, this.command, this.args);

  void printOut(data) {
    LineSplitter.split(data).forEach((line) {
      if (!line.endsWith(utf8.decode([27, 91, 50, 75]))) {
        print('${name}: ' + line);
        //List<int> bytes = utf8.encode(line); print(bytes);
      }
    });
  }

  void forceRestart() async {
    if (running) process.kill();
    sleep(const Duration(seconds:2));
    if (running) print('${name}!: failed to kill ');
    restartIfNotRunning();
  }

  void restartIfNotRunning() async {
    if (!running) {
      print('${name}!: starting: $command ${args.join(' ')}');
      process = await Process.start(command, args, workingDirectory:root);
      process.stderr.transform(utf8.decoder).listen(printOut);
      process.stdout.transform(utf8.decoder).listen(printOut);
      process.exitCode.then((code){
        running = false;
        print('${name}!: ended with code ${code}');
      });
      running = true;
      print('${name}!: started process');
    }
  }

  handleRequest(String newPath, HttpRequest request) async {
    await restartIfNotRunning();
    var proxy = new HttpClient();

    HttpClientRequest proxyRequest;
    int tries = 20;
    while (tries-- > 0) {
      try {
        //print(newPath + (request.uri.query.isEmpty ? '' : '?' + request.uri.query));
        proxyRequest = await proxy.open(request.method, 'localhost', localPort, newPath + (request.uri.query.isEmpty ? '' : '?' + request.uri.query));
        break;
      } catch (e) {
        print('${name}!: non responsive waiting...');
        sleep(const Duration(seconds:1));
      }
    }

    //print(request.headers);
    //print(proxyRequest.headers);
    request.headers.forEach((name, values){
      if (name != 'host') {
        values.forEach((value){
          //print('$name, $value');
          proxyRequest.headers.set(name, value);
        });
      }
    });
    //print(proxyRequest.headers);

    HttpClientResponse proxyResponse =await proxyRequest.addStream(request).then((_) => proxyRequest.close());
    //request.pipe(proxyRequest);

    //await proxyRequest.close();
    proxyResponse.headers.forEach((name, values){
      values.forEach((value){
        //print('$name, $value');
        request.response.headers.set(name, value);
      });
    });
    request.response.statusCode = proxyResponse.statusCode;
    request.response.headers.add('Access-Control-Allow-Origin', '*');
    /* proxyResponse.transform(utf8.decoder)
      .transform(const LineSplitter())
      .listen((line) {
        request.response.writeln(line);
      });
    await proxyResponse.drain();*/
    //await proxyResponse.pipe(request.response).transform(new StreamReplaceTransformer()); //
    //print(request.response.toString());
    //new LineSplitter();

    //transform(utf8.decoder).
    //.transform(utf8.encoder)

    if (newPath.endsWith('dwds/src/injected/client.js')){
      String result = "";
      await for (var contents in proxyResponse.transform(Utf8Decoder())) {
        result += contents.replaceAllMapped(new RegExp(r'/\$sseHandler\?'), (Match m) {
          return 'http://localhost:' + localPort.toString() + '/\$sseHandler?';
        });
      }
      request.response.headers.contentLength = result.length;
      request.response.write(result);
      request.response.close();
    } else {
      await proxyResponse/*.transform(new StreamTransformer.fromHandlers(handleData: handleData))*/.pipe(request.response);
    }
  }

}

class BuiltService extends Service {

  BuiltService (root) : super(0, root, 'null', []) {}

  handleRequest(String newPath, HttpRequest request) async {
    print(newPath);

    if (FileSystemEntity.typeSync(root + newPath) != FileSystemEntityType.file) {
      newPath = '/index.html';
    }
    var file = new File(root + newPath);
    print(file.path);
    if (file.existsSync()) {
      print(file.openRead());
      request.response.headers.contentType = ContentType.parse(mime(newPath.toLowerCase()) ?? 'application/octet-stream');
      request.response.statusCode = 200;
      await file.openRead().pipe(request.response);
    } else {
      request.response.headers.contentType = ContentType.text;
      request.response.statusCode = 404;
      request.response.write('404');
    }
  }

}

class SiteService extends Service {

  Random random;

  SiteService (root, port) : super(port, root, 'webdev', ['serve', 'web:' + port.toString()]) {
    random = Random.secure();
  }

  handleRequest(String newPath, HttpRequest request) async {

    if (
      request.headers['cache-control'] != null
      && request.headers['cache-control'].contains('no-cache')
      && request.headers['pragma'] != null
      && request.headers['pragma'].contains('no-cache')
      //&& request.headers['referer'] == null
    ) {
      //print(request.headers['referer']);
      var mainFile = new File(root + '/web/main.dart');
      var mainFileModified = await mainFile.lastModified();
      Duration difference = (new DateTime.now()).difference(mainFileModified);
      if (difference.inSeconds > 1) {
        print('FORCE FULL REFRESH');

        //TODO pub get etc
        String content = await mainFile.readAsString();
        var hash = random.nextInt(999999999).toString().padLeft(9, '0');
        if (LineSplitter
            .split(content)
            .last
            .startsWith('//random:')) {
          content = content.substring(0, content.length - 9);
          content += '$hash';
        } else {
          content += '\n//random:$hash';
        }
        await mainFile.writeAsString(content);
      }
    }

    List<String> extWhitelist = ['.js', '.css', '.map', '.svg', '.digests', '\$requireDigestsPath'];
    List<String> prefixesWhitelist = ['/packages'];

    String path = root + '/web' + newPath;
    bool whitelisted = false;
    extWhitelist.forEach((ext){
      if (path.endsWith(ext)) {
        whitelisted = true;
      }
    });
    prefixesWhitelist.forEach((prefix){
      if (newPath.startsWith(prefix)) {
        whitelisted = true;
      }
    });
    if (!whitelisted && FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound) {
      newPath = '/';
    }
    await super.handleRequest(newPath, request);
  }

}


class WatchedService extends Service {

  Service watched;

  WatchedService (port, root, command, args) : super(port, root, 'pub', ['run', 'build_runner', 'watch']) {
    //builder = new Service(0, root, 'pub', ['run', 'build_runner', 'watch']);
    watched = new Service(port, root, command, args);
  }

  handleRequest(String newPath, HttpRequest request) async {
    await restartIfNotRunning();
    return watched.handleRequest(newPath, request);
  }

  void printOut(data) {
    LineSplitter.split(data).forEach((line) {
      if (line.contains('Succeeded after')) {
        print('${name}!: RESTARTING SUB');
        watched.forceRestart();
      }
    });
    super.printOut(data);
  }

}


class DevServer{

  Map<String, Service> services;
  int port = 7000;

  DevServer(this.services);


  handleRequest(HttpRequest request) async {

      int start = new DateTime.now().millisecondsSinceEpoch;
      String domain;

      if (request.requestedUri.host.endsWith('.localhost')) {
        domain = request.requestedUri.host.substring(0, request.requestedUri.host.length - 10);
        try {
          await services[domain].handleRequest(request.uri.path, request);
        } catch (e) {
          request.response.headers.contentType = ContentType.text;
          request.response.statusCode = 503;
          request.response.write(e.toString());
        }
      } else {
        request.response.headers.contentType = ContentType.html;
        request.response.statusCode = 200;

        request.response.write('<html>');
        request.response.write('<head><title>Cado 7 dev server</title></head>');
        request.response.write('<body>');
        request.response.write('<p>dev server services:</p>');
        request.response.write('<ul>');
        services.forEach((domain, service){
          request.response.write('<li><a href="http://' + domain + '.localhost:' + port.toString() + '/">' + domain + '</a></li>');
        });
        request.response.write('</ul>');
        request.response.write('<p>cado7 powered</p>');
        request.response.write('</body>');
        request.response.write('</html>');
      }
      request.response.close();


      print("${request.method} ${domain}${request.uri} ${request.response.statusCode} [${new DateTime.now().millisecondsSinceEpoch - start}ms]");
  }

  Future serve() async {
    print("starting development server...");
    HttpServer server = await HttpServer.bind(InternetAddress.anyIPv4, port);
    print("listening on http://localhost:7000");
    server.listen(handleRequest);
  }
}

Future main () async {

  var services = new Map<String, Service>();
  int port = 8080;
  for (var fileOrDir in (new Directory('./apps')).listSync()) {
    if (fileOrDir is Directory) {
      String name = fileOrDir.path.split('/').last;
      print('registering: http://' + name + '.localhost:7000/');
      if (new File(fileOrDir.path + '/pubspec.yaml').existsSync()) {
        services[name] = new SiteService(fileOrDir.path, port++);
      } else {
        services[name] = new BuiltService(fileOrDir.path);
      }
    }
  }
  for (var fileOrDir in (new Directory('./services')).listSync()) {
    if (fileOrDir is Directory) {
      String name = fileOrDir.path.split('/').last;
      print('registering: http://' + name + '.localhost:7000/');
      Map config = json.decode(await new File(fileOrDir.path + '/config.json').readAsString());
      services[name] = new WatchedService(config['port'], fileOrDir.path, 'dart', ['bin/server.dart']);
    }
  }
  var server = new DevServer(services);
  await server.serve();
}
