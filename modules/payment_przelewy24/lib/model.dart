library module_payment_przelewy24;

import 'package:module_core/model.dart';
import 'package:module_core/site.dart';
import 'package:module_om/model.dart';
import 'package:swift_composer/swift_composer.dart';

import 'dart:html';
import 'dart:convert';

import 'package:crypto/crypto.dart';

abstract class Przelewy24PaymentMethod extends PaymentMethod {

  String get fullName => "Przelewy 24";

  @Inject
  Site get site;

  String get paymentActionUrl => 'https://sandbox.przelewy24.pl/trnDirect';

  Map<String, String> getPaymentActionFields(Order order, String returnUrl) {
    String sessionId = 'order.' + order.id.toString();
    String posId = '107381';
    String amount = order.totalPrice.toString();
    String currency = 'PLN';
    String hash = '6a301633ed73dffc';
    return {
      'p24_url_return' : returnUrl,
      'p24_session_id' : sessionId,
      'p24_merchant_id' : posId,
      'p24_pos_id' : posId,
      'p24_amount' : amount,
      'p24_currency' : currency,
      'p24_description' : 'payemnt for order ' + order.id.toString(),
      'p24_client' : 'Jan Kowalski',
      'p24_address' : 'ul. Polska 33/33',
      'p24_zip' : '66-777',
      'p24_city' : 'Poznań',
      'p24_country': 'PL',
      'p24_email': 'email@host.pl',
      'p24_language': 'pl',
      'p24_api_version': '3.2',
      'p24_sign': md5.convert(utf8.encode([sessionId, posId, amount, currency, hash].join('|'))).toString()
    };
  }
}
