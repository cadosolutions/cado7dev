// Copyright (c) CadoSolutions. Please see LICENSE file for details.

import 'dart:io';
import 'dart:async';
import 'package:args/args.dart';
import 'package:http_server/http_server.dart';
import "package:dart_amqp/dart_amqp.dart";
import "package:swift_ids/swift_ids.dart";

Future main(List<String> args) async {

  var parser = new ArgParser();
  parser.addOption('port', abbr:'p', help: 'Port to listen on', defaultsTo: '7201');
  parser.addFlag('help', abbr:'h', help: 'Prints help');
  parser.addOption('dir', abbr:'d', help: 'storage directory', defaultsTo: 'files');

  var arguments = parser.parse(args);
  if (arguments['help']) {
    print(parser.usage);
    return;
  }

  var dir = new Directory(arguments['dir']);
  //var counter = new File(arguments['dir'] . 'counter');
  //var sink = file.openWrite();
  //String contents = await file.readAsString();


  //dir.re
  var uploader = new Uploader(dir, int.parse(arguments['port']));
  //uploader.fileCount =
  await uploader.serve();
}

class Uploader {

  int port;
  int fileCount = 0;
  Directory dir;

  Uploader(this.dir, this.port);

  Future<int> notifyProcessor(String filePath) async {
      Client client = new Client();
      Channel channel = await client.channel();
      Queue queue = await channel.queue('uploaded');
      queue.publish(filePath);
      //print(" [x] Sent 'Hello World!'");
      int ret = queue.messageCount;
      client.close();
      return ret;
  }

  String pathFromUid(String uid) {
    String path = '';
    var letters = uid.padLeft(6,'0').split('');
    var i = 0;
    for (; i < letters.length - 2; i += 2) {
      path = path + '/' + letters.sublist(i, i+2).join('');
    }
    return path + '/' + letters.sublist(i, letters.length).join('');
  }

  Future saveFile(HttpBodyFileUpload file, fileCount) async {

    String ext = getExt(file.filename);
    String path = pathFromUid(Uid.encodeInt(fileCount));

    new Directory(dir.path + path).parent.createSync(recursive: true);
    final fw = new File(dir.path + path + '.' + ext);
    await fw.writeAsBytes(file.content, mode: FileMode.WRITE);

    notifyProcessor(path + '.' + ext);
  }

  Map _fileTypes = const {
    'jpe': 'jpg',
    'jpeg': 'jpg',
    'jpg': 'jpg',
    'png': 'png'
  };

  String getExt(String fileName) {
    int lastDot = fileName.lastIndexOf('.', fileName.length - 1);
    if (lastDot != -1) {
      String extension = fileName.substring(lastDot + 1);
      return _fileTypes[extension];
    } else
      return null;
  }

  Future serve() async {
    print("starting server...");
    HttpServer server = await HttpServer.bind(InternetAddress.ANY_IP_V4, port);


    server.listen((HttpRequest request) async {
      int start = new DateTime.now().millisecondsSinceEpoch;
      print("${request.method} ${request.uri} ${request.response.statusCode} [${new DateTime.now().millisecondsSinceEpoch - start}ms]");


      request.response.headers.set('Access-Control-Allow-Origin', '*');
      request.response.headers.contentType = ContentType.JSON;

      if (request.method == "POST") {
        var body = await HttpBodyHandler.processRequest(request);
        if (body.type == 'form') {
          print(body.body);
          String token = body.body['token'];
          HttpBodyFileUpload file = body.body['file'];
          fileCount ++;
          saveFile(file, fileCount);
          request.response.write('"http://files.swift.localhost/' + pathFromUid(Uid.encodeInt(fileCount)) + '.json"');
        }
      }

      request.response.close();
      print("${request.method} ${request.uri} ${request.response.statusCode} [${new DateTime.now().millisecondsSinceEpoch - start}ms]");
    });

    print("listening on port $port" );
  }
}
