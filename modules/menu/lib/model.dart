import 'package:swift_composer/swift_composer.dart';
import 'package:module_core/model.dart';

abstract class MenuItem extends Entity {

  @Field
  MenuItem parent;

  @Field
  String name;

  @Field
  String title;


}
