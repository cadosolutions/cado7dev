import 'model.dart';
export 'model.dart';

import "package:c7server/c7server.dart";
import "package:swift_ids/swift_ids.dart";

import 'package:mongo_dart/mongo_dart.dart';

class SaveStatus {
  Id id;
  bool success;
  List<String> messages;
}

@ComposeSubtypes
abstract class Repository<T extends Entity> {

  @Inject
  Server get server;

  @InjectInstances
  Map<String, T> get emptyInstances;
  @InjectInstances
  Map<String, Repository> get relatedRepositories;

  String get collectionName => "";


  @SubtypeFactory
  T instanceCreator(String className);

  Future<T> toEntity(item) async {
    print(item);
    String className = item['classes'][0];
    var ret = instanceCreator(className);
    ret.id = new Id(item['_id'].toInt());
    ret.fieldsFromJson(item);
    var relations = new Map<String, Entity>();
    for (var ek in ret.relations().keys) {
      for (var rk in relatedRepositories.keys) {
        if (relatedRepositories[rk].emptyInstances.containsKey(ret.relations()[ek])){
          if (item['_' + ek] != null) {
            relations[ek] = await relatedRepositories[rk].getById(new Id(item['_' + ek]));
          }
        }
      }
    }
    ret.setRelations(relations);
    return ret;
  }

  Map toJson(item) {
    String className = item['classes'].last;
    item['id'] = new Id(item['_id'].toInt()).toString();
    item.remove('_id');
    for (var field in emptyInstances[className].relations().keys) {
      item[field] = new Id(item['_' + field].toInt()).toString();
      item.remove('_' + field);
    }
    return item;
  }

  Future<List> rawQuery(query) async {
    return await (await server.db).collection(collectionName).find(query).toList();
  }

  void prefetchAggregations (){
    //TODO
    //List aggregations = [];

    //TODO find out collection, preformance tests, promise?
    /*for (String s in ret.relations()) {
      aggregations.add({
        '\$lookup':
          {
            'from': 'users', //TODO
            'localField': '_' + s,
            'foreignField': '_id',
            'as': s
          },
        });
      aggregations.add({
        '\$unwind': {
          'preserveNullAndEmptyArrays': true,
          'path' : '\$' + s
        }
      });
    }
    aggregations.add({
      '\$match' :
        {
          '_id' : id.value
        }
    });

    var test = await (await server.db).collection(collectionName).aggregate(aggregations, cursor: {'batchSize': 1});
    test = test['cursor']['firstBatch'].first;
    print(test);
    for (String s in ret.relations()) {
      if (test.containsKey(s)) {
        test[s]['id'] = new Id(test[s]['_id'].toInt()).toString();
      }
    }
    */
  }

  Future<SaveStatus> saveRaw(Map map) async {
    var status = new SaveStatus();
    var db = (await server.db);
    if (map['id'] == null) {
      var ret = await db.collection('counters').findAndModify(
        query: where.eq('_id', collectionName),
        update: modify.inc('seq', 1),
        returnNew: true
      );
      int id = ret['seq'].round();;
      status.id = new Id(id);
      map['_id'] = status.id.toString();
      map.remove('id');
      await db.collection(collectionName).insert(map);
    } else {
      status.id = new Id.fromString(map['id']);
      map['_id'] = status.id.value;
      map.remove('id');
      await db.collection(collectionName).save(map);
    }
    return status;
  }

  Future<T> getById(Id id) async {
    print('Q');
    print(collectionName);
    var raw = await (await server.db).collection(collectionName).findOne(
      {'_id' : id.value}
    );
    return await toEntity(raw);
  }

}

//TODO enable setting generics in DI
abstract class AppRepository extends Repository<App> {
  String get collectionName => 'apps';
}

abstract class UserRepository extends Repository<User> {
  String get collectionName => 'users';
}


abstract class AppController extends JsonAction {

    Future run() async {
      return await app;
    }

}
