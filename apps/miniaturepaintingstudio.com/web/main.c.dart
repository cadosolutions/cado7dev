// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'main.dart';

// **************************************************************************
// SwiftGenerator
// **************************************************************************

//no interceptor for [AnnotatedWith]
//no interceptor for [Pluggable]
//no interceptor for [TypePlugin]
//interceptor for [PaintingNavWidget]
//interceptor for PaintingNavWidget
//CT[171676771] => PaintingNavConfig[437879844]
//can be singleton: FALSE
//parent: PaintingNavWidget []
//parent: CmsWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $PaintingNavWidget extends PaintingNavWidget implements Pluggable {
  $PaintingNavWidget(config) {
//CT
    this.config = config;
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/PaintingNavWidget.html USED
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/cms/templates/PaintingNavWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/pim/templates/PaintingNavWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/core/templates/PaintingNavWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/PaintingNavWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../libs/swift_composer/templates/PaintingNavWidget.html
  String get html => '''

<div>
  <center>
    <a class="btn btn-primary btn-xl" data-type="local" href="${router.url<PaintingAction>([])}">
      <i class="fa fa-dollar-sign"></i> get a quote
    </a>
    <a class="btn btn-primary btn-xl" data-type="local" href="${router.url<PaintingAction>([])}">
      <i class="fa fa-link"></i> see more examples...
    </a>
  </center>

  <div class="container">
    <hr class="star-dark mb-5">
    <div class="row">
      <div class="col-6 col-sm-4 col-lg-2 mx-auto">
        <a class="level-button" data-type="local" href="${router.url<module_cms.PageAction>([
        'painting-level-1'
      ])}">
          <div class="hover-zoom">
            <img src="/img/level1.jpg" alt="">
          </div>
          Level 1
        </a>
      </div>
      <div class="col-6 col-sm-4 col-lg-2 mx-auto">
        <a class="level-button" data-type="local" href="${router.url<module_cms.PageAction>([
        'painting-level-2'
      ])}">
          <div class="hover-zoom">
            <img src="/img/level2.jpg" alt="">
          </div>
          Level 2
        </a>
      </div>
      <div class="col-6 col-sm-4 col-lg-2 mx-auto">
        <a class="level-button" data-type="local" href="${router.url<module_cms.PageAction>([
        'painting-level-3'
      ])}">
          <div class="hover-zoom">
            <img src="/img/level3.jpg" alt="">
          </div>
          Level 3
        </a>
      </div>
      <div class="col-6 col-sm-4 col-lg-2 mx-auto">
        <a class="level-button" data-type="local" href="${router.url<module_cms.PageAction>([
        'painting-level-4'
      ])}">
          <div class="hover-zoom">
            <img src="/img/level4.jpg" alt="">
          </div>
          Level 4
        </a>
      </div>
      <div class="col-6 col-sm-4 col-lg-2 mx-auto">
        <a class="level-button" data-type="local" href="${router.url<module_cms.PageAction>([
        'painting-level-5'
      ])}">
          <div class="hover-zoom">
            <img src="/img/level5.jpg" alt="">
          </div>
          Level 5
        </a>
      </div>
    </div>
    <div class="row">
      <div class="col-6 col-sm-4 col-lg-2 mx-auto">
        <a class="popup-show level-button" data-type="local" href="${router.url<module_cms.PageAction>([
        'assembling'
      ])}">
          <div class="hover-zoom circle">
            <img src="/img/assembling.jpg" alt="">
          </div>
          Assembling
        </a>
      </div>
      <div class="col-6 col-sm-4 col-lg-2 mx-auto">
        <a class="popup-show level-button" data-type="local" href="${router.url<module_cms.PageAction>([
        'bases-modeling'
      ])}">
          <div class="hover-zoom circle">
            <img src="/img/bases.jpg" alt="">
          </div>
          Bases
        </a>
      </div>
      <div class="col-6 col-sm-4 col-lg-2 mx-auto">
        <a class="popup-show level-button" data-type="local" href="${router.url<module_cms.PageAction>([
        'freehands'
      ])}">
          <div class="hover-zoom circle">
            <img src="/img/freehand.jpg" alt="">
          </div>
          Freehands
        </a>
      </div>
    </div>

  </div>

</div>

''';
}

//interceptor for [HomeAction]
//interceptor for HomeAction
//can be singleton: FALSE
//parent: HomeAction []
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $HomeAction extends HomeAction implements Pluggable {
  $HomeAction(args) {
//List<String>
    this.args = args;
//HomeWidget
//create
    this.home = new $HomeWidget();
  }
  T plugin<T>() {
    return null;
  }

  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "HomeAction";
}

//interceptor for [HomeWidget]
//interceptor for HomeWidget
//can be singleton: TRUE
//parent: HomeWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $HomeWidget extends HomeWidget implements Pluggable {
  $HomeWidget() {
//dynamic
//Slot
//create
    this.blogPosts = new module_core.Slot();
//Future<dynamic>
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
    {
      this.blogPosts.container = this;
      this.blogPosts.element = element.querySelector('#' + 'blogPosts');
      this.blogPosts.element.classes.add('slot');
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.blogPosts.widgets.forEach((widget) {
        this.blogPosts.element.append(widget.element);
      });
    }
  }

  BlogItemWidget createItemWidget(dynamic post) {
//BlogItemWidget
    return new $BlogItemWidget(post);
  }

  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/HomeWidget.html USED
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/cms/templates/HomeWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/pim/templates/HomeWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/core/templates/HomeWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/HomeWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../libs/swift_composer/templates/HomeWidget.html
  String get html => '''
<div>
  <!-- Header -->
  <header class="masthead text-white text-center">
    <div class="container">
      <h1 class="text-uppercase mb-0">ready for battle?</h1>
      <hr class="star-light">
      <h2 class="font-weight-light mb-0">complete miniature painting service</h2>
    </div>
  </header>

  <!-- About Section -->
  <section class="bg-primary text-white" id="about">
    <div class="container">
      <h2 class="text-center text-uppercase text-white mb-0">About</h2>
      <hr class="star-light mb-5">
      <div class="row">
        <div class="col-lg-4 ml-auto">
          <p class="lead">
            Our studio is located in Toruń, Poland.
            Passion for painting started in 2010.
            Being wargamers ourselves, we know how important it is that your models look beautiful.
          </p>
        </div>
        <div class="col-lg-4 mr-auto">
          <p class="lead">
            We paint models for all wargaming systems.
            We take much attention painting all models so they look stunning on the battlefield no matter what painting standard you will chose.
          </p>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <h2 class="text-center text-uppercase text-secondary mb-0">Latest Blog Posts</h2>
      <hr class="star-dark mb-5">
      <div id="blogPosts" class="row"></div>
    </div>
  </section>

</div>

''';
}

//interceptor for [BlogItemWidget]
//interceptor for BlogItemWidget
//can be singleton: FALSE
//parent: BlogItemWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $BlogItemWidget extends BlogItemWidget implements Pluggable {
  $BlogItemWidget(post) {
//dynamic
//dynamic
    this.post = post;
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/BlogItemWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/cms/templates/BlogItemWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/pim/templates/BlogItemWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/core/templates/BlogItemWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/BlogItemWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../libs/swift_composer/templates/BlogItemWidget.html
}

//interceptor for [ContactAction]
//interceptor for ContactAction
//can be singleton: FALSE
//parent: ContactAction []
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $ContactAction extends ContactAction implements Pluggable {
  $ContactAction(args) {
//List<String>
    this.args = args;
//ContactWidget
//create
    this.contact = new $ContactWidget();
  }
  T plugin<T>() {
    return null;
  }

  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "ContactAction";
}

//interceptor for [ContactWidget]
//interceptor for ContactWidget
//can be singleton: TRUE
//parent: ContactWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $ContactWidget extends ContactWidget implements Pluggable {
  $ContactWidget() {
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  module_core.Application get app => $om.module_core_Application;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/ContactWidget.html USED
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/cms/templates/ContactWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/pim/templates/ContactWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/core/templates/ContactWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/ContactWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../libs/swift_composer/templates/ContactWidget.html
  String get html => '''
<div>
  <!-- Contact Section -->
  <section id="contact">
    <div class="container">
      <h2 class="text-center text-uppercase text-secondary mb-0">Contact Us</h2>
      <hr class="star-dark mb-5">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <form name="sentMessage" id="contactForm">
            <div class="control-group">
              <div class="form-group floating-label-form-group controls mb-0 pb-2">
                <label>Name</label>
                <input class="form-control" id="name" name="name" type="text" placeholder="Name" required="required" data-validation-required-message="Please enter your name.">
                <p class="help-block text-danger"></p>
              </div>
            </div>
            <div class="control-group">
              <div class="form-group floating-label-form-group controls mb-0 pb-2">
                <label>Email Address</label>
                <input class="form-control" id="email" name="email" type="email" placeholder="Email Address" required="required" data-validation-required-message="Please enter your email address.">
                <p class="help-block text-danger"></p>
              </div>
            </div>
            <div class="control-group">
              <div class="form-group floating-label-form-group controls mb-0 pb-2">
                <label>Message</label>
                <textarea class="form-control" id="message" name="body" rows="5" placeholder="Message" required="required" data-validation-required-message="Please enter a message."></textarea>
                <p class="help-block text-danger"></p>
              </div>
            </div>
            <br>
            <div id="success"></div>
            <div class="form-group">
              <input type="hidden" name="robot" value="yes" />
              <button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton">Send</button>
            </div>
            <div class="sendingMessage">
              Sending your message..
            </div>
            <div class="successMessage">
              Your message was successfully sent.
              <br/>
              We will reply soon.
            </div>
            <div class="errorMessage">
              There was an error sending your message.
              <br/>
              Please try again later.
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>

''';
}

//interceptor for [PaintingAction]
//interceptor for PaintingAction
//can be singleton: FALSE
//parent: PaintingAction []
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $PaintingAction extends PaintingAction implements Pluggable {
  $PaintingAction(args) {
//List<String>
    this.args = args;
//PaintingWidget
//create
    this.painting = new $PaintingWidget();
  }
  T plugin<T>() {
    return null;
  }

  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "PaintingAction";
}

//interceptor for [PaintingWidget]
//interceptor for PaintingWidget
//can be singleton: TRUE
//parent: PaintingWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $PaintingWidget extends PaintingWidget implements Pluggable {
  $PaintingWidget() {
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/PaintingWidget.html USED
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/cms/templates/PaintingWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/pim/templates/PaintingWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/core/templates/PaintingWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/PaintingWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../libs/swift_composer/templates/PaintingWidget.html
  String get html => '''
<div>
  <!-- Portfolio Grid Section -->
  <section class="portfolio" id="levels">
    <div class="container">
      <h2 class="text-center text-uppercase text-secondary mb-0">Our Painting Service</h2>
      <hr class="star-dark mb-5">
      <div class="row">
        <div class="col-6 col-lg-4 mx-auto">
          <a class="level-button" data-type="local" href="${router.url<module_cms.PageAction>([
        'painting-level-1'
      ])}">
            <div class="hover-zoom">
              <img src="/img/level1.jpg" alt="">
            </div>
            Level 1
          </a>
        </div>
        <div class="col-6 col-lg-4 mx-auto">
          <a class="level-button" data-type="local" href="${router.url<module_cms.PageAction>([
        'painting-level-2'
      ])}">
            <div class="hover-zoom">
              <img src="/img/level2.jpg" alt="">
            </div>
            Level 2
          </a>
        </div>
        <div class="col-6 col-lg-4 mx-auto">
          <a class="level-button" data-type="local" href="${router.url<module_cms.PageAction>([
        'painting-level-3'
      ])}">
            <div class="hover-zoom">
              <img src="/img/level3.jpg" alt="">
            </div>
            Level 3
          </a>
        </div>
        <div class="col-6 col-lg-4 mx-auto">
          <a class="level-button" data-type="local" href="${router.url<module_cms.PageAction>([
        'painting-level-4'
      ])}">
            <div class="hover-zoom">
              <img src="/img/level4.jpg" alt="">
            </div>
            Level 4
          </a>
        </div>
        <div class="col-6 col-lg-4 mx-auto">
          <a class="level-button" data-type="local" href="${router.url<module_cms.PageAction>([
        'painting-level-5'
      ])}">
            <div class="hover-zoom">
              <img src="/img/level5.jpg" alt="">
            </div>
            Level 5
          </a>
        </div>
      </div>
      <div class="row">
        <div class="col-6 col-lg-4 mx-auto">
          <a class="popup-show level-button" data-type="local" href="${router.url<module_cms.PageAction>([
        'assembling'
      ])}">
            <div class="hover-zoom circle">
              <img src="/img/assembling.jpg" alt="">
            </div>
            Assembling
          </a>
        </div>
        <div class="col-6 col-lg-4 mx-auto">
          <a class="popup-show level-button" data-type="local" href="${router.url<module_cms.PageAction>([
        'bases-modeling'
      ])}">
            <div class="hover-zoom circle">
              <img src="/img/bases.jpg" alt="">
            </div>
            Bases
          </a>
        </div>
        <div class="col-6 col-lg-4 mx-auto">
          <a class="popup-show level-button" data-type="local" href="${router.url<module_cms.PageAction>([
        'freehands'
      ])}">
            <div class="hover-zoom circle">
              <img src="/img/freehand.jpg" alt="">
            </div>
            Freehands
          </a>
        </div>
      </div>

    </div>
  </section>

  <section class="portfolio bg-primary text-white" id="portfolio">
    <div class="container">
      <h2 class="text-center text-uppercase text-white mb-0">Portfolio</h2>
      <hr class="star-light mb-5">
      <div class="row">
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
          <a class="portfolio-item d-block mx-auto" target="_blank" href="http://miniaturepaintingstudio.blogspot.com/2019/06/age-of-sigmar-seraphon-army-level-3-and.html">
            <div class="hover-zoom square">
              <img class="img-fluid" src="/img/portfolio/project0.jpg" alt="">
            </div>
          </a>
        </div>
        <!--div class="col-6 col-sm-4 col-md-3 col-lg-2">
          <a class="portfolio-item d-block mx-auto" target="_blank" href="https://miniaturepaintingstudio.blogspot.com/2018/07/wfb-bretonnian-king-louen-leoncoeur.html">
            <div class="hover-zoom square">
              <img class="img-fluid" src="/img/portfolio/project1.jpg" alt="">
            </div>
          </a>
        </div-->
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
          <a class="portfolio-item d-block mx-auto" target="_blank" href="https://www.flickr.com/photos/136752285@N03/albums/72157699323955594">
            <div class="hover-zoom square">
              <img class="img-fluid" src="/img/portfolio/project6.jpg" alt="">
            </div>
          </a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
          <a class="portfolio-item d-block mx-auto" target="_blank" href="https://miniaturepaintingstudio.blogspot.com/2016/11/seraphon-army-commissions.html">
            <div class="hover-zoom square">
              <img class="img-fluid" src="/img/portfolio/project2.jpg" alt="">
            </div>
          </a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
          <a class="portfolio-item d-block mx-auto" target="_blank" href="https://miniaturepaintingstudio.blogspot.com/2017/11/kairos-fateweaver-daemons-of-chaos.html">
            <div class="hover-zoom square">
              <img class="img-fluid" src="/img/portfolio/project3.jpg" alt="">
            </div>
          </a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
          <a class="portfolio-item d-block mx-auto" target="_blank" href="https://miniaturepaintingstudio.blogspot.com/2018/04/warhammer-40k-belisarius-cawl-ryza.html">
            <div class="hover-zoom square">
              <img class="img-fluid" src="/img/portfolio/project4.jpg" alt="">
            </div>
          </a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
          <a class="portfolio-item d-block mx-auto" target="_blank" href="https://miniaturepaintingstudio.blogspot.com/2016/05/terrorgheist-vampire-counts-order-work.html">
            <div class="hover-zoom square">
              <img class="img-fluid" src="/img/portfolio/project5.jpg" alt="">
            </div>
          </a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
          <a class="portfolio-item d-block mx-auto" target="_blank" href="https://miniaturepaintingstudio.blogspot.com/2016/11/seraphon-army-commissions.html">
            <div class="hover-zoom square">
              <img class="img-fluid" src="/img/portfolio/project7.jpg" alt="">
            </div>
          </a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
          <a class="portfolio-item d-block mx-auto" target="_blank" href="https://miniaturepaintingstudio.blogspot.com/2016/12/deathwing-knights-commissions.html">
            <div class="hover-zoom square">
              <img class="img-fluid" src="/img/portfolio/project8.jpg" alt="">
            </div>
          </a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
          <a class="portfolio-item d-block mx-auto" target="_blank" href="https://miniaturepaintingstudio.blogspot.com/2018/06/warhammer-vampire-counts-black-coach.html">
            <div class="hover-zoom square">
              <img class="img-fluid" src="/img/portfolio/project9.jpg" alt="">
            </div>
          </a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
          <a class="portfolio-item d-block mx-auto" target="_blank" href="http://miniaturepaintingstudio.blogspot.com/2015/10/warhammer-beastmen-jabberslythe-level-3.html">
            <div class="hover-zoom square">
              <img class="img-fluid" src="/img/portfolio/project10.jpg" alt="">
            </div>
          </a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
          <a class="portfolio-item d-block mx-auto" target="_blank" href="http://miniaturepaintingstudio.blogspot.com/2019/01/infinity-usariadna-grunts-level-3.html">
            <div class="hover-zoom square">
              <img class="img-fluid" src="/img/portfolio/project11.jpg" alt="">
            </div>
          </a>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
          <a class="portfolio-item d-block mx-auto" target="_blank" href="https://miniaturepaintingstudio.blogspot.com/2018/03/adeptus-mechanicus-onager-dunecrawler.html">
            <div class="hover-zoom square">
              <img class="img-fluid" src="/img/portfolio/project12.jpg" alt="">
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>

  <section class="bg-primary text-white" id="quote">
    <div class="container">
      <h2 class="text-center text-uppercase mb-0">Order Calculator</h2>
      <hr class="star-light mb-5">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <form id="quoteForm">
            <div class="form-group row">
              <div class="col-4">
                <button id="unitUSD" class="btn btn-warning btn-block">
                  <strong>USD</strong>
                </button>
              </div>
              <div class="col-4">
                <button id="unitEUR" class="btn btn-secondary btn-block">
                  <strong>EUR</strong>
                </button>
              </div>
              <div class="col-4">
                <button id="unitGBP" class="btn btn-secondary btn-block">
                  <strong>GBP</strong>
                </button>
              </div>
            </div>
            <div class="control-group" id="quoteUnits">
            </div>


            <h4 class="text-center text-uppercase divider">
              <span>Painting Quality</span>
            </h4>

            <div class="form-group row">
              <div class="col-2 col-sm-4">

              </div>
              <div class="col-5 col-sm-4">
                <div class="text-center">
                  regular <a class="js-scroll-trigger align-top legend" href="#quoteLegend">*</a>
                </div>
                <button id="quotePrice11" class="btn btn-secondary btn-block">
                  <h6>Level 1</h6>
                  <span class="subtotal"></span>
                </button>
                <button id="quotePrice12" class="btn btn-secondary btn-block">
                  <h6>Level 2</h6>
                  <span class="subtotal"></span>
                </button>
                <button id="quotePrice13" class="btn btn-secondary btn-block">
                  <h6>Level 3</h6>
                  <span class="subtotal"></span>
                </button>
                <button id="quotePrice14" class="btn btn-secondary btn-block">
                  <h6>Level 4</h6>
                  <span class="subtotal"></span>
                </button>
                <button id="quotePrice15" class="btn btn-secondary btn-block">
                  <h6>Level 5</h6>
                  <span class="subtotal"></span>
                </button>
              </div>
              <div class="col-5 col-sm-4">
                <div class="text-center">
                  premium <a class="js-scroll-trigger align-top legend" href="#quoteLegend">*</a>
                </div>
                <button id="quotePrice21" class="btn btn-secondary btn-block">
                  <h6>Level 1</h6>
                  <span class="subtotal"></span>
                </button>
                <button id="quotePrice22" class="btn btn-secondary btn-block">
                  <h6>Level 2</h6>
                  <span class="subtotal"></span>
                </button>
                <button id="quotePrice23" class="btn btn-secondary btn-block">
                  <h6>Level 3</h6>
                  <span class="subtotal"></span>
                </button>
                <button id="quotePrice24" class="btn btn-secondary btn-block">
                  <h6>Level 4</h6>
                  <span class="subtotal"></span>
                </button>
                <button id="quotePrice25" class="btn btn-secondary btn-block">
                  <h6>Level 5</h6>
                  <span class="subtotal"></span>
                </button>
              </div>
            </div>

            <p id="quoteLegend">
              <span class="align-top legend">*</span> It is very common to paint heroes/special units on a higher level. If you would like all your models to be painted using same level please leave the second column blank.
            </p>
            <h4 class="text-center text-uppercase divider">
              <span>Bases Quality</span>
            </h4>
            <div class="form-group row">
              <div class="col-4 col-md-2 mx-auto mb-4">
                <button id="quoteBase1" class="btn btn-secondary btn-block">
                  <h6>Level 1</h6>
                  <span class="subtotal"></span>
                </button>
              </div>
              <div class="col-4 col-md-2 mx-auto mb-4">
                <button id="quoteBase2" class="btn btn-secondary btn-block">
                  <h6>Level 2</h6>
                  <span class="subtotal"></span>
                </button>
              </div>
              <div class="col-4 col-md-2 mx-auto mb-4">
                <button id="quoteBase3" class="btn btn-secondary btn-block">
                  <h6>Level 3</h6>
                  <span class="subtotal"></span>
                </button>
              </div>
              <div class="col-4 col-md-2 mx-auto mb-4">
                <button id="quoteBase4" class="btn btn-secondary btn-block">
                  <h6>Level 4</h6>
                  <span class="subtotal"></span>
                </button>
              </div>
              <div class="col-4 col-md-2 mx-auto mb-4">
                <button id="quoteBase5" class="btn btn-secondary btn-block">
                  <h6>Level 5</h6>
                  <span class="subtotal"></span>
                </button>
              </div>
            </div>
            <h4 class="text-center text-uppercase divider">
              <span>Order Type</span>
            </h4>
            <div class="form-group row">
              <div class="col-sm-6">
                <button id="typePaint" class="btn btn-secondary btn-block btn-lg btn-order-type">
                  <h6>just paint</h6>
                </button>
              </div>
              <div class="col-sm-6">
                <p>
                  Select this option if you would like to ship to us models that you have already assemblied.
                  You will need to cover shipping.
                </p>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-6">
                <button id="typeAssemble" class="btn btn-warning btn-block btn-lg btn-order-type">
                  <h6>assemble and paint</h6>
                  + <span id="assemblyPrice1"></span> for assembly
                </button>
              </div>
              <div class="col-sm-6">
                <p>
                  Select this option if you would like to ship your unassemblied models to us.
                  You will need to cover shipping.
                </p>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-6">
                <button id="typeBuy" class="btn btn-secondary btn-block btn-lg btn-order-type">
                  <h6>Buy, assemble and paint</h6>
                  + ? models cost
                  <br/>
                  + <span id="assemblyPrice2"></span> for assembly
                </button>
              </div>
              <div class="col-sm-6">
                <p>
                  We will buy and assemble models for you (directly from the manufacturer at a discount of 10-24%).
                  We will send you models cost after you submit your quote.
                </p>
              </div>
            </div>
            <h4 class="text-center text-uppercase divider">
              <span>Get a Quote</span>
            </h4>
            <p>
              As each model is different this price is just an estimate.
              Please click "Get a Quote" and fill all details about your units so our teem will send you a final price.
            </p>
            <div class="form-group">
              <button type="submit" class="btn btn-secondary btn-xl btn-block">
                <h6>Get a Quote</h6>
                est. total: <span id="totalPrice"></span>
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>

''';
}

//interceptor for [PaintingNavConfig]
//interceptor for PaintingNavConfig
//can be singleton: TRUE
//parent: PaintingNavConfig []
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $PaintingNavConfig extends PaintingNavConfig implements Pluggable {
  $PaintingNavConfig() {
//Id
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_cms.CmsWidgetConfig', 'PaintingNavConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'PaintingNavConfig':
        return new $PaintingNavConfig();
      case 'Painting':
        return new $Painting();
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target['slug'];
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [Painting]
//interceptor for Painting
//can be singleton: TRUE
//parent: Painting []
//parent: ProductBase [@bool get ComposeSubtypes]
//parent: Entity [@bool get ComposeSubtypes]
class $Painting extends Painting implements Pluggable {
  $Painting() {
//String
//int
//String
//ConfigurableField<UnitType>
//Image
//Id
//String
//String
//String
//String
//int
//int
//int
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_pim.ProductBase', 'Painting'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'PaintingNavConfig':
        return new $PaintingNavConfig();
      case 'Painting':
        return new $Painting();
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['image'] = "module_core.Image";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.image = relations['image'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
    {
      target['price'] = this.price;
    }
    {
      target['paintBasePrice'] = this.paintBasePrice;
    }
    {
      target['assemblyPrice'] = this.assemblyPrice;
    }
    {
      target['baseBasePrice'] = this.baseBasePrice;
    }
//@Field
    {
      target['name'] = this.name;
    }
    {
      target['shortDescription'] = this.shortDescription;
    }
    {
      target['slug'] = this.slug;
    }
    {
      target['category'] = this.category;
    }
    {
      target['name'] = this.name;
    }
    {
      target['examples'] = this.examples;
    }
//@Field
//@Field
    {
      target['image'] = this.image != null ? this.image.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.name = target['name'];
    }
    {
      this.shortDescription = target['shortDescription'];
    }
    {
      this.slug = target['slug'];
    }
    {
      this.category = target['category'];
    }
    {
      this.name = target['name'];
    }
    {
      this.examples = target['examples'];
    }
//@Field
    {
      this.price = target['price'];
    }
    {
      this.paintBasePrice = target['paintBasePrice'];
    }
    {
      this.assemblyPrice = target['assemblyPrice'];
    }
    {
      this.baseBasePrice = target['baseBasePrice'];
    }
//@Field
//@Field
    {
      if (target['image'] != null) {
        if (target['image'] is String) {
          this.image = relatedFactory("module_core.Image");
          this.image.id = idFromString(target['image']);
        } else {
          this.image = relatedFactory(target['image']['classes'].last);
          this.image.fromJson(target['image']);
        }
      } else {
        this.image = null;
      }
    }
  }
}

//no interceptor for [module_core.SiteAction]
//interceptor for [module_core.Error404Action]
//interceptor for module_core.Error404Action
//can be singleton: FALSE
//parent: Error404Action [@bool get Compose]
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $module_core_Error404Action extends module_core.Error404Action
    implements Pluggable {
  $module_core_Error404Action(args) {
//List<String>
    this.args = args;
//Error404Widget
//create
    this.index = new $module_core_Error404Widget();
  }
  T plugin<T>() {
    return null;
  }

  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.Error404Action";
}

//interceptor for [module_core.Error404Widget]
//interceptor for module_core.Error404Widget
//can be singleton: TRUE
//parent: Error404Widget []
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_Error404Widget extends module_core.Error404Widget
    implements Pluggable {
  $module_core_Error404Widget() {
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_core_Error404Widget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/cms/templates/module_core_Error404Widget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/pim/templates/module_core_Error404Widget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/core/templates/module_core_Error404Widget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_core_Error404Widget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../libs/swift_composer/templates/module_core_Error404Widget.html
}

//no interceptor for [module_core.SiteMenuItem]
//no interceptor for [module_core.SiteShowAction]
//no interceptor for [module_core.SiteListAction]
//interceptor for [module_core.ListWidget]
//interceptor for module_core.ListWidget
//can be singleton: TRUE
//parent: ListWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_ListWidget extends module_core.ListWidget
    implements Pluggable {
  $module_core_ListWidget() {
//dynamic
//Slot
//create
    this.items = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
    {
      this.items.container = this;
      this.items.element = element.querySelector('#' + 'items');
      this.items.element.classes.add('slot');
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.items.widgets.forEach((widget) {
        this.items.element.append(widget.element);
      });
    }
  }

  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_core_ListWidget.html USED
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/cms/templates/module_core_ListWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/pim/templates/module_core_ListWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/core/templates/module_core_ListWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_core_ListWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../libs/swift_composer/templates/module_core_ListWidget.html
  String get html => '''
<div>
  <section>
    <div class="container">
      <h2 class="text-center text-uppercase text-secondary mb-0">Under Construction</h2>
      <hr class="star-dark mb-5">
      <div id="items">
      </div>
      We are building a shop with modeling accessories here, in the meantime, pleasee see what we
      <a target="_blank" href="https://www.ebay.com/sch/zydogrze/m.html?_nkw=&_armrs=1&_ipg=&_from=">
        are selling on ebay
      </a>
      .
    </div>
  </section>
</div>

''';
}

//no interceptor for [module_core.ListItemWidget]
//no interceptor for [module_core.ShowItemWidget]
//no interceptor for [module_core.AnnotatedWith]
//no interceptor for [module_core.Pluggable]
//no interceptor for [module_core.TypePlugin]
//no interceptor for [module_core.PossibleTypoException]
//no interceptor for [module_core.Id]
//interceptor for [module_core.Site]
//interceptor for module_core.Site
//can be singleton: TRUE
//parent: Site [@bool get Compose]
class $module_core_Site extends module_core.Site implements Pluggable {
  $module_core_Site() {}
  T plugin<T>() {
    return null;
  }

  String get name => "Miniature Painting Studio";
  String get url => "miniaturepaintingstudio.com";
  module_core.Id get id => new module_core.Id.fromString("EQ6A");
}

//interceptor for [module_core.Application]
//interceptor for module_core.Application
//can be singleton: TRUE
//parent: Application [@bool get Compose]
class $module_core_Application extends module_core.Application
    implements Pluggable {
  $module_core_Application() {
//Map<String, Future<String>>
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  module_core.Site get site => $om.module_core_Site;
  module_core.Layout get layout => $om.module_core_Layout;
}

//interceptor for [module_core.Layout]
//interceptor for module_core.Layout
//can be singleton: TRUE
//parent: Layout [@bool get Compose]
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_Layout extends module_core.Layout implements Pluggable {
  $module_core_Layout() {
//dynamic
//Slot
//create
    this.navbar = new module_core.Slot();
//Slot
//create
    this.body = new module_core.Slot();
//Slot
//create
    this.beforeBody = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  module_core.Site get site => $om.module_core_Site;
  void initializeSlots() {
//compiled method
    {
      this.navbar.container = this;
      this.navbar.element = element.querySelector('#' + 'navbar');
      this.navbar.element.classes.add('slot');
    }
    {
      this.body.container = this;
      this.body.element = element.querySelector('#' + 'body');
      this.body.element.classes.add('slot');
    }
    {
      this.beforeBody.container = this;
      this.beforeBody.element = element.querySelector('#' + 'beforeBody');
      this.beforeBody.element.classes.add('slot');
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.navbar.widgets.forEach((widget) {
        this.navbar.element.append(widget.element);
      });
    }
    {
      this.body.widgets.forEach((widget) {
        this.body.element.append(widget.element);
      });
    }
    {
      this.beforeBody.widgets.forEach((widget) {
        this.beforeBody.element.append(widget.element);
      });
    }
  }

  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_core_Layout.html USED
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/cms/templates/module_core_Layout.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/pim/templates/module_core_Layout.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/core/templates/module_core_Layout.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_core_Layout.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../libs/swift_composer/templates/module_core_Layout.html
  String get html => '''
<div>
  <div id="navbar"></div>
  <nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
    <div class="container">
      <a data-type="local" class="navbar-brand js-scroll-trigger" href="${router.url<HomeAction>([])}">
        <img src="/img/logo.png">
        Miniature
        <br>
        Painting
        <br>
        Studio
      </a>
      <button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <i class="icon-menu"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item mx-0 mx-lg-1">
            <a data-type="local" class="nav-link py-3 px-0 px-lg-3" href="${router.url<HomeAction>([])}">About</a>
          </li>
          <li class="nav-item mx-0 mx-lg-1">
            <a data-type="local" class="nav-link py-3 px-0 px-lg-3" href="${router.url<PaintingAction>([])}">Painting</a>
          </li>
          <li class="nav-item mx-0 mx-lg-1">
            <a data-type="local" class="nav-link py-3 px-0 px-lg-3" href="${router.url<module_pim.ProductsAction>([])}">Shop</a>
          </li>
          <li class="nav-item mx-0 mx-lg-1">
            <a data-type="local" class="nav-link py-3 px-0 px-lg-3 js-scroll-trigger" href="${router.url<ContactAction>([])}">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <div id="menu"></div>
  <div id="beforeBody" ></div>
  <div id="body"></div>

  <!-- Footer -->
  <footer class="footer text-center">
    <div class="container">
      <ul class="list-inline mb-0">
        <li class="list-inline-item">
          <a class="btn btn-outline-light btn-social text-center rounded-circle" href="https://www.facebook.com/miniatureps/">
            <i class="icon-facebook"></i>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="btn btn-outline-light btn-social text-center rounded-circle" href="http://miniaturepaintingstudio.blogspot.com/">
            <i class="icon-blogger"></i>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="btn btn-outline-light btn-social text-center rounded-circle" href="https://www.instagram.com/miniaturepaintingstudio/">
            <i class="icon-instagram"></i>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="btn btn-outline-light btn-social text-center rounded-circle" href="https://www.youtube.com/channel/UC6DW9zlhA9Qe2M9PzQaIzFQ/videos?disable_polymer=1">
            <i class="icon-youtube"></i>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="btn btn-outline-light btn-social text-center rounded-circle" href="https://twitter.com/mpsminiatures">
            <i class="icon-twitter"></i>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="btn btn-outline-light btn-social text-center rounded-circle" href="https://www.ebay.com/sch/zydogrze/m.html?_nkw=&_armrs=1&_ipg=&_from=">
            <i class="icon-ebay"></i>
          </a>
        </li>
        <!--li class="list-inline-item">
          <a class="btn btn-outline-light btn-social text-center rounded-circle" href="https://www.flickr.com/photos/136752285@N03/albums">
            <i class="fab fa-fw fa-flickr"></i>
          </a>
        </li-->
      </ul>
    </div>
  </footer>

  <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
  <div class="scroll-to-top d-lg-none position-fixed ">
    <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
      <i class="fa fa-chevron-up"></i>
    </a>
  </div>
  <div id="modal">
    
  </div>
  <div class="copyright py-4 text-center text-white">
    <div class="container">
      <div class="row">
        <div class="col-md-6 text-md-left">
          <small>copyright &copy; Miniature Painting Studio 2020</small>
        </div>
        <div class="col-md-6 text-md-right">
          <small>powered by <a target="_blank" href="http://swift.shop/">swift.shop</a></small>
        </div>
      </div>
    </div>
  </div>
</div>

''';
}

//no interceptor for [module_core.ApiModule]
//no interceptor for [module_core.CRUDApi]
//no interceptor for [module_core.Action]
//interceptor for [module_core.Router]
//interceptor for module_core.Router
//can be singleton: TRUE
//parent: Router [@bool get Compose]
class $module_core_Router extends module_core.Router implements Pluggable {
  $module_core_Router() {
//Action
//Action
//Element
  }
  T plugin<T>() {
    return null;
  }

  String get indexUrl => "Home";
  module_core.Action actionFactory(String className, List<String> args) {
    switch (className) {
      case 'HomeAction':
        return new $HomeAction(args);
      case 'ContactAction':
        return new $ContactAction(args);
      case 'PaintingAction':
        return new $PaintingAction(args);
      case 'module_core.Error404Action':
        return new $module_core_Error404Action(args);
      case 'module_pim.ProductAction':
        return new $module_pim_ProductAction(args);
      case 'module_pim.ProductsAction':
        return new $module_pim_ProductsAction(args);
      case 'module_cms.PageAction':
        return new $module_cms_PageAction(args);
    }
  }

  String getCode<T extends module_core.Action>() {
    if (T == HomeAction) return 'HomeAction';
    if (T == ContactAction) return 'ContactAction';
    if (T == PaintingAction) return 'PaintingAction';
    if (T == module_core.Error404Action) return 'module_core.Error404Action';
    if (T == module_pim.ProductAction) return 'module_pim.ProductAction';
    if (T == module_pim.ProductsAction) return 'module_pim.ProductsAction';
    if (T == module_cms.PageAction) return 'module_cms.PageAction';
  }
}

//no interceptor for [module_core.Slot]
//interceptor for [module_core.NavbarLink]
//interceptor for module_core.NavbarLink
//can be singleton: TRUE
//parent: NavbarLink []
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_NavbarLink extends module_core.NavbarLink
    implements Pluggable {
  $module_core_NavbarLink() {
//dynamic
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_core_NavbarLink.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/cms/templates/module_core_NavbarLink.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/pim/templates/module_core_NavbarLink.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/core/templates/module_core_NavbarLink.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_core_NavbarLink.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../libs/swift_composer/templates/module_core_NavbarLink.html
}

//interceptor for [module_core.NavbarDropdown]
//interceptor for module_core.NavbarDropdown
//can be singleton: TRUE
//parent: NavbarDropdown []
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_NavbarDropdown extends module_core.NavbarDropdown
    implements Pluggable {
  $module_core_NavbarDropdown() {
//dynamic
//Slot
//create
    this.items = new module_core.Slot();
//String
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
    {
      this.items.container = this;
      this.items.element = element.querySelector('#' + 'items');
      this.items.element.classes.add('slot');
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.items.widgets.forEach((widget) {
        this.items.element.append(widget.element);
      });
    }
  }

  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_core_NavbarDropdown.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/cms/templates/module_core_NavbarDropdown.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/pim/templates/module_core_NavbarDropdown.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/core/templates/module_core_NavbarDropdown.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_core_NavbarDropdown.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../libs/swift_composer/templates/module_core_NavbarDropdown.html
}

//no interceptor for [module_core.SwiftUriPolicy]
//no interceptor for [module_core.Widget]
//no interceptor for [module_core.AsyncWidget]
//interceptor for [module_core.TextWidget]
//interceptor for module_core.TextWidget
//can be singleton: FALSE
//parent: TextWidget [@bool get Compose]
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_TextWidget extends module_core.TextWidget
    implements Pluggable {
  $module_core_TextWidget(text) {
//dynamic
//String
    this.text = text;
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_core_TextWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/cms/templates/module_core_TextWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/pim/templates/module_core_TextWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/core/templates/module_core_TextWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_core_TextWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../libs/swift_composer/templates/module_core_TextWidget.html
}

//no interceptor for [module_core.Entity]
//interceptor for [module_core.App]
//interceptor for module_core.App
//can be singleton: TRUE
//parent: App []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_App extends module_core.App implements Pluggable {
  $module_core_App() {
//Id
//String
//App
//User
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_core.App'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'PaintingNavConfig':
        return new $PaintingNavConfig();
      case 'Painting':
        return new $Painting();
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['parent'] = "module_core.App";
    }
    {
      target['owner'] = "module_core.User";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.parent = relations['parent'];
    }
    {
      this.owner = relations['owner'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['url'] = this.url;
    }
    {
      target['name'] = this.name;
    }
//@Field
//@Field
    {
      target['parent'] = this.parent != null ? this.parent.toJson() : null;
    }
    {
      target['owner'] = this.owner != null ? this.owner.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target['slug'];
    }
    {
      this.url = target['url'];
    }
    {
      this.name = target['name'];
    }
//@Field
//@Field
//@Field
    {
      if (target['parent'] != null) {
        if (target['parent'] is String) {
          this.parent = relatedFactory("module_core.App");
          this.parent.id = idFromString(target['parent']);
        } else {
          this.parent = relatedFactory(target['parent']['classes'].last);
          this.parent.fromJson(target['parent']);
        }
      } else {
        this.parent = null;
      }
    }
    {
      if (target['owner'] != null) {
        if (target['owner'] is String) {
          this.owner = relatedFactory("module_core.User");
          this.owner.id = idFromString(target['owner']);
        } else {
          this.owner = relatedFactory(target['owner']['classes'].last);
          this.owner.fromJson(target['owner']);
        }
      } else {
        this.owner = null;
      }
    }
  }
}

//interceptor for [module_core.User]
//interceptor for module_core.User
//can be singleton: TRUE
//parent: User []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_User extends module_core.User implements Pluggable {
  $module_core_User() {
//Id
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_core.User'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'PaintingNavConfig':
        return new $PaintingNavConfig();
      case 'Painting':
        return new $Painting();
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['email'] = this.email;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target['slug'];
    }
    {
      this.email = target['email'];
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_core.Image]
//interceptor for module_core.Image
//can be singleton: TRUE
//parent: Image []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_Image extends module_core.Image implements Pluggable {
  $module_core_Image() {
//Id
//String
//App
//String
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_core.Image'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'PaintingNavConfig':
        return new $PaintingNavConfig();
      case 'Painting':
        return new $Painting();
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['app'] = "module_core.App";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.app = relations['app'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['mimetype'] = this.mimetype;
    }
    {
      target['filename'] = this.filename;
    }
    {
      target['thumb'] = this.thumb;
    }
//@Field
//@Field
    {
      target['app'] = this.app != null ? this.app.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target['slug'];
    }
    {
      this.mimetype = target['mimetype'];
    }
    {
      this.filename = target['filename'];
    }
    {
      this.thumb = target['thumb'];
    }
//@Field
//@Field
//@Field
    {
      if (target['app'] != null) {
        if (target['app'] is String) {
          this.app = relatedFactory("module_core.App");
          this.app.id = idFromString(target['app']);
        } else {
          this.app = relatedFactory(target['app']['classes'].last);
          this.app.fromJson(target['app']);
        }
      } else {
        this.app = null;
      }
    }
  }
}

//interceptor for [module_pim.ProductAction]
//interceptor for module_pim.ProductAction
//T[269908264] => ProductBase[447620454]
//can be singleton: FALSE
//parent: ProductAction []
//parent: SiteShowAction [@bool get ComposeSubtypes]
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $module_pim_ProductAction extends module_pim.ProductAction
    implements Pluggable {
  $module_pim_ProductAction(args) {
//Future<T>
//T
//ShowItemWidget<T>
//List<String>
    this.args = args;
  }
  T plugin<T>() {
    return null;
  }

  module_core.CRUDApi<module_pim.ProductBase> get api => $om.module_pim_PimApi;
  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "module_pim.ProductAction";
  module_core.ShowItemWidget<module_pim.ProductBase> createWidget(
      module_pim.ProductBase item) {
//module_core.ShowItemWidget
    return new $module_pim_ShowProductWidget(item);
  }

  module_pim.ProductsAction listFactory() {
//module_pim.ProductsAction
    return new $module_pim_ProductsAction(args);
  }
}

//interceptor for [module_pim.ShowProductWidget]
//interceptor for module_pim.ShowProductWidget
//T[391897947] => ProductBase[447620454]
//can be singleton: FALSE
//parent: ShowProductWidget []
//parent: ShowItemWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $module_pim_ShowProductWidget extends module_pim.ShowProductWidget
    implements Pluggable {
  $module_pim_ShowProductWidget(item) {
//T
    this.item = item;
//dynamic
//Slot
//create
    this.buttons = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
    {
      this.buttons.container = this;
      this.buttons.element = element.querySelector('#' + 'buttons');
      this.buttons.element.classes.add('slot');
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.buttons.widgets.forEach((widget) {
        this.buttons.element.append(widget.element);
      });
    }
  }

  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_pim_ShowProductWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/cms/templates/module_pim_ShowProductWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/pim/templates/module_pim_ShowProductWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/core/templates/module_pim_ShowProductWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_pim_ShowProductWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../libs/swift_composer/templates/module_pim_ShowProductWidget.html
}

//interceptor for [module_pim.ProductsAction]
//interceptor for module_pim.ProductsAction
//T[182183646] => ProductBase[447620454]
//can be singleton: FALSE
//parent: ProductsAction [@SiteMenuItem SiteMenuItem(String section, String title)]
//parent: SiteListAction [@bool get ComposeSubtypes]
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $module_pim_ProductsAction extends module_pim.ProductsAction
    implements Pluggable {
  $module_pim_ProductsAction(args) {
//dynamic
//List<T>
//ListWidget
//create
    this.listWidget = new $module_core_ListWidget();
//List<String>
    this.args = args;
  }
  T plugin<T>() {
    return null;
  }

  module_core.CRUDApi<module_pim.ProductBase> get api => $om.module_pim_PimApi;
  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "module_pim.ProductsAction";
  module_core.ListItemWidget<module_pim.ProductBase> createListWidget(
      module_pim.ProductBase item) {
//module_core.ListItemWidget
    return new $module_pim_ProductWidget(item);
  }
}

//interceptor for [module_pim.ProductWidget]
//interceptor for module_pim.ProductWidget
//T[370206959] => ProductBase[447620454]
//can be singleton: FALSE
//parent: ProductWidget []
//parent: ListItemWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $module_pim_ProductWidget extends module_pim.ProductWidget
    implements Pluggable {
  $module_pim_ProductWidget(item) {
//T
    this.item = item;
//dynamic
//Slot
//create
    this.buttons = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
    {
      this.buttons.container = this;
      this.buttons.element = element.querySelector('#' + 'buttons');
      this.buttons.element.classes.add('slot');
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.buttons.widgets.forEach((widget) {
        this.buttons.element.append(widget.element);
      });
    }
  }

  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_pim_ProductWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/cms/templates/module_pim_ProductWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/pim/templates/module_pim_ProductWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/core/templates/module_pim_ProductWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_pim_ProductWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../libs/swift_composer/templates/module_pim_ProductWidget.html
}

//interceptor for [module_pim.PimApi]
//interceptor for module_pim.PimApi
//T[534851839] => ProductBase[447620454]
//can be singleton: TRUE
//parent: PimApi [@bool get Compose]
//parent: CRUDApi [@bool get ComposeSubtypes]
//parent: ApiModule [@bool get ComposeSubtypes]
class $module_pim_PimApi extends module_pim.PimApi implements Pluggable {
  $module_pim_PimApi() {}
  T plugin<T>() {
    return null;
  }

  Map<String, module_core.CRUDApi<module_core.Entity>> get relatedApis =>
      $om.instancesOfmodule_core_CRUDApi_module_core_Entity_;
  String get className => "module_pim.PimApi";
  module_core.Site get site => $om.module_core_Site;
  module_pim.ProductBase newEntityByType(String className) {
    switch (className) {
      case 'Painting':
        return new $Painting();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
    }
  }
}

//no interceptor for [module_pim.UnitType]
//no interceptor for [module_pim.ConfigurableField]
//no interceptor for [module_pim.ProductBase]
//interceptor for [module_pim.SimpleProduct]
//interceptor for module_pim.SimpleProduct
//can be singleton: TRUE
//parent: SimpleProduct []
//parent: ProductBase [@bool get ComposeSubtypes]
//parent: Entity [@bool get ComposeSubtypes]
class $module_pim_SimpleProduct extends module_pim.SimpleProduct
    implements Pluggable {
  $module_pim_SimpleProduct() {
//String
//int
//String
//ConfigurableField<UnitType>
//Image
//Id
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_pim.ProductBase', 'module_pim.SimpleProduct'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'PaintingNavConfig':
        return new $PaintingNavConfig();
      case 'Painting':
        return new $Painting();
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['image'] = "module_core.Image";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.image = relations['image'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
    {
      target['price'] = this.price;
    }
//@Field
    {
      target['name'] = this.name;
    }
    {
      target['shortDescription'] = this.shortDescription;
    }
    {
      target['slug'] = this.slug;
    }
//@Field
//@Field
    {
      target['image'] = this.image != null ? this.image.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.name = target['name'];
    }
    {
      this.shortDescription = target['shortDescription'];
    }
    {
      this.slug = target['slug'];
    }
//@Field
    {
      this.price = target['price'];
    }
//@Field
//@Field
    {
      if (target['image'] != null) {
        if (target['image'] is String) {
          this.image = relatedFactory("module_core.Image");
          this.image.id = idFromString(target['image']);
        } else {
          this.image = relatedFactory(target['image']['classes'].last);
          this.image.fromJson(target['image']);
        }
      } else {
        this.image = null;
      }
    }
  }
}

//interceptor for [module_cms.PageAction]
//interceptor for module_cms.PageAction
//T[269908264] => Page[414991068]
//can be singleton: FALSE
//parent: PageAction []
//parent: SiteShowAction [@bool get ComposeSubtypes]
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $module_cms_PageAction extends module_cms.PageAction
    implements Pluggable {
  $module_cms_PageAction(args) {
//Future<T>
//T
//ShowItemWidget<T>
//List<String>
    this.args = args;
  }
  T plugin<T>() {
    return null;
  }

  module_core.CRUDApi<module_cms.Page> get api => $om.module_cms_CmsPagesApi;
  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "module_cms.PageAction";
  module_core.ShowItemWidget<module_cms.Page> createWidget(
      module_cms.Page item) {
//module_core.ShowItemWidget
    return new $module_cms_ShowPageWidget(item);
  }

  module_cms.CmsWidget<module_cms.CmsWidgetConfig> createItemWidget(
      String className, module_cms.CmsWidgetConfig config) {
    switch (className) {
      case 'PaintingNavWidget':
        return new $PaintingNavWidget(config);
      case 'module_cms.HtmlWidget':
        return new $module_cms_HtmlWidget(config);
      case 'module_cms.ImageWidget':
        return new $module_cms_ImageWidget(config);
      case 'module_cms.ParagraphWidget':
        return new $module_cms_ParagraphWidget(config);
      case 'module_cms.HeadingWidget':
        return new $module_cms_HeadingWidget(config);
    }
  }
}

//interceptor for [module_cms.ShowPageWidget]
//interceptor for module_cms.ShowPageWidget
//T[391897947] => Page[414991068]
//can be singleton: FALSE
//parent: ShowPageWidget []
//parent: ShowItemWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $module_cms_ShowPageWidget extends module_cms.ShowPageWidget
    implements Pluggable {
  $module_cms_ShowPageWidget(item) {
//T
    this.item = item;
//dynamic
//Slot
//create
    this.pageContent = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
    {
      this.pageContent.container = this;
      this.pageContent.element = element.querySelector('#' + 'pageContent');
      this.pageContent.element.classes.add('slot');
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.pageContent.widgets.forEach((widget) {
        this.pageContent.element.append(widget.element);
      });
    }
  }

  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_cms_ShowPageWidget.html USED
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/cms/templates/module_cms_ShowPageWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/pim/templates/module_cms_ShowPageWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/core/templates/module_cms_ShowPageWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_cms_ShowPageWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../libs/swift_composer/templates/module_cms_ShowPageWidget.html
  String get html => '''
<div class="container">
 <section id="pageContent"></section>
</div>

''';
}

//no interceptor for [module_cms.CmsWidget]
//interceptor for [module_cms.HtmlWidget]
//interceptor for module_cms.HtmlWidget
//CT[171676771] => HtmlConfig[389581089]
//can be singleton: FALSE
//parent: HtmlWidget []
//parent: CmsWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $module_cms_HtmlWidget extends module_cms.HtmlWidget
    implements Pluggable {
  $module_cms_HtmlWidget(config) {
//CT
    this.config = config;
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_cms_HtmlWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/cms/templates/module_cms_HtmlWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/pim/templates/module_cms_HtmlWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/core/templates/module_cms_HtmlWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_cms_HtmlWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../libs/swift_composer/templates/module_cms_HtmlWidget.html
}

//interceptor for [module_cms.ImageWidget]
//interceptor for module_cms.ImageWidget
//CT[171676771] => ImageConfig[445488254]
//can be singleton: FALSE
//parent: ImageWidget []
//parent: CmsWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $module_cms_ImageWidget extends module_cms.ImageWidget
    implements Pluggable {
  $module_cms_ImageWidget(config) {
//CT
    this.config = config;
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_cms_ImageWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/cms/templates/module_cms_ImageWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/pim/templates/module_cms_ImageWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/core/templates/module_cms_ImageWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_cms_ImageWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../libs/swift_composer/templates/module_cms_ImageWidget.html
}

//interceptor for [module_cms.ParagraphWidget]
//interceptor for module_cms.ParagraphWidget
//CT[171676771] => ParagraphConfig[153581726]
//can be singleton: FALSE
//parent: ParagraphWidget []
//parent: CmsWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $module_cms_ParagraphWidget extends module_cms.ParagraphWidget
    implements Pluggable {
  $module_cms_ParagraphWidget(config) {
//CT
    this.config = config;
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_cms_ParagraphWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/cms/templates/module_cms_ParagraphWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/pim/templates/module_cms_ParagraphWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/core/templates/module_cms_ParagraphWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_cms_ParagraphWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../libs/swift_composer/templates/module_cms_ParagraphWidget.html
}

//interceptor for [module_cms.HeadingWidget]
//interceptor for module_cms.HeadingWidget
//CT[171676771] => HeadingConfig[26884972]
//can be singleton: FALSE
//parent: HeadingWidget []
//parent: CmsWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $module_cms_HeadingWidget extends module_cms.HeadingWidget
    implements Pluggable {
  $module_cms_HeadingWidget(config) {
//CT
    this.config = config;
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_cms_HeadingWidget.html USED
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/cms/templates/module_cms_HeadingWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/pim/templates/module_cms_HeadingWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../modules/core/templates/module_cms_HeadingWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/templates/module_cms_HeadingWidget.html
  ///home/fsw/workspace/cado7dev/apps/miniaturepaintingstudio.com/../../libs/swift_composer/templates/module_cms_HeadingWidget.html
  String get html => '''
<div>
  <h2 class="text-center text-uppercase text-secondary mb-0">${config.text}</h2>
  <hr class="star-dark mb-5">
</div>

''';
}

//interceptor for [module_cms.CmsPagesApi]
//interceptor for module_cms.CmsPagesApi
//T[534851839] => Page[414991068]
//can be singleton: TRUE
//parent: CmsPagesApi []
//parent: CRUDApi [@bool get ComposeSubtypes]
//parent: ApiModule [@bool get ComposeSubtypes]
class $module_cms_CmsPagesApi extends module_cms.CmsPagesApi
    implements Pluggable {
  $module_cms_CmsPagesApi() {}
  T plugin<T>() {
    return null;
  }

  Map<String, module_core.CRUDApi<module_core.Entity>> get relatedApis =>
      $om.instancesOfmodule_core_CRUDApi_module_core_Entity_;
  String get className => "module_cms.CmsPagesApi";
  module_core.Site get site => $om.module_core_Site;
  module_cms.Page newEntityByType(String className) {
    switch (className) {
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }
}

//interceptor for [module_cms.CmsWidgetConfig]
//interceptor for module_cms.CmsWidgetConfig
//can be singleton: TRUE
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_cms_CmsWidgetConfig extends module_cms.CmsWidgetConfig
    implements Pluggable {
  $module_cms_CmsWidgetConfig() {
//Id
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_cms.CmsWidgetConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'PaintingNavConfig':
        return new $PaintingNavConfig();
      case 'Painting':
        return new $Painting();
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target['slug'];
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_cms.HeadingConfig]
//interceptor for module_cms.HeadingConfig
//can be singleton: TRUE
//parent: HeadingConfig []
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_cms_HeadingConfig extends module_cms.HeadingConfig
    implements Pluggable {
  $module_cms_HeadingConfig() {
//Id
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_cms.CmsWidgetConfig', 'module_cms.HeadingConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'PaintingNavConfig':
        return new $PaintingNavConfig();
      case 'Painting':
        return new $Painting();
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['text'] = this.text;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target['slug'];
    }
    {
      this.text = target['text'];
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_cms.ImageConfig]
//interceptor for module_cms.ImageConfig
//can be singleton: TRUE
//parent: ImageConfig []
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_cms_ImageConfig extends module_cms.ImageConfig
    implements Pluggable {
  $module_cms_ImageConfig() {
//Id
//String
//Image
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_cms.CmsWidgetConfig', 'module_cms.ImageConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'PaintingNavConfig':
        return new $PaintingNavConfig();
      case 'Painting':
        return new $Painting();
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['image'] = "module_core.Image";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.image = relations['image'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
//@Field
//@Field
    {
      target['image'] = this.image != null ? this.image.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target['slug'];
    }
//@Field
//@Field
//@Field
    {
      if (target['image'] != null) {
        if (target['image'] is String) {
          this.image = relatedFactory("module_core.Image");
          this.image.id = idFromString(target['image']);
        } else {
          this.image = relatedFactory(target['image']['classes'].last);
          this.image.fromJson(target['image']);
        }
      } else {
        this.image = null;
      }
    }
  }
}

//interceptor for [module_cms.HtmlConfig]
//interceptor for module_cms.HtmlConfig
//can be singleton: TRUE
//parent: HtmlConfig []
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_cms_HtmlConfig extends module_cms.HtmlConfig
    implements Pluggable {
  $module_cms_HtmlConfig() {
//Id
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_cms.CmsWidgetConfig', 'module_cms.HtmlConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'PaintingNavConfig':
        return new $PaintingNavConfig();
      case 'Painting':
        return new $Painting();
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['html'] = this.html;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target['slug'];
    }
    {
      this.html = target['html'];
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_cms.ParagraphConfig]
//interceptor for module_cms.ParagraphConfig
//can be singleton: TRUE
//parent: ParagraphConfig []
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_cms_ParagraphConfig extends module_cms.ParagraphConfig
    implements Pluggable {
  $module_cms_ParagraphConfig() {
//Id
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_cms.CmsWidgetConfig', 'module_cms.ParagraphConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'PaintingNavConfig':
        return new $PaintingNavConfig();
      case 'Painting':
        return new $Painting();
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['body'] = this.body;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target['slug'];
    }
    {
      this.body = target['body'];
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_cms.Page]
//interceptor for module_cms.Page
//can be singleton: TRUE
//parent: Page []
//parent: Entity [@bool get ComposeSubtypes]
class $module_cms_Page extends module_cms.Page implements Pluggable {
  $module_cms_Page() {
//Id
//String
//App
//String
//List<CmsWidgetConfig>
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_cms.Page'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'PaintingNavConfig':
        return new $PaintingNavConfig();
      case 'Painting':
        return new $Painting();
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['app'] = "module_core.App";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.app = relations['app'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['title'] = this.title;
    }
//@Field
    {
      target['widgets'] = this.widgets.map((e) => e.toJson()).toList();
    }
//@Field
    {
      target['app'] = this.app != null ? this.app.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target['slug'];
    }
    {
      this.title = target['title'];
    }
//@Field
//@Field
    {
      this.widgets = [];
      if (target['widgets'] != null) {
        target['widgets'].forEach((data) {
          print('CLASSES');
          print(data['classes']);
          var e = relatedFactory(data['classes'].last);
          e.fromJson(data);
          this.widgets.add(e);
        });
      }
    }
//@Field
    {
      if (target['app'] != null) {
        if (target['app'] is String) {
          this.app = relatedFactory("module_core.App");
          this.app.id = idFromString(target['app']);
        } else {
          this.app = relatedFactory(target['app']['classes'].last);
          this.app.fromJson(target['app']);
        }
      } else {
        this.app = null;
      }
    }
  }
}

class $ObjectManager {
  $HomeWidget _homeWidget;
  $HomeWidget get homeWidget {
    if (_homeWidget == null) {
      _homeWidget = new $HomeWidget();
    }
    return _homeWidget;
  }

  $ContactWidget _contactWidget;
  $ContactWidget get contactWidget {
    if (_contactWidget == null) {
      _contactWidget = new $ContactWidget();
    }
    return _contactWidget;
  }

  $PaintingWidget _paintingWidget;
  $PaintingWidget get paintingWidget {
    if (_paintingWidget == null) {
      _paintingWidget = new $PaintingWidget();
    }
    return _paintingWidget;
  }

  $PaintingNavConfig _paintingNavConfig;
  $PaintingNavConfig get paintingNavConfig {
    if (_paintingNavConfig == null) {
      _paintingNavConfig = new $PaintingNavConfig();
    }
    return _paintingNavConfig;
  }

  $Painting _painting;
  $Painting get painting {
    if (_painting == null) {
      _painting = new $Painting();
    }
    return _painting;
  }

  $module_core_Error404Widget _module_core_Error404Widget;
  $module_core_Error404Widget get module_core_Error404Widget {
    if (_module_core_Error404Widget == null) {
      _module_core_Error404Widget = new $module_core_Error404Widget();
    }
    return _module_core_Error404Widget;
  }

  $module_core_ListWidget _module_core_ListWidget;
  $module_core_ListWidget get module_core_ListWidget {
    if (_module_core_ListWidget == null) {
      _module_core_ListWidget = new $module_core_ListWidget();
    }
    return _module_core_ListWidget;
  }

  $module_core_Site _module_core_Site;
  $module_core_Site get module_core_Site {
    if (_module_core_Site == null) {
      _module_core_Site = new $module_core_Site();
    }
    return _module_core_Site;
  }

  $module_core_Application _module_core_Application;
  $module_core_Application get module_core_Application {
    if (_module_core_Application == null) {
      _module_core_Application = new $module_core_Application();
    }
    return _module_core_Application;
  }

  $module_core_Layout _module_core_Layout;
  $module_core_Layout get module_core_Layout {
    if (_module_core_Layout == null) {
      _module_core_Layout = new $module_core_Layout();
    }
    return _module_core_Layout;
  }

  $module_core_Router _module_core_Router;
  $module_core_Router get module_core_Router {
    if (_module_core_Router == null) {
      _module_core_Router = new $module_core_Router();
    }
    return _module_core_Router;
  }

  $module_core_NavbarLink _module_core_NavbarLink;
  $module_core_NavbarLink get module_core_NavbarLink {
    if (_module_core_NavbarLink == null) {
      _module_core_NavbarLink = new $module_core_NavbarLink();
    }
    return _module_core_NavbarLink;
  }

  $module_core_NavbarDropdown _module_core_NavbarDropdown;
  $module_core_NavbarDropdown get module_core_NavbarDropdown {
    if (_module_core_NavbarDropdown == null) {
      _module_core_NavbarDropdown = new $module_core_NavbarDropdown();
    }
    return _module_core_NavbarDropdown;
  }

  $module_core_App _module_core_App;
  $module_core_App get module_core_App {
    if (_module_core_App == null) {
      _module_core_App = new $module_core_App();
    }
    return _module_core_App;
  }

  $module_core_User _module_core_User;
  $module_core_User get module_core_User {
    if (_module_core_User == null) {
      _module_core_User = new $module_core_User();
    }
    return _module_core_User;
  }

  $module_core_Image _module_core_Image;
  $module_core_Image get module_core_Image {
    if (_module_core_Image == null) {
      _module_core_Image = new $module_core_Image();
    }
    return _module_core_Image;
  }

  $module_pim_PimApi _module_pim_PimApi;
  $module_pim_PimApi get module_pim_PimApi {
    if (_module_pim_PimApi == null) {
      _module_pim_PimApi = new $module_pim_PimApi();
    }
    return _module_pim_PimApi;
  }

  $module_pim_SimpleProduct _module_pim_SimpleProduct;
  $module_pim_SimpleProduct get module_pim_SimpleProduct {
    if (_module_pim_SimpleProduct == null) {
      _module_pim_SimpleProduct = new $module_pim_SimpleProduct();
    }
    return _module_pim_SimpleProduct;
  }

  $module_cms_CmsPagesApi _module_cms_CmsPagesApi;
  $module_cms_CmsPagesApi get module_cms_CmsPagesApi {
    if (_module_cms_CmsPagesApi == null) {
      _module_cms_CmsPagesApi = new $module_cms_CmsPagesApi();
    }
    return _module_cms_CmsPagesApi;
  }

  $module_cms_CmsWidgetConfig _module_cms_CmsWidgetConfig;
  $module_cms_CmsWidgetConfig get module_cms_CmsWidgetConfig {
    if (_module_cms_CmsWidgetConfig == null) {
      _module_cms_CmsWidgetConfig = new $module_cms_CmsWidgetConfig();
    }
    return _module_cms_CmsWidgetConfig;
  }

  $module_cms_HeadingConfig _module_cms_HeadingConfig;
  $module_cms_HeadingConfig get module_cms_HeadingConfig {
    if (_module_cms_HeadingConfig == null) {
      _module_cms_HeadingConfig = new $module_cms_HeadingConfig();
    }
    return _module_cms_HeadingConfig;
  }

  $module_cms_ImageConfig _module_cms_ImageConfig;
  $module_cms_ImageConfig get module_cms_ImageConfig {
    if (_module_cms_ImageConfig == null) {
      _module_cms_ImageConfig = new $module_cms_ImageConfig();
    }
    return _module_cms_ImageConfig;
  }

  $module_cms_HtmlConfig _module_cms_HtmlConfig;
  $module_cms_HtmlConfig get module_cms_HtmlConfig {
    if (_module_cms_HtmlConfig == null) {
      _module_cms_HtmlConfig = new $module_cms_HtmlConfig();
    }
    return _module_cms_HtmlConfig;
  }

  $module_cms_ParagraphConfig _module_cms_ParagraphConfig;
  $module_cms_ParagraphConfig get module_cms_ParagraphConfig {
    if (_module_cms_ParagraphConfig == null) {
      _module_cms_ParagraphConfig = new $module_cms_ParagraphConfig();
    }
    return _module_cms_ParagraphConfig;
  }

  $module_cms_Page _module_cms_Page;
  $module_cms_Page get module_cms_Page {
    if (_module_cms_Page == null) {
      _module_cms_Page = new $module_cms_Page();
    }
    return _module_cms_Page;
  }

  Map<String, module_core.CRUDApi>
      get instancesOfmodule_core_CRUDApi_module_core_Entity_ {
    return {
      "module_pim.PimApi": new $module_pim_PimApi(),
      "module_cms.CmsPagesApi": new $module_cms_CmsPagesApi(),
    };
  }
}

$ObjectManager $om = new $ObjectManager();
//generated in 228ms
