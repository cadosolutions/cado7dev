library module_core;

import 'package:swift_composer/swift_composer.dart';
export 'package:swift_composer/swift_composer.dart';

import 'package:swift_ids/swift_ids.dart';
export 'package:swift_ids/swift_ids.dart';

import 'client.dart';
export 'client.dart';
import 'model.dart';
export 'model.dart';

import 'dart:html';
import 'dart:convert';
import 'dart:math';
import 'dart:async';
import 'dart:js';

@Compose
abstract class TODODODOD {
  @Inject
  Site site;

  @Inject
  AdminLayout layout;

  @Inject
  UserForm userForm;
  @Inject
  AdminNavbar navbar;

  void run() {
    layout.element;
    layout.menu.setChild(navbar);
    navbar.right.setChild(userForm);
    document.body.append(layout.element);
    //super.run();
  }
}

@Compose
abstract class AdminLayout extends Widget {

  @Create
  Slot menu;
  @Create
  Slot body;

  String get html => '''
  <div>
    <div id="menu"></div>
    <div id="body" class="pt-4 container"></div>
    <footer class="container pt-4 my-md-5 pt-md-5 border-top">
          <small class="d-block mb-3 text-muted text-right">powered by <a href="https://swift.shop/">&copy;swift.shop</a></small>
    </footer>
  </div>
  ''';
}

class AdminMenuItem {
  final String section;
  final String subsection;
  final String title;
  const AdminMenuItem(this.section, this.subsection, this.title);
}

abstract class AdminGridColumn<E> {

  bool fits(dynamic x) {
    return x() is E;
  }
  Element getCell(E entity);
}

class IdColumn extends AdminGridColumn<Entity> {
  Element getCell(Entity entity) {
    return new DivElement()..text = entity.id.toString();
  }
}

class ClassColumn extends AdminGridColumn<Entity> {
  Element getCell(Entity entity) {
    return new DivElement()..text = entity.classNames.join(',');
  }
}

@ComposeSubtypes
abstract class AdminGridFieldColumn<E, T> extends AdminGridColumn<E> { //extends EntityField
  @Inject
  InputFor<T> input;

  //Field<T> field(E entity);
  //TODO
  Element getCell(E entity) => null; //input.getElement(field(entity));
}

abstract class InputFor<T> {
  Element getElement(T field);
}

class StringInput extends InputFor<String> {
  Element getElement(String field){
    print(field);
    var elem = new InputElement();
    elem.value = field;
    elem.onChange.listen((e){
      field = elem.value;
    });
    return elem;
  }
}

class PriceInput extends InputFor<int> {
  Element getElement(int field){
    var elem = new InputElement()
        ..type="number";
    elem.value = field.toString();
    elem.onChange.listen((e){
      field = int.parse(elem.value);
    });
    return elem;
  }
}


abstract class AdminAction extends Action {
    @Inject
    User user;
    @Inject
    Router router;
    @Inject
    Application app;

    void push() {

      //TODO
      /*if (!user.isLoggedIn()) {
        router.goToUrl('/login');
      }*/
      //redirect to login
    }
}

abstract class IndexAction extends AdminAction {
  @Create
  Dashboard dashboard;

  void push() {
    app.layout.body.setChild(dashboard);
  }

}

@Compose
abstract class Dashboard extends Widget {
  String get html => '''
  <div>
    HOME
  </div>
  ''';

}

abstract class LoginAction extends Action {

    @Inject
    Application app;
    @Inject
    User user;
    @Inject
    UserLoginForm form;

    void push() {
      app.layout.body.setChild(form);
    }
}

abstract class LogoutAction extends Action {
    @Inject
    Router router;
    @Inject
    User user;
    void push() {
      //user.logOut();
      router.goToUrl('/login');
    }
}



@AdminMenuItem('Settings', 'Modules', 'Setup')
abstract class ModulesAction extends AdminAction {
  void push() {

  }
}

@AdminMenuItem('Settings', 'Modules', 'Config')
abstract class SettingsAction extends AdminAction {
  void push() {

  }
}

@ComposeSubtypes
abstract class AdminGridAction<T extends Entity> extends AdminAction {

  @Inject
  Application app;
  @SubtypeFactory
  T create(String className);

  @Inject
  CRUDApi<T> api;

  @InjectInstances
  Map<String, AdminGridColumn<T>> columns;

  @Create
  AdminGridView gridView;

  @Factory
  NavbarLink navbarLinkFactory();

  var query;

  List<T> items = [];

  void searchItems() {
    api.search('asd').then((ps) {
      items = ps;
      gridView.body.clear();
      ps.forEach((p){
        var row = new TableRowElement();
        columns.forEach((s, column){
          row.addCell()..append(column.getCell(p));
        });
        gridView.body.element.append(row);
      });
    });
  }
  bool _init = false;
  void init() {
    if (_init) return;
    _init = true;
    columns.forEach((s, c){
      gridView.header.element.append(new TableCellElement()..text = s);
    });

    gridView.searchBtn.element.onClick.listen((e){
      print('XXXX');
      searchItems();
    });

    gridView.saveBtn.element.onClick.listen((e){
      api.save(items);
      /*newItems.forEach((item){
        print(item.toJson());
      });*/
    });
    /*
    creators.forEach((s, creator){
      var a = navbarLinkFactory();
      a.href='#';
      a.title=s;
      a.element.onClick.listen((e){
        e.preventDefault();
        print('CREATING');
        T item = creator.createNew();
        var row = new TableRowElement();
        columns.forEach((s, column){
          //if(column.fits(item)) {
            row.addCell()..append(column.getCell(item));
          //}
        });
        gridView.body.element.append(row);
        items.add(item);
      });
      gridView.creatorButtons.append(a);
    });*/

  }

  void push() {
    super.push();
    app.layout.body.setChild(gridView);
    init();
    searchItems();
  }

}

@Compose
abstract class AdminGridView extends Widget {

  @Create
  Slot header;
  @Create
  Slot body;
  @Create
  Slot saveBtn;
  @Create
  Slot searchBtn;

  @Create
  Slot creatorButtons;

  String get html => '''
  <div>
    <div class="text-right">
      <div class="input-group mb-3">
        <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
        <div class="input-group-append">
          <button id="searchBtn" class="btn btn-outline-secondary" type="button">search</button>
        </div>
      </div>

      <div class="btn-group" role="group">
        <button id="saveBtn" type="button" class="btn btn-secondary">save changes</button>
        <!--button type="button" class="btn btn-secondary">something else</button-->
      </div>
      <div class="btn-group" role="group">
        <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          columns..
        </button>
        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
          <a class="dropdown-item" href="#">Dropdown link</a>
          <a class="dropdown-item" href="#">Dropdown link</a>
        </div>
      </div>
      <div class="btn-group" role="group">
        <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Create new..
        </button>
        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1" id="creatorButtons">
        </div>
      </div>
    </div>
    <table class="table">
      <thead>
        <tr id="header"></tr>
      </thead>
      <tbody id="body">
      </tbody>
    </table>
  </div>
  ''';
}

@Compose
abstract class UserLoginForm extends Widget {

  @Inject
  User user;
  @Inject
  Router router;
  @Inject
  Site site;

  Element get element {
    var ret = new Element.html(html);
    ButtonElement button = ret.querySelector('button');
    print(user);

    button.onClick.listen((e){
      /*user.logIn({

      }).then((success){
          if (success) {
            router.goToUrl('/');
          }
          //else errors
      });*/

      print('CLICK !!!');
    });
    return ret;
  }

  String get html => '''
  <div class="modal-shadow bg-primary" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <h5 class="text-light" id="modal-label">
        <img src="/packages/module_core/icon/icon.svg" width ="22"/>
        ${site.name} Admin Panel
      </h5>
      <div class="modal-content">
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Email:</label>
              <input type="text" class="form-control" id="recipient-name">
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Password:</label>
              <input type="password" class="form-control" id="recipient-name">
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary">Login</button>
        </div>
      </div>
    </div>
  </div>
  ''';
}

@Compose
abstract class AdminMenuLink extends Widget {
  bool active;
  String href;

  String get html => '''
    <li class="nav-item active">
      <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
    </li>
    ''';
}

@Compose
abstract class AdminNavbar extends Widget {

    @Create
    Slot menu;
    @Create
    Slot right;
    @Inject
    Router router;

    @Factory
    NavbarDropdown navbarDropdownFactory();
    @Factory
    NavbarLink navbarLinkFactory();

    void init() {

      Map <String, NavbarDropdown> menuItems = {};
      /*
      for (String type in router.actionTypes.keys) {
        router.actionTypes[type].metadata.whereType<AdminMenuItem>().forEach((var adminMenuItem){
          var href = type.replaceFirst('Action', '');
          href = '/${href[0].toLowerCase()}${href.substring(1)}';

          NavbarDropdown dd;
          if (menuItems.containsKey(adminMenuItem.section)) {
            dd = menuItems[adminMenuItem.section];
          } else {
            dd = navbarDropdownFactory();
            dd.name = adminMenuItem.section;
            menuItems[adminMenuItem.section] = dd;
            menu.append(dd);
          }
          //<div class="dropdown-divider"></div>

          var a = navbarLinkFactory();
          a.href = href;
          a.title = adminMenuItem.title;
          dd.items.append(a);

        });

      }*/
      print('navbar init');
    }

    String get html => '''
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
      <a class="navbar-brand" href="/" data-type="local"><img src="/packages/module_core/icon/icon.svg" width ="22"/></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto" id="menu">
        </ul>
        <ul class="navbar-nav mr-auto" id="right">
        </ul>
      </div>
    </nav>
    ''';

}

@Compose
abstract class UserForm extends Widget {

  String get html => '''
  <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
      User
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
      <a class="dropdown-item" data-type="local" href="/profile">Profile</a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" data-type="local" href="/logout">Logout</a>
    </div>
  </li>
  ''';

}
