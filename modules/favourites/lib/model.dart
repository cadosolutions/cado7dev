library mocule_favourites;

import 'package:module_core/model.dart';
import 'package:module_pim/model.dart';
import 'package:swift_composer/swift_composer.dart';

abstract class Favourites extends Entity {

  @Field
  List<ProductBase> list = [];

  void addProduct (ProductBase p) {
    list.add(p);
  }

  void removeProduct (ProductBase p) {
    list.removeWhere((element) => element.id.value == p.id.value);
  }

  bool containsProduct (ProductBase p) {
    return list.where((element) => element.id.value == p.id.value).isNotEmpty;
  }
}
