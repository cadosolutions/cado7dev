import 'model.dart';
export 'model.dart';

import "package:c7server/c7server.dart";
import "package:swift_ids/swift_ids.dart";

import "package:module_core/server.dart";


abstract class SnippetsRepository extends Repository<CmsWidgetConfig> {
  String get collectionName => 'snippets';

}

abstract class CmsRepository extends Repository<Page> {
  String get collectionName => 'cms';
}

abstract class GetByIdsController extends JsonAction {
  @PostArg
  List<String> ids;

  Future<List> run () async {
    print(appId);

  }
}

abstract class GetBySlugsController extends JsonAction {
  @PostArg
  List<String> slugs;

  @Inject
  CmsRepository get cmsRepository;

  Future<List> run () async {
    //transform ids and relations
    return (await cmsRepository.rawQuery({'slug' : { '\$in': slugs }})).map(cmsRepository.toJson).toList();
  }
}

abstract class SearchController extends JsonAction {

    @PostArg
    String query;

    @Inject
    CmsRepository get cmsRepository;

    Future<List> run () async {

        /*
        cmsRepository.getById(id)
        List<Map> products = new List<Map>();

        //Db db = new Db("mongodb://localhost:27017/swift");
        await db.open();
        products = await db.collection('cms').find().toList();
        products.forEach((p){
          p['id'] = new Id(p['_id'].toInt()).toString();
          p.remove('_id');
        });
        return products;
        */
    }
}

abstract class PagesSaveController extends JsonAction {

    @PostArg
    List<dynamic> items;


    Future run() async {
/*
        //List<SaveStatus> ret;
        //products = new List<Map>();
        Db db = new Db("mongodb://localhost:27017/swift");
        await db.open();
        List<Map<String, dynamic>> newDocuments = [];
        for (int i=0; i< items.length; i++){
          Map map = items[i] as Map;
          if (map['id'] == null) {
            var ret = await db.collection('counters').findAndModify(
              query: where.eq('_id', 'products'),
              update: modify.inc('seq', 1),
              returnNew: true
            );
            int id = ret['seq'].round();;
            map['_id'] = id;
            map.remove('id');
            newDocuments.add(map);
          } else {
            map['_id'] = new Id.fromString(map['id']).value;
            map.remove('id');
            await db.collection('cms').save(map);
          }
        }
        if (newDocuments.length > 0) {
          await db.collection('cms').insertAll(newDocuments);
        }

        items.forEach((p){
          p['id'] = new Id(p['_id'].toInt()).toString();
          p.remove('_id');
        });

        print(items);
        print('XXXXXX');
        //products = await db.collection('products').save();
        return true;
        */
    }
}
