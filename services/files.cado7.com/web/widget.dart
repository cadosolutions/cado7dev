
import 'dart:html';
import 'dart:convert';
import 'dart:async';

class InputFile {
    String name='foobar';
    InputElement input;
    SpanElement progress;
    DivElement label;

    String render(){
        return """
      				<label class="nice-file">
                <span class="progress"></span>
      				  <input type="file" name="$name" class="hidden"/>
      					<div class="label">add file</div>
      				</label>
              """;
    }

    bind(HtmlElement element){
      input = element.querySelector('input');
      progress = element.querySelector('span.progress');
      label = element.querySelector('div.label');
      input.onChange.listen((e){
        final files = input.files;
        if (files.length == 1) {
          final file = files[0];
          var form = new FormData();
          form.append('token', 'XXX');
          form.appendBlob('file', file, file.name);
          sendDatas(form);
        }
      });
      print(element);
    }

    onThumbnailLoaded(String res) {
      Map data = JSON.decode(res);
      print(data['thumb.svg']);

      label.className = 'label';
      label.setInnerHtml(data['thumb.svg'], treeSanitizer:NodeTreeSanitizer.trusted);
      label.children.first.attributes['viewBox'] = "0 0 ${label.children.first.attributes['width']} ${label.children.first.attributes['height']}";
      label.children.first.attributes['width'] = "100%";
      label.children.first.attributes['height'] = "100";
      label.children.first.attributes['filter'] = "";

      ImageElement image = new ImageElement(src: data['url']);
      image.onLoad.listen((e) {label.children = [image];});
    }

    updateThumbnail(String uid) {
      new Future.delayed(const Duration(seconds: 1), (){
        HttpRequest.getString(uid).then(onThumbnailLoaded, onError: (e) {
          print(e);
          updateThumbnail(uid);
        });
      });
    }

    /// send data to server
    sendDatas(FormData data) {
      final req = new HttpRequest();

      label.text = '0%';

      req.upload.onProgress.listen((ProgressEvent e) {
        label.text = (100 * e.loaded / e.total).round().toString() + '%';
      });
      req.onReadyStateChange.listen((Event e) {
        if (req.readyState == HttpRequest.DONE && (req.status == 200 || req.status == 0)) {
          label.text = '';
          label.className = 'label loader';
          updateThumbnail(JSON.decode(req.responseText));
        }
      });
      req.open("POST", "http://127.0.0.1:7201/");
      req.send(data);
    }

}

void main() {
  //test only
  window.onLoad.listen((e){
    HtmlElement container = document.querySelector('#test');
    var widget = new InputFile();
    final NodeValidatorBuilder _htmlValidator = new NodeValidatorBuilder.common()
      ..allowElement('*', attributes: ['style']);

    container.appendHtml(widget.render(), validator:_htmlValidator);
    widget.bind(container.children[0]);

  });
}
