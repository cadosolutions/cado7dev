import 'dart:html';
import 'dart:async';

import 'package:swift_composer/swift_composer.dart';

@ComposeSubtypes
abstract class Action {

  @Inject
  Router get router;

  @InjectClassName
  String get className;

  FutureOr<String> get title => "";

  String get url {
    String u = router.getPathName(className) + '/' + args.join('/');
    return (u == router.indexUrl) ? '/' : '/' + u;
  }

  Action get parent => null;

  @Require
  List<String> args;

  List<Future> get prepare => [];

  void ready() {}

  void run() {
    router.previous?.pop();

    Future.wait(prepare).then((v){
      print("READY");
      push();
      ready();
    });

    if (title is Future) {
        document.title = '...';
        (title as Future).then((s) {document.title = s;});
    } else {
        document.title = title;
    }
  }

  void pop() {}
  void push();

  @deprecated
  void activateMenu(String id, String href) {
    querySelector(id).querySelectorAll<AnchorElement>('a').forEach((e){
      e.parent.classes.toggle('active', e.href == href);
    });
  }

  @deprecated
  void selectSection(String id) {
    querySelector(id).parent.children.forEach((e){
      e.classes.remove('current');
    });
    querySelector(id).classes.add('current');
  }
}

@Compose
abstract class Router {

    @SubtypeFactory
    Action actionFactory(String className, List<String> args);
    @InjectSubtypesNames
    String getCode<T extends Action>();

    String url<T extends Action>(List<String> args) => actionFactory(getCode<T>(), args).url;

    String getClassCode(String pathName){
      /*String newKey = s;
      newKey = newKey.substring(newKey.indexOf('.') + 1);
      newKey = '${newKey[0].toLowerCase()}${newKey.substring(1)}';*/
      if (pathName.contains('.')) {
        return "module_${pathName}Action";
      } else {
        return "${pathName}Action";
      }
    }

    String getPathName(String classCode){
      classCode = classCode.replaceFirst('Action', '');
      classCode = classCode.replaceFirst('module_', '');
      return classCode;
    }

    @InjectConfig
    String get indexUrl;

    Action previous = null;
    Action current = null;

    void init() {

      /*actions.keys.forEach((s){
        String newKey = s;
        newKey = newKey.substring(newKey.indexOf('.') + 1);
        newKey = '${newKey[0].toLowerCase()}${newKey.substring(1)}';
        newKey = newKey.replaceFirst('Action', '');
        routing[newKey] = actions[s];
        print('$newKey =>  ${routing[newKey].className}');
      });*/

      window.onPopState.listen((PopStateEvent e) {
        e.preventDefault();
        _goToUrl(window.location.pathname);
      });
      _goToUrl(window.location.pathname);
    }

    Element scrollTarget;

    void scrollToElementStep(num time, num targetTime) {
      int targetOffset = scrollTarget.offsetTop;
      if (time < targetTime) {
        int currentOffset = document.scrollingElement.scrollTop;
        int change = ((targetOffset - currentOffset) * (1.0 - ((targetTime - time) / 500.0))).round();

        document.scrollingElement.scrollTop = currentOffset + change;
        window.animationFrame.then((time){
          scrollToElementStep(time, targetTime);
        });
      } else {
        document.scrollingElement.scrollTop = targetOffset;
      }
    }

    void smoothScrollTo(Element e) {
      scrollTarget = e;
      print('smooth scroll to ${e.id}');
      window.animationFrame.then((time){
        scrollToElementStep(time, time + 500.0);
      });
    }

    Action actionFromPath(String path) {
      path = path == '/' ? path + indexUrl : path;
      var args = path.split('/');
      args.removeAt(0);

      String code = args.isEmpty ? '' : args[0];
      code = getClassCode(code);

      if (args.isNotEmpty) args.removeAt(0);
      var ret = actionFactory(code, args);
      if (ret == null) {
        ret = actionFactory('module_core.Error404Action', []);
      }
      //print(code);
      return ret;
    }

    void goToUrl(String path) {
      _goToUrl(path);
      window.history.pushState(current.args, "", Uri.parse(path).path);
    }

    void _goToUrl(String path) {

      var base = document.head.querySelector('meta[name=c7base]');
      if (base != null) {
        path = path.replaceFirst(base.attributes['content'], '/');
      }
      print("route to: $path");

      previous = current;
      current = actionFromPath(path);
      //TODO: if previous state loading then cancel?
      current.run();
      //TODO: scroll to location
      smoothScrollTo(document.body);
    }

    void localiseAnchor(AnchorElement a){
      //print('localising anchor #${a.id}.${a.className} ${a.href} ${document.baseUri}');
      //if ((a.href != null) && (a.target != '_blank') && (a.href.startsWith(document.baseUri)) && (!a.href.startsWith(document.baseUri + '#'))) {
        a.onClick.listen((Event e){
          e.preventDefault();
          goToUrl(Uri.parse(a.href).path);
        });
       //}
    }
    void localiseArea(AreaElement a){
      if ((a.href != null) && (a.target != '_blank') && (a.href.startsWith(document.baseUri)) && (!a.href.startsWith(document.baseUri + '#'))) {
        a.onClick.listen((Event e){
          e.preventDefault();
          goToUrl(Uri.parse(a.href).path);
        });
       }
    }

    void updateLayout(){
      /*
      String path = window.location.href.replaceFirst(document.baseUri, '');
      if (path.indexOf('?') > -1 ) {
        path = path.substring(0, path.indexOf('?'));
      }

      print('GOING TO ${path}');
      for (String key in routes.keys) {
        if (path.startsWith(key)) {
          var args = path.replaceRange(0, key.length, '').split('/');
          print(args);
          document.title = routes[key].title;

          if (current != null) {
            previous = current;
          }
          current = routes[key];
          current.push(args);
          break;
        }
      }
      print('fake scroll');
      onScroll();
      */
    }

    /*AnchorElement localAnchor(){
      var x = new AnchorElement();
      //localiseAnchor(x);
      return x;
    }*/

}
