import 'package:swift_composer/swift_composer.dart';

import 'package:miniaturepaintingstudio.com/site.dart';

import 'package:module_core/site.dart' as module_core;
import 'package:module_pim/site.dart' as module_pim;
import 'package:module_cms/site.dart' as module_cms;

part 'main.c.dart';

void main() {
  $om.module_core_Application.run();
}

//random:08739594194513672804