import 'package:module_core/site.dart';
import 'model.dart';
export 'model.dart';

@Compose
abstract class PimApi extends CRUDApi<ProductBase> {
    String get moduleCode => 'pim';
    String get serviceCode => 'pim';
}
