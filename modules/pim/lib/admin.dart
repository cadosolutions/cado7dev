library mocule_pim;

import 'package:module_core/admin.dart';
import 'client.dart';
export 'client.dart';

import 'dart:html';


@AdminMenuItem('Catalog', 'Products', 'Simple')
abstract class ProductsAction extends AdminGridAction<ProductBase> {}


class NameColumn extends AdminGridFieldColumn<ProductBase, String> {
  field(p) => p.name;
}

class PriceColumn extends AdminGridFieldColumn<ProductBase, int> {
  field(p) => p.price;
}

class ShortDescriptionColumn extends AdminGridFieldColumn<ProductBase, String> {
  field(p) => p.shortDescription;
}
