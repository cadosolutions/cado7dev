
import 'package:module_core/model.dart';
import 'package:module_pim/model.dart';
import 'package:module_cms/model.dart' as module_cms;

abstract class PaintingNavConfig extends module_cms.CmsWidgetConfig {

}

abstract class Painting extends ProductBase {
  @Field
  String category;

  @Field
  String name;

  @Field
  String examples;

  @Field
  int paintBasePrice;
  @Field
  int assemblyPrice;
  @Field
  int baseBasePrice;
}
