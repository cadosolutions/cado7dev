#!/usr/bin/env dart

import "dart:async";
import "package:c7server/c7server.dart";

import "package:module_core/server.dart" as module_core;
import 'package:module_contact/server.dart' as module_contact;
import 'package:module_om/server.dart' as module_om;
import 'package:module_customdata/server.dart' as module_customdata;

part 'server.c.dart';

Future main (List<String> arguments) async {
  $om.server.serve();
}
