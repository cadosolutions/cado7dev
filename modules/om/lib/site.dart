library mocule_om;

import 'client.dart';
export 'client.dart';

import 'package:module_core/site.dart';
import 'package:module_pim/site.dart';

import 'dart:async';
import 'dart:html';
import 'dart:convert';


@Compose
abstract class ShoppingCart {

  @Factory
  Order orderFactory();

  Order _orderCache;
  Order get order {
    if (_orderCache == null) {
      _orderCache = orderFactory();
      if (window.localStorage.containsKey('order')) {
        _orderCache.fromJson(json.decode(window.localStorage['order']));
      }
    }
    return _orderCache;
  }

  void save() {
    //print(json.encode(order.toJson()));
    window.localStorage['order'] = json.encode(order.toJson());
  }
}

@SiteMenuItem('Cart', 'see cart')
abstract class CartAction extends SiteAction {

    String get title => "Shopping Cart";

    @Inject
    CartWidget get cart;

    void push() {
      app.layout.modal.setChild(cart);
      document.body.classes.add('modal');
      app.layout.modal.element.parent.parent.classes.add('side');
      app.layout.modal.element.parent.parent.classes.add('show');
      cart.regenerateItems();
    }

    void pop() {
      app.layout.modal.element.parent.parent.classes.remove('show');
      app.layout.modal.element.parent.parent.classes.remove('side');
      document.body.classes.remove('modal');
    }
}

abstract class PaymentSuccessAction extends SiteAction {

  @Inject
  PaymentSuccessWidget get paymentSuccessWidget;

  void push() {
    paymentSuccessWidget.orderId = args[0];
    app.layout.body.setChild(paymentSuccessWidget);
  }
}
abstract class PaymentSuccessWidget extends Widget {

  @Inject
  ShoppingCart get shoppingCart;

  String orderId;

  String get html => """
  <div>
    <h1>Order Paid</h1>
    <h1>order id: ${orderId}</h1>
    <h2></h2>
  </div>
  """;
}


abstract class PlaceOrderAction extends SiteAction {

  @Inject
  OmApi get omApi;

  @Inject
  ShoppingCart get shoppingCart;

  List<Future> get prepare => [
    omApi.placeOrder(shoppingCart.order)
  ];

  void push() {
    router.goToUrl(
      router.url<OrderStatusAction>([
        shoppingCart.order.id.toString()
      ])
    );
  }
}

abstract class PaymentMethodWidget extends Widget {
  @Require
  PaymentMethod paymentMethod;
  @Inject
  ShoppingCart get shoppingCart;

  @Inject
  Application get app;

  String get gatewayUrl => paymentMethod.paymentActionUrl;
  Map <String, String> get fields => paymentMethod.getPaymentActionFields(
    shoppingCart.order,
    app.site.getEnvUrl() + router.url<PaymentSuccessAction>([shoppingCart.order.id.toString()])
  );

  String get html => """
  <div>
    <form action="$gatewayUrl" method="post" class="form">
      """ + fields.map((key, value) { return new MapEntry(key, """
     <input type="hidden" name="$key" value="$value" />
     """); }).values.join('')
     + """
     <input name="submit_send" value="PAY WITH ${paymentMethod.fullName}" type="submit" />
    </form>
  </div>
  """;
}

abstract class OrderStatusAction extends SiteAction {

  @Inject
  OmApi get omApi;

  Order order;
  @Inject
  ShoppingCart get shoppingCart;

  List<Future> get prepare => [
    //omApi.getById(new Id.fromString(args[0])).then((o) => order = o)
  ];

  @Factory
  OrderStatusWidget orderStatusWidget(Order order);

  @InjectInstances
  Map<String, PaymentMethod> get paymentMethods;

  @Factory
  PaymentMethodWidget createPaymentMethodWidget(PaymentMethod paymentMethod);

  void push() {
    order = shoppingCart.order;
    OrderStatusWidget orderStatus = orderStatusWidget(order);
    app.layout.body.setChild(orderStatus);
    paymentMethods.forEach((code, paymentMethod){
      orderStatus.paymentInput.append(createPaymentMethodWidget(paymentMethod));
    });
  }
}

abstract class OrderStatusWidget extends Widget {

  @Require
  Order order;

  @Create
  Slot paymentInput;

  String get html => """
  <div>
    <div id="header">
      <h1>Order ${order.id.toString()}</h1>
      <h2>status: awaiting payment</h2>
    </div>
    <div class="container">
      <h2>Select Payment Method</h2>
      <div data-slot="paymentInput"></div>
    </div>
  </div>
  """;
}

abstract class CheckoutAction extends SiteAction {
  String get title => "...";

  @Inject
  ShoppingCart get shoppingCart;

  @Inject
  CheckoutWidget get checkoutWidget;

  @InjectInstances
  Map<String, DeliveryMethod> get deliveryMethods;

  @Factory
  DeliveryMethodWidget createDeliveryMethodWidget(DeliveryMethod deliveryMethod);

  void push() {

    //validate shopping cart
    app.layout.body.setChild(checkoutWidget);
    deliveryMethods.forEach((code, dm){
      checkoutWidget.deliveryInput.append(createDeliveryMethodWidget(dm));
    });
  }

}

abstract class DeliveryMethodWidget extends Widget {
  @Require
  DeliveryMethod deliveryMethod;
  @Inject
  ShoppingCart get shoppingCart;

  void init () {
    //TODO select if selected
    super.init();
    element.onClick.listen((e) {
      shoppingCart.order.deliveryMethod = deliveryMethod;
      shoppingCart.save();
    });
  }

  String get html => """
  <div>
    <label><input type="radio" name="delivery">${deliveryMethod.fullName} ${deliveryMethod.getDeliveryCost(shoppingCart.order)}</label>
  </div>
  """;

}

abstract class CheckoutWidget extends Widget {

  @Inject
  ShoppingCart get shoppingCart;

  @Create
  Slot deliveryInput;

  String get html => """
  <div>
    <div id="header">
      <h1>Checkout</h1>
      <h2>cart total: ${shoppingCart.order.totalPriceFormatted}</h2>
    </div>
    <div class="container form">
      <h2>Your Cart</h2>
      <h1>total: ${shoppingCart.order.totalPriceFormatted}</h1>
      <h2>Shipping Method</h2>
      <div data-slot="deliveryInput"></div>
      <div class="form-actions">
        <a data-type="local" href="${router.url<PlaceOrderAction>([])}" class="btn primary">
          PLACE ORDER
        </a>
      </div>
    </div>
  </div>
  """;
}

abstract class CartWidget extends Widget {

  @Inject
  ShoppingCart get shoppingCart;

  @Create
  Slot rows;

  @Factory
  CartItemWidget createCartItemWidget(OrderItem item);

  @Factory
  EmptyCartWidget createEmptyCartWidget();

  void regenerateItems() {
    rows.clear();
    if (shoppingCart.order.items.isEmpty) {
      rows.append(createEmptyCartWidget());
    } else {
      shoppingCart.order.items.forEach((i) {
        rows.append(createCartItemWidget(i));
      });
    }
    element.querySelector('#order').classes.toggle(
        'show',
        shoppingCart.order.items.isNotEmpty
    );
  }

  void init() {
    print('CART INIT');
    regenerateItems();
  }


  String get html => """
  <div>
    <h1 class="modal-title">Your Shopping Cart</h1>
    <div class="modal-body cart-items">
      <div class="cart-header">
        Product
        <div class="subtotal">Subtotal</div>
        <div class="qty">Qty</div>
      </div>
      <div data-slot="rows">
      </div>
    </div>
    <div class="modal-actions">
      <strong>total: <span class="cartTotal">${shoppingCart.order.totalPriceFormatted}</span></strong>
      <a id="order" data-type="local" href="${router.url<CheckoutAction>([])}" class="btn primary">
        Place Order
      </a>
    </div>
  </div>
  """;
}

abstract class EmptyCartWidget extends Widget {
  String get html => """
  <div>
    Your cart is empty.
  </div>
  """;
}

abstract class CartItemWidget extends Widget {

  @Require
  OrderItem item;

  @Inject
  CartWidget get cart;

  @Inject
  ShoppingCart get shoppingCart;

  void setQuantity(int quantity) {
    item.quantity = quantity;
    if (item.quantity < 1) {
      print('REMOVING');
      //TODO
      //element.remove();
      shoppingCart.order.items.remove(item);
      cart.regenerateItems();
    } else {
      //cart.repaint();
      updatePrice();
    }
    document.querySelector('.cartTotal').innerText = shoppingCart.order.totalPriceFormatted;
    document.querySelector('#cart .total').innerText = shoppingCart.order.totalPriceFormatted;
    //update total
    shoppingCart.save();
  }
  void init(){
    super.init();
    updatePrice();
    element.querySelector('.quantity').onChange.listen((e){
      NumberInputElement input = element.querySelector('.quantity');
      setQuantity(int.parse(input.value));

    });
    element.querySelector('.sub').onClick.listen((e) {
      NumberInputElement input = element.querySelector('.quantity');
      setQuantity(item.quantity - 1);
      input.value = item.quantity.toString();
    });
    element.querySelector('.inc').onClick.listen((e) {
      NumberInputElement input = element.querySelector('.quantity');
      setQuantity(item.quantity + 1);
      input.value = item.quantity.toString();
    });
  }

  bool updatePrice(){
    element.querySelector('.subtotal').innerText = item.itemPriceFormatted;
  }

  String get html => """
  <div class="cart-item">
    <img width="64" height="64" src="${item.product.image?.thumbUrl}" alt="${item.product.name}"/>
    <div class="details">
      <h3>${item.product.name}</h3>
      ${item.productPriceFormatted}
    </div>
    <div class="qty">
      <button class="sub"><span class="icon-minus"></span></button>
      <input class="quantity" type="number" value="${item.quantity}"/>
      <button class="inc"><span class="icon-plus"></span></button>
    </div>
    <div class="subtotal"></div>
  </div>
  """;
}

abstract class InitCartButton extends TypePlugin<Application> {

  @Inject
  ShoppingCart get shoppingCart;

  @MethodPlugin
  void afterRun()
  {
    //TODO cart button separate
    print('UPDATE CART');
    document.querySelector('#cart .total').innerText = shoppingCart.order.totalPriceFormatted;

  }
}

abstract class AddToCartButton extends Widget {

  @Require
  ProductBase product;

  @Inject
  Layout get layout;

  @Inject
  ShoppingCart get shoppingCart;

  void init() {
    super.init();


    element.onClick.listen((e){
      e.preventDefault();

      shoppingCart.order.addItem(product, 1);
      shoppingCart.save();
      print('ADDED ITEM');
      print(shoppingCart.order.totalPrice);
      //getOrder().addNewItem(poster, size: sizeSelectKey, language: currentLanguage);
      layout.element.querySelector('#cart .total').innerText = shoppingCart.order.totalPriceFormatted;

      var itemElement = element;
      while (itemElement != null && !itemElement.classes.contains('item')) {
        itemElement = itemElement.parent;
      }
      if (itemElement != null) {
        var badgeRect = layout.element.querySelector('#cart').getBoundingClientRect();
        var itemRect = itemElement.getBoundingClientRect();

        Element clone = itemElement.clone(true);// as Element;
        clone.style
          ..transition = 'top 1s ease, left 1s ease, width 1s ease, height 1s ease'
          ..position = 'fixed'
          ..overflow = 'hidden'
          ..left = (itemRect.left).round().toString() + 'px'
          ..top = (itemRect.top).round().toString() + 'px'
          ..width = (itemRect.width).round().toString() + 'px'
          ..height = (itemRect.height).round().toString() + 'px';

        document.body.append(clone);

        new Timer(const Duration(milliseconds: 10), (){
          clone.style
            ..left = (badgeRect.left).round().toString() + 'px'
            ..top = (badgeRect.top).round().toString() + 'px'
            ..width = (badgeRect.width).round().toString() + 'px'
            ..height = (badgeRect.height).round().toString() + 'px';
        });

        new Timer(const Duration(milliseconds: 1000), (){
          clone.remove();
        });
      }
    });
  }

  String get html => """
    <a href="/cart/add/${product.id}" class="fright btn" title="Add to Cart">
      <span class="icon-cart-add"></span>
      <span class="title">Add to Cart</span>
    </a>
  """;

}


abstract class AddToProductWidget extends TypePlugin<ProductWidget> {

  @Factory
  AddToCartButton newButton(ProductBase product);

  @MethodPlugin
  void afterInit(){
    parent.buttons.append(newButton(parent.item));
  }
}

abstract class AddToShowProductWidget extends TypePlugin<ShowProductWidget> {

  @Factory
  AddToCartButton newButton(ProductBase product);

  @MethodPlugin
  void afterInit(){
    AddToCartButton button = newButton(parent.item);
    button.element.classes.add('primary');
    parent.buttons.append(button);
  }
}
