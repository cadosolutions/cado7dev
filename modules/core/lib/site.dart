library module_core;

import 'dart:html';
import 'dart:convert';
import 'dart:math';
import 'dart:async';

import 'client.dart';
export 'client.dart';

@deprecated
String getApiUrl() {
  String domain = document.head.querySelector('meta[name=cado7domain]').attributes['content'];
  String site = document.head.querySelector('meta[name=cado7site]').attributes['content'];
  return document.baseUri.toString().replaceFirst(domain, 'api.cado7.com') + site + '/';
}

@ComposeSubtypes
abstract class SiteAction extends Action {
    @Inject
    User get user;
    @Inject
    Application get app;
    @Inject
    Layout get layout;
}

@Compose
abstract class Error404Action extends SiteAction {
  @Create
  Error404Widget index;

  String get title => '404 Error | ' + app.site.name;
  String get url => null;

  void push() {
    app.layout.body.setChild(index);
  }
}


abstract class Error404Widget extends Widget {

  String get html => """
  <div>
    <h1>Error 404: Not Found</h1>
    <h3>requested url was not found</h3>
  </div>
  """;
}



/*
class ActionLink extends AnchorElement {
  ActionLink.created() : super.created() {
    style.background = "red";
  }
  factory ActionLink(text) => new Element.tag('my-redbox')..text = text;
}
document.registerElement('my-redbox', ActionLink);
*/
/*
Map<String, View> views;

abstract class View {
  String title = '';
  void pop();
  void push(args);
}
*/


class SiteMenuItem {
  final String section;
  final String title;
  const SiteMenuItem(this.section, this.title);
}

@ComposeSubtypes
abstract class SiteShowAction<T extends Entity> extends SiteAction {
  Future<T> item;
  T itemSync;

  @Inject
  CRUDApi<T> get api;

  @Factory
  ShowItemWidget<T> createWidget(T item);

  ShowItemWidget<T> widget;

  List<Future> get prepare => [
    (item = api.getBySlug(args[0])).then((i) => itemSync = i)
    //(item = api.get(new Id.fromString(args[0]))).then((i) => itemSync = i)
  ];

  void push() {
    widget = createWidget(itemSync);
    app.layout.body.setChild(widget);
  }

}

@ComposeSubtypes
abstract class SiteListAction<T extends Entity> extends SiteAction {

  @Inject
  CRUDApi<T> get api;

  var query;

  List<T> items = [];

  @Create
  ListWidget listWidget;

  @Factory
  ListItemWidget<T> createListWidget(T item);

  void searchItems() {
    api.search('asd').then((ps) {
      print('SEARCH DONE');
      ps.forEach((p){
        var w = createListWidget(p);
        listWidget.items.append(w);
      });
    });
  }
  bool _init = false;
  void init() {
    if (_init) return;
    _init = true;
  }

  void push() {
    //app.layout.body.setChild(gridView);
    init();
    app.layout.body.setChild(listWidget);
    searchItems();
  }
}

abstract class ListWidget extends Widget {
  @Create
  Slot items;

  String get html => '''
    <div>
      <h1>Our Products</h1>
      <div id="items">
      </div>
    </div>
  ''';
}

@ComposeSubtypes
abstract class ListItemWidget<T extends Entity> extends Widget {
  @Require
  T item;
}

@ComposeSubtypes
abstract class ShowItemWidget<T extends Entity> extends Widget {
  @Require
  T item;
}
