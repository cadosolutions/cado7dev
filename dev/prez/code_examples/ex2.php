<?php

/**
 * Encode the mixed $data into the JSON format.
 * @param mixed $data
 * @return string
 */
public function encode($data)
{
	$this->translateInline->processResponseBody($data);
	return \Zend_Json::encode($data);
}


