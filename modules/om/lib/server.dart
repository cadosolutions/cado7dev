
import 'package:c7server/c7server.dart';
import 'dart:convert';
import 'package:mailer2/mailer.dart';
import 'package:module_core/server.dart';
import 'dart:math';
import 'model.dart';
export 'model.dart';

abstract class OrdersRepository extends Repository<Order> {
  String get collectionName => 'orders';
}

abstract class PlaceOrderController extends JsonAction {
    @PostArg
    Map order;

    @Inject
    OrdersRepository get repository;

    String emailBody() {
      return "";
    }

    Future<Map> run() async {
      //na wszelki
      print(json.encode(order));
      var status = await repository.saveRaw(order);

      return {
        'id': status.id.toString()
      };
      //repository
      /*
      var options = new SmtpOptions()
      ..name = 'orders@swift.shop'
      ..hostName = 'mail.cado7.com'
      ..port = 587
      ..requiresAuthentication = true
      ..username = 'orders@swift.shop'
      ..password = '25bf9f6086eea5ba062d9bf73df1c98a';

      var emailTransport = new SmtpTransport(options);

      //args['email'];

      // Create our mail/envelope.
      var envelope = new Envelope()
        ..from = 'orders@swift.shop'
        ..sender = 'orders@swift.shop'
        ..replyTos = ['maksbereski@gmail.com']
        ..recipients.add('${order['name']}<${order['email']}>')
        //..bccRecipients.add('maksbereski@gmail.com')
        //..bccRecipients.add('francio@francio.pl')

        ..subject = 'plakiat.com order confirmation ($orderId)'
        ..text = 'TODO'
        ..html = emailBody();


        emailTransport.send(envelope)
          .then((envelope) => print('Email sent!'))
          .catchError((e) => print('Error occurred: $e'));

      var envelope2 = new Envelope()
        ..from = 'orders@swift.shop'
        ..sender = 'orders@swift.shop'
        ..replyTos = ['${order['name']}<${order['email']}>']
        ..recipients.add('maksbereski@gmail.com')
        ..recipients.add('francio@francio.pl')

        ..subject = 'plakiat.com order ($orderId)'
        ..text = 'TODO'
        ..html = emailBody();


        emailTransport.send(envelope2)
          .then((envelope2) => print('Email sent!'))
          .catchError((e) => print('Error occurred: $e'));


      return new Future<String>((){
        return orderId.toString();
      });
      */
    }
}

@deprecated
abstract class CheckoutController extends JsonAction {

  @PostArg
  Map order;

  String emailBody() {

    String rows = "";
    (order['items'] as List).forEach((item){
      rows += """
      <tr>
        <td><img src="${item['thumbnail']}" alt="poster"/></td>
        <td>${item['quantity']}</td>
        <td>${item['name']}</td>
        <td>${item['size']}</td>
        <td>${item['language']}</td>
        <td>${item['price']} PLN</td>
      </tr>
      """;
    });

    return """
    <style type="text/css">
body {
  background-color: #f9f9f9;
  color: #1f1f1f;
  font-family: "Roboto", helvetica, arial, sans-serif;
  font-size: 16px;
  font-weight: 400;
}
h2 {
  margin: 30px 0;
}
table#orderItems {
  width: 100%;
  margin: 24px 0;
}
table#orderItems th {
  color: #f9f9f9;
  background: #1f1f1f;
  font-size: 18px;
  padding: 12px;
  text-align: left;
  vertical-align: middle;
  font-weight: 200;
}
table#orderItems td {
  padding: 12px;
  text-align: left;
  vertical-align: middle;
  font-weight: 300;
  font-size: 17px;
}

img {
  width: 64px;
}
</style>
<table width="600" cellpadding="0" cellspacing="0" align="center">
      <tr>
        <td width="600" colspan="2">
          <h1>Confirm your plakiat.com order</h1>
          <p>
            Following order was placed on <a href="http://plakiat.com/">plakiat.com</a> using your email address.
	  </p>
	  <p>
	    Please review and if everything is correct please follow instructions below.
          </p>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table id="orderItems">
            <tr><th>poster</th><th>quantity</th><th>name</th><th>size</th><th>language</th><th>price</th></tr>
                $rows
            <tr><td colspan="4"></td><td>shipping:</td><td>${order['shippingCost']}</td></tr>
            <tr><td colspan="4"></td><th>total:</th><th>${order['totalPLN']}</th></tr>
          </table>
        </td>
      </tr>
      <tr>
        <td>
          <h3>shipment details</h3>
            name: <strong>${order['name']}</strong><br/>
            email: <strong>${order['email']}</strong><br/>
            city: <strong>${order['shippingAddress']['city']}</strong><br/>
            zipcode: <strong>${order['shippingAddress']['zipcode']}</strong><br/>
            country: <strong>${order['shippingAddress']['country']}</strong><br/>
            street: <strong>${order['shippingAddress']['street']}</strong><br/>
            shippingMethod: <strong>${order['shippingMethod']}</strong><br/>
            comments: <strong>${order['comments']}</strong>
        </td>
        <td>
          <h3>invoice details</h3>
          company: <strong>${order['invoiceData']['company']}</strong><br/>
          name: <strong>${order['invoiceData']['name']}</strong><br/>
          NIP/TIN: <strong>${order['invoiceData']['nip']}</strong><br/>
          city: <strong>${order['invoiceData']['address']['city']}</strong><br/>
          zipcode: <strong>${order['invoiceData']['address']['zipcode']}</strong><br/>
          country: <strong>${order['invoiceData']['address']['country']}</strong><br/>
          street: <strong>${order['invoiceData']['address']['street']}</strong><br/>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <h2>Final step: payment</h2>
	  <p>Your order will be confirmed and shipped after you make a payment. If everything above is OK, please select a payment method and pay ${order['totalPLN']} balance (or ${order['totalUSD']} or ${order['totalEUR']}).</p>
	  <p>Feel free to contact me at maksbereski@gmail.com or replying to this email.</p>
	  <h4>Bank Transfer</h4>
          <p>
		account: <b>PL30 1020 5011 0000 9902 0323 3491</b><br/>
		title: <b>plakiat.com order $orderId</b>
	  </p>
	  <h4>PayPal</h4>
          <p>
		email: <b>maksbereski@gmail.com</b><br/>
		message: <b>plakiat.com order $orderId</b>
	  </p>
        </td>
      </tr>
      <tr>
	<td colspan="2">&nbsp;</td>
      </tr>
      <tr>
	<td></td>
	<td>
	  Thank you,<br/>
    &copy; Plakiat, Maks Bereski
	</td>
      </tr>
 </table>
      """;
  }

  int orderId;

  Future<String> run() {
    //na wszelki
    print(json.encode(order));

    orderId = ((10000 - 100) * (new Random()).nextDouble()).toInt() + 100;

    /*var options = new GmailSmtpOptions()
      ..username = 'slonce.karpat@gmail.com'
      ..password = 'GAzwtpub1';*/

    var options = new SmtpOptions()
    ..name = 'orders@swift.shop'
    ..hostName = 'mail.cado7.com'
    ..port = 587
    ..requiresAuthentication = true
    ..username = 'orders@swift.shop'
    ..password = '25bf9f6086eea5ba062d9bf73df1c98a';

    var emailTransport = new SmtpTransport(options);

    //args['email'];

    // Create our mail/envelope.
    var envelope = new Envelope()
      ..from = 'orders@swift.shop'
      ..sender = 'orders@swift.shop'
      ..replyTos = ['maksbereski@gmail.com']
      ..recipients.add('${order['name']}<${order['email']}>')
      //..bccRecipients.add('maksbereski@gmail.com')
      //..bccRecipients.add('francio@francio.pl')

      ..subject = 'plakiat.com order confirmation ($orderId)'
      ..text = 'TODO'
      ..html = emailBody();


      emailTransport.send(envelope)
        .then((envelope) => print('Email sent!'))
        .catchError((e) => print('Error occurred: $e'));

    var envelope2 = new Envelope()
      ..from = 'orders@swift.shop'
      ..sender = 'orders@swift.shop'
      ..replyTos = ['${order['name']}<${order['email']}>']
      ..recipients.add('maksbereski@gmail.com')
      ..recipients.add('francio@francio.pl')

      ..subject = 'plakiat.com order ($orderId)'
      ..text = 'TODO'
      ..html = emailBody();


      emailTransport.send(envelope2)
        .then((envelope2) => print('Email sent!'))
        .catchError((e) => print('Error occurred: $e'));


    return new Future<String>((){
      return orderId.toString();
    });
  }
}
