#!/usr/bin/env dart

import 'package:mongo_dart/mongo_dart.dart';
import 'package:swift_ids/swift_ids.dart';
import 'dart:io';

void main(List<String> args) async {

  var db = new Db("mongodb://localhost:27017/swift");
  await db.open();

  List users = await db.collection('users').find().toList();
  Map<int, String> usernames = {};
  users.forEach((user){
    usernames[user['_id']] = user['email'];
  });

  List apps = await db.collection('apps').find().toList();
  Map<int, String> appids = {};

  apps.forEach((app){

    String id = new Id(app['_id']).toString();
    if (app['_parent'] != null) {
      id = appids[app['_parent']] + '-' + id;
    }
    print("$id ${app['url']} \"${app['name']}\" (${usernames[app['_owner']]})");
    appids[app['_id']] = id;
  });

  await db.close();
}
