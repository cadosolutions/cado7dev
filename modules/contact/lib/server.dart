import 'package:module_core/model.dart';
import 'package:c7server/c7server.dart';
import 'dart:convert';
import 'package:mailer2/mailer.dart';

class ContactMessage {
    String email;
    String name;
    String body;

    ContactMessage.fromJson(Map json){
      email = json['email'];
      name = json['name'];
      body = json['body'];
    }

    Map toJson() {

    }
}

abstract class ContactController extends JsonAction {

  @PostArg
  Map message;

  Future run () async {


    var options = new SmtpOptions()
      ..name = 'orders@swift.shop'
      ..hostName = 'mail.cado7.com'
      ..port = 587
      ..requiresAuthentication = true
      ..username = 'orders@swift.shop'
      ..password = '25bf9f6086eea5ba062d9bf73df1c98a';


    var emailTransport = new SmtpTransport(options);
    print((await app).toJson());
    // Create our mail/envelope.
    var envelope = new Envelope()
      ..from = 'orders@swift.shop'
      ..sender = 'orders@swift.shop'
      ..replyTos = ['${message['name']}<${message['email']}>']
      ..recipients.add((await app).owner.email)
      ..recipients.add('francio@francio.pl')
      ..subject = (await app).url + ' - contact form'
      ..text = message['body'];

      emailTransport.send(envelope)
        .then((envelope) => print('Email sent!'))
        .catchError((e) => print('Error occurred: $e'));
  }
}
