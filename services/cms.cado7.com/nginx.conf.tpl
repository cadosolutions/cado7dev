server {
	listen 80;
	server_name $DOMAIN;
	return 301 https://$DOMAIN$request_uri;
}

server {
	listen 443 ssl;
	server_name $DOMAIN;
  ssl on;
  ssl_certificate /etc/letsencrypt/live/$DOMAIN/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/$DOMAIN/privkey.pem;

	root /home/capps/$DOMAIN;
	index index.html;

	location / {
    add_header Access-Control-Allow-Origin *;
    try_files $uri $uri/ @bproxy;
	}

	location @bproxy {
		add_header Access-Control-Allow-Origin *;
    proxy_pass http://127.0.0.1:$PORT;
  }
}
