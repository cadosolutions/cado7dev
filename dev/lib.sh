#!/bin/bash

projectDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

function init() {
	cd $projectDir
	echo "project $projectDir"
}

function libs() {
	echo "c7liquid c7frames c7orm"
}

function modules() {
	echo "c7core c7shop c7tickets"
}

init

