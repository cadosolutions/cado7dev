// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'server.dart';

// **************************************************************************
// SwiftGenerator
// **************************************************************************

//no interceptor for [DeferredLibrary]
//no interceptor for [DeferredLoadException]
//no interceptor for [FutureOr]
//no interceptor for [Future]
//no interceptor for [TimeoutException]
//no interceptor for [Completer]
//no interceptor for [Stream]
//no interceptor for [StreamSubscription]
//no interceptor for [EventSink]
//no interceptor for [StreamView]
//no interceptor for [StreamConsumer]
//no interceptor for [StreamSink]
//no interceptor for [StreamTransformer]
//no interceptor for [StreamTransformerBase]
//no interceptor for [StreamIterator]
//no interceptor for [MultiStreamController]
//no interceptor for [StreamController]
//no interceptor for [SynchronousStreamController]
//no interceptor for [Timer]
//no interceptor for [AsyncError]
//no interceptor for [ZoneSpecification]
//no interceptor for [ZoneDelegate]
//no interceptor for [Zone]
//no interceptor for [HttpAction]
//no interceptor for [UrlPattern]
//no interceptor for [Redirect]
//no interceptor for [Error404]
//no interceptor for [JsonAction]
//interceptor for [Routing]
//interceptor for Routing
//can be singleton: TRUE
//parent: Routing [@bool get Compose]
class $Routing extends Routing implements Pluggable {
  $Routing() {}
  T plugin<T>() {
    return null;
  }

  HttpAction createAction(String className) {
    switch (className) {
      case 'module_core.AppController':
        return new $module_core_AppController();
      case 'module_pim.GetBySlugsController':
        return new $module_pim_GetBySlugsController();
      case 'module_pim.SearchController':
        return new $module_pim_SearchController();
      case 'module_pim.SaveController':
        return new $module_pim_SaveController();
    }
  }
}

//interceptor for [ServerConfig]
//interceptor for ServerConfig
//can be singleton: TRUE
//parent: ServerConfig [@bool get Compose]
class $ServerConfig extends ServerConfig implements Pluggable {
  $ServerConfig() {
//Map<dynamic, dynamic>
//create
  }
  T plugin<T>() {
    return null;
  }
}

//interceptor for [Server]
//interceptor for Server
//can be singleton: TRUE
//parent: Server [@bool get Compose]
class $Server extends Server implements Pluggable {
  $Server() {
//String
//int
  }
  T plugin<T>() {
    return null;
  }

  Routing get routing => $om.routing;
  ServerConfig get config => $om.serverConfig;
}

//no interceptor for [AnnotatedWith]
//no interceptor for [Pluggable]
//no interceptor for [TypePlugin]
//no interceptor for [module_core.SaveStatus]
//no interceptor for [module_core.Repository]
//interceptor for [module_core.AppRepository]
//interceptor for module_core.AppRepository
//T[212882645] => App[275838045]
//can be singleton: TRUE
//parent: AppRepository []
//parent: Repository [@bool get ComposeSubtypes]
class $module_core_AppRepository extends module_core.AppRepository
    implements Pluggable {
  $module_core_AppRepository() {}
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  Map<String, module_core.App> get emptyInstances =>
      $om.instancesOfmodule_core_App;
  Map<String, module_core.Repository<module_core.Entity>>
      get relatedRepositories =>
          $om.instancesOfmodule_core_Repository_module_core_Entity_;
  module_core.App instanceCreator(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
    }
  }
}

//interceptor for [module_core.UserRepository]
//interceptor for module_core.UserRepository
//T[212882645] => User[533911946]
//can be singleton: TRUE
//parent: UserRepository []
//parent: Repository [@bool get ComposeSubtypes]
class $module_core_UserRepository extends module_core.UserRepository
    implements Pluggable {
  $module_core_UserRepository() {}
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  Map<String, module_core.User> get emptyInstances =>
      $om.instancesOfmodule_core_User;
  Map<String, module_core.Repository<module_core.Entity>>
      get relatedRepositories =>
          $om.instancesOfmodule_core_Repository_module_core_Entity_;
  module_core.User instanceCreator(String className) {
    switch (className) {
      case 'module_core.User':
        return new $module_core_User();
    }
  }
}

//interceptor for [module_core.AppController]
//interceptor for module_core.AppController
//can be singleton: TRUE
//parent: AppController []
//parent: JsonAction [@bool get ComposeSubtypes]
//parent: HttpAction [@bool get ComposeSubtypes]
class $module_core_AppController extends module_core.AppController
    implements Pluggable {
  $module_core_AppController() {
//HttpRequest
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  module_core.Repository<module_core.App> get appRepository =>
      $om.module_core_AppRepository;
  void setPostArgs(Map json) {
//compiled method
//@PostArg
//@PostArg
//@PostArg
  }
}

//no interceptor for [module_core.Entity]
//interceptor for [module_core.App]
//interceptor for module_core.App
//can be singleton: TRUE
//parent: App []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_App extends module_core.App implements Pluggable {
  $module_core_App() {
//Id
//String
//App
//User
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_core.App'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['parent'] = "module_core.App";
    }
    {
      target['owner'] = "module_core.User";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.parent = relations['parent'];
    }
    {
      this.owner = relations['owner'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['url'] = this.url;
    }
    {
      target['name'] = this.name;
    }
//@Field
//@Field
    {
      target['parent'] = this.parent != null ? this.parent.toJson() : null;
    }
    {
      target['owner'] = this.owner != null ? this.owner.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.url = target.containsKey('url') ? target['url'] : this.url;
    }
    {
      this.name = target.containsKey('name') ? target['name'] : this.name;
    }
//@Field
//@Field
//@Field
    {
      if (target['parent'] != null) {
        if (target['parent'] is String) {
          this.parent = relatedFactory("module_core.App");
          this.parent.id = idFromString(target['parent']);
        } else {
          this.parent = relatedFactory(target['parent']['classes'].last);
          this.parent.fromJson(target['parent']);
        }
      } else {
        this.parent = null;
      }
    }
    {
      if (target['owner'] != null) {
        if (target['owner'] is String) {
          this.owner = relatedFactory("module_core.User");
          this.owner.id = idFromString(target['owner']);
        } else {
          this.owner = relatedFactory(target['owner']['classes'].last);
          this.owner.fromJson(target['owner']);
        }
      } else {
        this.owner = null;
      }
    }
  }
}

//interceptor for [module_core.User]
//interceptor for module_core.User
//can be singleton: TRUE
//parent: User []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_User extends module_core.User implements Pluggable {
  $module_core_User() {
//Id
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_core.User'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['email'] = this.email;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.email = target.containsKey('email') ? target['email'] : this.email;
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_core.Image]
//interceptor for module_core.Image
//can be singleton: TRUE
//parent: Image []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_Image extends module_core.Image implements Pluggable {
  $module_core_Image() {
//Id
//String
//App
//String
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_core.Image'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['app'] = "module_core.App";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.app = relations['app'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['mimetype'] = this.mimetype;
    }
    {
      target['filename'] = this.filename;
    }
    {
      target['thumb'] = this.thumb;
    }
//@Field
//@Field
    {
      target['app'] = this.app != null ? this.app.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.mimetype =
          target.containsKey('mimetype') ? target['mimetype'] : this.mimetype;
    }
    {
      this.filename =
          target.containsKey('filename') ? target['filename'] : this.filename;
    }
    {
      this.thumb = target.containsKey('thumb') ? target['thumb'] : this.thumb;
    }
//@Field
//@Field
//@Field
    {
      if (target['app'] != null) {
        if (target['app'] is String) {
          this.app = relatedFactory("module_core.App");
          this.app.id = idFromString(target['app']);
        } else {
          this.app = relatedFactory(target['app']['classes'].last);
          this.app.fromJson(target['app']);
        }
      } else {
        this.app = null;
      }
    }
  }
}

//interceptor for [module_pim.ProductsRepository]
//interceptor for module_pim.ProductsRepository
//T[212882645] => ProductBase[447620454]
//can be singleton: TRUE
//parent: ProductsRepository []
//parent: Repository [@bool get ComposeSubtypes]
class $module_pim_ProductsRepository extends module_pim.ProductsRepository
    implements Pluggable {
  $module_pim_ProductsRepository() {}
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  Map<String, module_pim.ProductBase> get emptyInstances =>
      $om.instancesOfmodule_pim_ProductBase;
  Map<String, module_core.Repository<module_core.Entity>>
      get relatedRepositories =>
          $om.instancesOfmodule_core_Repository_module_core_Entity_;
  module_pim.ProductBase instanceCreator(String className) {
    switch (className) {
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
    }
  }
}

//interceptor for [module_pim.GetBySlugsController]
//interceptor for module_pim.GetBySlugsController
//can be singleton: TRUE
//parent: GetBySlugsController []
//parent: JsonAction [@bool get ComposeSubtypes]
//parent: HttpAction [@bool get ComposeSubtypes]
class $module_pim_GetBySlugsController extends module_pim.GetBySlugsController
    implements Pluggable {
  $module_pim_GetBySlugsController() {
//HttpRequest
//String
//String
//List<String>
  }
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  module_core.Repository<module_core.App> get appRepository =>
      $om.module_core_AppRepository;
  module_pim.ProductsRepository get repository =>
      $om.module_pim_ProductsRepository;
  void setPostArgs(Map json) {
//compiled method
//@PostArg
//@PostArg
//@PostArg
    {
      this.slugs =
          (json.containsKey('slugs') ? new List.from(json['slugs']) : null);
    }
  }
}

//interceptor for [module_pim.SearchController]
//interceptor for module_pim.SearchController
//can be singleton: TRUE
//parent: SearchController []
//parent: JsonAction [@bool get ComposeSubtypes]
//parent: HttpAction [@bool get ComposeSubtypes]
class $module_pim_SearchController extends module_pim.SearchController
    implements Pluggable {
  $module_pim_SearchController() {
//HttpRequest
//String
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  module_core.Repository<module_core.App> get appRepository =>
      $om.module_core_AppRepository;
  void setPostArgs(Map json) {
//compiled method
//@PostArg
    {
      this.query = (json.containsKey('query') ? json['query'] : null);
    }
//@PostArg
//@PostArg
  }
}

//no interceptor for [module_pim.SaveStatus]
//interceptor for [module_pim.SaveController]
//interceptor for module_pim.SaveController
//can be singleton: TRUE
//parent: SaveController []
//parent: JsonAction [@bool get ComposeSubtypes]
//parent: HttpAction [@bool get ComposeSubtypes]
class $module_pim_SaveController extends module_pim.SaveController
    implements Pluggable {
  $module_pim_SaveController() {
//HttpRequest
//String
//String
//List<dynamic>
  }
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  module_core.Repository<module_core.App> get appRepository =>
      $om.module_core_AppRepository;
  void setPostArgs(Map json) {
//compiled method
//@PostArg
//@PostArg
//@PostArg
    {
      this.items =
          (json.containsKey('items') ? new List.from(json['items']) : null);
    }
  }
}

//no interceptor for [module_pim.UnitType]
//no interceptor for [module_pim.ConfigurableField]
//no interceptor for [module_pim.ProductBase]
//interceptor for [module_pim.SimpleProduct]
//interceptor for module_pim.SimpleProduct
//can be singleton: TRUE
//parent: SimpleProduct []
//parent: ProductBase [@bool get ComposeSubtypes]
//parent: Entity [@bool get ComposeSubtypes]
class $module_pim_SimpleProduct extends module_pim.SimpleProduct
    implements Pluggable {
  $module_pim_SimpleProduct() {
//String
//int
//String
//ConfigurableField<UnitType>
//Image
//Id
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_pim.ProductBase', 'module_pim.SimpleProduct'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_pim.SimpleProduct':
        return new $module_pim_SimpleProduct();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['image'] = "module_core.Image";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.image = relations['image'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
    {
      target['price'] = this.price;
    }
//@Field
    {
      target['name'] = this.name;
    }
    {
      target['shortDescription'] = this.shortDescription;
    }
    {
      target['slug'] = this.slug;
    }
//@Field
//@Field
    {
      target['image'] = this.image != null ? this.image.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.name = target.containsKey('name') ? target['name'] : this.name;
    }
    {
      this.shortDescription = target.containsKey('shortDescription')
          ? target['shortDescription']
          : this.shortDescription;
    }
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
//@Field
    {
      this.price = target.containsKey('price') ? target['price'] : this.price;
    }
//@Field
//@Field
    {
      if (target['image'] != null) {
        if (target['image'] is String) {
          this.image = relatedFactory("module_core.Image");
          this.image.id = idFromString(target['image']);
        } else {
          this.image = relatedFactory(target['image']['classes'].last);
          this.image.fromJson(target['image']);
        }
      } else {
        this.image = null;
      }
    }
  }
}

class $ObjectManager {
  $Routing _routing;
  $Routing get routing {
    if (_routing == null) {
      _routing = new $Routing();
    }
    return _routing;
  }

  $ServerConfig _serverConfig;
  $ServerConfig get serverConfig {
    if (_serverConfig == null) {
      _serverConfig = new $ServerConfig();
    }
    return _serverConfig;
  }

  $Server _server;
  $Server get server {
    if (_server == null) {
      _server = new $Server();
    }
    return _server;
  }

  $module_core_AppRepository _module_core_AppRepository;
  $module_core_AppRepository get module_core_AppRepository {
    if (_module_core_AppRepository == null) {
      _module_core_AppRepository = new $module_core_AppRepository();
    }
    return _module_core_AppRepository;
  }

  $module_core_UserRepository _module_core_UserRepository;
  $module_core_UserRepository get module_core_UserRepository {
    if (_module_core_UserRepository == null) {
      _module_core_UserRepository = new $module_core_UserRepository();
    }
    return _module_core_UserRepository;
  }

  $module_core_AppController _module_core_AppController;
  $module_core_AppController get module_core_AppController {
    if (_module_core_AppController == null) {
      _module_core_AppController = new $module_core_AppController();
    }
    return _module_core_AppController;
  }

  $module_core_App _module_core_App;
  $module_core_App get module_core_App {
    if (_module_core_App == null) {
      _module_core_App = new $module_core_App();
    }
    return _module_core_App;
  }

  $module_core_User _module_core_User;
  $module_core_User get module_core_User {
    if (_module_core_User == null) {
      _module_core_User = new $module_core_User();
    }
    return _module_core_User;
  }

  $module_core_Image _module_core_Image;
  $module_core_Image get module_core_Image {
    if (_module_core_Image == null) {
      _module_core_Image = new $module_core_Image();
    }
    return _module_core_Image;
  }

  $module_pim_ProductsRepository _module_pim_ProductsRepository;
  $module_pim_ProductsRepository get module_pim_ProductsRepository {
    if (_module_pim_ProductsRepository == null) {
      _module_pim_ProductsRepository = new $module_pim_ProductsRepository();
    }
    return _module_pim_ProductsRepository;
  }

  $module_pim_GetBySlugsController _module_pim_GetBySlugsController;
  $module_pim_GetBySlugsController get module_pim_GetBySlugsController {
    if (_module_pim_GetBySlugsController == null) {
      _module_pim_GetBySlugsController = new $module_pim_GetBySlugsController();
    }
    return _module_pim_GetBySlugsController;
  }

  $module_pim_SearchController _module_pim_SearchController;
  $module_pim_SearchController get module_pim_SearchController {
    if (_module_pim_SearchController == null) {
      _module_pim_SearchController = new $module_pim_SearchController();
    }
    return _module_pim_SearchController;
  }

  $module_pim_SaveController _module_pim_SaveController;
  $module_pim_SaveController get module_pim_SaveController {
    if (_module_pim_SaveController == null) {
      _module_pim_SaveController = new $module_pim_SaveController();
    }
    return _module_pim_SaveController;
  }

  $module_pim_SimpleProduct _module_pim_SimpleProduct;
  $module_pim_SimpleProduct get module_pim_SimpleProduct {
    if (_module_pim_SimpleProduct == null) {
      _module_pim_SimpleProduct = new $module_pim_SimpleProduct();
    }
    return _module_pim_SimpleProduct;
  }

  Map<String, module_core.App> get instancesOfmodule_core_App {
    return {
      "module_core.App": new $module_core_App(),
    };
  }

  Map<String, module_core.Repository>
      get instancesOfmodule_core_Repository_module_core_Entity_ {
    return {
      "module_core.AppRepository": new $module_core_AppRepository(),
      "module_core.UserRepository": new $module_core_UserRepository(),
      "module_pim.ProductsRepository": new $module_pim_ProductsRepository(),
    };
  }

  Map<String, module_core.User> get instancesOfmodule_core_User {
    return {
      "module_core.User": new $module_core_User(),
    };
  }

  Map<String, module_pim.ProductBase> get instancesOfmodule_pim_ProductBase {
    return {
      "module_pim.SimpleProduct": new $module_pim_SimpleProduct(),
    };
  }
}

$ObjectManager $om = new $ObjectManager();
//generated in 122ms
