import 'dart:html';

import 'package:swift_composer/swift_composer.dart';

import 'package:module_core/site.dart' as module_core;
import 'package:module_pim/site.dart' as module_pim;
import 'package:module_om/site.dart' as module_om;
import 'package:module_favourites/site.dart' as module_favourites;
import 'package:module_breadcrumbs/site.dart' as module_breadcrumbs;

import 'package:module_cms/site.dart' as module_cms;
import 'package:module_pim_slider/site.dart' as module_pim_slider;
import 'package:module_payment_stripe/site.dart' as module_payment_stripe;
import 'package:module_payment_przelewy24/site.dart' as module_payment_przelewy24;
import 'package:module_delivery_pickup/site.dart' as module_delivery_pickup;
import 'package:module_delivery_flat/site.dart' as module_delivery_flat;

part 'main.c.dart';

abstract class InitScrollWatch extends TypePlugin<module_core.Application> {

  @MethodPlugin
  void afterRun()
  {
    document.body.classes.toggle(
        'top',
        document.scrollingElement.scrollTop == 0
    );
    window.onScroll.listen((e){
      document.body.classes.toggle(
          'top',
          document.scrollingElement.scrollTop == 0
      );
    });
  }
}


void main() {
  $om.module_core_Application.run();
}

//random:23965677794