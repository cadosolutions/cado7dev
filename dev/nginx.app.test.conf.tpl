server {
	listen 80;
	server_name $DOMAIN www.$DOMAIN;
	return 301 https://$DOMAIN$request_uri;
}

server {
	server_name $DOMAIN;

	listen 443 ssl;
  ssl on;
  ssl_certificate /etc/letsencrypt/live/$DOMAIN/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/$DOMAIN/privkey.pem;

	root /home/capps/$DOMAIN;
	index index.html;

  error_page 500 /500.html;

	auth_basic "Test Area";
	auth_basic_user_file /home/capps/.htpasswd;

	location / {
		try_files $uri /index.html;
	}
}
