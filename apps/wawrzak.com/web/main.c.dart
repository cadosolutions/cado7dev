// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'main.dart';

// **************************************************************************
// SwiftGenerator
// **************************************************************************

//no interceptor for [AnnotatedWith]
//no interceptor for [Pluggable]
//no interceptor for [TypePlugin]
//interceptor for [HomeAction]
//interceptor for HomeAction
//can be singleton: FALSE
//parent: HomeAction []
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $HomeAction extends HomeAction implements Pluggable {
  $HomeAction(args) {
//List<String>
    this.args = args;
//HomeWidget
//create
    this.widget = new $HomeWidget();
  }
  T plugin<T>() {
    return null;
  }

  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "HomeAction";
}

//interceptor for [HomeWidget]
//interceptor for HomeWidget
//can be singleton: TRUE
//parent: HomeWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $HomeWidget extends HomeWidget implements Pluggable {
  $HomeWidget() {
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/HomeWidget.html USED
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/cms/templates/HomeWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/core/templates/HomeWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/HomeWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../libs/swift_composer/templates/HomeWidget.html
  String get html => '''
<div id="home">
  <img src="https://files.cado7.com/kuba/works/OASIS/Oasis_No_1_Void.jpg"/>
</div>

''';
}

//no interceptor for [GalleryAction]
//interceptor for [WorksAction]
//interceptor for WorksAction
//can be singleton: FALSE
//parent: WorksAction []
//parent: GalleryAction [@bool get ComposeSubtypes]
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $WorksAction extends WorksAction implements Pluggable {
  $WorksAction(args) {
//List<String>
    this.args = args;
//WorksWidget
//create
    this.widget = new $WorksWidget();
  }
  T plugin<T>() {
    return null;
  }

  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "WorksAction";
}

//interceptor for [WorksWidget]
//interceptor for WorksWidget
//can be singleton: TRUE
//parent: WorksWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $WorksWidget extends WorksWidget implements Pluggable {
  $WorksWidget() {
//dynamic
//Slot
//create
    this.worksCarousel = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
    {
      this.worksCarousel.container = this;
      this.worksCarousel.element = element.querySelector('#' + 'worksCarousel');
      this.worksCarousel.element.classes.add('slot');
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.worksCarousel.widgets.forEach((widget) {
        this.worksCarousel.element.append(widget.element);
      });
    }
  }

  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/WorksWidget.html USED
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/cms/templates/WorksWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/core/templates/WorksWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/WorksWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../libs/swift_composer/templates/WorksWidget.html
  String get html => '''
<div class="section carousel slide" data-ride="carousel">
  <div class="carousel-inner" id="worksCarousel">
    <div class="carousel-item active"></div>
  </div>
  <a class="carousel-control-prev" href="#" role="button">
    <span>❮</span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#" role="button">
    <span>❯</span>
    <span class="sr-only">Next</span>
  </a>
  <ol class="carousel-indicators">
  </ol>
</div>

''';
}

//interceptor for [CommisionedAction]
//interceptor for CommisionedAction
//can be singleton: FALSE
//parent: CommisionedAction []
//parent: GalleryAction [@bool get ComposeSubtypes]
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $CommisionedAction extends CommisionedAction implements Pluggable {
  $CommisionedAction(args) {
//List<String>
    this.args = args;
//CommisionedWidget
//create
    this.widget = new $CommisionedWidget();
  }
  T plugin<T>() {
    return null;
  }

  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "CommisionedAction";
}

//interceptor for [CommisionedWidget]
//interceptor for CommisionedWidget
//can be singleton: TRUE
//parent: CommisionedWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $CommisionedWidget extends CommisionedWidget implements Pluggable {
  $CommisionedWidget() {
//dynamic
//Slot
//create
    this.commisionedCarousel = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
    {
      this.commisionedCarousel.container = this;
      this.commisionedCarousel.element =
          element.querySelector('#' + 'commisionedCarousel');
      this.commisionedCarousel.element.classes.add('slot');
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.commisionedCarousel.widgets.forEach((widget) {
        this.commisionedCarousel.element.append(widget.element);
      });
    }
  }

  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/CommisionedWidget.html USED
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/cms/templates/CommisionedWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/core/templates/CommisionedWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/CommisionedWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../libs/swift_composer/templates/CommisionedWidget.html
  String get html => '''
<div class="carousel slide" data-ride="carousel">
  <div class="carousel-inner" id="commisionedCarousel">
    <div class="carousel-item active"></div>
  </div>
  <a class="carousel-control-prev" href="#" role="button">
    <span>❮</span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#" role="button">
    <span>❯</span>
    <span class="sr-only">Next</span>
  </a>
  <ol class="carousel-indicators">
  </ol>
</div>

''';
}

//interceptor for [AwardsAction]
//interceptor for AwardsAction
//can be singleton: FALSE
//parent: AwardsAction []
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $AwardsAction extends AwardsAction implements Pluggable {
  $AwardsAction(args) {
//List<String>
    this.args = args;
//AwardsWidget
//create
    this.widget = new $AwardsWidget();
  }
  T plugin<T>() {
    return null;
  }

  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "AwardsAction";
}

//interceptor for [AwardsWidget]
//interceptor for AwardsWidget
//can be singleton: TRUE
//parent: AwardsWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $AwardsWidget extends AwardsWidget implements Pluggable {
  $AwardsWidget() {
//dynamic
//Slot
//create
    this.awardsContent = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
    {
      this.awardsContent.container = this;
      this.awardsContent.element = element.querySelector('#' + 'awardsContent');
      this.awardsContent.element.classes.add('slot');
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.awardsContent.widgets.forEach((widget) {
        this.awardsContent.element.append(widget.element);
      });
    }
  }

  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/AwardsWidget.html USED
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/cms/templates/AwardsWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/core/templates/AwardsWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/AwardsWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../libs/swift_composer/templates/AwardsWidget.html
  String get html => '''
<div class="container pad-top">
  <center>
    <h1>Awards</h1>
    <ul id="awardsContent">
    </ul>
  </center>
</div>

''';
}

//interceptor for [ExhibitionsAction]
//interceptor for ExhibitionsAction
//can be singleton: FALSE
//parent: ExhibitionsAction []
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $ExhibitionsAction extends ExhibitionsAction implements Pluggable {
  $ExhibitionsAction(args) {
//List<String>
    this.args = args;
//ExhibitionsWidget
//create
    this.widget = new $ExhibitionsWidget();
  }
  T plugin<T>() {
    return null;
  }

  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "ExhibitionsAction";
}

//interceptor for [ExhibitionsWidget]
//interceptor for ExhibitionsWidget
//can be singleton: TRUE
//parent: ExhibitionsWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $ExhibitionsWidget extends ExhibitionsWidget implements Pluggable {
  $ExhibitionsWidget() {
//dynamic
//Slot
//create
    this.exhibitionsContent = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
    {
      this.exhibitionsContent.container = this;
      this.exhibitionsContent.element =
          element.querySelector('#' + 'exhibitionsContent');
      this.exhibitionsContent.element.classes.add('slot');
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.exhibitionsContent.widgets.forEach((widget) {
        this.exhibitionsContent.element.append(widget.element);
      });
    }
  }

  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/ExhibitionsWidget.html USED
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/cms/templates/ExhibitionsWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/core/templates/ExhibitionsWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/ExhibitionsWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../libs/swift_composer/templates/ExhibitionsWidget.html
  String get html => '''
<div class="container pad-top">
  <center>
    <ul id="exhibitionsContent">
    </ul>
  </center>
</div>

''';
}

//interceptor for [AboutAction]
//interceptor for AboutAction
//can be singleton: FALSE
//parent: AboutAction []
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $AboutAction extends AboutAction implements Pluggable {
  $AboutAction(args) {
//List<String>
    this.args = args;
//AboutWidget
//create
    this.widget = new $AboutWidget();
  }
  T plugin<T>() {
    return null;
  }

  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "AboutAction";
}

//interceptor for [AboutWidget]
//interceptor for AboutWidget
//can be singleton: TRUE
//parent: AboutWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $AboutWidget extends AboutWidget implements Pluggable {
  $AboutWidget() {
//dynamic
//Slot
//create
    this.aboutContent = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
    {
      this.aboutContent.container = this;
      this.aboutContent.element = element.querySelector('#' + 'aboutContent');
      this.aboutContent.element.classes.add('slot');
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.aboutContent.widgets.forEach((widget) {
        this.aboutContent.element.append(widget.element);
      });
    }
  }

  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/AboutWidget.html USED
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/cms/templates/AboutWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/core/templates/AboutWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/AboutWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../libs/swift_composer/templates/AboutWidget.html
  String get html => '''
<div class="container pad-top">
  <center>
    <img src="/kuba.jpg" style="margin: 24px;"/>
    <div id="aboutContent">
    </div>
  </center>
</div>

''';
}

//interceptor for [PrepareData]
//interceptor for PrepareData
//T[362979300] => Application[123648522]
//can be singleton: FALSE
//parent: PrepareData []
//parent: TypePlugin [@bool get ComposeSubtypes]
class $PrepareData extends PrepareData implements Pluggable {
  $PrepareData(parent) {
//T
    this.parent = parent;
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
}

//no interceptor for [module_core.SiteAction]
//interceptor for [module_core.Error404Action]
//interceptor for module_core.Error404Action
//can be singleton: FALSE
//parent: Error404Action [@bool get Compose]
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $module_core_Error404Action extends module_core.Error404Action
    implements Pluggable {
  $module_core_Error404Action(args) {
//List<String>
    this.args = args;
//Error404Widget
//create
    this.index = new $module_core_Error404Widget();
  }
  T plugin<T>() {
    return null;
  }

  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "module_core.Error404Action";
}

//interceptor for [module_core.Error404Widget]
//interceptor for module_core.Error404Widget
//can be singleton: TRUE
//parent: Error404Widget []
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_Error404Widget extends module_core.Error404Widget
    implements Pluggable {
  $module_core_Error404Widget() {
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_core_Error404Widget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/cms/templates/module_core_Error404Widget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/core/templates/module_core_Error404Widget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_core_Error404Widget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../libs/swift_composer/templates/module_core_Error404Widget.html
}

//no interceptor for [module_core.SiteMenuItem]
//no interceptor for [module_core.SiteShowAction]
//no interceptor for [module_core.SiteListAction]
//interceptor for [module_core.ListWidget]
//interceptor for module_core.ListWidget
//can be singleton: TRUE
//parent: ListWidget []
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_ListWidget extends module_core.ListWidget
    implements Pluggable {
  $module_core_ListWidget() {
//dynamic
//Slot
//create
    this.items = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
    {
      this.items.container = this;
      this.items.element = element.querySelector('#' + 'items');
      this.items.element.classes.add('slot');
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.items.widgets.forEach((widget) {
        this.items.element.append(widget.element);
      });
    }
  }

  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_core_ListWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/cms/templates/module_core_ListWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/core/templates/module_core_ListWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_core_ListWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../libs/swift_composer/templates/module_core_ListWidget.html
}

//no interceptor for [module_core.ListItemWidget]
//no interceptor for [module_core.ShowItemWidget]
//no interceptor for [module_core.AnnotatedWith]
//no interceptor for [module_core.Pluggable]
//no interceptor for [module_core.TypePlugin]
//no interceptor for [module_core.PossibleTypoException]
//no interceptor for [module_core.Id]
//interceptor for [module_core.Site]
//interceptor for module_core.Site
//can be singleton: TRUE
//parent: Site [@bool get Compose]
class $module_core_Site extends module_core.Site implements Pluggable {
  $module_core_Site() {}
  T plugin<T>() {
    return null;
  }

  String get name => "Jakub Wawrzak";
  String get url => "wawrzak.com";
  module_core.Id get id => new module_core.Id.fromString("FV7B");
}

//interceptor for [module_core.Application]
//interceptor for module_core.Application
//can be singleton: TRUE
//parent: Application [@bool get Compose]
//plugin: PrepareData
class $module_core_Application extends module_core.Application
    implements Pluggable {
  PrepareData prepareData;
  $module_core_Application() {
//Map<String, Future<String>>
    prepareData = new $PrepareData(this);
  }
  T plugin<T>() {
    if (T == PrepareData) {
      return prepareData as T;
    }
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  module_core.Site get site => $om.module_core_Site;
  module_core.Layout get layout => $om.module_core_Layout;
  void run() {
    super.run();
    prepareData.afterRun();
  }
}

//interceptor for [module_core.Layout]
//interceptor for module_core.Layout
//can be singleton: TRUE
//parent: Layout [@bool get Compose]
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_Layout extends module_core.Layout implements Pluggable {
  $module_core_Layout() {
//dynamic
//Slot
//create
    this.navbar = new module_core.Slot();
//Slot
//create
    this.body = new module_core.Slot();
//Slot
//create
    this.beforeBody = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  module_core.Site get site => $om.module_core_Site;
  void initializeSlots() {
//compiled method
    {
      this.navbar.container = this;
      this.navbar.element = element.querySelector('#' + 'navbar');
      this.navbar.element.classes.add('slot');
    }
    {
      this.body.container = this;
      this.body.element = element.querySelector('#' + 'body');
      this.body.element.classes.add('slot');
    }
    {
      this.beforeBody.container = this;
      this.beforeBody.element = element.querySelector('#' + 'beforeBody');
      this.beforeBody.element.classes.add('slot');
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.navbar.widgets.forEach((widget) {
        this.navbar.element.append(widget.element);
      });
    }
    {
      this.body.widgets.forEach((widget) {
        this.body.element.append(widget.element);
      });
    }
    {
      this.beforeBody.widgets.forEach((widget) {
        this.beforeBody.element.append(widget.element);
      });
    }
  }

  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_core_Layout.html USED
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/cms/templates/module_core_Layout.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/core/templates/module_core_Layout.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_core_Layout.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../libs/swift_composer/templates/module_core_Layout.html
  String get html => '''
<div>
  <header>
    <div id="navbar"></div>
    <nav class="container navbar navbar-expand-lg navbar-auto">
      <a class="navbar-brand" data-type="local" href="/"><img width="44" src="/logo.png"/></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul id="mainmenu" class="navbar-nav mr-auto">
          <li class="nav-item dropdown active">
            <a class="nav-link dropdown-toggle" href="${router.url<WorksAction>([])}" id="navbarDropdown" role="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Works
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown" id="worksDropdown">
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="${router.url<CommisionedAction>([])}" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Commisioned
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown2" id="commisionedDropdown">
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-type="local" href="${router.url<AwardsAction>([])}">Awards</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-type="local" href="${router.url<ExhibitionsAction>([])}">Exhibitions</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-type="local" href="${router.url<AboutAction>([])}">About</a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <div>
    <div id="beforeBody"></div>
    <div id="body"></div>
  </div>
</div>

''';
}

//no interceptor for [module_core.ApiModule]
//no interceptor for [module_core.CRUDApi]
//no interceptor for [module_core.Action]
//interceptor for [module_core.Router]
//interceptor for module_core.Router
//can be singleton: TRUE
//parent: Router [@bool get Compose]
class $module_core_Router extends module_core.Router implements Pluggable {
  $module_core_Router() {
//Action
//Action
//Element
  }
  T plugin<T>() {
    return null;
  }

  String get indexUrl => "Home";
  module_core.Action actionFactory(String className, List<String> args) {
    switch (className) {
      case 'HomeAction':
        return new $HomeAction(args);
      case 'WorksAction':
        return new $WorksAction(args);
      case 'CommisionedAction':
        return new $CommisionedAction(args);
      case 'AwardsAction':
        return new $AwardsAction(args);
      case 'ExhibitionsAction':
        return new $ExhibitionsAction(args);
      case 'AboutAction':
        return new $AboutAction(args);
      case 'module_core.Error404Action':
        return new $module_core_Error404Action(args);
      case 'module_cms.PageAction':
        return new $module_cms_PageAction(args);
    }
  }

  String getCode<T extends module_core.Action>() {
    if (T == HomeAction) return 'HomeAction';
    if (T == WorksAction) return 'WorksAction';
    if (T == CommisionedAction) return 'CommisionedAction';
    if (T == AwardsAction) return 'AwardsAction';
    if (T == ExhibitionsAction) return 'ExhibitionsAction';
    if (T == AboutAction) return 'AboutAction';
    if (T == module_core.Error404Action) return 'module_core.Error404Action';
    if (T == module_cms.PageAction) return 'module_cms.PageAction';
  }
}

//no interceptor for [module_core.Slot]
//interceptor for [module_core.NavbarLink]
//interceptor for module_core.NavbarLink
//can be singleton: TRUE
//parent: NavbarLink []
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_NavbarLink extends module_core.NavbarLink
    implements Pluggable {
  $module_core_NavbarLink() {
//dynamic
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_core_NavbarLink.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/cms/templates/module_core_NavbarLink.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/core/templates/module_core_NavbarLink.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_core_NavbarLink.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../libs/swift_composer/templates/module_core_NavbarLink.html
}

//interceptor for [module_core.NavbarDropdown]
//interceptor for module_core.NavbarDropdown
//can be singleton: TRUE
//parent: NavbarDropdown []
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_NavbarDropdown extends module_core.NavbarDropdown
    implements Pluggable {
  $module_core_NavbarDropdown() {
//dynamic
//Slot
//create
    this.items = new module_core.Slot();
//String
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
    {
      this.items.container = this;
      this.items.element = element.querySelector('#' + 'items');
      this.items.element.classes.add('slot');
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.items.widgets.forEach((widget) {
        this.items.element.append(widget.element);
      });
    }
  }

  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_core_NavbarDropdown.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/cms/templates/module_core_NavbarDropdown.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/core/templates/module_core_NavbarDropdown.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_core_NavbarDropdown.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../libs/swift_composer/templates/module_core_NavbarDropdown.html
}

//no interceptor for [module_core.SwiftUriPolicy]
//no interceptor for [module_core.Widget]
//no interceptor for [module_core.AsyncWidget]
//interceptor for [module_core.TextWidget]
//interceptor for module_core.TextWidget
//can be singleton: FALSE
//parent: TextWidget [@bool get Compose]
//parent: Widget [@bool get ComposeSubtypes]
class $module_core_TextWidget extends module_core.TextWidget
    implements Pluggable {
  $module_core_TextWidget(text) {
//dynamic
//String
    this.text = text;
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_core_TextWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/cms/templates/module_core_TextWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/core/templates/module_core_TextWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_core_TextWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../libs/swift_composer/templates/module_core_TextWidget.html
}

//no interceptor for [module_core.Entity]
//interceptor for [module_core.App]
//interceptor for module_core.App
//can be singleton: TRUE
//parent: App []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_App extends module_core.App implements Pluggable {
  $module_core_App() {
//Id
//String
//App
//User
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_core.App'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['parent'] = "module_core.App";
    }
    {
      target['owner'] = "module_core.User";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.parent = relations['parent'];
    }
    {
      this.owner = relations['owner'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['url'] = this.url;
    }
    {
      target['name'] = this.name;
    }
//@Field
//@Field
    {
      target['parent'] = this.parent != null ? this.parent.toJson() : null;
    }
    {
      target['owner'] = this.owner != null ? this.owner.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target['slug'];
    }
    {
      this.url = target['url'];
    }
    {
      this.name = target['name'];
    }
//@Field
//@Field
//@Field
    {
      if (target['parent'] != null) {
        if (target['parent'] is String) {
          this.parent = relatedFactory("module_core.App");
          this.parent.id = idFromString(target['parent']);
        } else {
          this.parent = relatedFactory(target['parent']['classes'].last);
          this.parent.fromJson(target['parent']);
        }
      } else {
        this.parent = null;
      }
    }
    {
      if (target['owner'] != null) {
        if (target['owner'] is String) {
          this.owner = relatedFactory("module_core.User");
          this.owner.id = idFromString(target['owner']);
        } else {
          this.owner = relatedFactory(target['owner']['classes'].last);
          this.owner.fromJson(target['owner']);
        }
      } else {
        this.owner = null;
      }
    }
  }
}

//interceptor for [module_core.User]
//interceptor for module_core.User
//can be singleton: TRUE
//parent: User []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_User extends module_core.User implements Pluggable {
  $module_core_User() {
//Id
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_core.User'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['email'] = this.email;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target['slug'];
    }
    {
      this.email = target['email'];
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_core.Image]
//interceptor for module_core.Image
//can be singleton: TRUE
//parent: Image []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_Image extends module_core.Image implements Pluggable {
  $module_core_Image() {
//Id
//String
//App
//String
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_core.Image'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['app'] = "module_core.App";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.app = relations['app'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['mimetype'] = this.mimetype;
    }
    {
      target['filename'] = this.filename;
    }
    {
      target['thumb'] = this.thumb;
    }
//@Field
//@Field
    {
      target['app'] = this.app != null ? this.app.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target['slug'];
    }
    {
      this.mimetype = target['mimetype'];
    }
    {
      this.filename = target['filename'];
    }
    {
      this.thumb = target['thumb'];
    }
//@Field
//@Field
//@Field
    {
      if (target['app'] != null) {
        if (target['app'] is String) {
          this.app = relatedFactory("module_core.App");
          this.app.id = idFromString(target['app']);
        } else {
          this.app = relatedFactory(target['app']['classes'].last);
          this.app.fromJson(target['app']);
        }
      } else {
        this.app = null;
      }
    }
  }
}

//interceptor for [module_cms.PageAction]
//interceptor for module_cms.PageAction
//T[269908264] => Page[414991068]
//can be singleton: FALSE
//parent: PageAction []
//parent: SiteShowAction [@bool get ComposeSubtypes]
//parent: SiteAction [@bool get ComposeSubtypes]
//parent: Action [@bool get ComposeSubtypes]
class $module_cms_PageAction extends module_cms.PageAction
    implements Pluggable {
  $module_cms_PageAction(args) {
//Future<T>
//T
//ShowItemWidget<T>
//List<String>
    this.args = args;
  }
  T plugin<T>() {
    return null;
  }

  module_core.CRUDApi<module_cms.Page> get api => $om.module_cms_CmsPagesApi;
  module_core.User get user => $om.module_core_User;
  module_core.Application get app => $om.module_core_Application;
  module_core.Layout get layout => $om.module_core_Layout;
  module_core.Router get router => $om.module_core_Router;
  String get className => "module_cms.PageAction";
  module_core.ShowItemWidget<module_cms.Page> createWidget(
      module_cms.Page item) {
//module_core.ShowItemWidget
    return new $module_cms_ShowPageWidget(item);
  }

  module_cms.CmsWidget<module_cms.CmsWidgetConfig> createItemWidget(
      String className, module_cms.CmsWidgetConfig config) {
    switch (className) {
      case 'module_cms.HtmlWidget':
        return new $module_cms_HtmlWidget(config);
      case 'module_cms.ImageWidget':
        return new $module_cms_ImageWidget(config);
      case 'module_cms.ParagraphWidget':
        return new $module_cms_ParagraphWidget(config);
      case 'module_cms.HeadingWidget':
        return new $module_cms_HeadingWidget(config);
    }
  }
}

//interceptor for [module_cms.ShowPageWidget]
//interceptor for module_cms.ShowPageWidget
//T[391897947] => Page[414991068]
//can be singleton: FALSE
//parent: ShowPageWidget []
//parent: ShowItemWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $module_cms_ShowPageWidget extends module_cms.ShowPageWidget
    implements Pluggable {
  $module_cms_ShowPageWidget(item) {
//T
    this.item = item;
//dynamic
//Slot
//create
    this.pageContent = new module_core.Slot();
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
    {
      this.pageContent.container = this;
      this.pageContent.element = element.querySelector('#' + 'pageContent');
      this.pageContent.element.classes.add('slot');
    }
  }

  void repaintSlots() {
//compiled method
    {
      this.pageContent.widgets.forEach((widget) {
        this.pageContent.element.append(widget.element);
      });
    }
  }

  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_cms_ShowPageWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/cms/templates/module_cms_ShowPageWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/core/templates/module_cms_ShowPageWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_cms_ShowPageWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../libs/swift_composer/templates/module_cms_ShowPageWidget.html
}

//no interceptor for [module_cms.CmsWidget]
//interceptor for [module_cms.HtmlWidget]
//interceptor for module_cms.HtmlWidget
//CT[171676771] => HtmlConfig[389581089]
//can be singleton: FALSE
//parent: HtmlWidget []
//parent: CmsWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $module_cms_HtmlWidget extends module_cms.HtmlWidget
    implements Pluggable {
  $module_cms_HtmlWidget(config) {
//CT
    this.config = config;
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_cms_HtmlWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/cms/templates/module_cms_HtmlWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/core/templates/module_cms_HtmlWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_cms_HtmlWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../libs/swift_composer/templates/module_cms_HtmlWidget.html
}

//interceptor for [module_cms.ImageWidget]
//interceptor for module_cms.ImageWidget
//CT[171676771] => ImageConfig[445488254]
//can be singleton: FALSE
//parent: ImageWidget []
//parent: CmsWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $module_cms_ImageWidget extends module_cms.ImageWidget
    implements Pluggable {
  $module_cms_ImageWidget(config) {
//CT
    this.config = config;
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_cms_ImageWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/cms/templates/module_cms_ImageWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/core/templates/module_cms_ImageWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_cms_ImageWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../libs/swift_composer/templates/module_cms_ImageWidget.html
}

//interceptor for [module_cms.ParagraphWidget]
//interceptor for module_cms.ParagraphWidget
//CT[171676771] => ParagraphConfig[153581726]
//can be singleton: FALSE
//parent: ParagraphWidget []
//parent: CmsWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $module_cms_ParagraphWidget extends module_cms.ParagraphWidget
    implements Pluggable {
  $module_cms_ParagraphWidget(config) {
//CT
    this.config = config;
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_cms_ParagraphWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/cms/templates/module_cms_ParagraphWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/core/templates/module_cms_ParagraphWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_cms_ParagraphWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../libs/swift_composer/templates/module_cms_ParagraphWidget.html
}

//interceptor for [module_cms.HeadingWidget]
//interceptor for module_cms.HeadingWidget
//CT[171676771] => HeadingConfig[26884972]
//can be singleton: FALSE
//parent: HeadingWidget []
//parent: CmsWidget [@bool get ComposeSubtypes]
//parent: Widget [@bool get ComposeSubtypes]
class $module_cms_HeadingWidget extends module_cms.HeadingWidget
    implements Pluggable {
  $module_cms_HeadingWidget(config) {
//CT
    this.config = config;
//dynamic
  }
  T plugin<T>() {
    return null;
  }

  module_core.Router get router => $om.module_core_Router;
  void initializeSlots() {
//compiled method
  }
  void repaintSlots() {
//compiled method
  }

  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_cms_HeadingWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/cms/templates/module_cms_HeadingWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../modules/core/templates/module_cms_HeadingWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/templates/module_cms_HeadingWidget.html
  ///home/fsw/workspace/cado7dev/apps/wawrzak.com/../../libs/swift_composer/templates/module_cms_HeadingWidget.html
}

//interceptor for [module_cms.CmsPagesApi]
//interceptor for module_cms.CmsPagesApi
//T[534851839] => Page[414991068]
//can be singleton: TRUE
//parent: CmsPagesApi []
//parent: CRUDApi [@bool get ComposeSubtypes]
//parent: ApiModule [@bool get ComposeSubtypes]
class $module_cms_CmsPagesApi extends module_cms.CmsPagesApi
    implements Pluggable {
  $module_cms_CmsPagesApi() {}
  T plugin<T>() {
    return null;
  }

  Map<String, module_core.CRUDApi<module_core.Entity>> get relatedApis =>
      $om.instancesOfmodule_core_CRUDApi_module_core_Entity_;
  String get className => "module_cms.CmsPagesApi";
  module_core.Site get site => $om.module_core_Site;
  module_cms.Page newEntityByType(String className) {
    switch (className) {
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }
}

//interceptor for [module_cms.CmsWidgetConfig]
//interceptor for module_cms.CmsWidgetConfig
//can be singleton: TRUE
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_cms_CmsWidgetConfig extends module_cms.CmsWidgetConfig
    implements Pluggable {
  $module_cms_CmsWidgetConfig() {
//Id
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_cms.CmsWidgetConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target['slug'];
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_cms.HeadingConfig]
//interceptor for module_cms.HeadingConfig
//can be singleton: TRUE
//parent: HeadingConfig []
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_cms_HeadingConfig extends module_cms.HeadingConfig
    implements Pluggable {
  $module_cms_HeadingConfig() {
//Id
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_cms.CmsWidgetConfig', 'module_cms.HeadingConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['text'] = this.text;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target['slug'];
    }
    {
      this.text = target['text'];
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_cms.ImageConfig]
//interceptor for module_cms.ImageConfig
//can be singleton: TRUE
//parent: ImageConfig []
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_cms_ImageConfig extends module_cms.ImageConfig
    implements Pluggable {
  $module_cms_ImageConfig() {
//Id
//String
//Image
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_cms.CmsWidgetConfig', 'module_cms.ImageConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['image'] = "module_core.Image";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.image = relations['image'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
//@Field
//@Field
    {
      target['image'] = this.image != null ? this.image.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target['slug'];
    }
//@Field
//@Field
//@Field
    {
      if (target['image'] != null) {
        if (target['image'] is String) {
          this.image = relatedFactory("module_core.Image");
          this.image.id = idFromString(target['image']);
        } else {
          this.image = relatedFactory(target['image']['classes'].last);
          this.image.fromJson(target['image']);
        }
      } else {
        this.image = null;
      }
    }
  }
}

//interceptor for [module_cms.HtmlConfig]
//interceptor for module_cms.HtmlConfig
//can be singleton: TRUE
//parent: HtmlConfig []
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_cms_HtmlConfig extends module_cms.HtmlConfig
    implements Pluggable {
  $module_cms_HtmlConfig() {
//Id
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_cms.CmsWidgetConfig', 'module_cms.HtmlConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['html'] = this.html;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target['slug'];
    }
    {
      this.html = target['html'];
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_cms.ParagraphConfig]
//interceptor for module_cms.ParagraphConfig
//can be singleton: TRUE
//parent: ParagraphConfig []
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_cms_ParagraphConfig extends module_cms.ParagraphConfig
    implements Pluggable {
  $module_cms_ParagraphConfig() {
//Id
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_cms.CmsWidgetConfig', 'module_cms.ParagraphConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['body'] = this.body;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target['slug'];
    }
    {
      this.body = target['body'];
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_cms.Page]
//interceptor for module_cms.Page
//can be singleton: TRUE
//parent: Page []
//parent: Entity [@bool get ComposeSubtypes]
class $module_cms_Page extends module_cms.Page implements Pluggable {
  $module_cms_Page() {
//Id
//String
//App
//String
//List<CmsWidgetConfig>
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_cms.Page'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['app'] = "module_core.App";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.app = relations['app'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['title'] = this.title;
    }
//@Field
    {
      target['widgets'] = this.widgets.map((e) => e.toJson()).toList();
    }
//@Field
    {
      target['app'] = this.app != null ? this.app.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target['slug'];
    }
    {
      this.title = target['title'];
    }
//@Field
//@Field
    {
      this.widgets = [];
      if (target['widgets'] != null) {
        target['widgets'].forEach((data) {
          print('CLASSES');
          print(data['classes']);
          var e = relatedFactory(data['classes'].last);
          e.fromJson(data);
          this.widgets.add(e);
        });
      }
    }
//@Field
    {
      if (target['app'] != null) {
        if (target['app'] is String) {
          this.app = relatedFactory("module_core.App");
          this.app.id = idFromString(target['app']);
        } else {
          this.app = relatedFactory(target['app']['classes'].last);
          this.app.fromJson(target['app']);
        }
      } else {
        this.app = null;
      }
    }
  }
}

class $ObjectManager {
  $HomeWidget _homeWidget;
  $HomeWidget get homeWidget {
    if (_homeWidget == null) {
      _homeWidget = new $HomeWidget();
    }
    return _homeWidget;
  }

  $WorksWidget _worksWidget;
  $WorksWidget get worksWidget {
    if (_worksWidget == null) {
      _worksWidget = new $WorksWidget();
    }
    return _worksWidget;
  }

  $CommisionedWidget _commisionedWidget;
  $CommisionedWidget get commisionedWidget {
    if (_commisionedWidget == null) {
      _commisionedWidget = new $CommisionedWidget();
    }
    return _commisionedWidget;
  }

  $AwardsWidget _awardsWidget;
  $AwardsWidget get awardsWidget {
    if (_awardsWidget == null) {
      _awardsWidget = new $AwardsWidget();
    }
    return _awardsWidget;
  }

  $ExhibitionsWidget _exhibitionsWidget;
  $ExhibitionsWidget get exhibitionsWidget {
    if (_exhibitionsWidget == null) {
      _exhibitionsWidget = new $ExhibitionsWidget();
    }
    return _exhibitionsWidget;
  }

  $AboutWidget _aboutWidget;
  $AboutWidget get aboutWidget {
    if (_aboutWidget == null) {
      _aboutWidget = new $AboutWidget();
    }
    return _aboutWidget;
  }

  $module_core_Error404Widget _module_core_Error404Widget;
  $module_core_Error404Widget get module_core_Error404Widget {
    if (_module_core_Error404Widget == null) {
      _module_core_Error404Widget = new $module_core_Error404Widget();
    }
    return _module_core_Error404Widget;
  }

  $module_core_ListWidget _module_core_ListWidget;
  $module_core_ListWidget get module_core_ListWidget {
    if (_module_core_ListWidget == null) {
      _module_core_ListWidget = new $module_core_ListWidget();
    }
    return _module_core_ListWidget;
  }

  $module_core_Site _module_core_Site;
  $module_core_Site get module_core_Site {
    if (_module_core_Site == null) {
      _module_core_Site = new $module_core_Site();
    }
    return _module_core_Site;
  }

  $module_core_Application _module_core_Application;
  $module_core_Application get module_core_Application {
    if (_module_core_Application == null) {
      _module_core_Application = new $module_core_Application();
    }
    return _module_core_Application;
  }

  $module_core_Layout _module_core_Layout;
  $module_core_Layout get module_core_Layout {
    if (_module_core_Layout == null) {
      _module_core_Layout = new $module_core_Layout();
    }
    return _module_core_Layout;
  }

  $module_core_Router _module_core_Router;
  $module_core_Router get module_core_Router {
    if (_module_core_Router == null) {
      _module_core_Router = new $module_core_Router();
    }
    return _module_core_Router;
  }

  $module_core_NavbarLink _module_core_NavbarLink;
  $module_core_NavbarLink get module_core_NavbarLink {
    if (_module_core_NavbarLink == null) {
      _module_core_NavbarLink = new $module_core_NavbarLink();
    }
    return _module_core_NavbarLink;
  }

  $module_core_NavbarDropdown _module_core_NavbarDropdown;
  $module_core_NavbarDropdown get module_core_NavbarDropdown {
    if (_module_core_NavbarDropdown == null) {
      _module_core_NavbarDropdown = new $module_core_NavbarDropdown();
    }
    return _module_core_NavbarDropdown;
  }

  $module_core_App _module_core_App;
  $module_core_App get module_core_App {
    if (_module_core_App == null) {
      _module_core_App = new $module_core_App();
    }
    return _module_core_App;
  }

  $module_core_User _module_core_User;
  $module_core_User get module_core_User {
    if (_module_core_User == null) {
      _module_core_User = new $module_core_User();
    }
    return _module_core_User;
  }

  $module_core_Image _module_core_Image;
  $module_core_Image get module_core_Image {
    if (_module_core_Image == null) {
      _module_core_Image = new $module_core_Image();
    }
    return _module_core_Image;
  }

  $module_cms_CmsPagesApi _module_cms_CmsPagesApi;
  $module_cms_CmsPagesApi get module_cms_CmsPagesApi {
    if (_module_cms_CmsPagesApi == null) {
      _module_cms_CmsPagesApi = new $module_cms_CmsPagesApi();
    }
    return _module_cms_CmsPagesApi;
  }

  $module_cms_CmsWidgetConfig _module_cms_CmsWidgetConfig;
  $module_cms_CmsWidgetConfig get module_cms_CmsWidgetConfig {
    if (_module_cms_CmsWidgetConfig == null) {
      _module_cms_CmsWidgetConfig = new $module_cms_CmsWidgetConfig();
    }
    return _module_cms_CmsWidgetConfig;
  }

  $module_cms_HeadingConfig _module_cms_HeadingConfig;
  $module_cms_HeadingConfig get module_cms_HeadingConfig {
    if (_module_cms_HeadingConfig == null) {
      _module_cms_HeadingConfig = new $module_cms_HeadingConfig();
    }
    return _module_cms_HeadingConfig;
  }

  $module_cms_ImageConfig _module_cms_ImageConfig;
  $module_cms_ImageConfig get module_cms_ImageConfig {
    if (_module_cms_ImageConfig == null) {
      _module_cms_ImageConfig = new $module_cms_ImageConfig();
    }
    return _module_cms_ImageConfig;
  }

  $module_cms_HtmlConfig _module_cms_HtmlConfig;
  $module_cms_HtmlConfig get module_cms_HtmlConfig {
    if (_module_cms_HtmlConfig == null) {
      _module_cms_HtmlConfig = new $module_cms_HtmlConfig();
    }
    return _module_cms_HtmlConfig;
  }

  $module_cms_ParagraphConfig _module_cms_ParagraphConfig;
  $module_cms_ParagraphConfig get module_cms_ParagraphConfig {
    if (_module_cms_ParagraphConfig == null) {
      _module_cms_ParagraphConfig = new $module_cms_ParagraphConfig();
    }
    return _module_cms_ParagraphConfig;
  }

  $module_cms_Page _module_cms_Page;
  $module_cms_Page get module_cms_Page {
    if (_module_cms_Page == null) {
      _module_cms_Page = new $module_cms_Page();
    }
    return _module_cms_Page;
  }

  Map<String, module_core.CRUDApi>
      get instancesOfmodule_core_CRUDApi_module_core_Entity_ {
    return {
      "module_cms.CmsPagesApi": new $module_cms_CmsPagesApi(),
    };
  }
}

$ObjectManager $om = new $ObjectManager();
//generated in 202ms
