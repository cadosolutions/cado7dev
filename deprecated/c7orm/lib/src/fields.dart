// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.

part of c7orm;

/*
class _DbField {
  const _DbField();
}
const DbField = const _DbField();
*/

class Reverse {
  final Symbol symbol;
  const Reverse(this.symbol);
}

abstract class AbstractKey {
  final Symbol name;
  String dbDef(String fields);
  String get stringName => MirrorSystem.getName(this.name);
  const AbstractKey(this.name);
}

class _PrimaryKey extends AbstractKey {
  String dbDef(String fields) => "PRIMARY KEY ($fields)";
  const _PrimaryKey() : super(#PRIMARY_KEY);
}
const PrimaryKey = const _PrimaryKey();

class IndexKey extends AbstractKey {
  String dbDef(String fields) => "KEY `${this.stringName}` ($fields)";
  const IndexKey(Symbol name) : super(name);
}

class UniqueKey extends AbstractKey {
  String dbDef(String fields) => "UNIQUE KEY `${this.stringName}` ($fields)";
  const UniqueKey(Symbol name) : super(name);
}

class _AutoIncrement {
  const _AutoIncrement();
}
const AutoIncrement = const _AutoIncrement();

class IntField extends FieldController<int> {
  IntField(FieldController parent, VariableMirror declarationMirror) : super(null, parent, declarationMirror);
  @override
  String get getDbColumnDef {
    if (metadata.contains(AutoIncrement))
      return "int(11) NOT NULL AUTO_INCREMENT";
    if (metadata.contains(PrimaryKey))
      return "int(11) NOT NULL DEFAULT '0'";
    return "int(11) DEFAULT NULL";
  }
}

class DoubleField extends FieldController<int> {
  DoubleField(FieldController parent, VariableMirror declarationMirror) : super(null, parent, declarationMirror);
  @override
  String get getDbColumnDef {
    return "int(11) DEFAULT NULL";
  }
}

class StringField extends FieldController<String> {
  StringField(FieldController parent, VariableMirror declarationMirror) : super(null, parent, declarationMirror);
  @override
  String get getDbColumnDef => "varchar(255) DEFAULT NULL";
}

class DateTimeField extends FieldController<DateTime> {
  DateTimeField(FieldController parent, VariableMirror declarationMirror) : super(null, parent, declarationMirror);
  @override
  String get getDbColumnDef => "timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP";
}

class ManyField extends FieldController<Many> {
  ManyField(ClassMirror classMirror, FieldController parent, VariableMirror declarationMirror) : super(classMirror, parent, declarationMirror);
}

class ComplexField extends FieldController<Object> {
  ComplexField(ClassMirror classMirror, FieldController parent, VariableMirror declarationMirror) : super(classMirror, parent, declarationMirror);
}

class FieldController<T> {

  ClassMirror classMirror;
  FieldController parent;
  VariableMirror variableMirror;
  List<dynamic> metadata;
  Map<Symbol, FieldController> fields;

  //Rules
  //Widget
  //stuff
  List<FieldController> get allFields {
      var ret = new List<FieldController>();
      ret.add(this);
      fields?.forEach((var symbol, var field){
        ret.addAll(field.allFields);
      });
      return ret;
  }

  Map<String, dynamic> toDbRow(dynamic object){
      var ret = new Map<String, dynamic>();
      if (hasDbColumn){
        if (!variableMirror.metadata.contains(reflect(AutoIncrement)) || object != null) {
          ret[getColumnName] = object;
        }
      }
      fields?.forEach((var symbol, var field){
        ret.addAll(
          field.toDbRow(object == null ? null : reflect(object).getField(symbol).reflectee)
        );
      });
      return ret;
  }

  List<AbstractKey> get getKeys {
    List<AbstractKey> ret = metadata.where((e) => e is AbstractKey).toList();
    if (parent != null)
      ret.addAll(parent.getKeys);
    return ret;
  }

  String get getColumnName {
    if (parent == null) return '';
    String prefix = parent.getColumnName;
    String name = MirrorSystem.getName(variableMirror.simpleName);
    return name == '_' ? prefix : (prefix == '' ? name : prefix + '_' + name);
  }

  String get getDbColumnDef => null;
  bool get hasDbColumn => getDbColumnDef != null;

  dynamic buildFromDb(List row) {
    if (hasDbColumn) {
      print("$getColumnName = ${row[0]}");
      return row.removeAt(0);
    } else {
      var ret = classMirror.newInstance(new Symbol(''), [], {});
      fields?.forEach((var symbol, var field){
        ret.setField(symbol, field.buildFromDb(row));
      });
      return ret.reflectee;
    }
  }

  FieldController(this.classMirror, this.parent, this.variableMirror){
    metadata = variableMirror != null ? variableMirror.metadata.map((e) => e.reflectee) : new List<dynamic>();
  }

  factory FieldController.factory(ClassMirror classMirror, [FieldController parent = null, DeclarationMirror mirror = null, int depth = 0]) {
      if (classMirror.isAssignableTo(reflectClass(int))) {
          return new IntField(parent, mirror);
      } else if (classMirror.isAssignableTo(reflectClass(double))) {
          return new DoubleField(parent, mirror);
      } else if (classMirror == reflectClass(String)) {
          return new StringField(parent, mirror);
      } else if (classMirror == reflectClass(DateTime)) {
          return new DateTimeField(parent, mirror);
      } else {
        var ret = new ComplexField(classMirror, parent, mirror);
        ret.fields = new Map<Symbol, FieldController>();
        if (depth < 5){ //tmp
          for (var mirror in AnnotatedMember.getInstanceVariables(classMirror)){
              ret.fields[mirror.simpleName] = new FieldController.factory(mirror.type, ret, mirror, depth + 1);
          };
        }
        return ret;
      }
  }

}
