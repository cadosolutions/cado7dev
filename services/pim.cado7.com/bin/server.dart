#!/usr/bin/env dart

import "dart:async";
import "package:c7server/c7server.dart";

import 'package:module_core/server.dart' as module_core;
import 'package:module_pim/server.dart' as module_pim;

part 'server.c.dart';

Future main (List<String> arguments) async {
  await $om.server.serve();
}
