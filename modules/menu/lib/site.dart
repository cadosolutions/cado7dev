library mocule_pim;

import 'client.dart';
export 'client.dart';
import 'package:module_core/site.dart';

@Compose
abstract class SiteNavbar extends Widget {

  @Inject
  Router get router;

  @Inject
  Site get site;

  @Create
  Slot menu;

  @Factory
  //NavbarDropdown navbarDropdownFactory();
  @Factory
  //NavbarLink navbarLinkFactory();

  void init() {

    Map <String, NavbarDropdown> menuItems = {};

    for (String type in router.actions.keys) {
      /*router.actionTypes[type].metadata.whereType<SiteMenuItem>().forEach((var adminMenuItem){
        var href = type.replaceFirst('Action', '');
        href = '/${href[0].toLowerCase()}${href.substring(1)}';

        NavbarDropdown dd;
        if (menuItems.containsKey(adminMenuItem.section)) {
          dd = menuItems[adminMenuItem.section];
        } else {
          dd = navbarDropdownFactory();
          dd.name = adminMenuItem.section;
          menuItems[adminMenuItem.section] = dd;
          menu.append(dd);
        }

        var a = navbarLinkFactory();
        a.href = href;
        a.title = adminMenuItem.title;
        dd.items.append(a);
      });*/

    }
    print('navbar init');
  }

  String get html => '''
    <div class="container">
      <a id="brand" data-type="local" class="navbar-brand" href="/">${site.name}</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div id="navbarResponsive" class="collapse navbar-collapse">
        <ul id="menu" class="navbar-nav ml-auto">
          <!--li class="nav-item active">
            <a class="nav-link" href="#">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Contact</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <img src="/packages/module_core/fa/solid/shopping-basket.svg" width ="22"/>
            </a>
          </li-->
        </ul>
      </div>
    </div>
  ''';

}
