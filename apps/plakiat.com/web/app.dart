
import 'dart:html';
import 'dart:convert';
import 'dart:math';
import 'dart:async';
import 'package:html/parser.dart';

//document.head.addElement(new BaseElement());

import 'package:module_core/site.dart';

part 'widgets.dart';

int scrollCount = 0;

var currencyMap = null;
var countryMap = null;

class Size {
  String name;
  String title;
  String dimmensionsCm;
  String dimmensionsInch;
  int basePrice;
  String get fullName => "$name ($dimmensionsCm)";
  Size (this.name, this.title, this.dimmensionsCm, this.dimmensionsInch, this.basePrice);
}

Map<String, Size> availableSizes = {
  'A3' : new Size('A3', 'A3', '30 x 42 cm', '11.8 x 16.5"', 55),
  'B2' : new Size('B2', 'B2', '50 x 70 cm', '19.7 x 27.6"', 85),
  'B1' : new Size('B1', 'B1', '70 x 100 cm', '27.6 x 39.4"', 125),
  'narrow1' : new Size('narrow1', "A3*", '21 x 42 cm', '8.4 x 16.5"', 55),
  'narrow2' : new Size('narrow2', "B2*", '35 x 70 cm', '13.8 x 27.6"', 85),
  'narrow3' : new Size('narrow3', "B1*", '50 x 100 cm', '19.7 x 39.4"', 125),
};

class OrderItem {

  Order order;

  int quantity = 1;

  String name;
  String subtitle;
  String className;
  String thumbnail;

  List<String> availableLanguages;
  List<String> possibleSizes;
  Size size;
  String language;

  OrderItem(this.order, this.name, this.subtitle, this.className, this.thumbnail, this.availableLanguages, this.possibleSizes, this.size, this.language) {}

  OrderItem.clone(OrderItem other) {
    order = other.order;
    name = other.name;
    subtitle = other.subtitle;
    className = other.className;
    thumbnail = other.thumbnail;
    availableLanguages = other.availableLanguages;
    possibleSizes = other.possibleSizes;
    size = other.size;
    language = other.language;
    quantity = other.quantity;
  }

  OrderItem.fromJson(this.order, Map json) {
    quantity = json['quantity'];
    name = json['name'];
    subtitle = json['subtitle'];
    className = json['className'];
    thumbnail = json['thumbnail'];
    availableLanguages = new List<String>();
    json['availableLanguages'].forEach((e){
      availableLanguages.add(e.toString());
    });
    possibleSizes = new List<String>();
    json['possibleSizes'].forEach((e){
      possibleSizes.add(e.toString());
    });
    size = availableSizes[json['size']];
    language = json['language'];

  }

  Map toJson() {
    return {
      'quantity' : quantity,
      'name' : name,
      'subtitle' : subtitle,
      'className' : className,
      'thumbnail' : thumbnail,
      'availableLanguages' : availableLanguages,
      'possibleSizes' : possibleSizes,
      'size' : size.name,
      'language' : language,
      'price' : getPrice()
    };
  }


  double getPrice() {

    double p = 1.0 * size.basePrice;
    /*if (this.subtitle == '188th') {
      p = (p - 5.0) / 2.0;
    }*/
    return quantity * p;
  }

  void onUpdate() {
      if (_priceElement != null) {
        loadSettings((data){
          double total = getPrice();
          _priceElement
            ..innerHtml = ''
            ..append(
              new DivElement()
              ..className = 'major'
              ..innerHtml = (total * currencyMap['PLN']).toString() + ' pln'
            )

            ..append(
              new DivElement()
              ..className = 'minor'
              ..innerHtml = '&euro;' + (total * currencyMap['EUR']).ceil().toString()  + ' / \$' + (total * currencyMap['USD']).ceil().toString()
            );
          /* currencyMap.forEach((k,v){
            _priceElement.append(
              new DivElement()
              ..innerHtml = (itemTotal * v).toStringAsFixed(2) + ' ' + k
            );
          });*/
        });

      }
  }

  Element _titleElement;
  Element get titleElement {
    if (_titleElement == null) {
      _titleElement = new DivElement();
      _titleElement.append(new HeadingElement.h3()..innerHtml = this.name);
      _titleElement.append(new HeadingElement.h4()..innerHtml = this.subtitle);
      _titleElement.append(
        new DivElement()
          ..className = 'buttons'
          ..append(
            new ButtonElement()
              ..className = 'sml delete'
              ..innerHtml = 'delete'
              ..onClick.listen((e){
                order.items.remove(this);
                order.rebuildItemsForm();
                order.onUpdate();
              })
          )..append(
            new ButtonElement()
              ..className = 'sml duplicate'
              ..innerHtml = 'duplicate'
              ..onClick.listen((e){
                order.items.add(new OrderItem.clone(this));
                order.rebuildItemsForm();
                order.onUpdate();
              })
          )
        );
    }
    return _titleElement;
  }

  Element _thumbnailElement;
  Element get thumbnailElement {
    if (_thumbnailElement == null) {
      _thumbnailElement = new DivElement();
      _thumbnailElement.append(new ImageElement(src:this.thumbnail));
    }
    return _thumbnailElement;
  }

  DivElement _quantityElement;
  DivElement _quantityValueElement;
  void onQuantityUpdated() {
    _quantityValueElement.text = quantity.toString() + 'x';
    order.onUpdate();
  }

  Element get quantityElement {
    if (_quantityElement == null) {

      _quantityElement = new DivElement()..className='niceSelect';;
      _quantityValueElement = new DivElement()..className='value';;
      _quantityElement..append(
        new DivElement()..className = 'buttonLeft'
        ..onClick.listen((e){
          if (quantity == 1) {
            if (window.confirm("would you like to remove this item?")){
              order.items.remove(this);
              order.rebuildItemsForm();
              order.onUpdate();
            }
          } else {
            if (quantity > 1) quantity --;
            onQuantityUpdated();
          }
        })
      )..append(
        _quantityValueElement
      )..append(
        new DivElement()..className = 'buttonRight'
        ..onClick.listen((e){
          quantity ++;
          onQuantityUpdated();
        })
      );
      onQuantityUpdated();

      /*_quantityValueElement.text = quantity.toString();

      _quantityElement.onChange.listen((event){
        quantity = _quantityElement.valueAsNumber;
        order.onUpdate();
      });*/
    }
    return _quantityElement;
  }

  DivElement _sizeElement;
  DivElement _sizeNameElement;
  DivElement _sizeDimmensionsCmElement;
  DivElement _sizeDimmensionsInchElement;

  void onSizeUpdated() {
    _sizeNameElement.text = size.title;
    _sizeDimmensionsCmElement.text = size.dimmensionsCm;
    _sizeDimmensionsInchElement.text = size.dimmensionsInch;
    order.onUpdate();
  }

  Element get sizeElement {
      if (_sizeElement == null) {
        _sizeElement = new DivElement()..className='niceSelect';
        _sizeNameElement = new DivElement()..className='value';
        _sizeDimmensionsCmElement = new DivElement()..className='legend';
        _sizeDimmensionsInchElement = new DivElement();

        _sizeElement..append(
          new DivElement()..className = 'buttonLeft'
          ..onClick.listen((e){
            int idx = possibleSizes.indexOf(size.name);
            idx--;
            if (idx < 0) idx = possibleSizes.length - 1;
            size = availableSizes[possibleSizes[idx]];
            onSizeUpdated();
          })
        )..append(
          _sizeNameElement
        )..append(
          new DivElement()..className = 'buttonRight'
          ..onClick.listen((e){
            int idx = possibleSizes.indexOf(size.name);
            idx++;
            if (idx >= possibleSizes.length) idx = 0;
            size = availableSizes[possibleSizes[idx]];
            onSizeUpdated();
          })
        )..append(_sizeDimmensionsCmElement)
        ..append(_sizeDimmensionsInchElement);
        onSizeUpdated();
      }
      return _sizeElement;
  }

  SelectElement _languageElement;
  Element get languageElement {
    if (_languageElement == null) {
      _languageElement = new SelectElement();
      availableLanguages.forEach((lang){
        _languageElement.append(new OptionElement(data: lang, value: lang));
      });
      _languageElement.value = language;
      _languageElement.onChange.listen((e){
        language = _languageElement.value;
        order.save();
      });
    }
    return _languageElement;
  }

  Element _priceElement;
  Element get priceElement {
    if (_priceElement == null) {
      _priceElement = new DivElement();
      onUpdate();
    }
    return _priceElement;
  }
}

class InvoiceData {
  String company = '';
  String name = '';
  String nip = '';
  Address address;


  InvoiceData() {
    address = new Address();
  }

  InvoiceData.fromJson(Map json) {
    company = json['company'];
    name = json['name'];
    nip = json['nip'];
    address = new Address.fromJson(json['address']);
  }

  Map toJson() {
    return {
      'company': company,
      'name': name,
      'nip': nip,
      'address': address.toJson()
    };
  }
}

class Address {
  String city = '';
  String zipcode = '';
  String country = '';
  String street = '';

  Address() {}

  Address.fromJson(Map json) {
    city = json['city'];
    zipcode = json['zipcode'];
    country = json['country'];
    street = json['street'];
  }

  Map toJson() {
    return {
      'city': city,
      'zipcode': zipcode,
      'country': country,
      'street': street
    };
  }
}

class Order {

    List<OrderItem> items;

    String email = '';
    String name = '';

    String agree1 = '';
    String agree2 = '';
    String agree3 = '';

    Address shippingAddress;
    String shippingMethod;
    InvoiceData invoiceData;

    String comments = '';

    double total;

    Order() {
      items = new List<OrderItem>();
      shippingAddress = new Address();
      invoiceData = new InvoiceData();
    }

    Order.fromJson(Map json) {
      items = new List<OrderItem>();
      (json['items'] as List).forEach((j){
        items.add(new OrderItem.fromJson(this, j));
      });
      email = json['email'];
      name = json['name'];
      shippingAddress = new Address.fromJson(json['shippingAddress']);
      shippingMethod = json['shippingMethod'];
      invoiceData = new InvoiceData.fromJson(json['invoiceData']);
      comments = json['comments'];
      total = json.containsKey('total') ? json['total'] : 0.0;
    }

    Map toJson() {
      return {
        'items' : items.map<Map>((e){ return e.toJson(); }).toList(),
        'email' : email,
        'name' : name,
        'shippingAddress' : shippingAddress.toJson(),
        'shippingMethod' : shippingMethod,
        'invoiceData' : invoiceData.toJson(),
        'comments' : comments,
        'total' : total = getTotal(),
        'shippingCost' : getShippingCost().toString() + ' PLN',
        'totalPLN' : (total * currencyMap['PLN']).toString() + ' PLN',
        'totalUSD' : '\$' + (total * currencyMap['USD']).ceil().toString(),
        'totalEUR' : '&euro;' + (total * currencyMap['EUR']).ceil().toString()
      };
    }

    Map<String, String> errors = {};
    bool validate() {
      errors.clear();
      if (name == null || name.trim().isEmpty) {
        errors['name'] = '*required field';
      }
      if (email == null || email.trim().isEmpty) {
        errors['email'] = '*required field';
      }

      if (shippingAddress.city == null || shippingAddress.city.trim().isEmpty) {
        errors['shippingAddress.city'] = '*required field';
      }
      if (shippingAddress.zipcode == null || shippingAddress.zipcode.trim().isEmpty) {
        errors['shippingAddress.zipcode'] = '*required field';
      }
      if (shippingAddress.country == null || shippingAddress.country.trim().isEmpty) {
        errors['shippingAddress.country'] = '*required field';
      }
      if (shippingAddress.street == null || shippingAddress.street.trim().isEmpty) {
        errors['shippingAddress.street'] = '*required field';
      }
      if (agree1 == null || agree1.trim().isEmpty) {
        errors['agree1'] = 'accepting this is required';
      }
      if (agree2 == null || agree2.trim().isEmpty) {
        errors['agree2'] = 'accepting this is required';
      }
      if (agree3 == null || agree3.trim().isEmpty) {
        errors['agree3'] = 'accepting this is required';
      }
      return errors.isEmpty;
    }


    void save() {
      loadSettings((data){
        window.localStorage['order6'] = json.encode(this.toJson());
      });
    }

    bool isEmpty() {
      return items.length == 0;
    }

    void addNewItem(poster, {size:null, language:null}) {
      print(poster);
      List<String> langs = (poster['images'] is Map) ? (poster['images'] as Map).keys.map<String>((k) => k.toString()).toList() : ['english'];

      int p = (poster['title'] as String).indexOf('(');
      String name = p != -1 ? (poster['title'] as String).substring(0, p) : poster['title'];

      List<String> possibleSizes = new List<String>();
      if (poster.containsKey('sizes')) {
        poster['sizes'].forEach((e){
          possibleSizes.add(e.toString());
        });
      } else {
        possibleSizes = ['A3', 'B2', 'B1'];
      }
      items.add(new OrderItem(this, name.toLowerCase(), poster['lp'], poster['class'], poster['thumbnail'], langs, possibleSizes, availableSizes[size], language));
      onUpdate();
    }

    void onUpdate() {
      updateBadge();
      updateDrawer();
      items.forEach((i) => i.onUpdate());
      updateTotals();

      loadSettings((data){
        if (countryMap.containsKey(shippingAddress.country) && countryMap[shippingAddress.country].containsKey(shippingMethod)) {
          querySelector('#shipmentCost').innerHtml = countryMap[shippingAddress.country][shippingMethod][1].toString() + ' PLN';
          querySelector('#shipmentTime').innerHtml = countryMap[shippingAddress.country][shippingMethod][0];
        } else {
          querySelector('#shipmentCost').innerHtml = '';
          querySelector('#shipmentTime').innerHtml = '';
        }
        querySelector('#shipmentNote').innerHtml = data['shipmentNoteText'];
        querySelector('#shipmentNote').style.color = data['shipmentNoteColor'];
      });
      save();
    }

    int getShippingCost() {
      if (countryMap.containsKey(shippingAddress.country) && countryMap[shippingAddress.country].containsKey(shippingMethod)) {
       return countryMap[shippingAddress.country][shippingMethod][1];
      }
      return 0;
    }

    double getTotal() {
      double ret = 0.0;
      items.forEach((i){ ret += i.getPrice(); });
      ret += getShippingCost();
      return ret;
    }

    void updateTotals() {
      loadSettings((data){
        total = getTotal();
        querySelectorAll('.orderTotal').forEach((e){
          e.innerHtml = '';
          e
          ..append(
            new DivElement()
            ..className = 'major'
            ..innerHtml = (total * currencyMap['PLN']).toString() + ' pln'
          )
          ..append(
            new DivElement()
            ..className = 'minor'
            ..innerHtml = '&euro;' + (total * currencyMap['EUR']).ceil().toString()  + ' / \$' + (total * currencyMap['USD']).ceil().toString()
          );
          /*currencyMap.forEach((k,v){
            e.append(
              new DivElement()
              ..innerHtml = (total * v).toStringAsFixed(2) + ' ' + k
            );
          });*/
        });
      });

    }

    int totalCount() {
      int ret = 0;
      items.forEach((i){
        ret += i.quantity;
      });
      return ret;
    }

    void updateBadge() {
      document.querySelector('#cartBadge').classes.toggle('hidden', isEmpty());
      document.querySelector('#cartBadge').innerHtml = totalCount().toString();
      document.querySelector('#cartBadgeMobile').classes.toggle('hidden', isEmpty());
      document.querySelector('#cartBadgeMobile').innerHtml = totalCount().toString();
    }
    void updateDrawer() {
      UListElement drawer = document.querySelector('#cartDrawer ul.items');
      drawer.innerHtml = '';

      var later = new List<OrderItem>();
      int counter = 0;
      items.forEach((i){
        if (i.className != 'contain horizontal') counter ++;

        if ((i.className == 'contain horizontal') && counter.isOdd) {
          later.add(i);
        } else {
          var node = new LIElement()..className = (i.className == 'contain horizontal' ? 'contain' : '');
          node.append(new ImageElement(src:i.thumbnail));
          drawer.append(node);
        }
      });
      later.forEach((i){
        var node = new LIElement()..className = (i.className == 'contain horizontal' ? 'contain' : '');
        node.append(new ImageElement(src:i.thumbnail));
        drawer.append(node);
      });

    }

    void rebuildItemsForm() {

        loadSettings((data) {
          if (data['shopEnabled']) {
            querySelector('#orderFull').classes.toggle('hidden', isEmpty());
            querySelector('#orderEmpty').classes.toggle('hidden', !isEmpty());
            querySelector('#orderDisabled').classes.toggle('hidden', true);
          } else {
            querySelector('#orderFull').classes.toggle('hidden', true);
            querySelector('#orderEmpty').classes.toggle('hidden', true);
            querySelector('#orderDisabled').classes.toggle('hidden', false);
          }
        });



        Element body = querySelector('#orderItems tbody');
        body.innerHtml = '';
        this.items.forEach((orederItem){
          var itemNode = new TableRowElement();
          itemNode.append(new TableCellElement()
            ..className = 'thumbnail'
            ..append(orederItem.thumbnailElement));
          itemNode.append(new TableCellElement()
            ..className = 'title'
            ..append(orederItem.titleElement));
          itemNode.append(new TableCellElement()
            ..className = 'size'
            ..append(new DivElement()..className = 'cellHead'..innerHtml = 'size')
            ..append(orederItem.sizeElement));
          itemNode.append(new TableCellElement()
            ..className = 'quantity'
            ..append(new DivElement()..className = 'cellHead'..innerHtml = 'quantity')
            ..append(orederItem.quantityElement));
          itemNode.append(new TableCellElement()
            ..className = 'language'
            ..append(new DivElement()..className = 'cellHead'..innerHtml = '<div class="desktopOnly">poster</div>language')
            ..append(orederItem.languageElement));
          itemNode.append(new TableCellElement()
            ..className = 'price'
            ..append(new DivElement()..className = 'cellHead'..innerHtml = 'price')
            ..append(orederItem.priceElement));
          //orederItem.thumbnail
          body.append(itemNode);
        });
    }
}

Order _orderCache;

Order getOrder() {
  if (_orderCache == null) {
    if (window.localStorage.containsKey('order6')) {
      _orderCache = new Order.fromJson(json.decode(window.localStorage['order6']));
    } else {
      _orderCache = new Order();
    }
  }
  return _orderCache;
}

clearOrder() {
  _orderCache = new Order();
  _orderCache.onUpdate();
}


void scrollToElementStep(num count, Element e, num starttime, num ms) {

      if (count < scrollCount) {
        return;
      }
      int targetOffset = e.offsetTop;
      int currentOffset = document.scrollingElement.scrollTop;
      int change = ((targetOffset - currentOffset) * ((2000.0 - ms) / 3000.0)).round();
      //print('MOVE $count $starttime $ms $targetOffset ${e.style.display} $change');
      if (targetOffset > 0) {
        document.scrollingElement.scrollTop = currentOffset + change;
      }

      if (ms < 1000.0) {
        document.scrollingElement.scrollTop = targetOffset;
      } else {
        window.animationFrame.then((i){
          scrollToElementStep(count, e, starttime, 2000.0 - (i - starttime));
        });
      }
}


void smoothScrollTo(Element e) {
  //print('smooth scroll to ${e.id}');
  window.animationFrame.then((starttime){
    scrollToElementStep(++scrollCount, e, starttime, 2000.0);
  });
}

void onScroll(){

  //TODO:
  //querySelector('#menuContainer').offsetTop
  var top = querySelector('#menuContainer').getBoundingClientRect().top + window.pageYOffset - document.documentElement.clientTop;
  var up = window.scrollY > top;
  querySelector( "#menuContainer" ).classes.toggle("up", up);
  querySelector( "#mainMenuSpacing" ).classes.toggle( "menuUp", up);

  var top2 = querySelector('#projectMenu').getBoundingClientRect().top + window.pageYOffset - document.documentElement.clientTop;
  var fix = window.scrollY + 150 > top2;
  querySelector( "#projectMenu .inside" ).classes.toggle("fix", fix);

  querySelector( "#cartDrawerBg" ).classes.toggle("hidden", !up);
  querySelector( "#cartDrawer" ).classes.toggle( "hidden", !up);
}

class StartView {
  String title = 'Plakiat';
  String popupBack = '';
  void pop() {}
  void push(args) {
    if (previous != null) {
      previous.pop();
    }
  }
}

class SliderWidget {
  List<DivElement> slides;
  Element container;

  int currentSlide = 0;

  setSlide(int newSlide) {
    slides[currentSlide].classes.remove('current');
    slides[newSlide].classes.add('current');
    currentSlide = newSlide;
    animate();
  }

  nextSlide() {
    if (currentSlide < slides.length - 1) {
      setSlide(currentSlide + 1);
    } else {
      setSlide(0);
    }
  }

  prevSlide() {
    if (currentSlide > 0) {
      setSlide(currentSlide - 1);
    } else {
      setSlide(slides.length - 1);
    }
  }

  Timer timer;
  animate() {
    if (timer != null) timer.cancel();
    timer = new Timer(const Duration(milliseconds: 4000), (){
      this.nextSlide();
    });
  }

    SliderWidget(Element parent, List images){
      container = new DivElement();
      container.className = 'slideshow-container container';
      parent.append(container);
      slides = new List<DivElement>();
      //print(images);
      images.forEach((src){
        var slide = new DivElement()
        ..className = 'mySlides fade'
        ..append(new ImageElement(src:src));
        container.append(slide);
        slides.add(slide);
      });
      container.append(
        new AnchorElement()
        ..className = 'prev'
        ..text = '❮'
        ..onClick.listen((e){
          this.prevSlide();
        })
      );
      container.append(
        new AnchorElement()
        ..className = 'next'
        ..text = '❯'
        ..onClick.listen((e){
          this.nextSlide();
        })
      );
      setSlide(0);
    }
}

class HomeView extends StartView {
  String title = 'Plakiat';
  String popupBack = '';
  void pop() {
    print('POP');
    super.pop();
    querySelector('#start').classes.remove('current');
    querySelector('#mobileMenu').classes.remove('start');
    querySelector('#mobileMenu').classes.remove('unfolded');
  }
  void push(args) {
    super.push(args);
    print('PUSH');
    querySelector('#start').classes.add('current');
    querySelector('#mobileMenu').classes.add('start');
    querySelector('#mobileMenu').classes.add('unfolded');

    loadSettings((data){
      if (querySelector('#slider').childNodes.length == 0) {
        var s1 = new SliderWidget(querySelector('#slider'), data['slider']['mobile']);
        s1.container.classes.add('mobileOnly');

        var s2 = new SliderWidget(querySelector('#slider'), data['slider']['desktop']);
        s2.container.classes.add('desktopOnly');
      }
    });
  }
}

class ProjectView extends StartView {

  String subsection = '';

  void pop() {
    querySelector('#' + this.subsection).classes.remove('current');
    querySelector('#projectMenu a[href="' + this.subsection + '"]').classes.remove('current');
    querySelector('#project').classes.remove('current');
    super.pop();
  }

  void push(args){
    super.push(args);

    querySelector('#projectContent').classes.toggle('blurout1');
    querySelector('#projectContent').classes.toggle('blurout2');

    querySelector('#project').classes.add('current');

    querySelector('#' + this.subsection).classes.add('current');


    querySelector('#projectMenu a[href="' + this.subsection + '"]').classes.add('current');
    smoothScrollTo(querySelector('#project'));
    //smoothScrollTo(querySelector(subsection != 'news' ? '#' + subsection : '#project'));
  }
}

var dataGet = null;
void loadPosters(callback){
  loadData((Map news){
    var posters = {};
    news.forEach((var key, var news){
      if (news['url'].toString().startsWith('/poster/')) {
        String key = news['url'].toString().substring(8);
        posters[key] = news;
      }
      /*if (news.containsKey('lp')) {
        int x = int.parse(news['lp'].toString().replaceAll(new RegExp(r'[^0-9]'), ''));
        posters[x] = news;
      }*/
    });
    callback(posters);
  });
}

var settingsGet = null;
void loadSettings(callback){
  if (settingsGet == null){
    settingsGet = HttpRequest.getString(getApiUrl() + "customdata.CustomData/settings.json");
  }
  settingsGet.then((String responseText) {
    Map data = json.decode(responseText);
    print(data.containsKey('currencyMap'));
    print(data['currencyMap']);
    currencyMap = data['currencyMap'];
    countryMap = data['countryMap'];
    callback(data);
  });
}

void loadData(callback){
  if (dataGet == null){
    dataGet = HttpRequest.getString(getApiUrl() + "customdata.CustomData/news.json");
  }
  dataGet.then((String responseText) {
    callback(json.decode(responseText));
  });
}

var exhibitionsGet = null;
void loadExhibitions(callback){
  if (exhibitionsGet == null){
    exhibitionsGet = HttpRequest.getString(getApiUrl() + "customdata.CustomData/exhibitions.json");
    exhibitionsGet.then((String responseText) {
      callback(json.decode(responseText));
    });
  } else {
    exhibitionsGet.then((String responseText) {
      callback(json.decode(responseText));
    });
  }
}

final NodeValidatorBuilder _htmlValidator = new NodeValidatorBuilder.common()
  ..allowInlineStyles()
  ..allowNavigation(new _AnyUriPolicy());

class _AnyUriPolicy implements UriPolicy {
  bool allowsUri(uri) {
    return true;
  }
}

class NewsView extends ProjectView {
  String title = 'Plakiat - News';
  String subsection = 'news';

  void pop() {
    querySelector('#news').innerHtml = '';
    super.pop();
  }

  void push(args){
    super.push(args);
    loadData((Map news){

      news.forEach((var key, var value){
        Map news = (value as Map);
        var n = new DivElement();
        n.classes.add('newsItem');

        if (news.containsKey('class')){
          String cs = news['class'];
          cs.split(' ').forEach((c){
            n.classes.add(c);
          });
        }

        n.appendHtml('<div class="lp"><span>'+ (news.containsKey('lp') ? news['lp'] : '') + '</span></div>');
        var thumbnail = new AnchorElement(href:news['url']);
        thumbnail.classes.add('thumbnail');

        if (news['url'].toString().startsWith('http')) {
          thumbnail.target = '_blank';
        } else {
          thumbnail.href = document.baseUri + (news['url'] as String).substring(1);
          localiseAnchor(thumbnail);
        }

        var inside = new DivElement();
        thumbnail.append(inside);
        inside.append(new ImageElement(src:news['thumbnail']));
        n.append(thumbnail);

        var body = new DivElement()
          ..classes.add('body');

        body.setInnerHtml('<div class="top"><div><span>'
          + news['date'] + '</span><h4>'+ news['title'] + '</h4></div></div><div class="bottom">'
          + news['body'] + '</div></div>', validator: _htmlValidator);

        body.querySelectorAll('a').forEach((Element a){
          (a as AnchorElement).target = '_blank';
        });

        body.onClick.listen((MouseEvent e){
          thumbnail.click();
        });
        n.append(body);
        querySelector('#news').insertAdjacentElement('afterBegin', n);
      });
      //querySelector('#news').appendHtml('<div class="whiteFade"></div>');
    });
  }
}

class ArchiveView extends ProjectView {
  String title = 'Plakiat - Archive';
  String subsection = 'archive';

  void pop() {
    querySelector('#archive').innerHtml = '';
    super.pop();
  }

  void push(args){
    super.push(args);
    loadPosters((Map posters){
      posters.forEach((var key, var news){
        var a = new AnchorElement(href:document.baseUri + 'poster/' + key.toString());
        localiseAnchor(a);
        a.classes.add('newsItem');


        if (news.containsKey('class')){
          String cs = news['class'];
          cs.split(' ').forEach((c){
            a.classes.add(c);
          });
        }


        var i = new ImageElement(src:news['thumbnail']);

        var d = new DivElement();
        d.classes.add('thumbnail');
        a.append(d);

        var d2 = new DivElement();
        d.append(d2);

        d2.append(i);


        querySelector('#archive').insertAdjacentElement('afterBegin', a);
      });
    });
  }
}

class BehindView extends ProjectView {
  String title = 'Plakiat - Behind The Scenes';
  String subsection = 'behind';

  var galleryGet = null;

  void pop() {
    super.pop();
  }

  void push(args){
    super.push(args);
    if (galleryGet == null){
      galleryGet = HttpRequest.getString(getApiUrl() + "customdata.CustomData/gallery.json").then((String responseText) {
        var jsonString = responseText;
        Map gallery = json.decode(jsonString);
        gallery.forEach((key, value){
          Map element = value as Map;
          var elem = new AnchorElement();
          elem.href = element['image'];
          elem.target = '_blank';
          elem.classes.add('brick');
          elem.append(new ImageElement(src:element['thumbnail']));
          elem.appendHtml(
            '<div class="hover"><div class="top"><h5>' + element['title'] + '</h5></div><div class="line"></div>'+
            '<div class="bottom"><span>' + element['subtitle'] + '</span></div></div>');
          querySelector('#behind').insertAdjacentElement('afterBegin', elem);
        });
      });
    }
  }
}

class ExhibitionsView extends ProjectView {
  String title = 'Plakiat - Exhibitions';
  String subsection = 'exhibitions';
  var exhibitionsGet = null;

  void pop() {
    querySelector('#exhibitionsList').innerHtml = '';
    super.pop();
  }

  void push(args){
    super.push(args);
    loadExhibitions((exhibitions){
      exhibitions.forEach((key, element){
        var li = new LIElement();
        li.classes.add('ex' + key);
        li.appendHtml(
            '<div>' + element['name'] + '</div>' +
            '<div>' + element['place'] + '</div>' +
            '<div class="time">' + element['time'] + '</div>'
        );

        var a = new AnchorElement(href:'exhibition/'+ key);
        a.classes.add('mobileOnly');
        localiseAnchor(a);
        li.append(a);
        querySelector('#exhibitionsList').append(li);
      });
    });
  }

}

class ShopView extends StartView {

  String title = 'Plakiat - Shop';

  void pop() {
    querySelector('#shop').classes.remove('current');
    querySelector('#shop .sectionHeader').classes.remove('current');
    super.pop();
  }

  void push(args){
    super.push(args);
    querySelector('#shop').classes.add('current');
    window.animationFrame.then((i){
      querySelector('#shop .sectionHeader').classes.add('current');
    });
    smoothScrollTo(querySelector('#shop'));

    querySelector('#cartDrawer').classes.remove('visible');
    querySelector('#cartDrawerBg').classes.remove('visible');

    Order order = getOrder();

    loadSettings((data){
      if ((querySelector('#orderCountry') as SelectElement).options.isEmpty) {
        querySelector('#orderCountry').append(new OptionElement(data: '- country -', value: ''));
        countryMap.forEach((k, country){
          querySelector('#orderCountry').append(new OptionElement(data: k, value: k));
        });
        (querySelector('#orderCountry') as SelectElement).onChange.listen((e){
          String country = (querySelector('#orderCountry') as SelectElement).value;
          getOrder().shippingAddress.country = country;
          querySelector('#orderShipping').innerHtml = '';
          querySelector('#orderShipping').className = 'hidden';
          if (countryMap.containsKey(country)) {
            querySelector('#orderShipping').className = countryMap[country].length > 1 ? '' : 'hidden';
            countryMap[country].forEach((k, v){
              querySelector('#orderShipping').append(new OptionElement(data: k, value: k));
            });
          }
          (querySelector('#orderShipping') as SelectElement).value = 'standard';
          getOrder().shippingMethod = 'standard';
          getOrder().onUpdate();
        });

        String startCountry = getOrder().shippingAddress.country;
        (querySelector('#orderCountry') as SelectElement).value = startCountry;
        (querySelector('#orderCountry') as SelectElement).dispatchEvent(new Event('change'));


        (querySelector('#orderShipping') as SelectElement).onChange.listen((e){
          String shipping = (querySelector('#orderShipping') as SelectElement).value;
          getOrder().shippingMethod = shipping;
          getOrder().onUpdate();
        });
        (querySelector('#orderShipping') as SelectElement).value = getOrder().shippingMethod;

        //(querySelector('#orderCountry') as SelectElement).onChange.tri
        //querySelector('#orderShipping').append(new OptionElement(data: 'please select...', value: ''));
      }
      order.rebuildItemsForm();
      order.onUpdate();
    });
  }
}

class CheckoutView extends StartView {

  String title = 'Checkout | Plakiat';

  void pop() {
    querySelector('#checkout').classes.remove('current');
    super.pop();
  }

  void push(args){
    super.push(args);
    querySelector('#checkout').classes.add('current');
    smoothScrollTo(querySelector('#checkout'));

    Order order = getOrder();

    (querySelector( "#orderForm input[name=name]") as InputElement).value = order.name;
    (querySelector( "#orderForm input[name=email]") as InputElement).value = order.email;
    (querySelector( "#orderForm textarea[name=comments]") as TextAreaElement).value = order.comments;

    (querySelector( "#orderForm input[name=shippingAddress\\.city]") as InputElement).value = order.shippingAddress.city;
    (querySelector( "#orderForm input[name=shippingAddress\\.zipcode]") as InputElement).value = order.shippingAddress.zipcode;
    (querySelector( "#orderForm input[name=shippingAddress\\.street]") as InputElement).value = order.shippingAddress.street;
    (querySelector( "#orderForm input[name=shippingAddress\\.country]") as InputElement).value = order.shippingAddress.country;

    (querySelector( "#orderForm input[name=invoiceData\\.company]") as InputElement).value = order.invoiceData.company;
    (querySelector( "#orderForm input[name=invoiceData\\.name]") as InputElement).value = order.invoiceData.name;
    (querySelector( "#orderForm input[name=invoiceData\\.nip]") as InputElement).value = order.invoiceData.nip;
    (querySelector( "#orderForm input[name=invoiceData\\.address\\.city]") as InputElement).value = order.invoiceData.address.city;
    (querySelector( "#orderForm input[name=invoiceData\\.address\\.zipcode]") as InputElement).value = order.invoiceData.address.zipcode;
    (querySelector( "#orderForm input[name=invoiceData\\.address\\.street]") as InputElement).value = order.invoiceData.address.street;
    (querySelector( "#orderForm input[name=invoiceData\\.address\\.country]") as InputElement).value = order.invoiceData.address.country;

    (querySelector( "#orderForm input[name=agree1]") as CheckboxInputElement).checked = order.agree1 != null && !order.agree1.isEmpty;
    (querySelector( "#orderForm input[name=agree2]") as CheckboxInputElement).checked = order.agree2 != null && !order.agree2.isEmpty;
    (querySelector( "#orderForm input[name=agree3]") as CheckboxInputElement).checked = order.agree3 != null && !order.agree3.isEmpty;

    //TODO redirect if empty
  }
}

class SuccessView extends StartView {
  String title = 'Success | Plakiat';

  void pop() {
    querySelector('#success').classes.remove('current');
    super.pop();
  }

  void push(args){
    super.push(args);
    querySelector('#success').classes.add('current');
    smoothScrollTo(querySelector('#success'));
  }
}


class ContactView extends StartView {
  String title = 'Plakiat - Contact';

  void pop() {
    querySelector('#contact').classes.remove('current');
    querySelector('#contact .sectionHeader').classes.remove('current');
    super.pop();
  }

  void push(args){
    super.push(args);
    querySelector('#contact').classes.add('current');
    window.animationFrame.then((i){
      querySelector('#contact .sectionHeader').classes.add('current');
    });
    smoothScrollTo(querySelector('#contact'));
  }
}

class AboutView extends StartView {

  String title = 'Plakiat - About me';

  void pop() {
    querySelector('#about').classes.remove('current');
    querySelector('#about .sectionHeader').classes.remove('current');
    super.pop();
  }

  void push(args){
    super.push(args);
    querySelector('#about').classes.add('current');
    window.animationFrame.then((i){
      querySelector('#about .sectionHeader').classes.add('current');
    });
    smoothScrollTo(querySelector('#about'));
    loadSettings((data){
      querySelector('#static-block-about1-title').innerHtml = data['staticBlocks']['about1']['title'];
      querySelector('#static-block-about1-body').innerHtml = data['staticBlocks']['about1']['body'];
      querySelector('#static-block-about2-title').innerHtml = data['staticBlocks']['about2']['title'];
      querySelector('#static-block-about2-body').innerHtml = data['staticBlocks']['about2']['body'];
      querySelector('#static-block-about3-title').innerHtml = data['staticBlocks']['about3']['title'];
      querySelector('#static-block-about3-body').innerHtml = data['staticBlocks']['about3']['body'];
      querySelector('#static-block-about4-title').innerHtml = data['staticBlocks']['about4']['title'];
      querySelector('#static-block-about4-body').innerHtml = data['staticBlocks']['about4']['body'];


      var badgesHtml = '';
      querySelector('#aboutBadges').innerHtml = '';
      data['aboutBadges'].forEach((badge) {
        querySelector('#aboutBadges').append(
          new AnchorElement(href:badge['href'])
            ..target = 'blank'
            ..append(
              new ImageElement(src:badge['img02'])
              ..className = 'hoverFade'
            )..append(
              new ImageElement(src:badge['img01'])
              ..className = 'hoverUnfade'
            )
        );
      });
    });
  }
}

class SchemeView extends StartView {

  String title = 'Plakiat - Book';
  Timer timer;
  void pop() {
    querySelector('#bathtub').classes.remove('current');
    querySelector('#bathtub .sectionHeader').classes.remove('current');
    querySelector("#bathtub .sectionHeader .unwind").style.backgroundImage = 'url(media/baners/bathtub_b.png)';
    super.pop();
  }

  void blink(){
        querySelector("#bathtub .sectionHeader .unwind").style.backgroundImage = 'url(media/baners/bathtub_b.png)';
        timer = new Timer(const Duration(milliseconds: 200), (){
            querySelector("#bathtub .sectionHeader .unwind").style.backgroundImage = 'url(media/baners/bathtub_b2.png)';
            timer = new Timer(const Duration(milliseconds: 3000), (){
              blink();
            });
        });
  }

  void push(args){
    super.push(args);
    querySelector('#bathtub').classes.add('current');
    window.animationFrame.then((i){
      querySelector('#bathtub .sectionHeader').classes.add('current');
    });
    smoothScrollTo(querySelector('#bathtub'));
    (new ImageElement(src:'media/baners/bathtub_b2.png')).onLoad.listen((e) {
      // Draw once the image is loaded
    });

    new Timer(const Duration(milliseconds: 3000), (){
        querySelector("#bathtub .sectionHeader .unwind").style.backgroundImage = 'url(media/baners/bathtub_b2.png)';
    });
    new Timer(const Duration(milliseconds: 3300), (){
        querySelector("#bathtub .sectionHeader .unwind").style.backgroundImage = 'url(media/baners/bathtub_b.png)';
    });
    new Timer(const Duration(milliseconds: 3550), (){
        querySelector("#bathtub .sectionHeader .unwind").style.backgroundImage = 'url(media/baners/bathtub_b2.png)';
    });
    new Timer(const Duration(milliseconds: 3750), (){
        querySelector("#bathtub .sectionHeader .unwind").style.backgroundImage = 'url(media/baners/bathtub_b.png)';
    });
    new Timer(const Duration(milliseconds: 3900), (){
        querySelector("#bathtub .sectionHeader .unwind").style.backgroundImage = 'url(media/baners/bathtub_b2.png)';
    });
    new Timer(const Duration(milliseconds: 4000), (){
        querySelector("#bathtub .sectionHeader .unwind").style.backgroundImage = 'url(media/baners/bathtub_b.png)';
    });
    new Timer(const Duration(milliseconds: 4050), (){
        querySelector("#bathtub .sectionHeader .unwind").style.backgroundImage = 'url(media/baners/bathtub_b2.png)';
    });

    if (timer != null) timer.cancel();
    timer = new Timer(const Duration(milliseconds: 6000), (){
      blink();
    });
  }
}

class SingleNewsView extends NewsView {
  //TODO
  String popupBack = 'news';
}

class PosterView extends ArchiveView {

  String popupBack = 'archive';

  void push(args){
    if (previous != null) {
      //previous.push(args);
      popupBack = (previous is ArchiveView) ? 'archive' : 'news';
    } else {
      super.push(args);
    }
    loadPosters((Map posters){
      print(args);
      String posterId = args[0];
      Map poster = posters[posterId];

      querySelector('#popup').classes.add('show');
      querySelector('#popup').classes.remove('exhibition');
      querySelector('#popup .popupContent').classes.add('poster');
      querySelector('#popupImage').innerHtml = '';

      querySelector('#popup').classes.forEach((c){
        if (c.startsWith('poster-')) {
          querySelector('#popup').classes.remove(c);
        }
      });

      if (poster.containsKey('class')){
        String cs = poster['class'];

        cs.split(' ').forEach((c){
          //querySelector('#popupImage').classes.add(c);
          querySelector('#popup').classes.add('poster-' + c);
        });
      }
      querySelector('#popupTiles').classes.add('hidden');
      querySelector('#popupTiles').innerHtml = '';
      var languages = new DivElement();
      var currentLanguage;
      if (poster['images'] is Map) {

        languages.innerHtml = 'language: ';
        var current = true;
        for (var i in poster['images'].keys) {
          var a = new AnchorElement(href:poster['images'][i]);
          a.append(new ImageElement(src:poster['images'][i]));
          a.classes.add('lang');
          a.target = '_blank';
          querySelector('#popupImage').append(a);
          if ((poster['images'] as Map).length == 1) {
            a.classes.add('current');
            languages.appendText(i);
            currentLanguage = i;
          } else {
            var picker = new AnchorElement();
            picker.innerHtml = i;
            if (current) {
              a.classes.add('current');
              picker.classes.add('current');
              currentLanguage = i;
            } else {
              languages.append(new Text(' | '));
            }
            picker.onClick.listen((e){
              querySelectorAll('#popupImage a, #popup .options a').forEach((e){
                e.classes.remove('current');
              });
              a.classes.add('current');
              picker.classes.add('current');
              e.stopPropagation();
              e.preventDefault();
              currentLanguage = i;
            });
            languages.append(picker);
          }
          current = false;
        }
      } else {
        var a = new AnchorElement(href:poster['images'][0]);
        a.append(new ImageElement(src:poster['images'][0]));
        a.classes.add('current');
        a.target = '_blank';
        querySelector('#popupImage').append(a);
        currentLanguage = 'english';
        languages.innerHtml = 'language: english';
      }

      querySelector('#popupMobileTitle').setInnerHtml('<div class="vcenter"><div class="lp">- ' + poster['lp'] + ' -</div><h1>' + poster['title'] +'</h1></div>');
      querySelector('#popupLegend').setInnerHtml(
        '<div class="top"><span class="lp">- ' + poster['lp'] + ' -</span><h1>' + poster['title'] +'</h1><span class="stars">***</span></div>' +
        '<div class="center">' + poster['body'] + '</div>' +
        '<div class="bottom"><div class="options"></div></div>'
        , validator: _htmlValidator
      );


      var sizeSelectValue = new ImageElement(src:'media/size.png');
      var sizeSelectOptions = new DivElement()
        ..className = 'niceSizeSelectOptions faded';
      sizeSelectOptions.append(new DivElement()..className = 'lines');
      var possibleSizes = poster.containsKey('sizes') ? poster['sizes'] : ['A3', 'B2', 'B1'];
      var sizeSelectKey = possibleSizes.last;
      availableSizes.forEach((k, v){
        if (!possibleSizes.contains(k)) return;
        sizeSelectOptions..append(
          new ImageElement(src:'media/sizes/' + k + '.png')
            ..onClick.listen((e){
              //TODO
              if (posterId == '72' && k == 'B1') {
                window.alert('Sorry. This poster is not available in B1 size. Available in: A3 / B2.');
                return;
              }
              sizeSelectKey = k;
              sizeSelectValue.src = 'media/sizes/' + k + '.png';
            })
        );
      });

      var niceSizeSelect = new DivElement()
        ..className = 'niceSizeSelect'
        ..append(sizeSelectValue)
        ..appendHtml('size<br/><span class="desktopOnly">(cm)</span>')
        ..onClick.listen((e){
            sizeSelectOptions.classes.toggle('faded');
        })
        ..append(sizeSelectOptions);

      var buyButton = new ButtonElement();

      buyButton.append(new ImageElement(src:'media/sbuy.png'));
      buyButton.appendText('buy');
      buyButton.onClick.listen((e) {

        getOrder().addNewItem(poster, size: sizeSelectKey, language: currentLanguage);

        var badgeRect = querySelector('#cartBadge').getBoundingClientRect();
        if ((badgeRect.left == 0) && (badgeRect.top == 0)) {
            badgeRect = querySelector('#cartBadgeMobile').getBoundingClientRect();
        }
        var popupStyle = querySelector('#popup .popupContent').style;
        popupStyle.left = (badgeRect.left).round().toString() + 'px';
        popupStyle.top = (badgeRect.top).round().toString() + 'px';

        var timer = new Timer(const Duration(milliseconds: 1000), (){
          popupStyle.left = null;
          popupStyle.top = null;
        });

        closePopup();

      });

      loadSettings((data){
        if (!data['notForSalePosters'].contains(posterId)) {
          querySelector('#popupLegend .center').append(
            new DivElement()
              ..className = 'addToCart'
              ..append(niceSizeSelect)
              ..append(buyButton)
          );
        }
      });


      querySelector('#popupLegend .options').append(languages);
      if (poster.containsKey('behindthescenes')) {
        var bts = new AnchorElement(href:poster['behindthescenes']);
        bts.target = '_blank';
        bts.text = 'behind the scenes';
        querySelector('#popupLegend .options').append(bts);
      }

    });
  }
}

class ExhibitionView extends ExhibitionsView {
  String popupBack = 'exhibitions';

  void push(args){

    if (previous != null) {
      //popupBack =
      //previous.push(args);
    } else {
      super.push(args);
    }

    loadExhibitions((Map exhibitions){
      //print(args);
      Map exhibition = exhibitions[args[0]];

      if (exhibition['place'] == ''){
        return;
      }

      querySelector('#popup').classes.add('show');
      querySelector('#popup').classes.add('exhibition');

      querySelector('#popup').classes.toggle('long', exhibition['images'].length > 6);

      querySelector('#popup .popupContent').classes.remove('poster');
      /* TODO */
      querySelector('#popupImage').innerHtml = '';
      querySelector('#popupTiles').classes.remove('hidden');
      querySelector('#popupTiles').innerHtml = '';

      querySelector('#popupMobileTitle').setInnerHtml('<div class="vcenter"><h1>${exhibition['short_name']}</h1></div>');
      querySelector('#popupLegend').innerHtml = '<div id="polaroidSub">~ ${exhibition['short_name']} <span class="time">(${exhibition['short_time']})</span> ~</div>';
      //print(exhibition['images']);

      for (Map element in exhibition['images']){
        var elem = new AnchorElement();
        elem.href = element['image'];
        elem.target = '_blank';
        elem.classes.add('brick');
        elem.append(new ImageElement(src:element['thumbnail']));
        elem.appendHtml(
          '<div class="hover"><div class="top"><h5>' + element['title'] + '</h5></div><div class="line"></div>'+
          '<div class="bottom"><span>' + element['subtitle'] + '</span></div></div>');
        querySelector('#popupTiles').append(elem);
      }


    });
  }
}

Map routes = {
  'news': new NewsView(),
  'archive': new ArchiveView(),
  'behind': new BehindView(),
  'exhibitions': new ExhibitionsView(),
  'shop' : new ShopView(),
  'checkout' : new CheckoutView(),
  'success' : new SuccessView(),
  'contact': new ContactView(),
  'about' : new AboutView(),
  'book' : new SchemeView(),
  'poster/': new PosterView(),
  'exhibition/': new ExhibitionView(),
  '' : new HomeView(),
};

StartView previous;
StartView current;

void updateLayout(){
  String path = window.location.href.replaceFirst(document.baseUri, '');
  if (path.indexOf('?') > -1 ) {
    path = path.substring(0, path.indexOf('?'));
  }

  print('GOING TO ${path}');
  for (String key in routes.keys) {
    if (path.startsWith(key)) {
      var args = path.replaceRange(0, key.length, '').split('/');
      print(args);
      document.title = routes[key].title;

      if (current != null) {
        previous = current;
      }
      current = routes[key];
      current.push(args);
      break;
    }
  }
  print('fake scroll');
  onScroll();
}

void localiseAnchor(AnchorElement a){
  if ((a.href != null) && (a.target != '_blank') && (a.href.startsWith(document.baseUri))) {
    a.onClick.listen((Event e){
       window.history.pushState({}, 'Plakiat', a.href);
       updateLayout();
       e.preventDefault();
     });
   }
}

void localiseArea(AreaElement a){
  if ((a.href != null) && (a.target != '_blank') && (a.href.startsWith(document.baseUri))) {
    a.onClick.listen((Event e){
       window.history.pushState({}, 'Plakiat', a.href);
       updateLayout();
       e.preventDefault();
     });
  }
}



void closePopup() {
  querySelector( "#popup" ).classes.remove('show');
  window.history.pushState({}, 'Plakiat', document.baseUri + current.popupBack);
  /*
  if (previous != null){
    window.history.back();
  } else {
    window.history.pushState({}, 'Plakiat', current.popupBack);
    updateLayout();
  }*/
}

void changeWith(InputElement child, InputElement parent) {
  var currentValue = parent.value;
  if (child.value.isEmpty) { child.value = parent.value; }

  var handler = (Event e){
    if (child.value == currentValue) {
      child.value = parent.value;
      child.dispatchEvent(new Event('change'));
    }
    currentValue = parent.value;
  };
  parent..onChange.listen(handler)
    ..onKeyDown.listen(handler)
    ..onKeyUp.listen(handler)
    ..onCut.listen(handler)
    ..onPaste.listen(handler);
}

String _parseHtmlString(String htmlString) {
  var document = parse(htmlString);
  String parsedString = parse(document.body.text).documentElement.text;
  parsedString = parsedString.replaceAll('&', '&amp;');
  return parsedString;
}


void generateProductsXML(){

   loadPosters((Map posters){
     loadSettings((data){
     String xml = """<?xml version="1.0"?>
     <feed xmlns="http://www.w3.org/2005/Atom" xmlns:g="http://base.google.com/ns/1.0">
     	<title>Plakiat posters</title>
     	<link rel="self" href="https://plakiat.com"/>
     	<updated>20011-07-11T12:00:00Z</updated>
     	""";

      posters.forEach((posterId, poster){
          if (data['notForSalePosters'].contains(posterId)) return;

          print(posterId);
          poster['title'] = _parseHtmlString(poster['title']);
          poster['body'] = _parseHtmlString(poster['body']);

          String image = (poster['images'] is Map) ? (poster['images'] as Map).values.first : poster['images'][0];

          availableSizes.forEach((k,  size){

             xml += """<entry>
               <g:id>PLA-${posterId}-${size.name}</g:id>
               <g:title>${poster['title']} ${size.fullName} poster</g:title>
               <g:description>High quality, ${size.fullName} photographic print rolled in a cardboard tube. ${poster['lp']} plakiat poster. ${poster['body']}</g:description>
               <g:link>https://plakiat.com${poster['url']}</g:link>
               <g:image_link>${image}</g:image_link>
               <g:condition>new</g:condition>
               <g:availability>in stock</g:availability>
               <g:price>${size.basePrice}.00 PLN</g:price>
               """;
                 countryMap.forEach((k, country){
                   country.forEach((k2, v){
                     var mapa = {
                     "Australia": "AU",
                     "Austria": "AT",
                     "Belgium": "BE",
                     "Brazil": "BR",
                     "Canada": "CA",
                     "Croatia": "HR",
                     "Cyprus": "CY",
                     "Denmark": "DK",
                     "Estonia": "EE",
                     "Finland":"FI",
                     "France": "FR",
                     "Germany": "DE",
                     "Greece": "GR",
                     "Hong Kong": "HK",
                     "Hungary": "HU",
                     "Iceland": "IS",
                     "Ireland": "IE",
                     "Israel": "IL",
                     "Italy": "IT",
                     "Japan": "JP",
                     "Latvia": "LV",
                     "Lithuania": "LT",
                     "Luxembourg": "LU",
                     "Malaysia": "MY",
                     "Malta": "MT",
                     "Netherlands": "NL",
                     "New Zealand": "NZ",
                     "Norway": "NO",
                     "Poland": "PL",
                     "Portugal": "PT",
                     "Republic of China": "CN",
                     "Serbia": "RS",
                     "Singapore": "SG",
                     "Slovakia": "SK",
                     "Slovenia": "SI",
                     "South African Republic": "ZA",
                     "South Korea" : "KR",
                     "Spain" : "ES",
                     "Sweden" : "SE",
                     "Switzerland" : "CH",
                     "Thailand": "TH",
                     "Turkey": "TR",
                     "United Kingdom": "GB",
                     "USA": "US"
                   };


                     xml += """
                     <g:shipping>
                       <g:country>${mapa[k]}</g:country>
                       <g:service>$k2</g:service>
                       <g:price>${v[1]} PLN</g:price>
                     </g:shipping>
                     """;
                   });
                 });

              xml += """
               <g:brand>plakiat</g:brand>
               <g:mpn>PLA-${posterId}-${size.name}</g:mpn>
               <g:google_product_category>Home &amp; Garden > Decor > Artwork > Posters, Prints, &amp; Visual Artwork</g:google_product_category>
               <g:product_type>Poster</g:product_type>
             </entry>""";
          });
      });

     xml += """</feed>""";

     print(xml);
   });

   });
}


void main() {

  //generateProductsXML();

  getOrder().onUpdate();

  querySelector( "#drawOut" ).onMouseOver.listen((MouseEvent e){
    if(!getOrder().isEmpty()) {
      querySelector('#cartDrawer').classes.add('visible');
      querySelector('#cartDrawerBg').classes.add('visible');
    }
  });
  querySelector( "#drawOut" ).onMouseOut.listen((MouseEvent e){
    querySelector('#cartDrawer').classes.remove('visible');
    querySelector('#cartDrawerBg').classes.remove('visible');
  });

  querySelector( "#cartDrawer" ).onMouseOver.listen((MouseEvent e){
    print('X');
    querySelector('#cartDrawer').classes.add('visible');
    querySelector('#cartDrawerBg').classes.add('visible');
  });
  querySelector( "#cartDrawer" ).onMouseOut.listen((MouseEvent e){
    print('Y');
    querySelector('#cartDrawer').classes.remove('visible');
    querySelector('#cartDrawerBg').classes.remove('visible');
  });

  querySelector( "#popupClose" ).onClick.listen((e){
    closePopup();
  });

  updateLayout();

  querySelectorAll( "#mobileMenu a" ).forEach((e){
    e.onClick.listen((e) {
      querySelector( "#mobileMenu").classes.remove('unfolded');
    });
  });

  querySelector( "#mobileMenuUnfold" ).onClick.listen((e) {
    querySelector( "#mobileMenu").classes.add('unfolded');
  });

  querySelector( "#proceedButton" ).onClick.listen((e) {
    querySelector( "#orderCountry").parent.classes.remove('error');
    querySelectorAll( "#orderItems .errorMsg").forEach((e){ e.remove(); });

    if (getOrder().shippingAddress.country.isEmpty) {
      Element element = querySelector( "#orderCountry");
      element.parent.classes.add('error');
      element.parent.append(new DivElement()
        ..className = 'errorMsg'
        ..text = 'please select country'
      );
    } else {
      window.history.pushState({}, 'Plakiat', 'checkout');
      updateLayout();
    }
  });

  //window.location.href = 'xxxx';
  querySelectorAll('a').forEach((Element a) {
    localiseAnchor(a);
  });

  querySelectorAll('area').forEach((Element a) {
    localiseArea(a);
  });


  querySelector('#arrow').onClick.listen((e){
    e.stopPropagation();
    e.preventDefault();
    smoothScrollTo(querySelector('#arrowTarget'));
  });

  void highlightFam(part){
    querySelectorAll('#exhibitionsList li').forEach((e){
      e.classes.remove('current');
    });

    ImageElement fam = querySelector('#fam');
    ImageElement famMobile = querySelector('#famMobile');
    if (part == 0) {
      fam.src = 'media/exhibitions/0.png';
    } else {
      querySelectorAll('#exhibitionsList li').forEach((e){
        e.classes.remove('mobileShow');
      });
      querySelector('#exhibitionsList li.ex' + part).classes.add('current');
      querySelector('#exhibitionsList li.ex' + part).classes.add('mobileShow');
      fam.src = 'media/exhibitions/' + part + '.png';
      famMobile.src = 'media/exhibitions/' + part + '.png';
      loadExhibitions((exhibitions){
        querySelector('#fam').classes.toggle('disabled', exhibitions[part]['place'] == '');
        querySelector('#famMobile').classes.toggle('disabled', exhibitions[part]['place'] == '');
      });
    }
  }

  querySelectorAll('#mapMobile area').forEach((Element a) {
    a.onClick.listen((Event e){
      highlightFam(a.dataset['exhibition']);
    });
  });

  querySelectorAll('area').forEach((Element a) {

    a.onMouseOver.listen((MouseEvent e){
      highlightFam(a.dataset['part']);
    });
    a.onMouseOut.listen((MouseEvent e){
      highlightFam(0);
    });
  });


	window.onPopState.listen((PopStateEvent e) {
  	updateLayout();
    e.preventDefault();
	});

  window.onScroll.listen((e){
    onScroll();
  });


  AnchorElement mmm = querySelector('#mail');
  mmm.onClick.listen((e){
    mmm.href = mmm.href.replaceFirst('bad', '');
  });

  //here goes the function



  var eye = querySelector('#eye');
  var eyeRandomizer = new Random();
  Timer eyeTimer;
  var eyeUpdate;
  eyeUpdate = (var lookx, var looky) {
    var x = lookx;
    var y = looky;
    if (x != 0 && y!=0){
      var length = sqrt((x * x) + (y * y));
      x /= length;
      y /= length;
      eye.style.marginLeft = '${100 * (x * 3 + 0.5) / 346.0}%';
      eye.style.marginTop = '${100 * (y * 5 + 0.5) / 620.0}%';
    }
    eyeTimer.cancel();
    eyeTimer = new Timer(const Duration(milliseconds: 1500), (){ eyeUpdate(eyeRandomizer.nextInt(200) - 100, eyeRandomizer.nextInt(200) - 100); });
  };

  eyeTimer = new Timer(const Duration(milliseconds: 2000), (){ eyeUpdate(eyeRandomizer.nextInt(200) - 100, eyeRandomizer.nextInt(200) - 100); });

  querySelector('body').onMouseMove.listen((e){
    eyeUpdate(e.page.x - eye.documentOffset.x - 3, e.page.y - eye.documentOffset.y - 5);
  });

  //TODO
  querySelector( "#orderForm input[name=name]").onChange.listen((Event e){ getOrder().name = (e.target as InputElement).value; });
  querySelector( "#orderForm input[name=email]").onChange.listen((Event e){ getOrder().email = (e.target as InputElement).value; });
  querySelector( "#orderForm textarea[name=comments]").onChange.listen((Event e){ getOrder().comments = (e.target as TextAreaElement).value; });

  querySelector( "#orderForm input[name=shippingAddress\\.city]").onChange.listen((Event e){ getOrder().shippingAddress.city = (e.target as InputElement).value; });
  querySelector( "#orderForm input[name=shippingAddress\\.zipcode]").onChange.listen((Event e){ getOrder().shippingAddress.zipcode = (e.target as InputElement).value; });
  querySelector( "#orderForm input[name=shippingAddress\\.street]").onChange.listen((Event e){ getOrder().shippingAddress.street = (e.target as InputElement).value; });
  querySelector( "#orderForm input[name=shippingAddress\\.country]").onChange.listen((Event e){ getOrder().shippingAddress.country = (e.target as InputElement).value; });

  querySelector( "#orderForm input[name=invoiceData\\.company]").onChange.listen((Event e){ getOrder().invoiceData.company = (e.target as InputElement).value; });
  querySelector( "#orderForm input[name=invoiceData\\.name]").onChange.listen((Event e){ getOrder().invoiceData.name = (e.target as InputElement).value; });
  querySelector( "#orderForm input[name=invoiceData\\.nip]").onChange.listen((Event e){ getOrder().invoiceData.nip = (e.target as InputElement).value; });
  querySelector( "#orderForm input[name=invoiceData\\.address\\.city]").onChange.listen((Event e){ getOrder().invoiceData.address.city = (e.target as InputElement).value; });
  querySelector( "#orderForm input[name=invoiceData\\.address\\.zipcode]").onChange.listen((Event e){ getOrder().invoiceData.address.zipcode = (e.target as InputElement).value; });
  querySelector( "#orderForm input[name=invoiceData\\.address\\.street]").onChange.listen((Event e){ getOrder().invoiceData.address.street = (e.target as InputElement).value; });
  querySelector( "#orderForm input[name=invoiceData\\.address\\.country]").onChange.listen((Event e){ getOrder().invoiceData.address.country = (e.target as InputElement).value; });

  changeWith(querySelector( "#orderForm input[name=invoiceData\\.name]"), querySelector( "#orderForm input[name=name]"));
  changeWith(querySelector( "#orderForm input[name=invoiceData\\.address\\.city]"), querySelector( "#orderForm input[name=shippingAddress\\.city]"));
  changeWith(querySelector( "#orderForm input[name=invoiceData\\.address\\.zipcode]"), querySelector( "#orderForm input[name=shippingAddress\\.zipcode]"));
  changeWith(querySelector( "#orderForm input[name=invoiceData\\.address\\.street]"), querySelector( "#orderForm input[name=shippingAddress\\.street]"));
  changeWith(querySelector( "#orderForm input[name=invoiceData\\.address\\.country]"), querySelector( "#orderForm input[name=shippingAddress\\.country]"));

  querySelector( "#orderForm input[name=agree1]").onChange.listen((Event e){ getOrder().agree1 = (e.target as CheckboxInputElement).checked ? 'yes' : ''; });
  querySelector( "#orderForm input[name=agree2]").onChange.listen((Event e){ getOrder().agree2 = (e.target as CheckboxInputElement).checked ? 'yes' : ''; });
  querySelector( "#orderForm input[name=agree3]").onChange.listen((Event e){ getOrder().agree3 = (e.target as CheckboxInputElement).checked ? 'yes' : ''; });


  querySelector( "#orderForm" ).onSubmit.listen((e){
    e.preventDefault();

    //print(getOrder().toJson());
    querySelectorAll( "#orderForm .field").forEach((e){ e.classes.remove('error'); });
    querySelectorAll( "#orderForm .errorMsg").forEach((e){ e.remove(); });

    if (getOrder().validate()) {
      querySelectorAll("#orderForm").classes.add('running');
      FormElement form = e.target as FormElement;
      final req = new HttpRequest();

      req.onReadyStateChange.listen((Event e) {
        if (req.readyState == HttpRequest.DONE){
          querySelectorAll("#orderForm").classes.remove('running');
          if (req.status == 200) {
            var email = getOrder().email;
            clearOrder();
            window.history.pushState({}, 'Success | Plakiat', 'success');
            updateLayout();
            querySelector('#successEmail').innerHtml = email;
            querySelector('#successId').innerHtml = req.response.toString();
          } else {
            window.alert('Error placing order. Please try again');
          }
        }
      });
      new Timer(const Duration(milliseconds: 1000), (){
        loadSettings((data){
          req.open('POST', getApiUrl() + form.dataset['api']);
          req.send(json.encode({'order': getOrder().toJson()}));
        });
      });

    } else {
      getOrder().errors.forEach((k, v){
        String name = k.replaceAll('.', '\\.');
        Element element = querySelector( "#orderForm [name=$name]");
        element.parent.classes.add('error');
        DivElement errorNode = new DivElement();
        errorNode.classes.add('errorMsg');
        errorNode.text = v;
        element.parent.append(errorNode);
      });
      //errors
    }

  });

  querySelector( "#contactForm" ).onSubmit.listen((e){
      e.preventDefault();

      bool valid = true;

      querySelectorAll( "#contactForm .field").forEach((e){
        e.classes.remove('error');
      });
      querySelectorAll( "#contactForm .errorMsg").forEach((e){
        e.remove();
      });

      for (var required in ['name', 'email', 'body']) {
        Element element = querySelector( "#contactForm [name='$required']");
        String value = element is TextAreaElement ? (element as TextAreaElement).value : (element as InputElement).value;
        if (value == '') {
          element.parent.classes.add('error');
          DivElement errorNode = new DivElement();
          errorNode.classes.add('errorMsg');
          errorNode.text = 'required field';
          element.parent.append(errorNode);
          valid = false;
        }
      }

      if (valid) {
        (querySelector( "#contactForm button") as ButtonElement).disabled = true;

        FormElement form = e.target as FormElement;
        final req = new HttpRequest();

        req.onReadyStateChange.listen((Event e) {
          print('state ready');
          print(req.readyState);
          print(req.status);

          if (req.readyState == HttpRequest.DONE){
            if (req.status == 200) {
              querySelector('#contact').classes.add('success');
              (querySelector( "#contactForm [name='name']") as InputElement).value = '';
              (querySelector( "#contactForm [name='email']") as InputElement).value = '';
              (querySelector( "#contactForm [name='body']") as TextAreaElement).value = '';
              (querySelector( "#contactForm button") as ButtonElement).disabled = false;
            } else {
              querySelector('#contact').classes.add('error');
              (querySelector( "#contactForm button") as ButtonElement).disabled = false;
            }
            new Future.delayed(const Duration(milliseconds: 3000), (){
              querySelector('#contact').classes.remove('error');
              querySelector('#contact').classes.remove('success');
              querySelector('#contact').classes.remove('sent');
            });

          }
        });
        querySelector('#contact').classes.add('sent');

        req.open('POST', getApiUrl() + form.dataset['api']);

        //form.e
        //var post = ['name', 'email', 'body', 'robot'].map((key) => "${Uri.encodeComponent(key)}=" + Uri.encodeComponent(querySelector( "#contactForm [name='$key']").value)).join("&");

        String post = json.encode({
          'message': {
            'email': (querySelector( "#contactForm [name='email']") as InputElement).value,
            'name': (querySelector( "#contactForm [name='name']") as InputElement).value,
            'body': (querySelector( "#contactForm [name='body']") as TextAreaElement).value
          },
          'robot': (querySelector( "#contactForm [name='robot']") as InputElement).value
        });

        req.send(post);
      }

    });
}
