#!/usr/bin/env dart

import 'package:mongo_dart/mongo_dart.dart';
import 'package:swift_ids/swift_ids.dart';
import 'dart:io';
import 'dart:convert';

void main(List<String> args) async {

  var db = new Db("mongodb://localhost:27017/swift");
  await db.open();

  List images = await db.collection('images').find().toList();

  JsonEncoder encoder = new JsonEncoder.withIndent('  ');

  images.forEach((image){
    image['id'] = new Id(image['_id']).toString();
    image.remove('_id');
    image['app'] = new Id(image['_app']).toString();
    image.remove('_app');
    print(encoder.convert(image));
  });

  await db.close();
}
