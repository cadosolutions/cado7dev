// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.

library c7orm.test;

import 'package:test/test.dart';

class Model0Base {
  String field1 = 'default1';
  String field2 = 'default2';
  double field3 = 3.0;

  Model0Base original;
}

class Model0 extends Model0Base {
  String field4 = 'default4';
  double field5 = 5.0;
}

class Field1<T> {
  //@DbField;
  T _value;
  bool changed;

  Field1(this._value);

  T get value => this._value;

  void set value(T n) {
    this._value = n;
    this.changed = true;
  }
}

class Model1Base {
  Field1<String> field1 = new Field1<String>('default1');
  Field1<String> field2 = new Field1<String>('default2');
  Field1<double> field3 = new Field1<double>(3.0);
}
class Model1 extends Model1Base {
  Field1<String> field4 = new Field1<String>('default4');
  Field1<double> field5 = new Field1<double>(5.0);
}

void main() {

  group('Basic class validation', () {

      setUp(() {
      });

      test('First Test', () {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.start();
        stopwatch.reset();
        print(stopwatch.elapsedMicroseconds);
        for(int i=0; i< 10000; i++){
          Model1 model1 = new Model1();
        }
        print(stopwatch.elapsedMicroseconds);
        stopwatch.reset();
        print(stopwatch.elapsedMicroseconds);
        for(int i=0; i< 10000; i++){
        Model0 model0 = new Model0();
        }
        print(stopwatch.elapsedMicroseconds);

      });

    });
}
