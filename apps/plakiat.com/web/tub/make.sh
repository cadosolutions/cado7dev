#!/bin/bash

#echo "en"
#convert -density 150 ../../tub.pdf -quality 94 en/pages/page.png

#echo "pl"
#convert -density 150 ../../wanna.pdf -quality 94 pl/pages/page.png





for lang in "pl"; do

	echo $lang
	for i in $(seq 125);do
		echo $i
		p="$(($i-1))"
		echo $p
		pdftk ../../$lang.pdf cat $i output $lang/pages/page-$p.pdf
		convert -density 300 $lang/pages/page-$p.pdf -quality 94 -resize 750x1050 $lang/pages/page-$p.png

		if (( $p % 2 )); then
			composite -compose multiply -gravity NorthWest mask-right.png $lang/pages/page-$p.png $lang/pages/page-$p.png
		else
			composite -compose multiply -gravity NorthWest mask-left.png $lang/pages/page-$p.png $lang/pages/page-$p.png
		fi
		rm $lang/pages/page-$p.pdf
	done
done

#for i in $(seq 125);do
#	echo $i
#	if (( $i % 2 )); then
#		composite -compose multiply -gravity NorthWest mask-right.png pl/pages/page-$i.png pl/pages/page-$i.png
#	else
#		composite -compose multiply -gravity NorthWest mask-left.png pl/pages/page-$i.png pl/pages/page-$i.png
#	fi
#done
