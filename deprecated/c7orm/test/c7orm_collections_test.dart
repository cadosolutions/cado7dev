// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.

library c7orm.test;

import 'package:test/test.dart';
import 'package:c7orm/c7orm.dart';
import 'package:sqljocky/sqljocky.dart';

class Category {
    Uid uid;
    String name;
}

class Synonim {
    Uid uid;
    String name;
}

class FruitSynonim {
    @PrimaryKey
    One<Fruit> fruit;
    @PrimaryKey
    One<Synonim> synonim;
    DateTime added;
    int weight;
}

class Fruit {
    Uid uid;

    int _saveMe;

    @IndexKey(#category)
    One<Category> category;

    @UniqueKey(#name)
    String name;

    @Reverse(#fruit)
    Many<FruitSynonim> synonims;

    @Max(10.0)
    @IndexKey(#dimmensions)
    @UniqueKey(#weight)
    double weight;

    @Max(100.0)
    @IndexKey(#dimmensions)
    double size;

    //@SaveToDb
    //@Max(100.0, fields:['weight', 'size'], code:'density', msgtpl:'Density is @value but it needs to be below 100.0 please decrease weight or increase size')
    double get density => weight / size;

    void validate(Validator v) {
      if (density > 100.0) {
        v.addError(
          "Density equals 15 but it is supposed to be less than 100.",
          fields:['weight', 'size'],
          code:"density_error"
        );
      }
    }

    String description;

    int _completePrivate; //in db?
    int get completePrivate => _completePrivate;
    set completePrivate(int value) => _completePrivate = value;
}

class Banana extends Fruit {
  String get name => "Banana";
  String get description => "this is a yellow banana with $curvative curvative";
  double curvative;

}

void main() {

  group('Collections', () {

      Collection<Category> categories;
      Collection<Fruit> fruits;

      setUp(() async {
        var pool = new ConnectionPool(
          host: 'localhost', port: 3306,
          user: 'test', password: 'test',
          db: 'test', max: 5);

        categories = new Collection<Category>(pool);
        fruits = new Collection<Fruit>(pool);
        new Collection<Banana>(pool);
        new Collection<FruitSynonim>(pool);

        for (var symbol in Collection.getAllCollections.keys){
          var tableName = Collection.getAllCollections[symbol].tableName;
          try {
            var result = await Collection.getAllCollections[symbol].db.query('DROP TABLE `${tableName}`;');
          } catch (e) {
          }
        }

        var queries = await Migrator.quessMigrationQueries();
        for (var sql in queries){
          print("--running query:\n$sql\n");
          await pool.query(sql);
          print("DONE");
        }
        //pool.closeConnectionsNow();
      });

      test('First Test', () async {

        for (int i=1; i<=5; i++){
          var c = new Category();
          c.name = "Category $i";
          await categories.add(c);

        }
        var apple = new Fruit();
        apple.name = "Apple";
        //banana.category.set(c1);
        apple.weight = 0.5;
        apple.description = "apples are tasty";
        await fruits.add(apple);

        var banana = new Banana();
        //banana.category.set(c1);
        banana.weight = 1.5;
        await fruits.add(banana);

        fruits.getByUid(2);

        //Collection<Category>.getById();


      });

    });
}
