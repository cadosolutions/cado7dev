#!/usr/bin/env dart

import 'package:mongo_dart/mongo_dart.dart';
import 'package:swift_ids/swift_ids.dart';
import 'dart:io';

void main(List<String> args) async {

  String email = args[0];
  String url = args[1];
  String name = args[2];

  var db = new Db("mongodb://localhost:27017/swift");
  await db.open();

  var ret = await db.collection('counters').findAndModify(
    query: where.eq('_id', 'apps'),
    update: modify.inc('seq', 1),
    returnNew: true
  );
  Id id = new Id(ret['seq'].round());
  Id ownerId;

  var ret2 = await db.collection('users').findOne({
    'email': email
  });

  if (ret2 != null) {
    ownerId = new Id(ret2['_id'].round());
  } else {
    var ret3 = await db.collection('counters').findAndModify(
      query: where.eq('_id', 'users'),
      update: modify.inc('seq', 1),
      returnNew: true
    );
    ownerId = new Id(ret3['seq'].round());

    var ret2 = await db.collection('users').insert({
      '_id': ownerId.value,
      'email': email
    });
  }

  print("app ID = ${id.toString()} ${id.value}");
  print("owner ID = ${ownerId.toString()} ${ownerId.value}");


  List<String> path = url.split('/');
  String domain = path[0];

  var parent = await db.collection('apps').findOne({
    'url' : domain
  });

  String idString = id.toString();
  int parentId = null;

  if (parent != null) {
    print("assuming parent: ${parent.toString()}");
    idString = (new Id(parent['_id'])).toString() + ':' + idString;
    parentId = (new Id(parent['_id'])).value;
  }

  String config = """
  Site:
    name: "$name"
    url: $url
    id: $idString
  """;

  String configPath = 'apps/$domain/web/' + path.skip(1).join('/') + '/main.di.yaml';

  print("path: $configPath");
  print(config);

  await db.collection('apps').insert({
    '_id': id.value,
    '_parent': parentId,
    '_owner': ownerId.value,
    'name': name,
    'url': url
  });

  var dir = new Directory('apps/$domain/web');
  if (dir.existsSync()) {


  }

  /*dir.listSync().forEach((app){
    print(app.path);
    var web = new Directory(app.path + '/web');
    if (web.existsSync()) {
      web.listSync().forEach((app2){
        print(app2.path);
      });
    }
  });*/



  print('done');
  await db.close();
}
