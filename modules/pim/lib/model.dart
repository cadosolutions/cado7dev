
import 'package:swift_ids/swift_ids.dart';
import 'package:swift_composer/swift_composer.dart';
import 'package:module_core/model.dart';
//export 'package:module_core/model.dart';
//export 'package:swift_composer/swift_composer.dart';

enum UnitType {
  asd,
  asdasd
}

class ConfigurableField<T> {
  T defaultValue;
  bool validate() {

  }
}


@ComposeSubtypes
abstract class ProductBase extends Entity {

  @Field
  String name;
  @Field
  int price;

  @Field
  String shortDescription;

  //product:
  ConfigurableField<UnitType> unit;
  //FieldsOfType<ConfigurableField> configurables;

  //@Create
  //Factory<OrdeItem<super>> itemFactory;

    @Field
    Image image;
}

abstract class SimpleProduct extends ProductBase {


}
