library module_delivery_flat;

import 'package:module_core/model.dart';
import 'package:module_om/model.dart';
import 'package:swift_composer/swift_composer.dart';

import 'dart:html';

abstract class PickupDeliveryMethod extends DeliveryMethod {
    String get fullName => "Postal Delivery";
    int getDeliveryCost(Order order) {
        return 1000;
    }

}
