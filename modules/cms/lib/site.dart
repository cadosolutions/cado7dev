library mocule_cms;

import 'client.dart';
export 'client.dart';
import 'package:module_core/site.dart';

abstract class PageAction extends SiteShowAction<Page> {

  Future<String> get title => item.then((e) => e.title);

  @SubtypeFactory
  CmsWidget createItemWidget(String className, CmsWidgetConfig config);

  CmsWidget createWidgetFromConfig(CmsWidgetConfig config) {
    //TODO better way.
    String widgetClassName = config.classNames.last.replaceFirst('Config', 'Widget');
    return createItemWidget(widgetClassName, config);
  }

  void push() {
    super.push();
    itemSync.widgets.forEach((wc){
      (widget as ShowPageWidget).pageContent.append(
          createWidgetFromConfig(wc)
      );
    });
    /*if (itemSync.headerWidget != null) {
      (widget as ShowPageWidget).headerContent.setChild(
          createWidgetFromConfig(itemSync.headerWidget)
      );
    }*/
  }


}

abstract class ShowPageWidget extends ShowItemWidget<Page> {

  @Create
  Slot pageContent;

  /* @Create
  Slot headerContent; */

  String get html => '''
  <div>
     <h1>${item.title}</h1>
     <div data-slot="pageContent"></div>
  </div>
  ''';
}
