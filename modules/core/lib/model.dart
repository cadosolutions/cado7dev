import 'package:swift_ids/swift_ids.dart';
import 'package:swift_composer/swift_composer.dart';

@ComposeSubtypes
abstract class Entity {
    Id id;

    @Field
    String slug = null;

    @InjectClassNames
    List<String> get classNames;

    Map toJson() {
      Map<String, dynamic> ret = {
        'id': id != null ? id.toString() : null,
        'classes': classNames
      };

      this.fieldsToJson(ret);
      return ret;

    }

    @SubtypeFactory
    Entity relatedFactory(String className);

    Map<String, String> relations() {
      var ret = new Map<String, String>();
      relationsToList(ret);
      return ret;
    }

    @Compile
    void relationsToList(Map<String, String> target);

    @CompileFieldsOfType
    @AnnotatedWith(Field)
    void _relationsToListEntity(Map<String, String> target, String name, String className, Entity field) {
      target[name] = className;
    }

    @Compile
    void setRelations(Map<String, Entity> relations);

    @CompileFieldsOfType
    @AnnotatedWith(Field)
    void _setRelationsEntity(Map<String, Entity> relations, String name, Entity field) {
      field = relations[name];
    }


    @Compile
    void fieldsToJson(Map target);

    @CompileFieldsOfType
    @AnnotatedWith(Field)
    void _fieldsToJsonint(Map target, String name, int field) {
      target[name] = field;
    }

    @CompileFieldsOfType
    @AnnotatedWith(Field)
    void _fieldsToJsonString(Map target, String name, String field) {
      target[name] = field;
    }

    @CompileFieldsOfType
    @AnnotatedWith(Field)
    void _fieldsToJsonListEntity(Map target, String name, List<Entity> field) {
      target[name] = field.map((e) => e.toJson()).toList();
    }

    @CompileFieldsOfType
    @AnnotatedWith(Field)
    void _fieldsToJsonEntity(Map target, String name, Entity field) {
      target[name] = field != null ? field.toJson() : null;
    }

    void fromJson(Map data) {
      if (data['id'] != null) {
        id = new Id.fromString(data['id']);
      }
      //validate classNames
      for (var i=0; i<classNames.length; i++) {
        if (data.containsKey('classes')){
          if (data['classes'][i] != classNames[i]) {
            //throw exception
          }
        }
      }
      fieldsFromJson(data);
    }

    @Compile
    void fieldsFromJson(Map target);

    @CompileFieldsOfType
    @AnnotatedWith(Field)
    void _fieldsFromJsonString(Map target, String name, String field) {
      field = target.containsKey(name) ? target[name] : field;
    }

    @CompileFieldsOfType
    @AnnotatedWith(Field)
    void _fieldsFromJsonint(Map target, String name, int field) {
      field = target.containsKey(name) ? target[name] : field;
    }

    @CompileFieldsOfType
    @AnnotatedWith(Field)
    void _fieldsFromJsonListEntity(Map target, String name, List<Entity> field) {
      field = [];
      if (target[name] != null) {
        target[name].forEach((data){
          var e = relatedFactory(data['classes'].last);
          e.fromJson(data);
          field.add(e);
        });
      }
    }

    Id idFromString(String id) => new Id.fromString(id);

    @CompileFieldsOfType
    @AnnotatedWith(Field)
    void _fieldsFromJsonEntity(Map target, String name, String className, Entity field) {
      //print(target);
      if (target[name] != null) {
        if (target[name] is String) {
          field = relatedFactory(className);
          field.id = idFromString(target[name]);
        } else {
          field = relatedFactory(target[name]['classes'].last);
          field.fromJson(target[name]);
        }
      } else {
        field = null;
      }
    }
}

const Field = true;

abstract class App extends Entity {
  @Field
  App parent;

  @Field
  User owner;

  @Field
  String url;

  @Field
  String name;
}

abstract class User extends Entity {
  @Field
  String email;
}

abstract class Image extends Entity {

  @Field
  App app;

  @Field
  String mimetype;

  @Field
  String filename;

  @Field
  String thumb;

  String get ext => mimetype.split('/')[1];
  static const int nofakes = 1; //14;
  String get thumbUrl => '/p' + (id.value % nofakes + 1).toString().padLeft(2, '0') + '.png';// + ext;
  String get url => '/p' + (id.value % nofakes + 1).toString().padLeft(2, '0') + '.png';// + ext;

  //String get thumbUrl => 'https://files.cado7.com/' + id.toString() + '.300.' + ext;
  //String get url => 'https://files.cado7.com/' + id.toString() + '.' + ext;
}
