// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.

part of c7core;

class FlatPageFilter extends Filter<FlatPage> {

}

class FlatPage implements Content {
  Uid uid;
  @UniqueKey(#url)
  One<Site> site;
  One<Template> template;
  @UniqueKey(#url)
  String slug;
  String title;
  String keywords;
  String description;
  String body;

  /*
  static Future<FlatPage> getFromUrl(Site site, String slug) async {
    return flatPages.getByKey(#site_slug, [site, slug]);
  }
  */

}

class ActionFlatPage extends FrontendAction {

  @UrlParam FlatPage page;

  String get seoTitle => page.title;
  String get pageTitle => page.title;
  String get sectionContent => page.body;


}

/*
class AdminActionStaticPages extends AdminActionCrud {
  Collection collection = staticPages;
}
*/
