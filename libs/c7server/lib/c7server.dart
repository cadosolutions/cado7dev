library c7server;

// import 'package:dart_config/default_server.dart';

import 'dart:io';
import 'dart:async';
import 'dart:convert';
import 'package:mongo_dart/mongo_dart.dart' show Db;

import 'package:swift_composer/swift_composer.dart';
export 'package:swift_composer/swift_composer.dart';

import 'package:module_core/server.dart' as module_core;
import 'package:swift_ids/swift_ids.dart';

const PostArg = true;


@ComposeSubtypes
abstract class HttpAction {
  @Inject
  Server get server;

  @Compile
  void setPostArgs(Map json);

  @CompileFieldsOfType
  @AnnotatedWith(PostArg)
  void _setPostArgsString(Map json, String name, String field) {
    field = (json.containsKey(name) ? json[name] : null);
  }

  @CompileFieldsOfType
  @AnnotatedWith(PostArg)
  void _setPostArgsMap(Map json, String name, Map field) {
    field = (json.containsKey(name) ? json[name] : null);
  }

  @CompileFieldsOfType
  @AnnotatedWith(PostArg)
  void _setPostArgsList(Map json, String name, List field) {
    field = (json.containsKey(name) ? new List.from(json[name]) : null);
  }

  HttpRequest request;

  String appId;

  @Inject
  module_core.Repository<module_core.App> get appRepository;

  Future<module_core.App> get app {

    return appRepository.getById(new Id.fromString(appId));
    /*
    var ret = createAppEntity();
    Id id = new Id.fromString(appId);
    await (await server.db).collection('sites').findOne(where.eq('_id', id.value));

    //TODO !!!!
    switch (appId) {
      case 'JA2Q':
        ret.id = new Id(2);
        ret.mainDomain.value = 'miniaturepaintingstudio.com';
        ret.mainEmail.value = 'miniaturepaintingstudio@gmail.com';
        break;
      case '6JS':
        ret.id = new Id(3);
        ret.mainDomain.value = 'wawrzak.com';
        ret.mainEmail.value = 'regnardiaz@gmail.com';
        break;
    }
    */
  }

  String userId;
  Future<module_core.User> get user async {
    return null;
  }

  dynamic handleRequest();
}

class UrlPattern{
  final RegExp pattern;
  const UrlPattern(this.pattern);
}

class Redirect implements Exception {
  String uri;
  Redirect(this.uri);
}

class Error404 implements Exception {

  Error404();

}

@ComposeSubtypes
abstract class JsonAction extends HttpAction {

  Future prapareData() async {}
  Future run();

  dynamic handleRequest() async
  {

    String body = await utf8.decoder.bind(request).join('');
    //String method = request.uri.pathSegments.length > 1 ? request.uri.pathSegments[1] : 'index';
    if (body.length > 0) {
      var x = json.decode(body);
      if (x is Map){
        setPostArgs(x);
      }
    }

    String ret = json.encode(await this.run());

    request.response.statusCode = 200;
    request.response.headers.contentType = ContentType.json;
    request.response.write(ret);
  }
}

@Compose
abstract class Routing {

  @SubtypeFactory
  HttpAction createAction(String className);

  HttpAction getForRequest(HttpRequest request) {

    print(request.uri.pathSegments);
    if (request.uri.pathSegments.length < 2) {
      throw new Error404();
    }

    String appUid = request.uri.pathSegments[0];
    String className = request.uri.pathSegments[1];

    className = 'module_${className}Controller';
    print(className);
    HttpAction action = createAction(className);
    if (action == null) {
      throw new Error404();
    }
    action.appId = appUid;
    action.request = request;
    return action;
  }

}

@Compose
class ServerConfig {

  @Create
  Map data;

  load() async {
    String content = await new File('config.json').readAsString();
    data = json.decode(content);
  }

  T getRequired<T>(String code) {
    List<String> path = code.split('.');
    Map ret = data;
    for (int i=0; i < path.length - 1; i++) {
      ret = ret[path[i]];
    }
    return data[path.last];
  }
}

@Compose
abstract class Server {
  @Inject
  Routing get routing;
  @Inject
  ServerConfig get config;

  String workdir;
  int port;

  Db _db;
  Future<Db> get db async {
    if (_db == null) {
      //config
      _db = new Db("mongodb://localhost:27017/swift");
      await _db.open();
    }
    return _db;
  }

  Future serve() async {
    print("starting HTTP server...");
    await config.load();

    port = config.getRequired<int>('port');
    workdir = config.getRequired<String>('workdir');
    print('workdir: $workdir port: $port');

    HttpServer server = await HttpServer.bind(InternetAddress.anyIPv4, port);
    print("listening on http://*:$port");
    /*routing.actionTypes.forEach((s, f){
      print("action:$s");
    });*/
    server.listen(handleRequest);
  }

  Future handleRequest(HttpRequest request) async {
    int start = new DateTime.now().millisecondsSinceEpoch;
    try {

      await routing.getForRequest(request).handleRequest();

    } catch (error, stackTrace) {
      if (error is Error404) {
        request.response.statusCode = 404;
        request.response.headers.contentType = ContentType.html;
        request.response.write("<h1>404 ERROR</h1>");
      } else if (error is Redirect) {
        request.response.redirect(new Uri.http(request.uri.authority, error.uri));
      } else {
        request.response.statusCode = 500;
        request.response.headers.contentType = ContentType.html;
        request.response.write("<h1>500 ERROR</h1>");
        print(error.toString());
        print(stackTrace.toString());

        //request.response.write("<pre>${new HtmlEscape().convert()}</pre>");
        //request.response.write("<pre>${new HtmlEscape().convert(stackTrace.toString())}</pre>");
      }
    }
    request.response.close();
    print("${request.method} ${request.uri} ${request.response.statusCode} [${new DateTime.now().millisecondsSinceEpoch - start}ms]");
  }

}
