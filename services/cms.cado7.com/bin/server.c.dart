// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'server.dart';

// **************************************************************************
// SwiftGenerator
// **************************************************************************

//no interceptor for [DeferredLibrary]
//no interceptor for [DeferredLoadException]
//no interceptor for [FutureOr]
//no interceptor for [Future]
//no interceptor for [TimeoutException]
//no interceptor for [Completer]
//no interceptor for [Stream]
//no interceptor for [StreamSubscription]
//no interceptor for [EventSink]
//no interceptor for [StreamView]
//no interceptor for [StreamConsumer]
//no interceptor for [StreamSink]
//no interceptor for [StreamTransformer]
//no interceptor for [StreamTransformerBase]
//no interceptor for [StreamIterator]
//no interceptor for [StreamController]
//no interceptor for [SynchronousStreamController]
//no interceptor for [Timer]
//no interceptor for [AsyncError]
//no interceptor for [ZoneSpecification]
//no interceptor for [ZoneDelegate]
//no interceptor for [Zone]
//no interceptor for [HttpAction]
//no interceptor for [UrlPattern]
//no interceptor for [Redirect]
//no interceptor for [Error404]
//no interceptor for [JsonAction]
//interceptor for [Routing]
//interceptor for Routing
//can be singleton: TRUE
//parent: Routing [@bool get Compose]
class $Routing extends Routing implements Pluggable {
  $Routing() {}
  T plugin<T>() {
    return null;
  }

  HttpAction createAction(String className) {
    switch (className) {
      case 'module_core.AppController':
        return new $module_core_AppController();
      case 'module_cms.GetByIdsController':
        return new $module_cms_GetByIdsController();
      case 'module_cms.GetBySlugsController':
        return new $module_cms_GetBySlugsController();
      case 'module_cms.SearchController':
        return new $module_cms_SearchController();
      case 'module_cms.PagesSaveController':
        return new $module_cms_PagesSaveController();
    }
  }
}

//interceptor for [ServerConfig]
//interceptor for ServerConfig
//can be singleton: TRUE
//parent: ServerConfig [@bool get Compose]
class $ServerConfig extends ServerConfig implements Pluggable {
  $ServerConfig() {
//Map<dynamic, dynamic>
//create
  }
  T plugin<T>() {
    return null;
  }
}

//interceptor for [Server]
//interceptor for Server
//can be singleton: TRUE
//parent: Server [@bool get Compose]
class $Server extends Server implements Pluggable {
  $Server() {
//String
//int
  }
  T plugin<T>() {
    return null;
  }

  Routing get routing => $om.routing;
  ServerConfig get config => $om.serverConfig;
}

//no interceptor for [AnnotatedWith]
//no interceptor for [Pluggable]
//no interceptor for [TypePlugin]
//no interceptor for [module_core.SaveStatus]
//no interceptor for [module_core.Repository]
//interceptor for [module_core.AppRepository]
//interceptor for module_core.AppRepository
//T[212882645] => App[275838045]
//can be singleton: TRUE
//parent: AppRepository []
//parent: Repository [@bool get ComposeSubtypes]
class $module_core_AppRepository extends module_core.AppRepository
    implements Pluggable {
  $module_core_AppRepository() {}
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  Map<String, module_core.App> get emptyInstances =>
      $om.instancesOfmodule_core_App;
  Map<String, module_core.Repository<module_core.Entity>>
      get relatedRepositories =>
          $om.instancesOfmodule_core_Repository_module_core_Entity_;
  module_core.App instanceCreator(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
    }
  }
}

//interceptor for [module_core.UserRepository]
//interceptor for module_core.UserRepository
//T[212882645] => User[533911946]
//can be singleton: TRUE
//parent: UserRepository []
//parent: Repository [@bool get ComposeSubtypes]
class $module_core_UserRepository extends module_core.UserRepository
    implements Pluggable {
  $module_core_UserRepository() {}
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  Map<String, module_core.User> get emptyInstances =>
      $om.instancesOfmodule_core_User;
  Map<String, module_core.Repository<module_core.Entity>>
      get relatedRepositories =>
          $om.instancesOfmodule_core_Repository_module_core_Entity_;
  module_core.User instanceCreator(String className) {
    switch (className) {
      case 'module_core.User':
        return new $module_core_User();
    }
  }
}

//interceptor for [module_core.AppController]
//interceptor for module_core.AppController
//can be singleton: TRUE
//parent: AppController []
//parent: JsonAction [@bool get ComposeSubtypes]
//parent: HttpAction [@bool get ComposeSubtypes]
class $module_core_AppController extends module_core.AppController
    implements Pluggable {
  $module_core_AppController() {
//HttpRequest
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  module_core.Repository<module_core.App> get appRepository =>
      $om.module_core_AppRepository;
  void setPostArgs(Map json) {
//compiled method
//@PostArg
//@PostArg
//@PostArg
  }
}

//no interceptor for [module_core.Entity]
//interceptor for [module_core.App]
//interceptor for module_core.App
//can be singleton: TRUE
//parent: App []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_App extends module_core.App implements Pluggable {
  $module_core_App() {
//Id
//String
//App
//User
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_core.App'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['parent'] = "module_core.App";
    }
    {
      target['owner'] = "module_core.User";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.parent = relations['parent'];
    }
    {
      this.owner = relations['owner'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['url'] = this.url;
    }
    {
      target['name'] = this.name;
    }
//@Field
//@Field
    {
      target['parent'] = this.parent != null ? this.parent.toJson() : null;
    }
    {
      target['owner'] = this.owner != null ? this.owner.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.url = target.containsKey('url') ? target['url'] : this.url;
    }
    {
      this.name = target.containsKey('name') ? target['name'] : this.name;
    }
//@Field
//@Field
//@Field
    {
      if (target['parent'] != null) {
        if (target['parent'] is String) {
          this.parent = relatedFactory("module_core.App");
          this.parent.id = idFromString(target['parent']);
        } else {
          this.parent = relatedFactory(target['parent']['classes'].last);
          this.parent.fromJson(target['parent']);
        }
      } else {
        this.parent = null;
      }
    }
    {
      if (target['owner'] != null) {
        if (target['owner'] is String) {
          this.owner = relatedFactory("module_core.User");
          this.owner.id = idFromString(target['owner']);
        } else {
          this.owner = relatedFactory(target['owner']['classes'].last);
          this.owner.fromJson(target['owner']);
        }
      } else {
        this.owner = null;
      }
    }
  }
}

//interceptor for [module_core.User]
//interceptor for module_core.User
//can be singleton: TRUE
//parent: User []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_User extends module_core.User implements Pluggable {
  $module_core_User() {
//Id
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_core.User'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['email'] = this.email;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.email = target.containsKey('email') ? target['email'] : this.email;
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_core.Image]
//interceptor for module_core.Image
//can be singleton: TRUE
//parent: Image []
//parent: Entity [@bool get ComposeSubtypes]
class $module_core_Image extends module_core.Image implements Pluggable {
  $module_core_Image() {
//Id
//String
//App
//String
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_core.Image'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['app'] = "module_core.App";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.app = relations['app'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['mimetype'] = this.mimetype;
    }
    {
      target['filename'] = this.filename;
    }
    {
      target['thumb'] = this.thumb;
    }
//@Field
//@Field
    {
      target['app'] = this.app != null ? this.app.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.mimetype =
          target.containsKey('mimetype') ? target['mimetype'] : this.mimetype;
    }
    {
      this.filename =
          target.containsKey('filename') ? target['filename'] : this.filename;
    }
    {
      this.thumb = target.containsKey('thumb') ? target['thumb'] : this.thumb;
    }
//@Field
//@Field
//@Field
    {
      if (target['app'] != null) {
        if (target['app'] is String) {
          this.app = relatedFactory("module_core.App");
          this.app.id = idFromString(target['app']);
        } else {
          this.app = relatedFactory(target['app']['classes'].last);
          this.app.fromJson(target['app']);
        }
      } else {
        this.app = null;
      }
    }
  }
}

//interceptor for [module_cms.SnippetsRepository]
//interceptor for module_cms.SnippetsRepository
//T[212882645] => CmsWidgetConfig[225799969]
//can be singleton: TRUE
//parent: SnippetsRepository []
//parent: Repository [@bool get ComposeSubtypes]
class $module_cms_SnippetsRepository extends module_cms.SnippetsRepository
    implements Pluggable {
  $module_cms_SnippetsRepository() {}
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  Map<String, module_cms.CmsWidgetConfig> get emptyInstances =>
      $om.instancesOfmodule_cms_CmsWidgetConfig;
  Map<String, module_core.Repository<module_core.Entity>>
      get relatedRepositories =>
          $om.instancesOfmodule_core_Repository_module_core_Entity_;
  module_cms.CmsWidgetConfig instanceCreator(String className) {
    switch (className) {
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
    }
  }
}

//interceptor for [module_cms.CmsRepository]
//interceptor for module_cms.CmsRepository
//T[212882645] => Page[414991068]
//can be singleton: TRUE
//parent: CmsRepository []
//parent: Repository [@bool get ComposeSubtypes]
class $module_cms_CmsRepository extends module_cms.CmsRepository
    implements Pluggable {
  $module_cms_CmsRepository() {}
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  Map<String, module_cms.Page> get emptyInstances =>
      $om.instancesOfmodule_cms_Page;
  Map<String, module_core.Repository<module_core.Entity>>
      get relatedRepositories =>
          $om.instancesOfmodule_core_Repository_module_core_Entity_;
  module_cms.Page instanceCreator(String className) {
    switch (className) {
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }
}

//interceptor for [module_cms.GetByIdsController]
//interceptor for module_cms.GetByIdsController
//can be singleton: TRUE
//parent: GetByIdsController []
//parent: JsonAction [@bool get ComposeSubtypes]
//parent: HttpAction [@bool get ComposeSubtypes]
class $module_cms_GetByIdsController extends module_cms.GetByIdsController
    implements Pluggable {
  $module_cms_GetByIdsController() {
//HttpRequest
//String
//String
//List<String>
  }
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  module_core.Repository<module_core.App> get appRepository =>
      $om.module_core_AppRepository;
  void setPostArgs(Map json) {
//compiled method
//@PostArg
//@PostArg
//@PostArg
    {
      this.ids = (json.containsKey('ids') ? new List.from(json['ids']) : null);
    }
  }
}

//interceptor for [module_cms.GetBySlugsController]
//interceptor for module_cms.GetBySlugsController
//can be singleton: TRUE
//parent: GetBySlugsController []
//parent: JsonAction [@bool get ComposeSubtypes]
//parent: HttpAction [@bool get ComposeSubtypes]
class $module_cms_GetBySlugsController extends module_cms.GetBySlugsController
    implements Pluggable {
  $module_cms_GetBySlugsController() {
//HttpRequest
//String
//String
//List<String>
  }
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  module_core.Repository<module_core.App> get appRepository =>
      $om.module_core_AppRepository;
  module_cms.CmsRepository get cmsRepository => $om.module_cms_CmsRepository;
  void setPostArgs(Map json) {
//compiled method
//@PostArg
//@PostArg
//@PostArg
    {
      this.slugs =
          (json.containsKey('slugs') ? new List.from(json['slugs']) : null);
    }
  }
}

//interceptor for [module_cms.SearchController]
//interceptor for module_cms.SearchController
//can be singleton: TRUE
//parent: SearchController []
//parent: JsonAction [@bool get ComposeSubtypes]
//parent: HttpAction [@bool get ComposeSubtypes]
class $module_cms_SearchController extends module_cms.SearchController
    implements Pluggable {
  $module_cms_SearchController() {
//HttpRequest
//String
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  module_core.Repository<module_core.App> get appRepository =>
      $om.module_core_AppRepository;
  module_cms.CmsRepository get cmsRepository => $om.module_cms_CmsRepository;
  void setPostArgs(Map json) {
//compiled method
//@PostArg
    {
      this.query = (json.containsKey('query') ? json['query'] : null);
    }
//@PostArg
//@PostArg
  }
}

//interceptor for [module_cms.PagesSaveController]
//interceptor for module_cms.PagesSaveController
//can be singleton: TRUE
//parent: PagesSaveController []
//parent: JsonAction [@bool get ComposeSubtypes]
//parent: HttpAction [@bool get ComposeSubtypes]
class $module_cms_PagesSaveController extends module_cms.PagesSaveController
    implements Pluggable {
  $module_cms_PagesSaveController() {
//HttpRequest
//String
//String
//List<dynamic>
  }
  T plugin<T>() {
    return null;
  }

  Server get server => $om.server;
  module_core.Repository<module_core.App> get appRepository =>
      $om.module_core_AppRepository;
  void setPostArgs(Map json) {
//compiled method
//@PostArg
//@PostArg
//@PostArg
    {
      this.items =
          (json.containsKey('items') ? new List.from(json['items']) : null);
    }
  }
}

//interceptor for [module_cms.CmsWidgetConfig]
//interceptor for module_cms.CmsWidgetConfig
//can be singleton: TRUE
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_cms_CmsWidgetConfig extends module_cms.CmsWidgetConfig
    implements Pluggable {
  $module_cms_CmsWidgetConfig() {
//Id
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_cms.CmsWidgetConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_cms.HeadingConfig]
//interceptor for module_cms.HeadingConfig
//can be singleton: TRUE
//parent: HeadingConfig []
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_cms_HeadingConfig extends module_cms.HeadingConfig
    implements Pluggable {
  $module_cms_HeadingConfig() {
//Id
//String
//String
//String
//int
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_cms.CmsWidgetConfig', 'module_cms.HeadingConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
    {
      target['level'] = this.level;
    }
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['title'] = this.title;
    }
    {
      target['subtitle'] = this.subtitle;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.title = target.containsKey('title') ? target['title'] : this.title;
    }
    {
      this.subtitle =
          target.containsKey('subtitle') ? target['subtitle'] : this.subtitle;
    }
//@Field
    {
      this.level = target.containsKey('level') ? target['level'] : this.level;
    }
//@Field
//@Field
  }
}

//interceptor for [module_cms.ImageConfig]
//interceptor for module_cms.ImageConfig
//can be singleton: TRUE
//parent: ImageConfig []
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_cms_ImageConfig extends module_cms.ImageConfig
    implements Pluggable {
  $module_cms_ImageConfig() {
//Id
//String
//Image
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_cms.CmsWidgetConfig', 'module_cms.ImageConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['image'] = "module_core.Image";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.image = relations['image'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['caption'] = this.caption;
    }
//@Field
//@Field
    {
      target['image'] = this.image != null ? this.image.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.caption =
          target.containsKey('caption') ? target['caption'] : this.caption;
    }
//@Field
//@Field
//@Field
    {
      if (target['image'] != null) {
        if (target['image'] is String) {
          this.image = relatedFactory("module_core.Image");
          this.image.id = idFromString(target['image']);
        } else {
          this.image = relatedFactory(target['image']['classes'].last);
          this.image.fromJson(target['image']);
        }
      } else {
        this.image = null;
      }
    }
  }
}

//interceptor for [module_cms.HtmlConfig]
//interceptor for module_cms.HtmlConfig
//can be singleton: TRUE
//parent: HtmlConfig []
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_cms_HtmlConfig extends module_cms.HtmlConfig
    implements Pluggable {
  $module_cms_HtmlConfig() {
//Id
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_cms.CmsWidgetConfig', 'module_cms.HtmlConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['html'] = this.html;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.html = target.containsKey('html') ? target['html'] : this.html;
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_cms.ParagraphConfig]
//interceptor for module_cms.ParagraphConfig
//can be singleton: TRUE
//parent: ParagraphConfig []
//parent: CmsWidgetConfig [@bool get Compose]
//parent: Entity [@bool get ComposeSubtypes]
class $module_cms_ParagraphConfig extends module_cms.ParagraphConfig
    implements Pluggable {
  $module_cms_ParagraphConfig() {
//Id
//String
//String
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames =>
      ['module_cms.CmsWidgetConfig', 'module_cms.ParagraphConfig'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
  }
  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
  }
  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['body'] = this.body;
    }
//@Field
//@Field
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.body = target.containsKey('body') ? target['body'] : this.body;
    }
//@Field
//@Field
//@Field
  }
}

//interceptor for [module_cms.Page]
//interceptor for module_cms.Page
//can be singleton: TRUE
//parent: Page []
//parent: Entity [@bool get ComposeSubtypes]
class $module_cms_Page extends module_cms.Page implements Pluggable {
  $module_cms_Page() {
//Id
//String
//App
//String
//List<CmsWidgetConfig>
  }
  T plugin<T>() {
    return null;
  }

  List<String> get classNames => ['module_cms.Page'];
  module_core.Entity relatedFactory(String className) {
    switch (className) {
      case 'module_core.App':
        return new $module_core_App();
      case 'module_core.User':
        return new $module_core_User();
      case 'module_core.Image':
        return new $module_core_Image();
      case 'module_cms.CmsWidgetConfig':
        return new $module_cms_CmsWidgetConfig();
      case 'module_cms.HeadingConfig':
        return new $module_cms_HeadingConfig();
      case 'module_cms.ImageConfig':
        return new $module_cms_ImageConfig();
      case 'module_cms.HtmlConfig':
        return new $module_cms_HtmlConfig();
      case 'module_cms.ParagraphConfig':
        return new $module_cms_ParagraphConfig();
      case 'module_cms.Page':
        return new $module_cms_Page();
    }
  }

  void relationsToList(Map<String, String> target) {
//compiled method
//@Field
    {
      target['app'] = "module_core.App";
    }
  }

  void setRelations(Map<String, module_core.Entity> relations) {
//compiled method
//@Field
    {
      this.app = relations['app'];
    }
  }

  void fieldsToJson(Map target) {
//compiled method
//@Field
//@Field
    {
      target['slug'] = this.slug;
    }
    {
      target['title'] = this.title;
    }
//@Field
    {
      target['widgets'] = this.widgets.map((e) => e.toJson()).toList();
    }
//@Field
    {
      target['app'] = this.app != null ? this.app.toJson() : null;
    }
  }

  void fieldsFromJson(Map target) {
//compiled method
//@Field
    {
      this.slug = target.containsKey('slug') ? target['slug'] : this.slug;
    }
    {
      this.title = target.containsKey('title') ? target['title'] : this.title;
    }
//@Field
//@Field
    {
      this.widgets = [];
      if (target['widgets'] != null) {
        target['widgets'].forEach((data) {
          var e = relatedFactory(data['classes'].last);
          e.fromJson(data);
          this.widgets.add(e);
        });
      }
    }
//@Field
    {
      if (target['app'] != null) {
        if (target['app'] is String) {
          this.app = relatedFactory("module_core.App");
          this.app.id = idFromString(target['app']);
        } else {
          this.app = relatedFactory(target['app']['classes'].last);
          this.app.fromJson(target['app']);
        }
      } else {
        this.app = null;
      }
    }
  }
}

class $ObjectManager {
  $Routing _routing;
  $Routing get routing {
    if (_routing == null) {
      _routing = new $Routing();
    }
    return _routing;
  }

  $ServerConfig _serverConfig;
  $ServerConfig get serverConfig {
    if (_serverConfig == null) {
      _serverConfig = new $ServerConfig();
    }
    return _serverConfig;
  }

  $Server _server;
  $Server get server {
    if (_server == null) {
      _server = new $Server();
    }
    return _server;
  }

  $module_core_AppRepository _module_core_AppRepository;
  $module_core_AppRepository get module_core_AppRepository {
    if (_module_core_AppRepository == null) {
      _module_core_AppRepository = new $module_core_AppRepository();
    }
    return _module_core_AppRepository;
  }

  $module_core_UserRepository _module_core_UserRepository;
  $module_core_UserRepository get module_core_UserRepository {
    if (_module_core_UserRepository == null) {
      _module_core_UserRepository = new $module_core_UserRepository();
    }
    return _module_core_UserRepository;
  }

  $module_core_AppController _module_core_AppController;
  $module_core_AppController get module_core_AppController {
    if (_module_core_AppController == null) {
      _module_core_AppController = new $module_core_AppController();
    }
    return _module_core_AppController;
  }

  $module_core_App _module_core_App;
  $module_core_App get module_core_App {
    if (_module_core_App == null) {
      _module_core_App = new $module_core_App();
    }
    return _module_core_App;
  }

  $module_core_User _module_core_User;
  $module_core_User get module_core_User {
    if (_module_core_User == null) {
      _module_core_User = new $module_core_User();
    }
    return _module_core_User;
  }

  $module_core_Image _module_core_Image;
  $module_core_Image get module_core_Image {
    if (_module_core_Image == null) {
      _module_core_Image = new $module_core_Image();
    }
    return _module_core_Image;
  }

  $module_cms_SnippetsRepository _module_cms_SnippetsRepository;
  $module_cms_SnippetsRepository get module_cms_SnippetsRepository {
    if (_module_cms_SnippetsRepository == null) {
      _module_cms_SnippetsRepository = new $module_cms_SnippetsRepository();
    }
    return _module_cms_SnippetsRepository;
  }

  $module_cms_CmsRepository _module_cms_CmsRepository;
  $module_cms_CmsRepository get module_cms_CmsRepository {
    if (_module_cms_CmsRepository == null) {
      _module_cms_CmsRepository = new $module_cms_CmsRepository();
    }
    return _module_cms_CmsRepository;
  }

  $module_cms_GetByIdsController _module_cms_GetByIdsController;
  $module_cms_GetByIdsController get module_cms_GetByIdsController {
    if (_module_cms_GetByIdsController == null) {
      _module_cms_GetByIdsController = new $module_cms_GetByIdsController();
    }
    return _module_cms_GetByIdsController;
  }

  $module_cms_GetBySlugsController _module_cms_GetBySlugsController;
  $module_cms_GetBySlugsController get module_cms_GetBySlugsController {
    if (_module_cms_GetBySlugsController == null) {
      _module_cms_GetBySlugsController = new $module_cms_GetBySlugsController();
    }
    return _module_cms_GetBySlugsController;
  }

  $module_cms_SearchController _module_cms_SearchController;
  $module_cms_SearchController get module_cms_SearchController {
    if (_module_cms_SearchController == null) {
      _module_cms_SearchController = new $module_cms_SearchController();
    }
    return _module_cms_SearchController;
  }

  $module_cms_PagesSaveController _module_cms_PagesSaveController;
  $module_cms_PagesSaveController get module_cms_PagesSaveController {
    if (_module_cms_PagesSaveController == null) {
      _module_cms_PagesSaveController = new $module_cms_PagesSaveController();
    }
    return _module_cms_PagesSaveController;
  }

  $module_cms_CmsWidgetConfig _module_cms_CmsWidgetConfig;
  $module_cms_CmsWidgetConfig get module_cms_CmsWidgetConfig {
    if (_module_cms_CmsWidgetConfig == null) {
      _module_cms_CmsWidgetConfig = new $module_cms_CmsWidgetConfig();
    }
    return _module_cms_CmsWidgetConfig;
  }

  $module_cms_HeadingConfig _module_cms_HeadingConfig;
  $module_cms_HeadingConfig get module_cms_HeadingConfig {
    if (_module_cms_HeadingConfig == null) {
      _module_cms_HeadingConfig = new $module_cms_HeadingConfig();
    }
    return _module_cms_HeadingConfig;
  }

  $module_cms_ImageConfig _module_cms_ImageConfig;
  $module_cms_ImageConfig get module_cms_ImageConfig {
    if (_module_cms_ImageConfig == null) {
      _module_cms_ImageConfig = new $module_cms_ImageConfig();
    }
    return _module_cms_ImageConfig;
  }

  $module_cms_HtmlConfig _module_cms_HtmlConfig;
  $module_cms_HtmlConfig get module_cms_HtmlConfig {
    if (_module_cms_HtmlConfig == null) {
      _module_cms_HtmlConfig = new $module_cms_HtmlConfig();
    }
    return _module_cms_HtmlConfig;
  }

  $module_cms_ParagraphConfig _module_cms_ParagraphConfig;
  $module_cms_ParagraphConfig get module_cms_ParagraphConfig {
    if (_module_cms_ParagraphConfig == null) {
      _module_cms_ParagraphConfig = new $module_cms_ParagraphConfig();
    }
    return _module_cms_ParagraphConfig;
  }

  $module_cms_Page _module_cms_Page;
  $module_cms_Page get module_cms_Page {
    if (_module_cms_Page == null) {
      _module_cms_Page = new $module_cms_Page();
    }
    return _module_cms_Page;
  }

  Map<String, module_core.App> get instancesOfmodule_core_App {
    return {
      "module_core.App": new $module_core_App(),
    };
  }

  Map<String, module_core.Repository>
      get instancesOfmodule_core_Repository_module_core_Entity_ {
    return {
      "module_core.AppRepository": new $module_core_AppRepository(),
      "module_core.UserRepository": new $module_core_UserRepository(),
      "module_cms.SnippetsRepository": new $module_cms_SnippetsRepository(),
      "module_cms.CmsRepository": new $module_cms_CmsRepository(),
    };
  }

  Map<String, module_core.User> get instancesOfmodule_core_User {
    return {
      "module_core.User": new $module_core_User(),
    };
  }

  Map<String, module_cms.CmsWidgetConfig>
      get instancesOfmodule_cms_CmsWidgetConfig {
    return {
      "module_cms.CmsWidgetConfig": new $module_cms_CmsWidgetConfig(),
      "module_cms.HeadingConfig": new $module_cms_HeadingConfig(),
      "module_cms.ImageConfig": new $module_cms_ImageConfig(),
      "module_cms.HtmlConfig": new $module_cms_HtmlConfig(),
      "module_cms.ParagraphConfig": new $module_cms_ParagraphConfig(),
    };
  }

  Map<String, module_cms.Page> get instancesOfmodule_cms_Page {
    return {
      "module_cms.Page": new $module_cms_Page(),
    };
  }
}

$ObjectManager $om = new $ObjectManager();
//generated in 247ms
