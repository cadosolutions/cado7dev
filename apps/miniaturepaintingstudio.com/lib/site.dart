
import 'package:miniaturepaintingstudio.com/quoteform.dart';
import 'package:module_core/site.dart';
import 'package:module_cms/site.dart' as module_cms;
import 'package:jsonp/jsonp.dart' as jsonp;
import 'quoteform.dart';
import 'model.dart';
export 'model.dart';

import 'dart:html';
import 'dart:convert';
import 'dart:async';

abstract class PaintingNavWidget extends module_cms.CmsWidget<PaintingNavConfig> {

}

abstract class HomeAction extends SiteAction {
  @Create
  HomeWidget home;

  String get title => app.site.name;

  void push() {
    app.layout.body.setChild(home);
  }
}

abstract class HomeWidget extends Widget {

  @Create
  Slot blogPosts;

  @Factory
  BlogItemWidget createItemWidget(var post);

  void handleBlogResponse(var data) {
    for (var post in data['items']) {
      blogPosts.append(createItemWidget(post));
    }
  }

  static Future<dynamic> result;

  void init(){
    if (result == null) {
      result = jsonp.fetch(
        uri: "https://www.googleapis.com/blogger/v3/blogs/2469892625798690459/posts?key=AIzaSyD12CqE7K1FIHKBXqsk4iyE5hrqO2n7L3w&fetchImages=true&fetchBodies=false&maxResults=6&callback=?"
      );
    }
    result.then(handleBlogResponse);
  }
}

abstract class BlogItemWidget extends Widget {
  @Require
  var post;

  String truncate(String string) {
     return (string.length > 53) ? string.substring(0,50) + '...' : string;
  }

  String get html => """
    <div class="col-md-6 col-lg-4">
      <a class="blog-button" target="_blank" href="${post['url']}">
        <div class="blog-thumb">
          <div class="hover-zoom circle">
            <img src="${post['images'][0]['url']}" alt=""/>
          </div>
        </div>
        <div class="blog-post">
          <div class="blog-published">${post['published']}</div>
          <div class="blog-title">${truncate(post['title'])}</div>
          <div class="blog-tags">${truncate(post['labels'].join(' '))}</div>
        </div>
      </a>
    </div>
  """;
}

abstract class ContactAction extends SiteAction {
  @Create
  ContactWidget contact;

  String get title => 'Contact Us | ' + app.site.name;

  void push() {
    app.layout.body.setChild(contact);
  }
}


abstract class ContactWidget extends Widget {

  @Inject
  Application get app;

  bool validateForm() {
    element.querySelectorAll( "#contactForm .field").forEach((e){
      e.classes.remove('error');
    });
    element.querySelectorAll( "#contactForm .errorMsg").forEach((e){
      e.remove();
    });
    bool valid = true;
    for (var required in ['name', 'email', 'body']) {
      var filedElement = element.querySelector( "#contactForm [name='$required']");
      String value = filedElement is TextAreaElement ? filedElement.value : (filedElement as InputElement).value;
      if (value == '') {
        filedElement.parent.classes.add('error');
        DivElement errorNode = new DivElement();
        errorNode.classes.add('errorMsg');
        errorNode.text = 'required field';
        filedElement.parent.append(errorNode);
        valid = false;
      }
    }
    return valid;
  }

  void init() {
    element.querySelector( "#contactForm" ).onSubmit.listen((e){
        e.preventDefault();

        if (validateForm()) {
          (querySelector( "#contactForm button") as ButtonElement).disabled = true;

          FormElement form = e.target as FormElement;

          final req = new HttpRequest();
          req.onReadyStateChange.listen((Event e) {
            print('state ready');
            print(req.readyState);
            print(req.status);

            if (req.readyState == HttpRequest.DONE){
              if (req.status == 200) {
                querySelector('#contact').classes.add('success');
                (querySelector( "#contactForm [name='name']") as InputElement).value = '';
                (querySelector( "#contactForm [name='email']") as InputElement).value = '';
                (querySelector( "#contactForm [name='body']") as TextAreaElement).value = '';
                (querySelector( "#contactForm button") as ButtonElement).disabled = false;
              } else {
                querySelector('#contact').classes.add('error');
                (querySelector( "#contactForm button") as ButtonElement).disabled = false;
              }
              new Future.delayed(const Duration(milliseconds: 3000), (){
                querySelector('#contact').classes.remove('error');
                querySelector('#contact').classes.remove('success');
                querySelector('#contact').classes.remove('sent');
              });

            }
          });
          querySelector('#contact').classes.add('sent');

          req.open('POST', app.site.getApiUrl() + 'contact.Contact');
          //var post = ['name', 'email', 'body', 'robot'].map((key) => "${Uri.encodeComponent(key)}=" + Uri.encodeComponent(querySelector( "#contactForm [name='$key']").value)).join("&");
          String post = json.encode({
            'message': {
              'email': (querySelector( "#contactForm [name='email']") as InputElement).value,
              'name': (querySelector( "#contactForm [name='name']") as InputElement).value,
              'body': (querySelector( "#contactForm [name='body']") as TextAreaElement).value
            },
            'robot': (querySelector( "#contactForm [name='robot']") as InputElement).value
          });

          req.send(post);
        }

      });
  }
}

abstract class PaintingAction extends SiteAction {
  @Create
  PaintingWidget painting;

  String get title => 'Painting | ' + app.site.name;

  void push() {
    app.layout.body.setChild(painting);
    initQuoteForm((String message){
      //print(message);
      router.goToUrl(router.url<ContactAction>([]));
      new Timer(const Duration(milliseconds: 500),(){
        (querySelector( "#contactForm [name='body']") as TextAreaElement).value = message;
      });
    });
  }
}

abstract class PaintingWidget extends Widget {}
