class User
{
  String firstname = "";
  String lastname = "";

  User (this.firstname, this.lastname);

  void method(String param) => null;
  get fullname => "$firstname $lastname";
}

void doSomethingWithUser(User x) => print(x.fullname);

void main(args) {
  var user = new User("Jan", "Kowalski");
  user.firstname = "Edward";
  user.lastname = "Nowak";
  user.method("asd");
  doSomethingWithUser(user);

  doSomethingWithUser(
    new User("Jan", "Kowalski")
    ..firstname = "Edward"
    ..lastname = "Nowak"
    ..method("asd")
  );
}
