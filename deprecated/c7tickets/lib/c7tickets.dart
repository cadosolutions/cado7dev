// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.

library c7tickets;

//this shall export stuff
import 'package:c7core/c7core.dart';
import 'package:c7orm/c7orm.dart';
import 'package:c7liquid/c7liquid.dart';

part 'models.dart';
part 'actions.dart';


class TicketsModule extends Module {

  Collection<Workflow> workflows;
  Collection<Project> projects;
  Collection<Ticket> tickets;
  Collection<Comment> comments;

  TicketsModule(){
    workflows = new Collection<Workflow>(db);
    projects = new Collection<Project>(db);
    tickets = new Collection<Ticket>(db);
    comments = new Collection<Comment>(db);
  }

}
